#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <sys/time.h>
#include <pips_runtime.h>
#include <assert.h>
double a[2][20 + 2];
extern void init_array();
extern void print_array();
extern double rtclock();
extern int main();
void init_array()
{
  int i;
  for (i = 0; i <= 21; i += 1)
    a[0][i] = ((double) i) / 20;

}

void print_array()
{
  int i;
  for (i = 0; i <= 19; i += 1)
  {
    fprintf(stderr, "%lf ", a[10 % 2][i]);
    if ((i % 80) == 20)
      fprintf(stderr, "\n");

  }

  fprintf(stderr, "\n");
}

double rtclock()
{
  struct timeval Tp;
  int stat;
  stat = gettimeofday(&Tp, 0);
  if (stat != 0)
    printf("Error return from gettimeofday: %d", stat);

  return Tp.tv_sec + (Tp.tv_usec * 1.0e-6);
}

int main()
{
  int t;
  int i;
  double t_start;
  double t_end;
  int t_t;
  int i_t;
  init_array();
  t_start = rtclock();
  int block = 0;
  #pragma @ICE loop=jacobi1d
  for (t_t = 0; t_t <= 7; t_t += 1)
    for (i_t = pips_max_2(t_t - 10, (-t_t) - 1); i_t <= pips_min_2(t_t, (-t_t) + 4); i_t += 1){
	    
    for (t = 0; t <= 9; t += 1) {
    for (i = pips_max_3(1, (t - (4 * i_t)) - 2, ((4 * t_t) - t) + 1); i <= pips_min_3(20, (t - (4 * i_t)) + 1, ((4 * t_t) - t) + 4); i += 1) {
	printf("[%d] t_t=%d i_t=%d t=%d i=%d\n",block,t_t, i_t, t, i);
    a[(t + 1) % 2][i] = 0.33333 * ((a[t % 2][i] + a[t % 2][i + 1]) + a[t % 2][i - 1]);
    }
    }
  block++;
    }
  



  t_end = rtclock();
  fprintf(stdout, "Jacobi2d size = %d steps = %d | Time = %7.5lf ms\n", 20, 10, (t_end - t_start) * 1.0e3);
  if (fopen(".test", "r"))
    print_array();

  return 0;
}

