#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <sys/time.h>
#include <pips_runtime.h>

#include <assert.h>

#define N 2000
#define T 200

//#pragma declarations
double a[N][N];
//#pragma enddeclarations

//#ifdef TIME
#define IF_TIME(foo) foo;
//aelse
//#define IF_TIME(foo)
//#endif

void init_array()
{
    int i, j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            a[i][j] = i*i+j*j;
        }
    }
}


void print_array()
{
    int i, j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            fprintf(stderr, "%0.2lf ", a[i][j]);
            if (j%80 == 20) fprintf(stderr, "\n");
        }
    }
    fprintf(stderr, "\n");
}

double rtclock()
{
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

#define __PLACE_TO_INSERT_FORWARD_DECLARATIONS

int main()
{
    int i, j, t;

    double t_start, t_end;

    init_array() ;

#ifdef PERFCTR
    PERF_INIT; 
#endif

    IF_TIME(t_start = rtclock())

#pragma @ICE loop=seidel2d
    for (t=0; t<=T-1; t++)  {
        for (i=1; i<=N-2; i++)  {
            for (j=1; j<=N-2; j++)  {
                a[i][j] = (a[i-1][j-1] + a[i-1][j] + a[i-1][j+1] 
                        + a[i][j-1] + a[i][j] + a[i][j+1]
                        + a[i+1][j-1] + a[i+1][j] + a[i+1][j+1])/9.0;
            }
        }
    }

    IF_TIME(t_end = rtclock())
//    IF_TIME(fprintf(stdout, "%0.6lfs\n", t_end - t_start));
    IF_TIME(printf("Seidel2d size = %d steps = %d | Time = %7.5lf ms\n", N,T,(t_end - t_start)*1.0e3))

#ifdef PERFCTR
    PERF_EXIT; 
#endif

  if (fopen(".test", "r")) {
#ifdef MPI
    if (my_rank == 0) {
        print_array();
    }
#else
    print_array();
#endif
  }

    return 0;
}
