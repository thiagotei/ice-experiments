#include <stdio.h>
#include <stdlib.h>


void jacobi3d(double *A, double *B, const int niter, const size_t x, const size_t y, const size_t z,
        const double C0, const double C1);


void printPlane(double *P, size_t dim1, size_t dim2, size_t dim3) {
    int i;
    for(i = 0; i < dim1*dim2*dim3; i++) {
        printf("%f ", P[i]); 
        if((i+1) % dim3 == 0 ) printf("\n");
        if((i+1) % (dim2*dim3) == 0 ) printf("\n");
    }
}

void dummy(double *p);

double mysecond(void);

int main(int argc, char ** argv) {
    size_t dim1 = 4;
    int niter = 1;
    double C0 = 2.0;
    double C1 = 3.0;
  
    if(argc < 3) {
        fprintf(stderr,"Need 2 parameters - dimension and num of iterations.\n");
        exit(1);
    }

    dim1 = atoi(argv[1]);
    niter = atoi(argv[2]); 

    if(dim1 <= 0) {
        fprintf(stderr, "dim is now %ld, it must be bigger than 0.\n", dim1);
        exit(1);
    }
    if(niter <= 0) {
        fprintf(stderr, "niter is now %d, it must be bigger than 0.\n", niter);
        exit(1);
    }

    size_t dim2 = dim1;
    size_t dim3 = dim1;
 
    if(argc == 5) {
        C0 = atof(argv[3]);  
        C1 = atof(argv[4]);  
    }

    printf("Stencil3d Dim(%ld, %ld, %ld) niter %d C0 %lf C1 %lf \n", dim1, dim2, dim3, niter, C0, C1);

    double * A = (double *) malloc(dim1*dim2*dim3*sizeof(double));
    if(!A) {fprintf(stderr, "Error! Could not allocate A.\n");}

    double * B = (double *) malloc(dim1*dim2*dim3*sizeof(double));
    if(!B) {fprintf(stderr, "Error! Could not allocate B.\n");}
    
    int i;
    for (i = 0; i < dim1*dim2*dim3; i++) {
        A[i] = 0.0;
    }
    for (i = 0; i < dim2*dim3; i++) {
        A[i] = 1.0;
    }

#ifdef DEBUG_IN
    printPlane(A, dim1, dim2, dim3);
    printf("\n#####\n");
#endif

    double tStart, tEnd, t;
    int repet = 10;

    tStart = mysecond();
    for(i = 0; i < repet; i++) {
        jacobi3d(A, B, niter, dim1, dim2, dim3, C0, C1);
    }
    tEnd = mysecond();
    t = (tEnd - tStart)/repet;


#ifdef PRINT_OUT
    // print the result
    if(niter % 2 == 0) {
        printPlane(A, dim1, dim2, dim3);
    } else {
        printPlane(B, dim1, dim2, dim3);
    }
#else
    if(niter % 2 == 0) {
        dummy(A);
    } else {
        dummy(B);
    }
#endif

    printf("Time per jacobi = %.2e secs\n", t);

    free(A);
    free(B);

    return 0;
}
