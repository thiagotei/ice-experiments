#include <io.h>

void readHeader(FILE * f, MKL_INT *M, MKL_INT *N, MKL_INT *nz, MM_typecode *matcode)
{
    int ret_code;
    //MM_typecode matcode;
    if (mm_read_banner(f, matcode) != 0)
    {
        printf("Could not process Matrix Market banner.\n");
        exit(1);
    }


    /*  This is how one can screen matrix types if their application */
    /*  only supports a subset of the Matrix Market data types.      */

    if (mm_is_complex(*matcode) && mm_is_matrix(*matcode) && 
            mm_is_sparse(*matcode) )
    {
        printf("Sorry, this application does not support ");
        printf("Market Market type: [%s]\n", mm_typecode_to_str(*matcode));
        exit(1);
    }

/*    if(mm_is_pattern(matcode))
    {
        printf("Sorry, pattern matrices not accepted!\n");
        printf("Market Market type: [%s]\n", mm_typecode_to_str(matcode));
        exit(1);
    }
*/
    /* find out size of sparse matrix .... */
    int iM, iN, inz;
    if ((ret_code = mm_read_mtx_crd_size(f, &iM, &iN, &inz)) !=0)
        exit(1);

    *M = (MKL_INT) iM;
    *N = (MKL_INT) iN;
    *nz = (MKL_INT) inz;
}

