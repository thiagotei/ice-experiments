#ifndef _IO_H
#define _IO_H
#include <stdio.h>
#include <stdlib.h>
#include <mmio.h>
#include <mkl_types.h>


#ifdef __cplusplus
extern "C" {
#endif
void readHeader(FILE * f, MKL_INT *M, MKL_INT *N, MKL_INT *nz, MM_typecode *matcode);
#ifdef __cplusplus
}
#endif

#endif

