#!/usr/bin/env python

from scipy.sparse import csc_matrix
from scipy.io import loadmat
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt, sys
import numpy as np

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input matlab format file!")
args = parser.parse_args()

#mat = loadmat('1138_bus.mat')
mat = loadmat(args.i)
prob = mat['Problem']
#print(prob.shape)
#print(prob.size)
#print(prob.dtype)
#print(prob['A'])
A = prob['A'][0,0]
#print(f"size: {A.size} shape: {A.shape} type: {A.dtype}")
print("{}".format(A.shape))
#print(A[10,0])
#b = np.random.rand(1138)
b = [x for x in range(0,A.shape[1])]
#print(len(b))
#print(b,A[0,:])

#A = csc_matrix([[1, 2, 0], [0, 0, 3], [4, 0, 5]])
#print(A.dtype)


r = A.dot(b)

#bp = " ".join(["{:7.5f}".format(x) for x in r])
#print("res:\n{}".format(bp), file=sys.stderr)


plt.spy(A)
plt.show()

#for x in mat['Problem'][0]:
    #for y in np.nditer(x):
     #   print(f"{y} \n")
#    print(x)



#print(mat)
