/* 
*   Matrix Market I/O example program
*
*   Read a real (non-complex) sparse matrix from a Matrix Market (v. 2.0) file.
*   and copies it to stdout.  This porgram does nothing useful, but
*   illustrates common usage of the Matrix Matrix I/O routines.
*   (See http://math.nist.gov/MatrixMarket for details.)
*
*   Usage:  a.out [filename] > output
*
*       
*   NOTES:
*
*   1) Matrix Market files are always 1-based, i.e. the index of the first
*      element of a matrix is (1,1), not (0,0) as in C.  ADJUST THESE
*      OFFSETS ACCORDINGLY offsets accordingly when reading and writing 
*      to files.
*
*   2) ANSI C requires one to use the "l" format modifier when reading
*      double precision floating point numbers in scanf() and
*      its variants.  For example, use "%lf", "%lg", or "%le"
*      when reading doubles, otherwise errors will occur.
*/

#include <io.h>
#include <mmio.h>
#include <mysecond.h>
#include <string.h>
//#include <mkl_types.h>
#include <mkl_spblas.h>

#define CSR 0
#define BSR 1

void execSpMV(sparse_matrix_t *A, int symmetric, 
        double *x, double *y, sparse_operation_t op, 
        double *t_it, int iterations)
{
    struct timespec t_start, t_end;
    double alpha = 1.0, beta = 0.0;
    struct matrix_descr descr;
    if(symmetric) { 
        fprintf(stderr,"SpMV Symmetric ...\n");
        descr.type = SPARSE_MATRIX_TYPE_SYMMETRIC;
        descr.mode = SPARSE_FILL_MODE_LOWER;
        descr.diag = SPARSE_DIAG_NON_UNIT;
    } else {
        fprintf(stderr, "SpMV General ...\n");
        descr.type = SPARSE_MATRIX_TYPE_GENERAL;
    }

    for (int it = 0; it < iterations; it++) {
        //memset(y, 0, M * sizeof(double));
        mygettime(&t_start);
        sparse_status_t stat = mkl_sparse_d_mv(op, alpha, *A, descr, x, beta, y);
        //sparse_status_t stat = SPARSE_STATUS_SUCCESS;
        mygettime(&t_end);
        if (stat != SPARSE_STATUS_SUCCESS) {
            fprintf(stderr, "MKL SpMV failed!\n");
            exit(1);
        }
        t_it[it] = mydifftimems(&t_start, &t_end);

        /*if(it==0 && fopen(".test", "r")) {
            for(MKL_INT i=0; i < origN; ++i) {
                fprintf(stderr, "%7.5lf ", y[i]);
            }
        }*/
    }
}

int main(int argc, char *argv[])
{
    FILE *f;
    MKL_INT M, N, nz, origM, origN, *I, *J, i;
    int iterations = 5;
    double *val;
    MM_typecode matcode;
    double alpha = 1.0, beta = 0.0;

    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s [martix-market-filename]\n", argv[0]);
        exit(1);
    }

    fprintf(stdout,"mat,fmt,fmtblk,nblocks,blockx,blocky,indx,indendx,indy,indendy,nz,iterations,time\n");

    int ninps, n;
    for(n = 1; n < argc; n++)
    {

        if ((f = fopen(argv[n], "r")) == NULL) 
            exit(1);

        readHeader(f, &M, &N, &nz, &matcode);
        int isPattern = mm_is_pattern(matcode);

        origM = M;
        origN = N;
        int patchmatshape = 1;
        if (patchmatshape) 
        {
            MKL_INT mul = 120; //lcm 2,3,4,5,6
            if(M % mul) M += (mul-(M % mul));
            if(N % mul) N += (mul-(N % mul));
            if(origM % mul || origN % mul) fprintf(stderr, "Patched shapes to accept BCSR blocks... New values M=%lld N=%lld\n",M, N);
        }

        /* reseve memory for matrices */
        I = (MKL_INT *) malloc(nz * sizeof(MKL_INT));
        J = (MKL_INT *) malloc(nz * sizeof(MKL_INT));
        val = (double *) malloc(nz * sizeof(double));

        fprintf(stderr, "Reading matrix Market Market type: [%s]\n", mm_typecode_to_str(matcode));
        /* NOTE: when reading in doubles, ANSI C requires the use of the "l"  */
        /*   specifier as in "%lg", "%lf", "%le", otherwise errors will occur */
        /*  (ANSI C X3.159-1989, Sec. 4.9.6.2, p. 136 lines 13-15)            */
        if (isPattern) {
            for (i=0; i<nz; i++) {
                fscanf(f, "%lld %lld\n", &I[i], &J[i]);
                val[i] = 1.0;
                I[i]--;  /* adjust from 1-based to 0-based */
                J[i]--;
            }
        } else {
            for (i=0; i<nz; i++) {
                fscanf(f, "%lld %lld %lg\n", &I[i], &J[i], &val[i]);
                I[i]--;  /* adjust from 1-based to 0-based */
                J[i]--;
#ifdef DEBUG
                fprintf(stderr, "(%d, %d) %7.5lf ", J[i], I[i], val[i]);
#endif
            }
        }
#ifdef DEBUG
        fprintf(stderr,"\n====\n");
#endif

        if (f !=stdin) fclose(f);

        double percnz = 0.0;
        if(mm_is_symmetric(matcode))
        {
            percnz = (100.0*nz*2)/((double)(M)*N);
        } else {
            percnz = (100.0*nz)/((double)(M)*N);
        }
        fprintf(stderr, "Converting to MKL COO M= %lld N= %lld nz= %lld percnz=%lf%%\n", M, N, nz, percnz);
        /************************/
        /* Calculation */
        /************************/

        double * x = (double *) malloc(N * sizeof(double));
        double * y = (double *) malloc(M * sizeof(double));
        for (i=0; i < N; ++i) {
            x[i] = (double) i;

#ifdef DEBUG
            fprintf(stderr, "%7.5lf ", x[i]);
#endif
        }
#ifdef DEBUG
        fprintf(stderr,"\n====\n");
#endif

        double *t_it = (double *) malloc(iterations * sizeof(double));
        if (!t_it){fprintf(stderr, "Could not allocate t_it.\n"); exit(1);}

        MKL_INT nblks = 4;
        MKL_INT mstep = M / nblks;
        MKL_INT nstep = N / nblks;

        MKL_INT *Iblk = (MKL_INT *) malloc(nz * sizeof(MKL_INT));
        MKL_INT *Jblk = (MKL_INT *) malloc(nz * sizeof(MKL_INT));
        double *valblk = (double *) malloc(nz * sizeof(double));

        memset(y, 0, M * sizeof(double));

        // Split matrix and execute it per block 
        for(MKL_INT iR=0; iR < M; iR += mstep) {
            for(MKL_INT iC=0; iC < N; iC += nstep) {

                //fprintf(stdout, "Running M= %lld %lld N= %lld %lld ...\n", iR,mstep,iC,nstep)
                int diag = iR == iC; 

                MKL_INT nzblk = 0;
                for (i=0; i<nz; i++) {
                    if( I[i] >= iR && I[i] < iR+mstep &&
                        J[i] >= iC && J[i] < iC+nstep) {
                        Iblk[nzblk] = I[i]-iR;
                        Jblk[nzblk] = J[i]-iC;
                        valblk[nzblk] = val[i];
                        ++nzblk;
                    }
                }
                fprintf(stderr, "Running M= %lld %lld N= %lld %lld nz= %lld ...\n", iR,iR+mstep,iC,iC+nstep, nzblk);

                if(nzblk == 0) continue;

                sparse_matrix_t Acoo;
                sparse_index_base_t indexing = SPARSE_INDEX_BASE_ZERO;
                sparse_status_t stat = mkl_sparse_d_create_coo(&Acoo, indexing, mstep, nstep, nzblk, Iblk, Jblk, valblk);
                if (stat != SPARSE_STATUS_SUCCESS) {
                    fprintf(stderr, "MKL COO creation failed!\n");
                }

                int fmt[6][2] = {{CSR,0},{BSR, 2},{BSR, 3},{BSR, 4},{BSR, 5},{BSR, 6}};
                for(int k=0; k<6; ++k)
                {
                    sparse_operation_t op = SPARSE_OPERATION_NON_TRANSPOSE;
                    sparse_matrix_t A;
                    int curfmt = fmt[k][0];
                    int blkfmt = fmt[k][1];
#pragma @ICE block=spmv
                    if (curfmt == CSR) {
                        fprintf(stderr, "Converting from COO to CSR ...\n");
                        sparse_status_t stat = mkl_sparse_convert_csr( Acoo, op, &A);
                        if (stat != SPARSE_STATUS_SUCCESS) {
                            fprintf(stderr, "MKL COO->CSR convertion failed! %d\n",stat);
                            exit(1);
                        }
                    } else {
                        fprintf(stderr, "Converting from COO to BSR %d blk ...\n", blkfmt);
                        sparse_layout_t blay = SPARSE_LAYOUT_ROW_MAJOR;
                        sparse_status_t stat = mkl_sparse_convert_bsr( Acoo, blkfmt, blay, op, &A);
                        if (stat != SPARSE_STATUS_SUCCESS) {
                            fprintf(stderr, "MKL COO->CSR convertion failed! %d\n",stat);
                            //exit(1);
                            continue;
                        }
                    }
#pragma @ICE endblock

                    execSpMV(&A, diag && mm_is_symmetric(matcode), &x[iC], &y[iR], op, t_it, iterations);

                    mkl_sparse_destroy(A);

                    for(int it = 0; it < iterations; it++) {
                        fprintf(stdout,"%s,%s,%d,%lld,%lld,%lld,%lld,%lld,%lld,%lld,%lld,%d,",
                            argv[n], curfmt == 0 ? "CSR" : "BSR", blkfmt, nblks, 
                            iR/mstep, iC/nstep, 
                            iR, iR+mstep, 
                            iC, iC+nstep, nzblk, iterations);
                        printf("%7.5lf\n" ,t_it[it]);
                    }

                    /*fprintf(stdout, "Sparse %s matblock %lld %lld nblocks %lld  M= %lld %lld N= %lld %lld nz= %lld Iterations= %d Time(milisec) ", 
                            argv[n], iR/mstep, iC/nstep, nblks, iR, iR+mstep, iC, iC+nstep, nzblk, iterations);
                    for(int it = 0; it < iterations; it++) {
                        printf("%7.5lf " ,t_it[it]);
                    }
                    printf("\n");
                    */
                }
                mkl_sparse_destroy(Acoo);
            }
        }
        free(Iblk);
        free(Jblk);
        free(valblk);
        free(I);
        free(J);
        free(val);
        free(x);
        free(y);
        free(t_it);
    }
}

