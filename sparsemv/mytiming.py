import re
import statistics as stat

def getTiming(std, error):
    #print("std=",std)
    r = re.search('Sparse.*',std)
    m = []
    if r:
        #print("R= ", r)
        m = re.split('\s+', r.group())
    #print("M= ",m)
    if m and len(m) >= 10:
        vals = []
        unit = m[10]
        unit = unit[unit.rfind('(')+1:unit.rfind(')')]
        numit = int(m[9])
        for it in range(11,11+numit):
            vals.append(float(m[it]))
        ##
        metric = stat.median(vals)
        #print(f"EITA $$$ ### {metric} {type(metric)}")
        result = {'metric': metric, 
                  # it must be tuple of tuples despite being a single timer
                  'exps': tuple([(v,) for v in vals]), 
                  'unit': (unit,),
                  'desc': ('Total',)}
        #rddesult = float(m[6])
    else:
        result = {'metric': 'N/A'} #'N/A'
    #

    return result
###
