#!/usr/bin/env python3

import scipy.io as sio
import numpy as np
import argparse

parser = argparse.ArgumentParser(description="")
parser.add_argument('-i', type=str, required=True, help="Path to the sparse matrix.")
parser.add_argument('-o', type=str, required=False, default='answer.txt', help="Path to the sparse matrix.")

args=parser.parse_args()


#name="./inps/3dtube/3dtube.mtx"
name = args.i
m = sio.mmread(name)
print(m.toarray())
x = np.arange(0,m.shape[1])
print(x)
y = m.dot(x)

outname = args.o #"answer.txt"
with open(outname, 'w') as out:
    print("Writing results to ",outname)
    out.write(name+'\n')
    out.write("\ny:\n")
    for i, v in enumerate(y):
        #out.write("{0:7.5f} ".format(v))
        out.write("{0:e} ".format(v))
        if i % 80 == 79:
            out.write("\n")
        #
    ##
    out.write("\n")
###
