/* 
*   Matrix Market I/O example program
*
*   Read a real (non-complex) sparse matrix from a Matrix Market (v. 2.0) file.
*   and copies it to stdout.  This porgram does nothing useful, but
*   illustrates common usage of the Matrix Matrix I/O routines.
*   (See http://math.nist.gov/MatrixMarket for details.)
*
*   Usage:  a.out [filename] > output
*
*       
*   NOTES:
*
*   1) Matrix Market files are always 1-based, i.e. the index of the first
*      element of a matrix is (1,1), not (0,0) as in C.  ADJUST THESE
*      OFFSETS ACCORDINGLY offsets accordingly when reading and writing 
*      to files.
*
*   2) ANSI C requires one to use the "l" format modifier when reading
*      double precision floating point numbers in scanf() and
*      its variants.  For example, use "%lf", "%lg", or "%le"
*      when reading doubles, otherwise errors will occur.
*/

#include <math.h>
#include <io.h>
#include <mmio.h>
#include <mysecond.h>
#include <string.h>
//#include <mkl_types.h>
#include <mkl_spblas.h>

#define EPS 0.000001f
#define BUFNAMESIZE 32
#define CSR 0
#define BSR 1

void print_vector(double * y, MKL_INT M, double * x, MKL_INT N, char * matname)
{
    MKL_INT j;
    char bufname[BUFNAMESIZE];
    snprintf(bufname, BUFNAMESIZE, "answer-spmv-%lld.txt",M);
    FILE *fout = fopen(bufname,"w");
    if(!fout){
          fprintf(stderr,"ERROR! Could not open %s to output results!\n", bufname);
          exit(1);
    }

    fprintf(fout, "%s\n", matname);
/*    fprintf(fout, "%s\nx:\n", matname);
    for (j=0; j<N; j++) {
        fprintf(fout, "%lf ", x[j]) ;
        if (j%80 == 79) fprintf(fout, "\n");
    }
*/
    fprintf(fout, "\ny:\n");

    for (j=0; j<M; j++) {
        //fprintf(fout, "%7.5lf ", y[j]) ;
        fprintf(fout, "%e ", y[j]) ;
        if (j%80 == 79) fprintf(fout, "\n");
    }
    fprintf(fout, "\n");

    fclose(fout);
}

// Based on https://numpy.org/doc/1.18/reference/generated/numpy.allclose.html
int allclose(double *y, double *yref, const int m, double rtol=1e-5, double atol=1e-8)
{
    for (MKL_INT i = 0; i < m; ++i) {
        double diff = fabs(y[i] - yref[i]);
        double tolerance = atol + rtol*fabs(yref[i]);
        //fprintf(stderr,"%lld diff: %e tol: %e rtol: %e\n",i,diff, tolerance,rtol*fabs(yref[i]));
        if (diff > tolerance) {
            fprintf(stderr,"y[%lld] %e != %e  yref[%lld]\n",i,y[i],yref[i],i);
            return 1;
        }
    }
    return 0;
}

void execSpMV(sparse_matrix_t *A, int symmetric, 
        double *x, double *y, sparse_operation_t op, 
        double *t_it, int niter)
{
    struct timespec t_start, t_end;
    double alpha = 1.0, beta = 1.0;
    struct matrix_descr descr;
    if(symmetric) { 
        fprintf(stderr,"SpMV Symmetric ...\n");
        descr.type = SPARSE_MATRIX_TYPE_SYMMETRIC;
        descr.mode = SPARSE_FILL_MODE_LOWER;
        descr.diag = SPARSE_DIAG_NON_UNIT;
    } else {
        //fprintf(stderr, "SpMV General ...\n");
        descr.type = SPARSE_MATRIX_TYPE_GENERAL;
    }

    for (int it = 0; it < niter; it++) {
        //memset(y, 0, M * sizeof(double));
        //printf("Eita computing!\n");
        mygettime(&t_start);
        sparse_status_t stat = mkl_sparse_d_mv(op, alpha, *A, descr, x, beta, y);
        mygettime(&t_end);
        if (stat != SPARSE_STATUS_SUCCESS) {
            fprintf(stderr, "MKL SpMV failed!\n");
            exit(1);
        }
        t_it[it] += mydifftimems(&t_start, &t_end);

    }
}

void convA(const MKL_INT rS, const MKL_INT rE, 
           const MKL_INT cS, const MKL_INT cE, 
           const MKL_INT nz, 
           const MKL_INT * const I, const MKL_INT * const J,
           const double * const val,
           double *x, double *y,
           const int curfmt, const int blkfmt, const int symmetric,
           double *t_it, int niter, 
           char * matname)
{
    MKL_INT *Iblk = (MKL_INT *) malloc(nz * sizeof(MKL_INT));
    MKL_INT *Jblk = (MKL_INT *) malloc(nz * sizeof(MKL_INT));
    double *valblk = (double *) malloc(nz * sizeof(double));
    MKL_INT mstep = rE-rS, nstep = cE-cS;

    int diag = rS == cS; 

    MKL_INT nzblk = 0;
    for (MKL_INT i=0; i<nz; i++) {
        if( I[i] >= rS && I[i] < rE &&
            J[i] >= cS && J[i] < cE) {
            Iblk[nzblk] = I[i]-rS;
            Jblk[nzblk] = J[i]-cS;
            valblk[nzblk] = val[i];
            ++nzblk;
        }
    }
    fprintf(stderr, "Running M= %lld %lld N= %lld %lld nz= %lld ... ", rS,rE,cS,cE, nzblk);

    if(nzblk == 0) {
        free(Iblk);
        free(Jblk);
        free(valblk);
        return;
    }

    sparse_matrix_t Acoo;
    sparse_index_base_t indexing = SPARSE_INDEX_BASE_ZERO;
    sparse_status_t stat = mkl_sparse_d_create_coo(&Acoo, indexing, mstep, nstep, nzblk, Iblk, Jblk, valblk);
    if (stat != SPARSE_STATUS_SUCCESS) {
        fprintf(stderr, "MKL COO creation failed!\n");
        exit(1);
    }

    sparse_operation_t op = SPARSE_OPERATION_NON_TRANSPOSE;
    sparse_matrix_t A;

    if (curfmt == CSR) {
        fprintf(stderr, "COO to CSR ...\n");
        sparse_status_t stat = mkl_sparse_convert_csr( Acoo, op, &A);
        if (stat != SPARSE_STATUS_SUCCESS) {
            fprintf(stderr, "MKL COO->CSR convertion failed! %d\n",stat);
            exit(1);
        }
    } else {
        fprintf(stderr, "COO to BSR %d blk ...\n", blkfmt);
        sparse_layout_t blay = SPARSE_LAYOUT_ROW_MAJOR;
        sparse_status_t stat = mkl_sparse_convert_bsr( Acoo, blkfmt, blay, op, &A);
        if (stat != SPARSE_STATUS_SUCCESS) {
            fprintf(stderr, "MKL COO->CSR convertion failed! %d\n",stat);
            exit(1);
        }
    }

    execSpMV(&A, diag && symmetric, &x[cS], &y[rS], op, t_it, niter);

/*    for(int it = 0; it < niter; it++) {
        fprintf(stdout,"%s,%s,%d,%lld,%lld,%lld,%lld,%lld,%d,",
            matname, curfmt == 0 ? "CSR" : "BSR", blkfmt, 
            rS, rE, cS, cE, nzblk, niter);
        printf("%7.5lf\n" ,t_it[it]);
    }
*/
    mkl_sparse_destroy(A);
    mkl_sparse_destroy(Acoo);
    free(Iblk);
    free(Jblk);
    free(valblk);
}

void execSpMV2(sparse_operation_t op, double alpha, sparse_matrix_t A,
        struct matrix_descr *descr, double *x, double beta, double *y)
{
        sparse_status_t stat = mkl_sparse_d_mv(op, alpha, A, *descr, x, beta, y);
        if (stat != SPARSE_STATUS_SUCCESS) {
            fprintf(stderr, "MKL SpMV failed!\n");
            exit(1);
        }
}

sparse_matrix_t convA2(const MKL_INT rS, const MKL_INT rE, 
           const MKL_INT cS, const MKL_INT cE, 
           const MKL_INT nz, 
           const MKL_INT * const I, const MKL_INT * const J,
           const double * const val,
           const int curfmt, const int blkfmt,
           sparse_operation_t op)
{
    MKL_INT *Iblk = (MKL_INT *) malloc(nz * sizeof(MKL_INT));
    MKL_INT *Jblk = (MKL_INT *) malloc(nz * sizeof(MKL_INT));
    double *valblk = (double *) malloc(nz * sizeof(double));
    MKL_INT mstep = rE-rS, nstep = cE-cS;

    int diag = rS == cS; 

    MKL_INT nzblk = 0;
    for (MKL_INT i=0; i<nz; i++) {
        if( I[i] >= rS && I[i] < rE &&
            J[i] >= cS && J[i] < cE) {
            Iblk[nzblk] = I[i]-rS;
            Jblk[nzblk] = J[i]-cS;
            valblk[nzblk] = val[i];
            ++nzblk;
        }
    }
    fprintf(stderr, "Running M= %lld %lld N= %lld %lld nz= %lld ... ", rS,rE,cS,cE, nzblk);

    if(nzblk == 0) {
        free(Iblk);
        free(Jblk);
        free(valblk);
        return NULL;
    }

    sparse_matrix_t Acoo;
    sparse_index_base_t indexing = SPARSE_INDEX_BASE_ZERO;
    sparse_status_t stat = mkl_sparse_d_create_coo(&Acoo, indexing, mstep, nstep, nzblk, Iblk, Jblk, valblk);
    if (stat != SPARSE_STATUS_SUCCESS) {
        fprintf(stderr, "MKL COO creation failed!\n");
        exit(1);
    }

    sparse_matrix_t A;

    if (curfmt == CSR) {
        fprintf(stderr, "COO to CSR ...\n");
        sparse_status_t stat = mkl_sparse_convert_csr( Acoo, op, &A);
        if (stat != SPARSE_STATUS_SUCCESS) {
            fprintf(stderr, "MKL COO->CSR convertion failed! %d\n",stat);
            exit(1);
        }
    } else {
        fprintf(stderr, "COO to BSR %d blk ...\n", blkfmt);
        sparse_layout_t blay = SPARSE_LAYOUT_ROW_MAJOR;
        sparse_status_t stat = mkl_sparse_convert_bsr( Acoo, blkfmt, blay, op, &A);
        if (stat != SPARSE_STATUS_SUCCESS) {
            fprintf(stderr, "MKL COO->CSR convertion failed! %d\n",stat);
            exit(1);
        }
    }

    mkl_sparse_destroy(Acoo);
    free(Iblk);
    free(Jblk);
    free(valblk);

    return A;
}

int main(int argc, char *argv[])
{
    FILE *f;
    MKL_INT M, N, nz, origM, origN, i;
    int niter = 5;
    double *val;
    MM_typecode matcode;

    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s [martix-market-filename]\n", argv[0]);
        exit(1);
    }

    fprintf(stdout,"mat,fmt,fmtblk,nblocks,blockx,blocky,indx,indendx,indy,indendy,nz,iterations,time\n");

    int ninps, n;
    for(n = 1; n < argc; n++)
    {

        if ((f = fopen(argv[n], "r")) == NULL) 
            exit(1);

        readHeader(f, &M, &N, &nz, &matcode);
        int isPattern = mm_is_pattern(matcode);
        int isSymmetric = mm_is_symmetric(matcode);
        fprintf(stderr, "MatrixMarket %s M=%lld N=%lld nz=%lld [%s]\n", argv[n], M, N, nz, mm_typecode_to_str(matcode));

        origM = M;
        origN = N;
        int patchmatshape = 1;
        if (patchmatshape) 
        {
            // patched shapes to accept BCSR blocks
            MKL_INT mul = 240; //lcm 2,3,4,5,6... the subblocks also need to multiple of 2,3,4,5,6
            if(M % mul) M += (mul-(M % mul));
            if(N % mul) N += (mul-(N % mul));
            if(origM % mul || origN % mul) fprintf(stderr, "Patched new values M=%lld N=%lld\n",M, N);
        }

        /* reseve memory for matrices */
        MKL_INT *I = (MKL_INT *) malloc(nz * sizeof(MKL_INT));
        MKL_INT *J = (MKL_INT *) malloc(nz * sizeof(MKL_INT));
        val = (double *) malloc(nz * sizeof(double));
        MKL_INT nzdiag = 0;

        /* NOTE: when reading in doubles, ANSI C requires the use of the "l"  */
        /*   specifier as in "%lg", "%lf", "%le", otherwise errors will occur */
        /*  (ANSI C X3.159-1989, Sec. 4.9.6.2, p. 136 lines 13-15)            */
        if (isPattern) {
            for (i=0; i<nz; i++) {
                fscanf(f, "%lld %lld\n", &I[i], &J[i]);
                val[i] = 1.0;
                I[i]--;  /* adjust from 1-based to 0-based */
                J[i]--;
                if (I[i] == J[i]) ++nzdiag;
            }
        } else {
            for (i=0; i<nz; i++) {
                fscanf(f, "%lld %lld %lg\n", &I[i], &J[i], &val[i]);
                I[i]--;  /* adjust from 1-based to 0-based */
                J[i]--;
                if (I[i] == J[i]) ++nzdiag;
#ifdef DEBUG
                fprintf(stderr, "(%d, %d) %7.5lf ", J[i], I[i], val[i]);
#endif
            }
        }
#ifdef DEBUG
        fprintf(stderr,"\n====\n");
#endif

        if (f !=stdin) fclose(f);

        double * x = (double *) malloc(N * sizeof(double));
        for (i=0; i < N; ++i) {
            x[i] = (double) i;

#ifdef DEBUG
            fprintf(stderr, "%7.5lf ", x[i]);
#endif
        }
#ifdef DEBUG
        fprintf(stderr,"\n====\n");
#endif

        double *t_it = (double *) calloc(niter, sizeof(double));
        if (!t_it){fprintf(stderr, "Could not allocate t_it.\n"); exit(1);}

        double * y = (double *) calloc(M, sizeof(double));
        double * yref = (double *) calloc(M, sizeof(double));
        fprintf(stderr,"Creating a yreference...\n");
        convA(0, M, 0, N, nz, I, J, val, x, yref, CSR, 0, isSymmetric, t_it, 1, argv[n]);

        //Represeting symmetric matrices as full for make easier blocking.
        if (isSymmetric) {
            MKL_INT newnz = nz + nz - nzdiag;
            //allocate bigger areas
            MKL_INT *newI = (MKL_INT *) malloc(newnz * sizeof(MKL_INT));
            MKL_INT *newJ = (MKL_INT *) malloc(newnz * sizeof(MKL_INT));
            double *newval = (double *) malloc(newnz * sizeof(double));

            MKL_INT j = 0;
            for (i=0; i<nz && j<newnz; ++i, ++j) {
                newI[j] = I[i];
                newJ[j] = J[i];
                newval[j] = val[i];
#ifdef DEBUG
                fprintf(stderr, "(%lld, %lld) %7.5lf -> (%lld %lld) %7.5lf\n", I[i], J[i], val[i], newI[j], newJ[j], newval[j]);
#endif
                if (I[i] > J[i]) {
                    ++j;
                    newI[j] = J[i];
                    newJ[j] =  I[i];
                    newval[j] = val[i];
#ifdef DEBUG
                    fprintf(stderr, "** (%lld, %lld) %7.5lf -> (%lld %lld) %7.5lf\n", I[i], J[i], val[i], newI[j], newJ[j], newval[j]);
#endif
                }
            }
            free(I);
            free(J);
            free(val);
            I = newI;
            J = newJ;
            val = newval;
            nz = newnz;
            double percnz = (100.0*nz)/((double)(M)*N);
            fprintf(stderr, "Symmetric matrix is now: M=%lld N=%lld new nz=%lld  percnz=%lf%%\n", M, N, nz, percnz);
        } else {
            double percnz = (100.0*nz)/((double)(M)*N);
            fprintf(stderr, "Generic matrix is now: M=%lld N=%lld new nz=%lld percnz=%lf%%\n", M, N, nz, percnz);
        }

        /************************/
        /* Calculation */
        /************************/

        int isReallySymmetric = 0;
        sparse_operation_t op = SPARSE_OPERATION_NON_TRANSPOSE;
        double alpha = 1.0, beta = 1.0;
        struct matrix_descr * descr = (struct matrix_descr *) malloc(sizeof(struct matrix_descr));;
        if(isReallySymmetric) { 
            //fprintf(stderr,"SpMV Symmetric ...\n");
            descr->type = SPARSE_MATRIX_TYPE_SYMMETRIC;
            descr->mode = SPARSE_FILL_MODE_LOWER;
            descr->diag = SPARSE_DIAG_NON_UNIT;
        } else {
            //fprintf(stderr, "SpMV General ...\n");
            descr->type = SPARSE_MATRIX_TYPE_GENERAL;
        }

        //memset(t_it, 0, niter*sizeof(double));
        struct timespec t_start, t_end;
#pragma @ICE block=spmv
        for (int it = 0; it < niter; it++) {
            memset(y, 0, sizeof(double) * M);
            sparse_matrix_t A = convA2(0, M, 0, N, nz, I, J, val, CSR, 0, op);
            mygettime(&t_start);
            if(A != NULL) execSpMV2(op, alpha, A, descr, x, beta, y);
            mygettime(&t_end);
            mkl_sparse_destroy(A);
            t_it[it] = mydifftimems(&t_start, &t_end);
        }
#pragma @ICE endblock 
        fprintf(stdout,"Sparsemv %s,%lld,%lld,%lld,%lld,%lld, Iterations %d Time(milisec) ",
                argv[n], 0l, M, 0l, N, nz, niter);
        for(int it = 0; it < niter; it++) {
            printf("%7.5lf " ,t_it[it]);
        }
        fprintf(stdout, "\n");


        if (fopen(".test", "r")) {
            print_vector(y, origM, x, origN, argv[n]);;
        }

        int incorrect = allclose(y, yref, origM);
        if (incorrect) {
            exit(1);
        }

        free(descr);
        free(I);
        free(J);
        free(val);
        free(x);
        free(y);
        free(t_it);
    }
}
