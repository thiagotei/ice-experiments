#!/usr/bin/env bash

matfile=$1
export ICELOCUS_SPMV=$matfile
export ICELOCUS_STOPBLKS='4'
ice-locus-opentuner.py -t spmv-horiz.locus -f spmvt3.cpp --tfunc mytiming.py:getTiming --timeout --search --equery --exprec --saveexprec -o suffix --ntests 1000 #&> out_`basename ${ICELOCUS_SPMV}`.txt 
