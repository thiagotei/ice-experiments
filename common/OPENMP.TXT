#!/bin/bash
export LD_LIBRARY_PATH=/opt/intel-2017.1/lib/intel64:$LD_LIBRARY_PATH
echo OMP_PLACES=cores OMP_PROC_BIND=close OMP_NUM_THREADS=10 ./bin

echo ./howtosearch.sh &> seidel-${OMP_NUM_THREADS}threads-`date +%Y%m%d%H%M`.txt

echo grep "OpenTunerToolLocus: Run"

echo :syn match Comment "#.*$"
