#!/bin/bash

die () {
	echo >&2 "$@"
	exit 1
}

NTESTS=3
SEARCH=""
LOCUS=""
SRCFILE=""
DEBUG=""
PREPROC=" "


while getopts :sn:t:f:dp: option
do
case "${option}"
in
s) SEARCH="--search";;
n) NTESTS=${OPTARG};;
t) LOCUS=${OPTARG};;
f) SRCFILE=${OPTARG};;
d) DEBUG="--debug";;
p) PREPROC=${OPTARG};;
?) echo "${option} arg not recognized!"
esac
done

#if [ "$#" -eq 1 ];then ntests=${1}; fi #|| die "1 filename required -- input to be encrypted and the output will be input + .enc , $# provided
cmd="ice-locus-opentuner.py ${DEBUG} ${SEARCH} -t ${LOCUS} -f ${SRCFILE} --tfunc mytiming.py:getTimingMatMul -o suffix -u .ice  --ntests ${NTESTS}" 
echo $cmd
${cmd}
#ice-locus-opentuner.py ${DEBUG} ${SEARCH} -t ${LOCUS} -f ${SRCFILE} --tfunc mytiming.py:getTimingMatMul -o suffix -u ".ice"  --ntests ${NTESTS} --debug  #--preproc ${PREPROC}
