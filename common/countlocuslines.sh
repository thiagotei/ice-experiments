#!/usr/bin/env bash

for inp in "$@"
do
echo -n "Counting lines of $inp "
sed '/OptSeq/,$!d' "$inp" | egrep  -cv '^\s*$|^\s*#'
#egrep  -cv '^\s*$|^\s*#' $1
done

