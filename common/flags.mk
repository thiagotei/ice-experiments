
CC ?= icc


ifeq ($(CC), icc)
OPTFLAGS := -O3 -xHost -ansi-alias -ipo -fp-model precise
OMPFLAGS := -qopenmp
else
  ifeq ($(CC), xlc)
    OPTFLAGS := -O3 -qhot
    OMPFLAGS := #-qsmp
  else
    #gcc
    OPTFLAGS := -O3 -mtune=native -ftree-vectorize -Wall -fopenmp # -march=native
    OMPFLAGS := -fopenmp
endif
endif

INC += -I.
CFLAGS += -Wall
LDFLAGS += -lm

