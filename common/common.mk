#ALL: all-redirect

# Change these to match your compilers
#CC = clang
#CC = gcc
CC ?= icc

ifeq ($(CC), icc)
OPTFLAGS := -O3 -xHost -ansi-alias -ipo -fp-model precise
OMPFLAGS := -qopenmp
else
#gcc
OPTFLAGS := -O3 -march=native -mtune=native -ftree-vectorize -Wall -fopenmp -I.
OMPFLAGS := -fopenmp
endif

INC +=
CFLAGS += -Wall
LDFLAGS += -lm

#all-redirect: $(SRC)

$(SRC): $(SRC).o dummy.o mysecond.o pips_runtime.o
	$(CC) -o $@ $(OPTFLAGS) $(OMPFLAGS) $(CFLAGS) $^ $(LDFLAGS)

$(ORIG): $(ORIG).o dummy.o mysecond.o pips_runtime.o
	$(CC) -o $@ $(OPTFLAGS) $(OMPFLAGS)  $(CFLAGS) $^ $(LDFLAGS)

.c.o:
	$(CC) -c $(INC) $(OPTFLAGS) $(OMPFLAGS)  $(CFLAGS) $<
.f.o:
	$(FC) -c $(FCFLAGS) $<

clean:
	rm -f *.o
	rm -f $(SRC)
	rm -f $(ORIG)


.PHONY: clean all-redirect
