#ALL: all-redirect

# Change these to match your compilers
#CC = clang
#CC = gcc
CC ?= icc


ifeq ($(CC), icc)
OPTFLAGS := -O3 -xHost -ansi-alias -ipo -fp-model precise -Wno-unknown-pragmas
OMPFLAGS := -qopenmp
else
  ifeq ($(CC), xlc)
    OPTFLAGS := -O3 -qhot
    OMPFLAGS := #-qsmp
  else
    #gcc
    OPTFLAGS := -O3 -mtune=native -ftree-vectorize -Wall -fopenmp # -march=native
    OMPFLAGS := -fopenmp
endif
endif

INC += -I.
CFLAGS += -Wall
LDFLAGS += -lm

#all-redirect: $(SRC)

$(SRC): $(SRC).c  pips_runtime.c
	$(CC) -o $@ $(INC) $(OPTFLAGS) $(OMPFLAGS) $(USERDEFS) $(CFLAGS) $^ $(LDFLAGS)

orig: $(ORIG).c #pips_runtime.c
	$(CC) -o $@ $(INC) $(OPTFLAGS) $(USERDEFS) $(CFLAGS) $^ $(LDFLAGS)

#.c.o:
#	$(CC) -c $(INC) $(OPTFLAGS) $(OMPFLAGS)  $(CFLAGS) $<
#.f.o:
#	$(FC) -c $(FCFLAGS) $<

clean:
	rm -f *.o
	rm -f $(SRC)
	rm -f $(ORIG)
	rm -f orig


.PHONY: clean all-redirect
