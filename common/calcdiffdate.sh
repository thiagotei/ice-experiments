#!/bin/bash

#fileinp=output-4096-p9-remote-2lvltile-gcc-8-2-0-1thread-201812301951.txt
for fileinp in "$@"
do
#fileinp=$1
initdate=`grep '\[201[0-9]-[0-9]' $fileinp | head -1 | awk '{print $1 " " $2}' | sed 's/\[//g' | sed 's/\]//g'` #(head -1;echo "===="; tail -1) | awk '{print $1 $2}'
enddate=`grep '\[201[0-9]-[0-9]' $fileinp | tail -1 | awk '{print $1 " "  $2}' | sed 's/\[//g' | sed 's/\]//g'`
echo $fileinp $initdate $enddate "Total time: " `date -d @$(( $(date -d "$enddate" +%s) - $(date -d "$initdate" +%s) )) -u +'%H:%M:%S'`
done
