#!/usr/bin/env bash


#start=5
#end=12
#for p in `seq $start $end`; do
    #pptwo=`echo "2^${p}" | bc`
    #echo Running $p $pptwo
ccc="clang-9" #"icc"
cxx="clang++-9" #"icpc"
$ccc --version
$cxx --version
for p in `seq 512 512 4096`; do
    echo Running $p 
    pptwo=$p

    make CC=${ccc} CXX=${cxx} USERDEFS="-DM=${pptwo} -DN=${pptwo} -DK=${pptwo}" clean orig
    ./orig
    #mv answer-matmul-${pptwo}.txt orig-answer-matmul-${pptwo}.txt 
    #echo Done $p $pptwo
    echo Done $p
done

