#!/bin/bash

#export PYTHONPATH=~/Documents/ice-personal:
#icepath=/home/thiago/Documents/ice-personal/bin
#pipspath=/home/thiago/Documents/pips_partile/MYPIPS
pipspath=/home/thiago/Documents/pips/MYPIPS
intelpath=/opt/intel/
arch="intel64"
compoptspath=/home/thiago/Documents/uiuc-compiler-opts/install

export ICE_CMPOPTS_PATH=$compoptspath
export COMPILERVARS_ARCHITECTURE=$arch
. ${intelpath}/bin/iccvars.sh 
export LD_LIBRARY_PATH=${intelpath}/lib/intel64:$LD_LIBRARY_PATH
#export LD_LIBRARY_PATH=/usr/lib/jvm/java-8-oracle/jre/lib/amd64/server:${LD_LIBRARY_PATH}
#export PATH=${icepath}:$PATH
. ${pipspath}/pipsrc.sh

