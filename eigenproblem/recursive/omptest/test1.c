#define _GNU_SOURCE
#include <omp.h>
#include <stdio.h>
#include <unistd.h>
#include <sched.h>

int main()
{
    #pragma omp parallel
    #pragma omp single
    {

        #pragma omp task untied
        {
            #pragma omp critical
            printf("Task 1 Start - Thread %d Core %d\n",omp_get_thread_num(), sched_getcpu());

            #pragma omp task untied mergeable
            {
                sleep(10);
                #pragma omp critical
                printf("Task 1.1 - Thread %d Core %d\n",omp_get_thread_num(), sched_getcpu());
            }

            #pragma omp task untied mergeable
            {
                sleep(10);
                #pragma omp critical
                printf("Task 1.2 - Thread %d Core %d\n",omp_get_thread_num(), sched_getcpu());
            }

            #pragma omp taskwait

            #pragma omp critical
            printf("Task 1 Merging End - Thread %d Core %d\n", omp_get_thread_num(), sched_getcpu());
        }

        #pragma omp task untied
        {
            #pragma omp critical
            printf("Task 2 Start - Thread %d Core %d\n",omp_get_thread_num(), sched_getcpu());

            #pragma omp task untied mergeable
            {
                sleep(10);
                #pragma omp critical
                printf("Task 2.1 - Thread %d Core %d\n",omp_get_thread_num(), sched_getcpu());
            }

            #pragma omp task untied mergeable
            {
                sleep(3);
                #pragma omp critical
                printf("Task 2.2 - Thread %d Core %d\n",omp_get_thread_num(), sched_getcpu());
            }

            #pragma omp taskwait

            #pragma omp critical
            printf("Task 2 Merging End - Thread %d Core %d\n", omp_get_thread_num(), sched_getcpu());
        }

        #pragma omp taskwait

        #pragma omp critical
        printf("Task 0 Merging Endi - Thread %d Core %d\n", omp_get_thread_num(), sched_getcpu());
    }
}
