#!/usr/bin/env bash

. ./runloop-base.sh

nruns=`seq 1` #3
ntests=20000000000
cfile=eigen.cpp

baseuplimit=10
incuplimit=10

basestopaf=5 #25
incstopaf=15
stopafval=$basestopaf

locusfile=eigen.locus
approxvalues=100
searchtools=ice-locus-hyperopt.py #"ice-locus-opentuner.py" #
#
inpvalues="`seq 128 256 2048`" #"128 768 1280 1536 1792 2048" #"`seq 128 128 2048`" #"128" #


search_loop_body_prolog() {

    xdebug="" #"--debug "
    xparam="--timeout --exprec --saveexprec --stop-after ${stopafval} " #--stop-after #--ntests ${ntests} 

    stopv=16
    export ICELOCUS_MATSHAPE=$inp
    export ICELOCUS_EIGEN_STOP=$stopv
    export ICELOCUS_CC=gcc
    export ICELOCUS_CXX=g++
}

search_loop
