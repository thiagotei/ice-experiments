#!/usr/bin/env bash

origdir=../../orig-src

# Build the base


bins="qrtd,eigcalc_colmj.exe  bisection,eigcalc_bisect.exe lapDC,eigcalc_dc.exe "
inprange="`seq 128 128 2048`"
machine=`hostname` 
comp="gcc75"
prob="eigen"


basetest=0 # 0 is false,1 is true with this (())

if (($basetest)); then
    make clean all -C $origdir

    # Run QRTD, lapack, bisect
    for bin in $bins ;do
        strg=${bin%,*}
        exe=${bin#*,}
        logname="${machine}-${comp}-${prob}-${strg}-none-`date +%Y%m%d%H%M%S`.txt"
        touch $logname
        for inp in $inprange ; do
            echo Running $logname
            ${origdir}/${exe} $inp ./inps/mat_${inp}.inp &>> $logname
        done
    done
fi

# Run search
#searchbins="cobliv,runsearch-cobliv.sh rchoice,runsearch-rec.sh"
#searchbins="rchoice,runsearch-rec.sh "
searchbins="cobliv,runsearch-cobliv.sh "
for sbin in $searchbins ; do
    strg=${sbin%,*}
    exe=${sbin#*,}
    logname="${machine}-${comp}-${prob}-${strg}-none-`date +%Y%m%d%H%M%S`.txt"
    echo Running $logname
    ./${exe} &> $logname
done 


