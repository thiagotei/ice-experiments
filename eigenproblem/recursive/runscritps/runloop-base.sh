#!/usr/bin/env bash

xdebug=""
xparam=""

declare -A mixdsc
mixdsc=(["rand"]="None" ["tpe"]="None" ["anneal"]="None"
        ["mixA"]=".25:rand:.75:tpe" ["mixB"]=".25:rand:.75:anneal"
        ["mixC"]=".25:rand:.50:anneal:0.25:tpe" ["mixD"]=".50:rand:.50:anneal"
        ["mixE"]=".75:rand:.25:anneal")

search_loop_body_prolog(){
    xdebug=""
    xparam="--timeout --upper-limit ${uplimit} --stop-after $stopaf "
}

search_loop_body() {

    search_loop_body_prolog


    if [[ $search == *hyperopt* ]]
    then
        alg="mix"
        mixalg="mixC"
        xparam+="--hypalg $alg --hypmixdsc ${mixdsc[${mixalg}]} "
        #alg="rand"
        #xparam+="--hypalg $alg"
    fi

    rm -rf opetuner.*

    echo Searching matshape= $ICELOCUS_MATSHAPE stop= $ICELOCUS_EIGEN_STOP $search r= $r $ICELOCUS_CC $ICELOCUS_CXX `date`
    echo XPARAM= $xparam

    $search -t ${locusfile} -f ${cfile} \
        --tfunc mytiming.py:getTiming \
        -o suffix -u '.ice' --search \
        $xparam $xdebug
    echo Ended search_loop_body `date`

    stopafval=$(($stopafval + $incstopaf))

}

search_loop() {

    for search in $searchtools; do
        for inp in $inpvalues; do
            for r in $nruns; do
                search_loop_body
            done
        done
    done
}
