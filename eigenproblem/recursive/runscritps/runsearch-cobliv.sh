#!/usr/bin/env bash

. ./runloop-base.sh

nruns=`seq 1` #3
ntests=20000000000
cfile=eigen.cpp

baseuplimit=10
incuplimit=10

basestopaf=40 #25
incstopaf=40

locusfile=eigen-cacheobliv.locus
approxvalues=100
searchtools="ice-locus-opentuner.py" #ice-locus-hyperopt.py #
#
inpvalues="128" #"128 `seq 256 256 2048`" #"`seq 128 128 2048`" #"128 256 512 1024 2048" #"1024" # #"128" #


search_loop_body_prolog() {

    stopv=16
    xdebug="--debug " #"" #
    xparam="--ntests $((2*($inp/$stopv)))" #"--timeout --exprec --saveexprec  " #--ntests ${ntests}

    export ICELOCUS_MATSHAPE=$inp
    export ICELOCUS_EIGEN_STOP=$stopv
    export ICELOCUS_CC=gcc
    export ICELOCUS_CXX=g++
}

search_loop
