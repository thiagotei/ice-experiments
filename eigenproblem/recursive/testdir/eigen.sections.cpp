//#include <lapacke.h>
#include <omp.h>
#include <mysecond.h>
#include "util.h"

using namespace std;

int main(int argc, char** argv)
{
/*
 *  Calculate eigenproblem n x n symmetric matrix
 */
    int ninp = 0;
    int niterations = 5;
    float checkrate = 1.0;
    string inpname = "N/A";

    if (argc >= 3) {
        ninp = std::stoi(argv[1]);
        inpname = argv[2];
        if (argc >= 4) {
            checkrate = std::stof(argv[3]);
        }
    } else {
        std::cerr << "Usage: ./<bin> <dim> <file path> [<checkrate of results>]" << endl;
        return 1;
    }
    int neigeneval = (int) ninp*checkrate;
    std::cout << "Symmetric Eigenproblem n: " << ninp << " inp: " << inpname << 
        " checking rate: " << checkrate << " (" << neigeneval << " eigenvalues and vectors)" << std::endl;

    lapack_int n=ninp, lda=n, ldc=n, ldz=n, lwork;
    TYPE * Aread = new TYPE[n*n];
    TYPE * A = new TYPE[n*n];
    TYPE * Z = new TYPE[n*n];
    TYPE * D = new TYPE[n];
    TYPE * E = new TYPE[n-1];
    TYPE * tau = new TYPE[n]; //new TYPE[n-1];
    TYPE * work = new TYPE[1];
    char uplo = 'U';
    char trans = 'N';
    char side = 'L';

    struct timespec t_start, t_end, t_core_start, t_core_end;
    double * t_it = new double[niterations];
    int incorrect = false;
    TYPE* LHS= new TYPE[n*n];
    TYPE* RHS= new TYPE[n*n]; 
    TYPE* RHStransp = new TYPE[n*n]; 

    if(int ret = readfile(inpname, Aread, ninp)) return ret;

#ifdef DEBUG
    cout << "Aread:" << endl;
    writeresult("out", Aread, n, n);
#endif
#ifdef DEBUG
    cout << "Starting lapack..." << endl;
#endif

    cout << "Eigproblem= "<< n << " Numthreads= ";
#ifdef _OPENMP
    cout << omp_get_max_threads(); 
#else
    cout << "NoOpenMP";
#endif
    cout << " Iterations= " << niterations << " Time(milisec)=";
    for (int it = 0; it < niterations; it++) {

        int index = 0;
        // Pass the data in column major
        for(int j=0; j < n; j++) {
            for(int i=0; i < n; i++ ) {
                //cout << "A[" << index << "] = " << Aread[i*n+j]  << endl;
                A[index++] = Aread[i*n+j];
            }
        }

        //
        // Calculate the T = QAQ', tridiagonal matrix
        // 
        mygettime(&t_start);
        lwork = -1;
        if(lapack_int ret = LAPACKE_dsytrd_work(LAPACK_COL_MAJOR, uplo, n, A, lda, D, E, tau, work, lwork)) {
            cerr << "[Error] dsytrd failed!" << endl;
            return ret;
        }

        lwork=work[0];
        delete [] work;

        work = new TYPE[lwork];

        if(lapack_int ret = LAPACKE_dsytrd_work(LAPACK_COL_MAJOR, uplo, n, A, lda, D, E, tau, work, lwork)) {
            cerr << "[Error] dsytrd II failed!" << endl;
            return ret;
        }

        delete [] work;
        ///////////////

        double one = 1.0;
        double zero = 0.0;
        char uploSET = 'F';
        laset_wrap(&uploSET, &n, &n, &zero, &one, Z, &ldz);

        // Used by the divideandconquer. Must be the same for all the recursion.
        int *indxq = new int[n];

        mygettime(&t_core_start);

        //
        // Call QRTD
        //
#pragma @ICE block=eigen
      int cutpnt = 512;
      TYPE rho = E[cutpnt-1];
      D[cutpnt-1] = D[cutpnt-1] - fabs(rho);
      D[cutpnt]   = D[cutpnt]   - fabs(rho);

#pragma omp parallel
{
  #pragma omp critical
  cout << "Num threads " << omp_get_num_threads() << " Th: " << omp_get_thread_num() << endl; 


  //#pragma omp single 
  #pragma omp sections
  {
    //#pragma omp task
    //{
      //#pragma omp task firstprivate(rho,cutpnt)
      #pragma omp section
      {
          #pragma omp critical
        cout << "Task 0 Thread id: " << omp_get_thread_num() << endl;
        int cutpnt = 256;
        TYPE rho = E[cutpnt-1];
        D[cutpnt-1] = D[cutpnt-1] - fabs(rho);
        D[cutpnt]   = D[cutpnt]   - fabs(rho);
        //#pragma omp task firstprivate(rho,cutpnt)
        {
          int cutpnt = 128;
          TYPE rho = E[cutpnt-1];
          D[cutpnt-1] = D[cutpnt-1] - fabs(rho);
          D[cutpnt]   = D[cutpnt]   - fabs(rho);
         // #pragma omp task firstprivate(rho,cutpnt)
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "It: " << it  << " mS-mE " << 0 << "-" << 128 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            fillIndxq(&indxq[0],128);
            QRTD(&D[0], &Z[0], &E[0], 128, ldz);
          }
          //#pragma omp task firstprivate(rho,cutpnt)
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "It: " << it  << " mS-mE " << 128 << "-" << 256 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            fillIndxq(&indxq[128],128);
            QRTD(&D[128], &Z[131200], &E[128], 128, ldz);
          }
          //#pragma omp taskwait
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "Reduce=> It: " << it  << " mS-mE " << 0 << "-" << 256 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            TYPE * workDC = new TYPE[65536+4*256];
            if(int ret = ComputeEig(&D[0], &Z[0], workDC, &indxq[0], rho, 128, 128, ldz)){
              cerr << "[Error] ComputeEig 0-256 failed! ret error " << ret << endl;
            }
            delete [] workDC;
          }
        }
        //#pragma omp task firstprivate(rho,cutpnt)
        {
          int cutpnt = 384;
          TYPE rho = E[cutpnt-1];
          D[cutpnt-1] = D[cutpnt-1] - fabs(rho);
          D[cutpnt]   = D[cutpnt]   - fabs(rho);
          //#pragma omp task firstprivate(rho,cutpnt)
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "It: " << it  << " mS-mE " << 256 << "-" << 384 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            fillIndxq(&indxq[256],128);
            QRTD(&D[256], &Z[262400], &E[256], 128, ldz);
          }
          //#pragma omp task firstprivate(rho,cutpnt)
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "It: " << it  << " mS-mE " << 384 << "-" << 512 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            fillIndxq(&indxq[384],128);
            QRTD(&D[384], &Z[393600], &E[384], 128, ldz);
          }
          //#pragma omp taskwait
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "Reduce=> It: " << it  << " mS-mE " << 256 << "-" << 512 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            TYPE * workDC = new TYPE[65536+4*256];
            if(int ret = ComputeEig(&D[256], &Z[262400], workDC, &indxq[256], rho, 128, 128, ldz)){
              cerr << "[Error] ComputeEig 256-512 failed! ret error " << ret << endl;
            }
            delete [] workDC;
          }
        }
        //#pragma omp taskwait
        {
          #ifdef DEBUG_OMP
          #pragma omp critical
          cerr << "Reduce=> It: " << it  << " mS-mE " << 0 << "-" << 512 << " Thread id: " << omp_get_thread_num() << endl;
          #endif
          TYPE * workDC = new TYPE[262144+4*512];
          if(int ret = ComputeEig(&D[0], &Z[0], workDC, &indxq[0], rho, 256, 256, ldz)){
            cerr << "[Error] ComputeEig 0-512 failed! ret error " << ret << endl;
          }
          delete [] workDC;
        }
      }
      //#pragma omp task firstprivate(rho,cutpnt)
      #pragma omp section
      {
          #pragma omp critical
        cout << "Task 1 Thread id: " << omp_get_thread_num() << endl;
        int cutpnt = 768;
        TYPE rho = E[cutpnt-1];
        D[cutpnt-1] = D[cutpnt-1] - fabs(rho);
        D[cutpnt]   = D[cutpnt]   - fabs(rho);
       // #pragma omp task firstprivate(rho,cutpnt)
        {
          int cutpnt = 640;
          TYPE rho = E[cutpnt-1];
          D[cutpnt-1] = D[cutpnt-1] - fabs(rho);
          D[cutpnt]   = D[cutpnt]   - fabs(rho);
          //#pragma omp task firstprivate(rho,cutpnt)
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "It: " << it  << " mS-mE " << 512 << "-" << 640 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            fillIndxq(&indxq[512],128);
            QRTD(&D[512], &Z[524800], &E[512], 128, ldz);
          }
          //#pragma omp task firstprivate(rho,cutpnt)
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "It: " << it  << " mS-mE " << 640 << "-" << 768 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            fillIndxq(&indxq[640],128);
            QRTD(&D[640], &Z[656000], &E[640], 128, ldz);
          }
        //  #pragma omp taskwait
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "Reduce=> It: " << it  << " mS-mE " << 512 << "-" << 768 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            TYPE * workDC = new TYPE[65536+4*256];
            if(int ret = ComputeEig(&D[512], &Z[524800], workDC, &indxq[512], rho, 128, 128, ldz)){
              cerr << "[Error] ComputeEig 512-768 failed! ret error " << ret << endl;
            }
            delete [] workDC;
          }
        }
       // #pragma omp task firstprivate(rho,cutpnt)
        {
          int cutpnt = 896;
          TYPE rho = E[cutpnt-1];
          D[cutpnt-1] = D[cutpnt-1] - fabs(rho);
          D[cutpnt]   = D[cutpnt]   - fabs(rho);
          //#pragma omp task firstprivate(rho,cutpnt)
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "It: " << it  << " mS-mE " << 768 << "-" << 896 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            fillIndxq(&indxq[768],128);
            QRTD(&D[768], &Z[787200], &E[768], 128, ldz);
          }
        //  #pragma omp task firstprivate(rho,cutpnt)
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "It: " << it  << " mS-mE " << 896 << "-" << 1024 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            fillIndxq(&indxq[896],128);
            QRTD(&D[896], &Z[918400], &E[896], 128, ldz);
          }
        //  #pragma omp taskwait
          {
            #ifdef DEBUG_OMP
            #pragma omp critical
            cerr << "Reduce=> It: " << it  << " mS-mE " << 768 << "-" << 1024 << " Thread id: " << omp_get_thread_num() << endl;
            #endif
            TYPE * workDC = new TYPE[65536+4*256];
            if(int ret = ComputeEig(&D[768], &Z[787200], workDC, &indxq[768], rho, 128, 128, ldz)){
              cerr << "[Error] ComputeEig 768-1024 failed! ret error " << ret << endl;
            }
            delete [] workDC;
          }
        }
      //  #pragma omp taskwait
        {
          #ifdef DEBUG_OMP
          #pragma omp critical
          cerr << "Reduce=> It: " << it  << " mS-mE " << 512 << "-" << 1024 << " Thread id: " << omp_get_thread_num() << endl;
          #endif
          TYPE * workDC = new TYPE[262144+4*512];
          if(int ret = ComputeEig(&D[512], &Z[524800], workDC, &indxq[512], rho, 256, 256, ldz)){
            cerr << "[Error] ComputeEig 512-1024 failed! ret error " << ret << endl;
          }
          delete [] workDC;
        }
      }
  //    #pragma omp taskwait
    //}
  }
}

        #ifdef DEBUG_OMP
        #pragma omp critical
        cerr << "Reduce=> It: " << it  << " mS-mE " << 0 << "-" << 1024 << " Thread id: " << omp_get_thread_num() << endl;
        #endif
        TYPE * workDC = new TYPE[1048576+4*1024];
        if(int ret = ComputeEig(&D[0], &Z[0], workDC, &indxq[0], rho, 512, 512, ldz)){
          cerr << "[Error] ComputeEig 0-1024 failed! ret error " << ret << endl;
        }
        delete [] workDC;

#pragma @ICE endblock
        ///////////////

        mygettime(&t_core_end);
        double t_it_core = mydifftimems(&t_core_start, &t_core_end);

        delete [] indxq;

        //
        // Apply Q to find A eigenvectors.   - ormtr
        //
        lwork = -1;
        work = new TYPE[1];
        if(lapack_int ret = LAPACKE_dormtr_work(LAPACK_COL_MAJOR, side, uplo, trans, n, n, A, lda, tau, Z, ldc, work, lwork)) {
            cerr << "[Error] dormtr failed!" << endl;
            return ret;
        }

        lwork=work[0];
        delete [] work;
        work = new TYPE[lwork];

        if(lapack_int ret = LAPACKE_dormtr_work(LAPACK_COL_MAJOR, side, uplo, trans, n, n, A, lda, tau, Z, ldc, work, lwork)) {
            cerr << "[Error] dormtr II failed!" << endl;
            return ret;
        }
        mygettime(&t_end);
        t_it[it] = mydifftimems(&t_start, &t_end);
        ///////////////
#ifdef CHKRES
#ifdef DEBUG
        cerr << "Eigenvalues: " << endl ;
        writeresult("out", D, 1, n);
        cerr << "Eigenvectors:" << endl;
        writeresult("out", Z, n, n);
#endif

        // Eval just a portion of eigenvectors
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, n, neigeneval, n, 1.0, Aread, n, Z, n, 1.0, LHS, n);

#ifdef DEBUG
        cerr << "LHS:" << endl;
        writeresult("out", LHS, n, n);
#endif
        //cout << "Aread:" << endl;
        //writeresult("out", Aread, n, n);
        for(int i = 0; i < neigeneval; i++) {
            cblas_daxpy(n, D[i], &Z[i*n], 1, &RHS[i*n], 1);
        }

        //Transpose the RHS
        transpose(RHS, RHStransp, n, n);
#ifdef DEBUG
        cerr << "RHStransp:" << endl;
        writeresult("out", RHStransp, n, n);
#endif

        incorrect = allclose(LHS, RHStransp, n, n);
        if (incorrect) {
            cerr << "[Error] Incorret result!!";
            break;
        } else {
            //cout << " " << t_it[it] << " (" << t_it_core << ")";
            fprintf(stdout," %7.5lf ( %7.5lf )", t_it[it], t_it_core);
        }
#else
            fprintf(stdout," %7.5lf ( %7.5lf )", t_it[it], t_it_core);
#endif
    }
    cout << endl;

    delete [] t_it;
    delete [] RHS;
    delete [] RHStransp;
    delete [] LHS;
    delete [] Aread;
    delete [] A;
    delete [] Z;
    delete [] D;
    delete [] E;
    delete [] tau;
    delete [] work;

    return incorrect;
}
