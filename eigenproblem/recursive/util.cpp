#include "util.h"

void laed1_wrap(int *n, double *D, double *Q, int *ldq, int *indxq, double *rho, int *cutpnt, double *work, int *iwork, int *info) {
  dlaed1_(n, D, Q, ldq, indxq, rho, cutpnt, work, iwork, info);
}
void laed1_wrap(int *n, float *D, float *Q, int *ldq, int *indxq, float *rho, int *cutpnt, float *work, int *iwork, int *info) {
  slaed1_(n, D, Q, ldq, indxq, rho, cutpnt, work, iwork, info);
}

void lascl_wrap(char *type, int *kl, int *ku, float *cfrom, float *cto, int *m, int *n, float *a, int *lds, int *info) {
    slascl_(type, kl, ku, cfrom, cto, m, n, a, lds, info);
}
void lascl_wrap(char *type, int *kl, int *ku, double *cfrom, double *cto, int *m, int *n, double *a, int *lds, int *info) {
    dlascl_(type, kl, ku, cfrom, cto, m, n, a, lds, info);
}

float lanst_wrap(char *norm, int *n, float *d, float *e) {
    return slanst_(norm, n, d, e);
}
double lanst_wrap(char *norm, int *n, double *d, double *e) {
    return dlanst_(norm, n, d, e);
}

void laset_wrap(char *uplo, int *m, int *n, float *alpha, float *beta, float *a, int *lda) {
    slaset_(uplo, m, n, alpha, beta, a, lda);
}
void laset_wrap(char *uplo, int *m, int *n, double *alpha, double *beta, double *a, int *lda) {
    dlaset_(uplo, m, n, alpha, beta, a, lda);
}

int readfile(string inpname,  double * A, const int n) {
#ifdef DEBUG
    cout << "readfile..." << endl;
#endif
    std::ifstream inpstream;
    inpstream.open(inpname, ios::in);
    if(inpstream.fail()) {
        cerr << "[Error] Opening file " << inpname << " failed!" << endl;
        return 1;
    }

    if (inpstream.is_open()) {
#ifdef DEBUG
        cout << "file " << inpname<< " is open..." << endl;
#endif
        int idx = 0;
        string line;
        char delimiter = '\n';
        while (getline(inpstream, line, delimiter)) {
            stringstream ss(line);
            string token;
            while(getline(ss, token, ' ')){
                //cout <<"token: " << token<< endl;
                A[idx++] = (double) stoi(token);
                //cout << "token " << token << " ? "<< A[idx-1] << endl;
                //cout << "A[" << idx-1 << "]=" << A[idx-1] << endl;
            }
        }
#ifdef DEBUG
        cout << endl;
#endif
        inpstream.close();
    } else {
        cerr << "[Error] file " << inpname<< " could not be open!" << endl;
        return 1;
    }
#ifdef DEBUG
    cout << "readfile done." << endl;
#endif
    return 0;
}

int writeresult(string outname, double * V, const int n, const int m) 
{
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++ ) {
            cerr << V[i*m+j] << " ";
        }
        cerr << endl;
    }
    return 0;
}

int transpose(TYPE * in, TYPE * out, const int n, const int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++ ) {
            out[j*n+i] = in[i*m+j];
        }
    }
    return 0;
}

// Based on https://numpy.org/doc/1.18/reference/generated/numpy.allclose.html
int allclose(TYPE * A, TYPE *B, const int n, const int m, double rtol, double atol)
{
    int Aallzero = 1, Ballzero = 1;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++ ) {
            int idx = i*m+j;
            if (A[idx] != 0.0) Aallzero = 0;
            if (B[idx] != 0.0) Ballzero = 0;
            double diff = fabs(A[idx] - B[i*m+j]);
            double tolerance = atol + rtol*fabs(B[i*m+j]);
            if (diff > tolerance) {
                cerr << " A["<<idx<<"] " << A[idx] << " != B["<<idx<<"] " << B[idx] << " => " << diff << " > " << tolerance << endl;
                return 1;
            }
        }
    }
    if (Aallzero || Ballzero) {
        cerr << "[Error] Aallzero = " << Aallzero << " Ballzero = " << Ballzero << endl;
        return 1;
    } else {
        return 0;
    }
}

/*double rtclock()
{
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}*/


/* This is required for the leaf to fill up the indxq when using Divide and conquer. */
void fillIndxq(int *indxq, int n) {
    for(int i=0; i< n; i++) {
        indxq[i] = i+1;
    }
}

int ComputeEig(TYPE* eigval, TYPE* eigvec, TYPE *work, int *indxq, double rho, int n1, int n2, int ldq) {

    int info =0;
    int cutpnt = n1;
    int size = n1+n2;
    int *iwork = new int[4*size];

    laed1_wrap(&size, eigval, eigvec, &ldq, indxq, &rho, &cutpnt, work, iwork, &info);

    delete [] iwork;
    return info;
}

// In col major
// Assuming src[x1-x0][y1-y0]
void Copy2D(TYPE *dst, TYPE *src, int lddst, int x0, int y0, int x1, int y1) {
    int isrc = 0;
    for(int i=x0; i<x1; i++) {
        for(int j=y0; j<y1; j++) {
            dst[i*lddst+j] = src[isrc++];
        }
    }
}

void Copy1D(TYPE *dst, TYPE *src, int n)
{
    for(int i=0; i<n; i++) {
        dst[i] = src[i];
    }
}

// Input: Eigval[n] (diagonal elements of T),  
//        Ein[n-1]  (off-diagonal of T)
// Ouput: Eigval[n] (n eigenvalues) 
//        Eigvec[n,n] (n orthonormal eigenvectors)
//        Ein is overwriten
int QRTD(TYPE *eigval, TYPE *eigvec, TYPE *Ein,  int n, int ldz) {
    char compz = 'I';
    lapack_int lwork = 2*n-2, ret = 0;
    TYPE * work2 = new TYPE[std::max(1, lwork)];

    //if(lapack_int ret = LAPACKE_dsteqr_work(LAPACK_COL_MAJOR, compz, n, D, E, Z, ldz, work2)) {
    if((ret = LAPACKE_dsteqr_work(LAPACK_COL_MAJOR, compz, n, eigval, Ein, eigvec, ldz, work2))) {
        cerr << "[Error] dsteqr failed!" << endl;
        //return ret;
    }

    delete [] work2;
    return ret;
}

int Bisection(TYPE *D, TYPE *Z, TYPE *E, int n, int ldz) {
    char range = 'A', order = 'B';
    double vl = 0.0, vu = 0.0, abstol = 0.0;
    int m, nsplit, il=0, iu=0;
    int * iblock = new int[n];
    int * isplit = new int[n];
    int * ifail  = new int[n];
    int * iwork  = new int[3*n];
    TYPE * W = new TYPE[n];
    lapack_int ret = 0;

    TYPE *work = new TYPE[4*n];
    if((ret = LAPACKE_dstebz_work(range, order, n, vl, vu, il, iu, abstol, D, E, &m, &nsplit, W, iblock, isplit, work, iwork))) {
        cerr << "[Error] dstebz at Bisection failed! ret code: " << ret << endl;
        return ret;
    }
    delete [] work;

    work = new TYPE[5*n];
    delete [] iwork;
    iwork = new int[n];
    if((ret = LAPACKE_dstein_work(LAPACK_COL_MAJOR, n, D, E, m, W, iblock, isplit, Z, ldz, work, iwork, ifail))) {
        cerr << "[Error] dstein at Bisection failed! ret code: " << ret << endl;
        return ret;
    }

    for (int i = 0; i < n; i++ ) {
        D[i] = W[i];
    }

    delete [] W;
    delete [] iblock;
    delete [] isplit;
    delete [] ifail;
    delete [] work;
    delete [] iwork;

    return ret;
}


/* Lapacks version of solving the eigenproblem using divide and conquer.*/
int LapackDC(TYPE *D, TYPE *Z, TYPE *E, int n, int ldz) {
    lapack_int lwork = 1 + (4*n) + (n*n);
    TYPE * work = new TYPE[lwork];
    lapack_int liwork = 3+(5*n);
    lapack_int * iwork = new lapack_int[liwork];
    lapack_int ret = LAPACKE_dstedc_work(LAPACK_COL_MAJOR, 'I', n, D, E ,Z, ldz, 
            work, lwork, iwork, liwork);
    delete [] work;
    delete [] iwork;
    if(ret) {
        cerr << "[Error] dstedc failed! ret error " << ret  << endl;
    }
    return ret;
}

double mylanst(char *norm, int *n, double *d, double *e) {
    double anorm;
    if (*norm == 'M') {
        anorm = fabs(d[(*n)-1]);
        for(int i=0; i<(*n)-1; i++) {
            double sum = fabs(d[i]);
            if(anorm < sum) anorm = sum;
            sum = fabs(e[i]);
            if(anorm < sum) anorm = sum;
        }
    } else {
        cerr << "[Error] mylanst failed Norm not recognized." << endl;
        exit(1);
    }

    return anorm;
}



