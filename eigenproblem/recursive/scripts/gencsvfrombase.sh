#!/usr/bin/env bash

echo leg,comp,searchtool,run,len,time
for filep in "$@"; do
    if [ ! -f $filep ]; then
        echo $filep does not exist >&2
        continue
    fi
    echo "Processing $filep ..." >&2
    comp=`awk -F- '{print $2}' <<< $filep`
    label=`awk -F- '{print $4}' <<< $filep`
    stool="None"
    run=1

    echo "#" $filep
    grep "Eigproblem=" $filep | awk -v run=$run -v leg=$label -v cc=$comp -v stool=$stool \
                    '{for(i=6; i<6+$4; i++) {print leg","cc","stool","run","$2","$i; }}'


done
