#!/usr/bin/env bash

echo leg,comp,searchtool,run,len,time
for filep in "$@"; do
    if [ ! -f $filep ]; then
        echo $filep does not exist >&2
        continue
    fi
    echo "Processing $filep ..." >&2
    comp=`awk -F- '{print $2}' <<< $filep`
    label=`awk -F- '{print $4}' <<< $filep`
    #stool="None"
    #run=1

    csplit $filep /Searching/ {*} -f $filep -b "_%02d.log" -z -s

    echo "#" $filep
    for inp in ${filep}_*.log; do
        sline=`grep "Searching " $inp`
        stool=`echo $sline | cut -d" " -f 6`
        run=`echo $sline | cut -d" " -f 8`
        #len=`cat $sline | cut -d" " -f 3`

        prol="${label},${comp},${stool},${run}"
        grep "Eigproblem=" $inp | awk -v pr=$prol '{for (i=6; i<$4+6; i++) {print pr","$2","$i }}'

        rm -v $inp >&2
    done
done
