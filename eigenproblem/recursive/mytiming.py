import re, statistics

def getTimingBkp1(std, error):
    r = re.search('.*Time.*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    result = 'N/A'
    if m and len(m) >= 11:
        vals = []
        unit = m[6]
        unit = unit[unit.rfind('(')+1:unit.rfind(')')]
        numit = int(m[5])
        for it in m[9:9+(numit*4):4]: #range(9,7+4*numit:4):
            vals.append(float(it))
        ##
        metric = statistics.median(vals)
        result = {'metric': metric, 'evals': vals, 'unit': unit}
        #rddesult = float(m[6])
    else:
        result = {'metric': 'N/A'} #'N/A'
    #
    return result
#

def getTiming(std, error):
    r = re.search('.*Time.*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    #
    result = 'N/A'
    if m and len(m) >= 11:
        unit = m[6]
        unit = unit[unit.rfind('(')+1:unit.rfind(')')]
        numit = int(m[5])

        # Core clock
        corevals = []
        for it in m[9:9+(numit*4):4]:
            corevals.append(float(it))
        ##
        metric = statistics.median(corevals)

        # Total clock
        totalvals = []
        for it in m[7:7+(numit*4):4]:
            totalvals.append(float(it))
        ##

        # res: {'metric': 7.21292,
        #       'exps': [(12.41161, 7.48545), (10.53209, 7.21292),
        #                (10.50103, 7.21561), (10.49045, 7.20725),
        #                (10.55268, 7.19714)],
        #       'unit': ('milisec', 'milisec'),
        #       'desc': ('Total', 'Core')}
        result = {'metric': metric, 
                  'exps': tuple(zip(totalvals, corevals)), 
                  'unit': (unit,unit),
                  'desc': ('Total','Core')}
    else:
        result = {'metric': 'N/A'}
    #
    return result
###

def getTimingSOA(std, error):
    r = re.search('.*Time.*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    #
    result = 'N/A'
    if m and len(m) >= 11:
        dictexps = {}
        unit = m[6]
        unit = unit[unit.rfind('(')+1:unit.rfind(')')]
        numit = int(m[5])

        #
        # Saves the two timers in the result dictionary: Core and Total for each
        # experiment
        #

        # Core clock
        coredict = {}
        vals = []
        for it in m[9:9+(numit*4):4]:
            vals.append(float(it))
        ##
        metric = statistics.median(vals)

        coredict['unit'] = unit
        coredict['evals'] = vals
        dictexps['Core'] = coredict

        ######

        # Total clock
        totaldict = {}
        vals = []
        for it in m[7:7+(numit*4):4]:
            vals.append(float(it))
        ##

        totaldict['unit'] = unit
        totaldict['evals'] = vals
        dictexps['Total'] = totaldict

        ######

        result = {'metric': metric, 'exps': dictexps}

    else:
        result = {'metric': 'N/A'} #'N/A'
    #
    return result
###


