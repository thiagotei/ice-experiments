#ifndef __UTIL_H__
#define __UTIL_H__

#include <lapacke.h>
#include <cblas.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>
#include <extern.h>

#define TYPE double
using namespace std;

int readfile(string inpname,  double * A, const int n);
int writeresult(string outname, double * V, const int n, const int m);
int transpose(TYPE * in, TYPE * out, const int n, const int m);
int allclose(TYPE * A, TYPE *B, const int n, const int m, double rtol=1e-5, double atol=1e-8);
double rtclock();
int ComputeEig(TYPE* eigval, TYPE* eigvec, TYPE *work, TYPE * Q1, TYPE* Q2, double rho, int n1, int n2, int ldq);
int QRTD(TYPE *eigval, TYPE *eigvec, TYPE *Ein,  int n, int ldz);
void laed1_wrap(int *n, double *D, double *Q, int *ldq, int *indxq, double *rho, int *cutpnt, double *work, int *iwork, int *info);
void laed1_wrap(int *n, float *D, float *Q, int *ldq, int *indxq, float *rho, int *cutpnt, float *work, int *iwork, int *info);
void lascl_wrap(char *type, int *kl, int *ku, float *cfrom, float *cto, int *m, int *n, float *a, int *lds, int *info);
void lascl_wrap(char *type, int *kl, int *ku, double *cfrom, double *cto, int *m, int *n, double *a, int *lds, int *info);
float lanst_wrap(char *norm, int *n, float *d, float *e);
double lanst_wrap(char *norm, int *n, double *d, double *e);
double mylanst(char *norm, int *n, double *d, double *e);
void laset_wrap(char *uplo, int *m, int *n, float *alpha, float *beta, float *a, int *lda);
void laset_wrap(char *uplo, int *m, int *n, double *alpha, double *beta, double *a, int *lda);
int Bisection(TYPE *D, TYPE *Z, TYPE *E, int n, int ldz);
void laed0_wrap(int *icompq, int *qsiz, int *n, double *D, double *E, double *Q, int *ldq, double *qstore, int *ldqs, double *work, int *iwork, int *info);
void laed0_wrap(int *icompq, int *qsiz, int *n, float *D, float *E, float *Q, int *ldq, float *qstore, int *ldqs, float *work, int *iwork, int *info);

#endif
