#include <lapacke.h>
#include "util.h"

using namespace std;

int main(int argc, char** argv)
{
/*
 *  Calculate eigenproblem n x n symmetric matrix
 */
    int ninp = 0;
    int niterations = 5;
    string inpname = "N/A";

    if (argc >= 2) {
        ninp = std::stoi(argv[1]);
        inpname = argv[2];
        std::cout << "Symmetric Eigenproblem n: " << ninp << " inp: " << inpname << std::endl;
        //#inpfile.open();
    } else {
        std::cerr << "Usage: ./<bin> <dim> <file path>" << endl;
        return 1;
    }

    lapack_int n=ninp, lda=n, ldc=n, ldz=n, lwork;
    TYPE * Aread = new TYPE[n*n];
    TYPE * A = new TYPE[n*n];
    TYPE * Z = new TYPE[n*n];
    TYPE * D = new TYPE[n];
    TYPE * E = new TYPE[n-1];
    TYPE * tau = new TYPE[n]; //new TYPE[n-1];
    TYPE * work = new TYPE[1];
    char uplo = 'U';
    char trans = 'N';
    char compz = 'I';
    char side = 'L';

    double t_start, t_end;
    double * t_it = new double[niterations];
    int incorrect = false;
    TYPE* LHS= new TYPE[n*n];
    TYPE* RHS= new TYPE[n*n]; 
    TYPE* RHStransp = new TYPE[n*n]; 

    if(int ret = readfile(inpname, Aread, ninp)) return ret;

#ifdef DEBUG
    cout << "Aread:" << endl;
    writeresult("out", Aread, n, n);
#endif
#ifdef DEBUG
    cout << "Starting lapack..." << endl;
#endif
    cout << "Running QRTD LAPACKE_dsteqr_work" << endl;
    cout << "Eigproblem= "<< n << " Iterations= " << niterations << " Time(milisec)=";
    for (int it = 0; it < niterations; it++) {

        int index = 0;
        // Pass the data in column major
        for(int j=0; j < n; j++) {
            for(int i=0; i < n; i++ ) {
                //cout << "A[" << index << "] = " << Aread[i*n+j]  << endl;
                A[index++] = Aread[i*n+j];
            }
        }

        t_start = rtclock();
        lwork = -1;
        if(lapack_int ret = LAPACKE_dsytrd_work(LAPACK_COL_MAJOR, uplo, n, A, lda, D, E, tau, work, lwork)) {
            cerr << "[Error] dsytrd failed!" << endl;
            return ret;
        }

        lwork=work[0];
        delete [] work;

        work = new TYPE[lwork];

        if(lapack_int ret = LAPACKE_dsytrd_work(LAPACK_COL_MAJOR, uplo, n, A, lda, D, E, tau, work, lwork)) {
            cerr << "[Error] dsytrd II failed!" << endl;
            return ret;
        }

        delete [] work;

        lwork=2*n-2;
        TYPE * work2 = new TYPE[std::max(1, lwork)];
        if(lapack_int ret = LAPACKE_dsteqr_work(LAPACK_COL_MAJOR, compz, n, D, E, Z, ldz, work2)) {
            cerr << "[Error] dsteqr failed!" << endl;
            return ret;
        }
        delete [] work2;

        //ormtr
        lwork = -1;
        work = new TYPE[1];
        if(lapack_int ret = LAPACKE_dormtr_work(LAPACK_COL_MAJOR, side, uplo, trans, n, n, A, lda, tau, Z, ldc, work, lwork)) {
            cerr << "[Error] dormtr failed!" << endl;
            return ret;
        }

        lwork=work[0];
        delete [] work;
        work = new TYPE[lwork];

        if(lapack_int ret = LAPACKE_dormtr_work(LAPACK_COL_MAJOR, side, uplo, trans, n, n, A, lda, tau, Z, ldc, work, lwork)) {
            cerr << "[Error] dormtr II failed!" << endl;
            return ret;
        }
        t_end = rtclock();
        t_it[it] = t_end - t_start;

#ifdef DEBUG
        cerr << "Eigenvalues: " << endl ;
        writeresult("out", D, 1, n);
        cerr << "Eigenvectors:" << endl;
        writeresult("out", Z, n, n);
#endif

        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, n, n, n, 1.0, Aread, n, Z, n, 1.0, LHS, n);

#ifdef DEBUG
        cerr << "LHS:" << endl;
        writeresult("out", LHS, n, n);
#endif
        //cout << "Aread:" << endl;
        //writeresult("out", Aread, n, n);
        for(int i = 0; i < n; i++) {
            cblas_daxpy(n, D[i], &Z[i*n], 1, &RHS[i*n], 1);
        }

        //Transpose the RHS
        transpose(RHS, RHStransp, n, n);
#ifdef DEBUG
        cout << "RHStransp:" << endl;
        writeresult("out", RHStransp, n, n);
#endif

        incorrect = allclose(LHS, RHStransp, n, n);
        if (incorrect) {
            cerr << "[Error] Incorret result!!";
            break;
        } else {
            cout << " " << t_it[it]*1.0e3;
        }
    }
    cout << endl;

    delete [] t_it;
    delete [] RHS;
    delete [] RHStransp;
    delete [] LHS;
    delete [] Aread;
    delete [] A;
    delete [] Z;
    delete [] D;
    delete [] E;
    delete [] tau;
    delete [] work;

    return incorrect;
}
