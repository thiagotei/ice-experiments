#include <lapacke.h>
#include <iostream>
#include <fstream>
#include <string>

#define TYPE double

using namespace std;



int readfile(string inpname,  double * A, const int n) {
    cout << "readfile..." << endl;
    std::ifstream inpstream;
    inpstream.open(inpname, ios::in);
    if(inpstream.fail()) {
        cerr << "Opening file " << inpname << " failed!" << endl;
        return 1;
    }

    if (inpstream.is_open()) {
        cout << "file " << inpname<< " is open..." << endl;
        int idx = 0;
        string line;
        char delimiter = '\n';
        while (getline(inpstream, line, delimiter)) {
            stringstream ss(line);
            string token;
            while(getline(ss, token, ' ')){
                //cout <<"token: " << token<< endl;
                A[idx++] = stoi(token);
                //cout << "token " << token << " ? "<< A[idx-1] << endl;
                //cout << "A[" << idx-1 << "]=" << A[idx-1] << endl;
            }
        }
        cout << endl;
        inpstream.close();
    }
    cout << "readfile done." << endl;
    return 0;
}

int writeresult(string outname, double * V, const int n) 
{
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++ ) {
            cout << V[i*n+j] << " ";
        }
        cout << endl;
    }
    return 0;
}

//int convtoColMajor(TYPE * A)

int main(int argc, char** argv)
{
/*
 *  Calculate eigenproblem n x n symmetric matrix
 */
    int ninp = 0;
    string inpname = "N/A";

    if (argc >= 2) {
        ninp = std::stoi(argv[1]);
        inpname = argv[2];
        std::cout << "Symmetric Eigenproblem n: " << ninp << " inp: " << inpname << std::endl;
        //#inpfile.open();
    } else {
        std::cerr << "Usage: ./<bin> <dim> <file path>" << endl;
        return 1;
    }

    lapack_int n=ninp, lda=n, ldc=n, ldz=n, lwork;
    TYPE * Aread = new TYPE[n*n];
    TYPE * A = new TYPE[n*n];
    TYPE * Z = new TYPE[n*n];
    TYPE * D = new TYPE[n];
    TYPE * E = new TYPE[n-1];
    TYPE * tau = new TYPE[n]; //new TYPE[n-1];
    TYPE * work = new TYPE[1];
    char uplo = 'U';
    char trans = 'N';
    char compz = 'I';
    char side = 'L';

    int ret = 0;
    if( (ret = readfile(inpname, A, ninp))) return ret;

/*    int index = 0;
    for(int j=0; j < n; j++) {
        for(int i=0; i < n; i++ ) {
            cout << "A[" << index << "] = " << Aread[i*n+j]  << endl;
            A[index++] = Aread[i*n+j];
        }
    }
*/
    cout << "Starting lapack..." << endl;

    lwork = -1;
    lapack_int ret1 = LAPACKE_dsytrd_work(LAPACK_ROW_MAJOR, uplo, n, A, lda, D, E, tau, work, lwork);

    lwork=work[0];
    delete [] work;

    work = new TYPE[lwork];

    lapack_int ret2 = LAPACKE_dsytrd_work(LAPACK_ROW_MAJOR, uplo, n, A, lda, D, E, tau, work, lwork);

    delete [] work;

    lwork=2*n-2;
    TYPE * work2 = new TYPE[std::max(1, lwork)];
    lapack_int ret3 = LAPACKE_dsteqr_work(LAPACK_ROW_MAJOR, compz, n, D, E, Z, ldz, work2);
    delete [] work2;

    //ormtr
    lwork = -1;
    work = new TYPE[1];
    lapack_int ret4 = LAPACKE_dormtr_work(LAPACK_ROW_MAJOR, side, uplo, trans, n, n, A, lda, tau, Z, ldc, work, lwork);

    lwork=work[0];
    delete [] work;
    work = new TYPE[lwork];

    lapack_int ret5 = LAPACKE_dormtr_work(LAPACK_ROW_MAJOR, side, uplo, trans, n, n, A, lda, tau, Z, ldc, work, lwork);
    //LAPACKE_dormtr_worm

    writeresult("out", Z, n);

    delete [] A;
    delete [] Z;
    delete [] D;
    delete [] E;
    delete [] tau;
    delete [] work;

    return 0;
}
