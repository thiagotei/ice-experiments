Compilation
==========
make CC=xlc affinity-test1

Execution
=========
You can see the affinity difference using different flags (power9 with 40 cores, 160 threads):
OMP_NUM_THREADS=20 OMP_PLACES=cores OMP_PROC_BIND=close ./affinity-test1 |sort -n -k 7
OMP_NUM_THREADS=20 OMP_PLACES=cores OMP_PROC_BIND=spread ./affinity-test1 |sort -n -k 7


source: http://pages.tacc.utexas.edu/~eijkhout/pcse/html/omp-affinity.html

Power 9 (2 packages x 20 cores/pkg x 4 threads/core (40 total cores), p9.ncsa.illinois.edu)
=======
To run one thread per core and use only the 1st socket:
OMP_NUM_THREADS=20 OMP_PLACES=cores OMP_PROC_BIND=close ./affinity-test1 |sort -n -k 7

Intel (gienah 2 packages x 10 cores/pkg x 2 threads/core (20 total cores))
======
I had to run icc with export KMP_AFFINITY=VERBOSE to find out how the cores and threads would play out.

Running lscpu I would get this:
$> lscpu
NUMA node0 CPU(s):     0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38
NUMA node1 CPU(s):     1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39


Running the program I would this:
$>OMP_NUM_THREADS=10 OMP_PLACES=cores OMP_PROC_BIND=close ./affinity-test1  |sort -n -k 7

OMP: Info #171: KMP_AFFINITY: OS proc 0 maps to package 0 core 0 thread 0 
OMP: Info #171: KMP_AFFINITY: OS proc 20 maps to package 0 core 0 thread 1 
OMP: Info #171: KMP_AFFINITY: OS proc 2 maps to package 0 core 1 thread 0 
OMP: Info #171: KMP_AFFINITY: OS proc 22 maps to package 0 core 1 thread 1 
OMP: Info #171: KMP_AFFINITY: OS proc 4 maps to package 0 core 2 thread 0 
OMP: Info #171: KMP_AFFINITY: OS proc 24 maps to package 0 core 2 thread 1 
OMP: Info #171: KMP_AFFINITY: OS proc 6 maps to package 0 core 3 thread 0 
OMP: Info #171: KMP_AFFINITY: OS proc 26 maps to package 0 core 3 thread 1 

So, the hyperthread falling on the same core is through virtual core 0 and 20, 2 and 22...
I thought it was 0 and 2, 4 and 6.
Therefore to one thread per core and use only the 1st socket:
OMP_NUM_THREADS=10 OMP_PLACES=cores OMP_PROC_BIND=close
