#!/usr/bin/env python

import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pydot import Dot, Edge, Node

parser = argparse.ArgumentParser()
#parser.add_argument('-i', type=int, required=True, help="Number of elements")
parser.add_argument('-i', action='store', dest='inpvals',
                            type=int, nargs='*', default=[128],
                            help="Number of elements for each execution. Examples: -i item1 item2, -i item3")
parser.add_argument('-s', type=int, required=False, 
                    default=32, help="Stop value")
parser.add_argument('-o', type=str, required=False,
                    default="myfig.png",help="Name of the")
parser.add_argument('-f', action='store_true', required=False, default=False,
                    help="use to generate the figure")
parser.add_argument('-p', action='store_true', required=False, default=False,
                    help="Plot all functions")
parser.add_argument('--ft', action='store_true', required=False, default=False,
                    help="Run transp func")
parser.add_argument('--fg', action='store_true', required=False, default=False,
                    help="Run gemm func")
args=parser.parse_args()

mstop=args.s
nstop=mstop
kstop=mstop
melem = args.inpvals[0] #512 
nelem = melem
kelem = melem

numnodes=0
def recFunc(mS, mE, nS, nE, parid, lvl, g, nodesdone, edgesdone):
    global numnodes
    nlvl = lvl + 1
    mrng=mE-mS
    nrng=nE-nS
    #if mrng > mstop and nrng > nstop:
    if mrng > mstop and nrng > nstop:
        pivm = mS+((mrng)//2)
        pivn = nS+((nrng)//2)
        spaces = '  '*lvl
        if pivm-mS > mstop:
            #print(f"{spaces} A) Gen node M={mS},{mE} N={nS},{nE}")
            nodeid = parid+"SpM"+str(mrng) #str(nlvl)
            #lbl=f"M={mS},{pivm} N={nS},{nE}\nM={pivm},{mE} N={nS},{nE}"
            lbl=f"M/{melem//(mrng//2)}"
            if nodeid not in nodesdone:
                #g.add_node(Node(nodeid, label=lbl))
                if args.f:
                    g.add_node(Node(nodeid, label=lbl, shape='rect'))
                #
                numnodes += 1
                #print(f"{nodeid}) {numnodes} {lbl}\n")
                nodesdone[nodeid] = []
            #
            nodesdone[nodeid].append(lbl)
            if nodeid not in edgesdone:
                if args.f:
                    g.add_edge(Edge(parid, nodeid, label="Divide M"))
                edgesdone[nodeid] = True
            #
            recFunc(mS, pivm, nS, nE, nodeid, nlvl, g, nodesdone, edgesdone)
            recFunc(pivm, mE, nS, nE, nodeid, nlvl, g, nodesdone, edgesdone)
        #

        if pivn-nS > nstop:
            #print(f"{spaces} B) Gen node M={mS},{mE} N={nS},{nE}")
            nodeid = parid+"SpN"+str(nrng) #str(nlvl)
            #g.add_node(Node(nodeid, label=str(nodeid)))
            #lbl=f"M={mS},{mE} N={nS},{pivn}\nM={mS},{mE} N={pivn},{nE}"
            lbl=f"N/{nelem//(nrng//2)}"
            if nodeid not in nodesdone:
                #g.add_node(Node(nodeid, label=lbl))
                if args.f:
                    g.add_node(Node(nodeid, label=lbl, shape='rect'))
                #
                numnodes += 1
                #print(f"{nodeid}) {numnodes} {lbl}\n")
                nodesdone[nodeid] = []
            #
            nodesdone[nodeid].append(lbl)
            if nodeid not in edgesdone:
                if args.f:
                    g.add_edge(Edge(parid, nodeid, label="Divide N"))
                edgesdone[nodeid] = True
            #
            recFunc(mS, mE, nS, pivn, nodeid, nlvl, g, nodesdone, edgesdone)
            recFunc(mS, mE, pivn, nE, nodeid, nlvl, g, nodesdone, edgesdone)
        #
    #
###

def recFuncGemm(mS, mE, nS, nE, kS, kE, parid, lvl, g, nodesdone, edgesdone):
    global numnodes
    nlvl = lvl + 1
    mrng=mE-mS
    nrng=nE-nS
    krng=kE-kS
    #if mrng > mstop and nrng > nstop:
    if mrng > mstop and nrng > nstop:
        pivm = mS+((mrng)//2)
        pivn = nS+((nrng)//2)
        pivk = kS+((krng)//2)
        spaces = '  '*lvl
        if pivm-mS > mstop:
            #print(f"{spaces} A) Gen node M={mS},{mE} N={nS},{nE}")
            nodeid = parid+"SpM"+str(mrng) #str(nlvl)
            #lbl=f"M={mS},{pivm} N={nS},{nE}\nM={pivm},{mE} N={nS},{nE}"
            lbl=f"M/{melem//(mrng//2)}"
            if nodeid not in nodesdone:
                #g.add_node(Node(nodeid, label=lbl))
                if args.f:
                    g.add_node(Node(nodeid, label=lbl, shape='rect'))
                #
                numnodes += 1
                #print(f"{nodeid}) {numnodes} {lbl}\n")
                nodesdone[nodeid] = []
            #
            nodesdone[nodeid].append(lbl)
            if nodeid not in edgesdone:
                if args.f:
                    g.add_edge(Edge(parid, nodeid, label="Divide M"))
                edgesdone[nodeid] = True
            #
            recFuncGemm(mS, pivm, nS, nE, kS, kE, nodeid, nlvl, g, nodesdone, edgesdone)
            recFuncGemm(pivm, mE, nS, nE, kS, kE, nodeid, nlvl, g, nodesdone, edgesdone)
        #

        if pivn-nS > nstop:
            #print(f"{spaces} B) Gen node M={mS},{mE} N={nS},{nE}")
            nodeid = parid+"SpN"+str(nrng) #str(nlvl)
            #g.add_node(Node(nodeid, label=str(nodeid)))
            #lbl=f"M={mS},{mE} N={nS},{pivn}\nM={mS},{mE} N={pivn},{nE}"
            lbl=f"N/{nelem//(nrng//2)}"
            if nodeid not in nodesdone:
                #g.add_node(Node(nodeid, label=lbl))
                if args.f:
                    g.add_node(Node(nodeid, label=lbl, shape='rect'))
                #
                numnodes += 1
                #print(f"{nodeid}) {numnodes} {lbl}\n")
                nodesdone[nodeid] = []
            #
            nodesdone[nodeid].append(lbl)
            if nodeid not in edgesdone:
                if args.f:
                    g.add_edge(Edge(parid, nodeid, label="Divide N"))
                edgesdone[nodeid] = True
            #
            recFuncGemm(mS, mE, nS, pivn, kS, kE, nodeid, nlvl, g, nodesdone, edgesdone)
            recFuncGemm(mS, mE, pivn, nE, kS, kE, nodeid, nlvl, g, nodesdone, edgesdone)
        #

        if pivk-kS > kstop:
            #print(f"{spaces} B) Gen node M={mS},{mE} N={nS},{nE}")
            nodeid = parid+"SpK"+str(krng) #str(nlvl)
            #g.add_node(Node(nodeid, label=str(nodeid)))
            #lbl=f"M={mS},{mE} N={nS},{pivn}\nM={mS},{mE} N={pivn},{nE}"
            lbl=f"K/{kelem//(krng//2)}"
            if nodeid not in nodesdone:
                #g.add_node(Node(nodeid, label=lbl))
                if args.f:
                    g.add_node(Node(nodeid, label=lbl, shape='rect'))
                #
                numnodes += 1
                #print(f"{nodeid}) {numnodes} {lbl}\n")
                nodesdone[nodeid] = []
            #
            nodesdone[nodeid].append(lbl)
            if nodeid not in edgesdone:
                if args.f:
                    g.add_edge(Edge(parid, nodeid, label="Divide K"))
                edgesdone[nodeid] = True
            #
            recFuncGemm(mS, mE, nS, nE, kS, pivk, nodeid, nlvl, g, nodesdone, edgesdone)
            recFuncGemm(mS, mE, nS, nE, pivk, kE, nodeid, nlvl, g, nodesdone, edgesdone)
        #

    #
###


def recFunc_2(mS, mE, nS, nE, parid, suf, lvl):
    nlvl = lvl + 1
    melem=mE-mS
    nelem=nE-nS
    spaces = '  '*lvl

    if melem > mstop and nelem > nstop:
        #print(f"{spaces} Gen node M={mS},{mE} N={nS},{nE}")
        nodeid = parid+suf #str(nlvl)
        nodelb = f"M={mS},{mE} N={nS},{nE}"
        g.add_node(Node(nodeid, label=nodelb))
        if parid != "0":
            g.add_edge(Edge(parid, nodeid, label=suf))
        #

        pivm = mS+((melem)//2)
        #print(f"{spaces} A) Gen node M={mS},{mE} N={nS},{nE}")
        #nodeid = parid+"SpM"+str(melem) #str(nlvl)
        #g.add_node(Node(nodeid, label=str(nodeid)))
        #g.add_edge(Edge(parid, nodeid, label="Divide M"))
        recFunc_2(mS, pivm, nS, nE, nodeid, "SpML", nlvl)
        recFunc_2(pivm, mE, nS, nE, nodeid, "SpMR", nlvl)

        pivn = nS+((nelem)//2)
        #print(f"{spaces} B) Gen node M={mS},{mE} N={nS},{nE}")
        #nodeid = parid+"SpN"+str(nelem) #str(nlvl)
        #g.add_node(Node(nodeid, label=str(nodeid)))
        #g.add_node(Node(nodeid, label=f"M={mS},{mE} N={nS},{nE}"))
        #g.add_edge(Edge(parid, nodeid, label="Divide N"))
        recFunc_2(mS, mE, nS, pivn, nodeid, "SpNL", nlvl)
        recFunc_2(mS, mE, pivn, nE, nodeid, "SpNR", nlvl)
    else:
        print(f"Leaf: {lvl}")
    #
###

def driverFuncTransp():
    #title="Recursive Space - Base case minimum"+str(args.i)+" stop= "+str(args.s)
    global numnodes
    nodeid="0"
    g = None
    if args.f:
        #title="Recursive Space - Base case minimum = "+str(args.s) + " Elements"
        title=""
        g = Dot(graph_type='digraph', label=title, labelloc='t')
        lbl= f"M=0,{melem} N=0,{nelem}"
        g.add_node(Node(nodeid, label=lbl, shape='rect'))
    #
    numnodes=0
    nodesdone= {}
    edgesdone= {}
    numnodes += 1
    recFunc(0, melem, 0, nelem, nodeid, 0, g, nodesdone, edgesdone)

    #print(f"Nodes: {nodesdone}")
    #[print(k,len(v),":",v) for k,v in nodesdone.items()]
    #g.write_png("Fun1tree_"+str(melem)+"_"+str(mstop)+".png")
    figname=args.o
    if args.f:
        g.write_png(args.o)
        print(f"Saved in file {figname}")
    #
    print(f"Num nodes transp: {melem} stop: {args.s} maxlvls: {((melem//args.s)-1)*2} nodes: {numnodes}")

###

def driverFuncGemm():
    #title="Recursive Space - Base case minimum"+str(args.i)+" stop= "+str(args.s)
    global numnodes
    nodeid="0"
    g = None
    if args.f:
        #title="Recursive Space - Base case minimum = "+str(args.s) + " Elements"
        title=""
        g = Dot(graph_type='digraph', label=title, labelloc='t')
        lbl= f"M=0,{melem} N=0,{nelem} K=0,{kelem}"
        g.add_node(Node(nodeid, label=lbl, shape='rect'))
    #
    numnodes=0
    nodesdone= {}
    edgesdone= {}
    numnodes += 1
    recFuncGemm(0, melem, 0, nelem, 0, kelem, nodeid, 0, g, nodesdone, edgesdone)

    #print(f"Nodes: {nodesdone}")
    #[print(k,len(v),":",v) for k,v in nodesdone.items()]
    #g.write_png("Fun1tree_"+str(melem)+"_"+str(mstop)+".png")
    figname=args.o
    if args.f:
        g.write_png(args.o)
        print(f"Saved in file {figname}")
    #
    print(f"Num nodes Gemm: {melem} stop: {args.s} maxlvls: {((melem//args.s)-1)*3} nodes: {numnodes}")
    return numnodes
###

genFun = args.ft #True
genFunGemm=args.fg  #True #False
genFun2 = False
plot1 = args.p

if genFun:
    for v in args.inpvals:
        melem = v
        nelem = melem
        kelem = melem
        driverFuncTransp()
    #
#
if genFunGemm:
    for v in args.inpvals:
        melem = v
        nelem = melem
        kelem = melem
        driverFuncGemm()
#

if genFun2:
    title="Recursive Space"
    g = Dot(graph_type='digraph', label=title, labelloc='t')
    nodeid= "0"
    recFunc_2(0, melem, 0, nelem, nodeid, "N/A",  0)
    figname = args.o #"tree_"+str(melem)+"_"+str(mstop)+".png"
    g.write_png(figname)
    print(f"Saved in file {figname}")
#

if plot1:
    nngemm = []
    nntransp = []
    for v in args.inpvals:
        melem = v
        nelem = melem
        kelem = melem
        driverFuncTransp()
        nngemm.append(numnodes)
        melem = v
        nelem = melem
        kelem = melem
        driverFuncGemm()
        nntransp.append(numnodes)
    ##
    print(f"X: {len(args.inpvals)} Y Gemm: {len(nngemm)} {nngemm} Y Transp: {len(nntransp)} {nntransp}")
    plt.plot(args.inpvals, nngemm, label="Gemm", marker='.')
    plt.plot(args.inpvals, nntransp, label="Transp", marker='x')
    plt.xlabel("N")
    plt.legend(loc='upper left')
    plt.ylabel("Number of tree nodes")
    plt.savefig('chart.png')
    plt.show()
#

