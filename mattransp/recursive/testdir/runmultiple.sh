#!/usr/bin/env bash


mach="thunder"
prob="transp"
xinfo="none"
complst="clang++-8,clang-8  icpc,icc g++,gcc"
strat="rchoice-vert-rec" #"rchoice-vert-vec"
runscript="./runsearch-rec.sh"  #"./runsearch-symmetrical.sh"
for comps in $complst; do 
    echo $comps
    cxxcomp=${comps%,*}
    cccomp=${comps#*,}
    export ICELOCUS_CXX=${cxxcomp} #g++ #clang++-8  #icpc #
    export ICELOCUS_CC=${cccomp}  # gcc #clang-8 #icc 
    datev=`date +%Y%m%d%H%M%S`
    echo Running $ICELOCUS_CXX $ICELOCUS_CC $datev
    ##./runsearch-symmetrical.sh &> thunder_icc18_trcheobliv_regtile4_`date +%Y%m%d%H%M`.txt
    $runscript &> ${mach}_${cccomp}_${prob}_${strat}_${xinfo}_${datev}.txt
done
