#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
//#include <sys/time.h>
#include <float.h>
#include <mysecond.h>
#include "variants.h"

#define IF_TIME(foo) foo;

void init_array(MATTYPE A)
{
    int i, j;

    for (i=0; i<M; i++) {
        for (j=0; j<N; j++) {
            //A[i][j] = (i + j);
            //A[ind(i,j,N)] = (i * j);
            A[ind(i,j,N)] = ind(i,j,N);
            //fprintf(stdout, "%lf ",A[ind(i,j,N)]);
        }
        //fprintf(stdout, "\n");
    }
}

void print_array(MATTYPE C)
{
    int i, j;
    char bufname[32];
    snprintf(bufname, BUFNAMESIZE, "answer-mattransp-%d-%d.txt",N, M);
    FILE *fout = fopen(bufname,"w");
    if(!fout){
          fprintf(stderr,"ERROR! Could not open %s to output results!\n", bufname);
          exit(1);
    }

    for (i=0; i<N; i++) {
        for (j=0; j<M; j++) {
            fprintf(fout, "%lf ", C[ind(i,j,M)]);
            if (j%80 == 79) fprintf(fout, "\n");
        }
        fprintf(fout, "\n");
    }

    fclose(fout);
}

/*double rtclock()
{
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}*/


int main()
{
    int iterations = 10;
    double * t_it = (double *) malloc(iterations * sizeof(double));
    if (!t_it){ printf("Could not allocate t_it.\n"); exit(1);}

    struct timespec t_start, t_end;
    //double min=DBL_MAX, max=DBL_MIN, avg, t_all = 0.0, t_it;
#ifdef ALIGN_MALLOC
    double *A = (double *) aligned_alloc(ALIGNFACTOR, M * N * sizeof(double));
    double *B = (double *) aligned_alloc(ALIGNFACTOR, N * M * sizeof(double));
    printf("Using aligned_alloc!\n");
#else
    double *A = (double *) malloc(M * N * sizeof(double));
    double *B = (double *) malloc(N * M * sizeof(double));
    printf("Using regular malloc!\n");
#endif
    if (!A){ printf("Could not allocate A.\n"); exit(1);}
    if (!B){ printf("Could not allocate B.\n"); exit(1);}

    init_array(A);

#ifdef _OPENMP
    printf("M=%d, N=%d, numthreads=%d\n", M, N, omp_get_max_threads());
#else
    printf("M=%d, N=%d, K=%d, numthreads=NoOMP\n", M, N, K);
#endif

    for (int it = 0; it < iterations; it++) {
        //IF_TIME(t_start = rtclock());
        IF_TIME(mygettime(&t_start));

#pragma @ICE block=transp
        naiveIJ(0, M, 0, N, M, N, A, B);
        //naiveIJRegTileI8(0, M, 0, N, M, N, A, B);
#pragma @ICE endblock

        //IF_TIME(t_end = rtclock());
        IF_TIME(mygettime(&t_end));
        //IF_TIME(t_it = t_end - t_start);
        IF_TIME(t_it[it] = mydifftimems(&t_start, &t_end));

        //IF_TIME(t_it[it] = )
        //IF_TIME(if (t_it > 0 && t_it < min) min = t_it);
        //IF_TIME(if (t_it > 0 && t_it > max) max = t_it);
        //IF_TIME(t_all += t_it);

        if (it== 0 && fopen(".test", "r")) {
#ifdef MPI
            if (my_rank == 0) {
                print_array(B);
            }
#else
            print_array(B);
#endif
       }
    }
    //IF_TIME(avg = t_all/iterations);

    //IF_TIME(printf("Matrixtransp= %d %d | Time(ms) min= %7.5lf max= %7.5lf avg= %7.5lf | Iterations %d\n", M, N, min*1.0e3, max*1.0e3, avg*1.0e3, iterations));
    IF_TIME(printf("Matrixtransp= %d %d | Iterations= %d Time(milisec) ", M, N, iterations));
    for (int it = 0; it < iterations; it++) {
        IF_TIME(printf("%7.5lf ", t_it[it]));
    }
    IF_TIME(printf("\n"));

#ifdef PERFCTR
    PERF_EXIT; 
#endif

  free(t_it);
  free(A);
  free(B);
  return 0;
}
