#!/usr/bin/env bash

#grep "Matrixsize"  $1| sort -k2,2n  -k8,8n | uniq -w 20

# loop over input files

echo leg,comp,searchtool,run,matshape,min,max,avg
for fileinp in "$@"; do
    if [ ! -f $fileinp ]; then
        echo "$fileinp does not exisit!" >&2
        continue
    fi
    echo "Processing $fileinp ..." >&2
    comp=`awk -F_ '{print $2}' <<< $fileinp`
    label=`awk -F_ '{print $4}' <<< $fileinp`
    stool="None"
    run=1

    # sort by Shape (2,2) and min (7,7) 
    grep "Matrixtransp" $fileinp | awk '$2 == $3 {print $0}' | sort -k2,2n -k7,7n | uniq -w 20 | \
        awk -v run=$run -v leg=$label -v cc=$comp -v stool=$stool  \
        '{print leg","cc","stool","run","$2","$7","$9","$11}'
done
