#!/usr/bin/env bash

# parse the logs to get a chart on performance of cache oblivious according to the number of levels

echo leg,comp,searchtool,run,matshape,min,lvls
for fileinp in "$@"; do
    if [ ! -f $fileinp ]; then
        echo "$fileinp does not exisit!" >&2
        continue
    fi
    echo "Processing $fileinp ..." >&2
    comp=`awk -F_ '{print $2}' <<< $fileinp`
    label=`awk -F_ '{print $4}' <<< $fileinp`

    csplit $fileinp /Searching/ {*} -f $fileinp -b "_%02d.log" -z -s

    echo "#"$fileinp

    for inp  in ${fileinp}_*.log; do
        # printing stool ($? or $10) run ($? or $12) shape ($? or $5) min ($21) lvls ($35)
        preline=`grep "Searching " $inp | awk -v leg=$label -v cc=$comp '{printf("%s,%s,",leg,cc)} NF<=16{print $6","$8","$4} NF>=17{print $10","$12","$5}' | tr '\n' ','`

        #grep "Variant result" $inp | sort -k2,2n -k8,8n | uniq -w 20 | awk '{print $2","$8","$10","$12}'
        grep "Variant result" $inp |  awk -v pre=$preline '{print pre$21","substr($35, 1, length($35)-1)}'
        rm -v $inp >&2
    done
done

