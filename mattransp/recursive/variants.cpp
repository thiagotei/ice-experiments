#include "variants.h"

/////////////////////
//
// I-J-K
//
/////////////////////

void naiveIJ(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC lenM, ITERC lenN,
	   MATTYPE A, MATTYPE B){

ITER i, j;

#pragma @ICE loop=kernelIJ
for(i = mS; i < mE; i++) {
  for(j = nS; j < nE; j++) {
    B[ind(j,i,lenM)] = A[ind(i,j,lenN)];
  }
}
#pragma @ICE endloop
}

void naiveIJRegTileI4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC lenM, ITERC lenN,
	   MATTYPE A, MATTYPE B){

ITER i, j;

#pragma @ICE loop=kernelIJI4
for(i = mS; i < mE; i+=4) {
  for(j = nS; j < nE; j++) {
    B[ind(j,i,lenM)] = A[ind(i,j,lenN)];
    B[ind(j,i+1,lenM)] = A[ind(i+1,j,lenN)];
    B[ind(j,i+2,lenM)] = A[ind(i+2,j,lenN)];
    B[ind(j,i+3,lenM)] = A[ind(i+3,j,lenN)];
  }
}
#pragma @ICE endloop
}

void naiveIJRegTileI8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC lenM, ITERC lenN,
	   MATTYPE A, MATTYPE B){

ITER i, j;

#pragma @ICE loop=kernelIJI8
for(i = mS; i < mE; i+=8) {
  for(j = nS; j < nE; j++) {
    B[ind(j,i,lenM)] = A[ind(i,j,lenN)];
    B[ind(j,i+1,lenM)] = A[ind(i+1,j,lenN)];
    B[ind(j,i+2,lenM)] = A[ind(i+2,j,lenN)];
    B[ind(j,i+3,lenM)] = A[ind(i+3,j,lenN)];
    B[ind(j,i+4,lenM)] = A[ind(i+4,j,lenN)];
    B[ind(j,i+5,lenM)] = A[ind(i+5,j,lenN)];
    B[ind(j,i+6,lenM)] = A[ind(i+6,j,lenN)];
    B[ind(j,i+7,lenM)] = A[ind(i+7,j,lenN)];
  }
}
#pragma @ICE endloop
}
