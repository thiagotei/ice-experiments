#!/usr/bin/env bash


nrun=`seq 3`
mach="thunder"
prob="transp"
xinfo="none"
complst="clang++-8,clang-8  icpc,icc g++-6,gcc-6"

#strat="rchoice-vert-rec,./runsearch-rec.sh rchoice-vert-vec,./runsearch-symmetrical.sh cacheobliv,./runsearch-symmetrical.sh" #
#runscript="./runsearch-symmetrical.sh"
#runscript="./runsearch-rec.sh"

#exec1="rchoice-vert-rec,./runsearch-rec.sh"
exec1="rchoice-rec,./runsearch-rec.sh"


strat=${exec1%,*}
runscript=${exec1#*,}

for run in $nrun; do
for comps in $complst; do 
    echo $comps
    cxxcomp=${comps%,*}
    cccomp=${comps#*,}
    export ICELOCUS_CXX=${cxxcomp} #g++ #clang++-8  #icpc #
    export ICELOCUS_CC=${cccomp}  # gcc #clang-8 #icc 
    datev=`date +%Y%m%d%H%M%S`
    echo Running $run $strat $runscript $ICELOCUS_CXX $ICELOCUS_CC $datev
    ##./runsearch-symmetrical.sh &> thunder_icc18_trcheobliv_regtile4_`date +%Y%m%d%H%M`.txt
    $runscript &> ${mach}_${cccomp}_${prob}_${strat}_${xinfo}_${datev}.txt
done
done
