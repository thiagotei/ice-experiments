#!/usr/bin/env python

import os, glob, shutil, logging, subprocess, sys
#from locus.tools.search.hyperopttool_locus import main as hyperopt_main
#from locus.tools.search.opentunertool_locus import main as opentuner_main

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
console = logging.StreamHandler()
log.addHandler(console)


def cmdexec(cmd):
    retcode = 0
    try:
        log.info(f'Running cmd {cmd} ...')
        cplproc = subprocess.run(cmd, stdout=sys.stdout, stderr=sys.stderr, shell=False)
        log.info(f'Done cmd.')
    except Exception as e:
        raise
    #finally:
        #retcode = cplproc.returncode
        #print(f'{cplproc.stdout}\nstderr:\n{cplproc.stderr}')
    return retcode
#


class SearchTool():
    def __init__(self):
        pass
    ###
####

class OpenTuner(SearchTool):
    def __init__(self):
        #self.main = opentuner_main
        pass
    ###

    def __str__(self):
        return (f"Opentuner")
    ###

    #@staticmethod
    def exec(self, args=None):
        filelst = glob.glob("./opentuner.*")
        for f in filelst:
            if os.path.isfile(f):
                os.remove(f)
                log.info(f"Removed file {f}")
            elif os.path.isdir(f):
                shutil.rmtree(f, ignore_errors=True)
                log.info(f"Removed dir {f}")
            #
        ##
        #self.main(args)
        nargs = ['ice-locus-opentuner.py']+args
        cmdexec(nargs)
        #opentuner_main(args)
    ###
####

class HyperOpt(SearchTool):

    _mixdsc = {"rand": ("rand","None"),
               "tpe" : ("tpe","None"),
               "anneal": ("anneal","None"),
               "mixA": ("mix",".25:rand:.75:tpe"),
               "mixB": ("mix",".25:rand:.75:anneal"),
               "mixC": ("mix",".25:rand:.50:anneal:0.25:tpe"),
               "mixD": ("mix",".50:rand:.50:anneal"),
               "mixE": ("mix",".75:rand:.25:anneal")}

    def __init__(self):
        #self.main = hyperopt_main
        pass
    ###

    def __str__(self):
        return (f"Hyperopt")
    ###

    def exec(self, args=None, alg='mixC'):
        #self.main(args)
        #hyperopt_main(args)
        nargs = ['ice-locus-hyperopt.py','--hypalg', self._mixdsc[alg][0],
                 '--hypmixdsc', self._mixdsc[alg][1]] + args
        cmdexec(nargs)
    ###
####

class Problem():
    def __init__(self, name, shapenames, stopname=None):
        self.name = name
        self.shapenames = shapenames
        self.stopname = stopname
    ###

    def __str__(self):
        return (f"Problem: {self.name}")
    ###
####

class Strat():
    def __init__(self, prob, stool, stopfunc, args, cmplxargs=None):
        self.prob = prob
        self.stool = stool
        self.stopfunc = stopfunc
        self.args = args
        self.cmplxargs = cmplxargs
    ###

    def __str__(self):
        return (f"Strategy: prob= {self.prob} searchtool= {self.stool}"
               f" cmplxargs= {self.cmplxargs}")
    ###

    def exec(self, xargs=None, key=None):
        xargs = xargs or []
        if key is not None and self.cmplxargs is not None:
            log.debug(f"key: {key} cmplargs: {self.cmplxargs}")
            for c in self.cmplxargs:
                log.debug(f"c: {c}")
                if key in self.cmplxargs[c]:
                    xargs += [str(c), str(self.cmplxargs[c][key])]
                #
            ##
        #
        log.debug(f"xargs: {xargs}")
        self.stool.exec(self.args+xargs)
    ###
####

def genOrigAnswer(origfilename=None, dstfilename=None, 
                  bldcmd=None, runcmd=None):
    #origprefix = origprefix or 'answer-mattransp-'
    #dstprefix = dstprefix or "orig-answer-mattransp-"

    cmdexec(bldcmd)
    cmdexec(runcmd)

    os.rename(origfilename, dstfilename)
    ##
###

def genOrigMTransp(shapes):
    if not os.path.isfile('.test'):
        open('.test','a').close()
    #
    for sh in shapes:
        suf="-".join([str(v) for v in sh])+".txt"
        bldcmd = ['make','ORIG=mattranspargc','CXX=g++','CC=gcc','clean','orig']
        runcmd = ['./orig']+[str(v) for v in sh]
        origfilename = "answer-mattransp-"+suf
        dstfilename = "orig-answer-mattransp-"+suf
        if not os.path.isfile(dstfilename):
            genOrigAnswer(origfilename, dstfilename, bldcmd, runcmd)
        #
    ##
###

def runall():
    approx = [100]
    nruns = 1
    comps = [('clang++-8','clang-8'), ('icpc','icc'), ('g++-6','gcc-6')]
    #comps = [('clang++-8','clang-8')]
    shapes = [(x,x) for x in range(1024, 8192+1, 1024)]
    #shapes = [(1024, 1024)]
    strategies = []

    transp = Problem('transp', 
                    ['ICELOCUS_MATSHAPE_M','ICELOCUS_MATSHAPE_N'],
                    'ICELOCUS_MTRANSP_STOP')

    genOrigMTransp(shapes)

    baseargs=['--tfunc','mytiming.py:getTiming', '-o','suffix','--search',
            '-f','mattranspargc.cpp']

    #cacheobliv = Strat(prob=transp, stool=OpenTuner(), stopfunc=,
    cacheobliv = Strat(prob=transp, stool=OpenTuner(), stopfunc=None,
            args=baseargs+['--ntests','10','-t','mattransp-cacheobliv-argc.locus'])

    rchoice_vert_vec = Strat(prob=transp, stool=OpenTuner(), stopfunc=None,
            args=baseargs+['--ntests','5','-t','mattransp-vec-sym-argc.locus'])

    recargs = ['--timeout', '--exprec', '--approxexprec','100','--saveexprec']
    baseuplim = 10
    incuplim =  10
    upperlimvals = [x for x in range(baseuplim, len(shapes)*incuplim+baseuplim,incuplim)]
    basestopv = 15 
    incstopv = 5
    stopafvals = [x for x in range(basestopv, len(shapes)*incstopv+basestopv,incstopv)]
    reccmplxargs = {'--upper-limit': {l: r for l,r in zip(shapes,upperlimvals)} ,
                        '--stop-after': {l: r for l,r in zip(shapes, stopafvals)}}

    rchoice_vert_rec = Strat(prob=transp, stool=HyperOpt(), stopfunc=lambda x:x//8,
            args=baseargs + recargs + ['-t','mattransp-rec-sym-argc.locus'],
            cmplxargs=reccmplxargs)

    rchoice_horiz = Strat(prob=transp, stool=HyperOpt(), stopfunc=lambda x: x//8,
            args=baseargs + recargs + ['-t','mattransp-rec-argc.locus'],
            cmplxargs=reccmplxargs)

    #strategies += [cacheobliv, rchoice_vert_vec, rchoice_vert_rec, rchoice_horiz]
    strategies += [rchoice_vert_rec, rchoice_horiz]

    # number of runs
    for nr in range(nruns):
        # strategies
        for strat in strategies:
            # comps
            for cxx, cc in comps:
                setEnv('ICELOCUS_CXX', cxx)
                setEnv('ICELOCUS_CC', cc)

                # shapes 
                for sh in shapes:
                    for shname, shval in zip(strat.prob.shapenames, sh):
                        setEnv(shname, str(shval))
                    ##
                    if strat.prob.stopname is not None and \
                            strat.stopfunc is not None:
                        setEnv(strat.prob.stopname, str(strat.stopfunc(sh[0])))
                    #
                    log.info(f"Searching run= {nr} strat= {strat} comps= {cxx}"
                             f" shapes= {sh}")
                    strat.exec(key=sh)
                ##
            ##
        ##
    ##
###

def setEnv(name, val):
    os.environ[name] = val
####

if __name__ == '__main__':
    runall()
#
