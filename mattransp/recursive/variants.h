#ifndef __VARIANTS_H__
#define __VARIANTS_H__

#include <stdlib.h>
#include "globals.tmp.h"
#define ind(i,j, ncol) (i)*(ncol)+(j)
#define BUFNAMESIZE 32

void naiveIJ(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC lenM, ITERC lenN,
	   MATTYPE A, MATTYPE B);

void naiveIJRegTileI4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC lenM, ITERC lenN,
	   MATTYPE A, MATTYPE B);

void naiveIJRegTileI8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC lenM, ITERC lenN,
	   MATTYPE A, MATTYPE B);

#endif

