#!/usr/bin/env bash

. ./runloop-base.sh

nruns=`seq 1` #3
ntests=20000000000
cfile=mattransp.cpp

baseuplimit=10
incuplimit=10

basestopaf=15 #25
incstopaf=5

#locusfile=mattransp-rec-sym.locus
locusfile=mattransp-rec.locus
approxvalues=100
searchtools=ice-locus-hyperopt.py #"ice-locus-opentuner.py" #
#
inpvalues="`seq 1024 1024 8192`"
#inpvalues="`seq 1024 1024 1024`"

search_loop_body_pre(){
    echo Starting rec search_loop_body_pre...
    xdebug="" #"--debug" #
    xparam=" --timeout --upper-limit ${uplimit} --stop-after $stopaf --exprec --approxexprec $approx --saveexprec " # --ntests ${ntests} "
    #stopv=$(( inp / 256 ))
    stopv=$(( inp / 8 ))

    export ICELOCUS_MATSHAPE_M=$inp #512 #1024 #
    export ICELOCUS_MATSHAPE_N=$inp
    #export ICELOCUS_CXX=icpc
    #export ICELOCUS_CC=icc
    export ICELOCUS_MTRANSP_STOP=$stopv 

    echo Ending rec search_loop_body_pre
}

search_loop
