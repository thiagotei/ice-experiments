#!/usr/bin/env bash

xdebug=""
xparam=""

declare -A mixdsc
mixdsc=(["rand"]="None" ["tpe"]="None" ["anneal"]="None" 
        ["mixA"]=".25:rand:.75:tpe" ["mixB"]=".25:rand:.75:anneal" 
        ["mixC"]=".25:rand:.50:anneal:0.25:tpe" ["mixD"]=".50:rand:.50:anneal" 
        ["mixE"]=".75:rand:.25:anneal")

search_loop_body_pre(){
    echo Starting base search_loop_body_pre...
    xdebug="" #"--debug" #
    xparam+="--timeout --upper-limit ${uplimit} --stop-after $stopaf " #"--exprec --approxexprec $approx --saveexprec"
    echo Ending base search_loop_body_pre
}

search_loop_body() {
    #echo xparam= $xparam
    #echo uplimit= $uplimit
    echo Starting search_loop_body

    search_loop_body_pre

    echo $xparam

    sed -e "s/define M 256/define M ${ICELOCUS_MATSHAPE_M}/g" \
        -e "s/define N 256/define N ${ICELOCUS_MATSHAPE_N}/g" globals.h > globals.tmp.h;

    if [ ! -f ./orig-answer-mattransp-${ICELOCUS_MATSHAPE_N}-${ICELOCUS_MATSHAPE_M}.txt ]; then
        echo "Generating orig answer..."
        make CXX=$ICELOCUS_CXX CC=$ICELOCUS_CC clean orig
        ./orig
        mv answer-mattransp-${ICELOCUS_MATSHAPE_N}-${ICELOCUS_MATSHAPE_M}.txt \
            orig-answer-mattransp-${ICELOCUS_MATSHAPE_N}-${ICELOCUS_MATSHAPE_M}.txt
    fi

    if [[ $search == *hyperopt* ]]
    then
        alg="mix"
        mixalg="mixC"
        xparam+="--hypalg $alg --hypmixdsc ${mixdsc[${mixalg}]}"
        #alg="rand"
        #xparam+="--hypalg $alg"
    fi

    rm -rvf opentuner.*

    echo Searching approx= $approx matshape= $ICELOCUS_MATSHAPE_M x $ICELOCUS_MATSHAPE_N stop= $ICELOCUS_MTRANSP_STOP $search r= $r $ICELOCUS_CC $ICELOCUS_CXX `date`
    echo xparam= $xparam

    $search   \
        -t ${locusfile} -f ${cfile} \
        --tfunc mytiming.py:getTiming \
        -o suffix -u '.ice' --search \
        $xparam $xdebug
    echo Ending search_loop_body
}

search_loop() {
    echo Startgin search_looop... $approxvalues $searchtools $inpvalues $runs
for approx in $approxvalues; do
    for search in $searchtools; do
        uplimit=$baseuplimit
        stopaf=$basestopaf
        for inp in $inpvalues; do
            for r in $nruns; do

                search_loop_body

            done
            uplimit=$((uplimit + incuplimit))
            stopaf=$((stopaf + incstopaf))
        done
    done
done
    echo Ending search_loop...
}
