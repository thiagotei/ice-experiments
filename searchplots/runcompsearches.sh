#!/bin/bash

./compsearches.py --filesA ../matmul/plutobench/min_values_output-4096-p9-remote-xlc-16-1-1-random* --filesB ../matmul/plutobench/min_values_output-4096-p9-remote-xlc-16-1-1-initseed-1thread-2019061* --t "Comparing Initial Configurations on IBM Power" --interval 180 --limit 3600
