#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
import sys


parser = argparse.ArgumentParser()
parser.add_argument('--t', type=str, required=True, help="Title for the chart")
parser.add_argument('--o', type=str, required=False, help="Output chart name")
parser.add_argument('--filesA',  nargs='+', required=False, help="Receive a list of files.")
parser.add_argument('--filesB',  nargs='+', required=False, help="Receive a list of files.")
parser.add_argument('--interval', type=int, required=False, default=60,help="Interval in seconds")
parser.add_argument('--limit', type=int, required=False,
        default=3600,help="Chart limit in seconds")
parser.add_argument('--lowP', type=int, required=False, default=None, help="Lower Percentile [0,50].")
parser.add_argument('--upP', type=int, required=False, default=None, help="Upper Percentile [50,100].")
args = parser.parse_args()

def plot(files, fmt, leg):
    pltx = []
    plty = []

    if args.lowP and (args.lowP < 0 or args.lowP > 50):
        print("ERROR! lower percentitle must be between 0 and 50.")
        return
    #endig
    if args.upP and (args.upP < 50 or args.upP > 100):
        print("ERROR! upper percentitle must be between 50 and 100.")
        return
    #endif

    #minlen = sys.maxsize
    rngelem = range(args.interval, args.limit, args.interval)
    numelem = len(rngelem)
    lstresx = list(rngelem)

    with open(leg.replace(" ","_")+".csv", "w") as wfile:
        wfile.write("x,"+",".join([str(x) for x in lstresx])+"\n")
        for i, inpfile in enumerate(files):
            print ("Plotting ", i,": ", inpfile)
            data = []
            with open(inpfile, 'r') as fi:
                duration = int(next(fi).split()[3])
                for line in fi:
                    v = line.split()
                    data.append((int(v[0]), float(v[1])))
            #end with

            xinp = [v[0] for v in data]
            yinp = [v[1] for v in data]

            resdic = {}

            #for i in range(0, args.limit, args.interval):
            for inte in rngelem:
                for ixp, iyp in zip(xinp, yinp):
                    if ixp < inte and (inte not in resdic or iyp < resdic[inte]):
                        resdic[inte] = iyp
                        #print ("resdic[",inte,"] = ", iyp)
                #endfor
            #endfor

            # if input data has values starting out the range
            if not resdic and yinp:
                for inte in rngelem:
                    resdic[inte] = yinp[0]
            #endif

            if resdic:
                lstresy = [resdic[ty] for ty in sorted(resdic)]
                if len(lstresy) < numelem:
                    misselem = numelem - len(resdic)

                    # expand to the left so the arrays have
                    # the same length
                    tmp = [lstresy[0]]*misselem
                    tmp.extend(lstresy)
                    lstresy = tmp
                #endif
                #print ("lstresy: ",lstresy)

                pltx.append(lstresx)
                plty.append(lstresy)
                wfile.write("y"+str(i)+","+",".join([str(y) for y in lstresy])+"\n")
                print (len(lstresy), lstresy)
                #print (pltx[-1:],"----",plty[-1:])
            else:
                print ("resdic has no values for this range on this file!")
            #endif

            #plt.plot(xinp, yinp, label=leg)
        #endfor
    #endwith

    #xarr = np.array([np.array(k[-minlen:]) for k in pltx])
    #yarr = np.array([np.array(k[-minlen:]) for k in plty])
    xarr = np.array([np.array(k) for k in pltx])
    yarr = np.array([np.array(k) for k in plty])
    #print (plty)

    if args.upP:
        upperP = np.percentile(yarr, args.upP, axis=0)
        print (args.upP,"upper Percentile:", upperP)
    #endif

    if args.lowP:
        lowerP = np.percentile(yarr, args.lowP, axis=0)
        print (args.lowP,"lower Percentile:", lowerP)
    #endif


    yarrTmp = yarr.copy()
    if args.upP:
        yarrTmp = yarrTmp.clip(max=upperP)
    #endif

    if args.lowP:
        yarrTmp = yarrTmp.clip(min=lowerP)
    #endif

    #print(yarr, type(yarr), type(yarr[0]), yarr[1][1])

    #d2arr = np.array([[0,20,40],[60,80,100]])
    #print (d2arr)
    #print (np.median(d2arr, axis=0))
    ymedian = np.median(yarrTmp, axis=0)
    print("Median", ymedian)
    #ymedian = np.median(yarr, axis=0)
    ymin = np.min(yarrTmp, axis=0)
    yminerr = ymedian-ymin
    print("Min", ymin)
    print("Minerr", yminerr)

    ymax = np.max(yarrTmp, axis=0)
    ymaxerr = ymax-ymedian
    print("Max", ymax)
    print("Maxerr", ymaxerr)

#    print (ymin)
#    print (ymax)
#    print (ymedian)

    #plt.scatter(pltx, plty, label=leg)
    #plt.scatter(pltx, plty, label=leg)
    plt.errorbar(xarr[0], ymedian, yerr=[yminerr, ymaxerr], fmt=fmt, label=leg,
            capsize=2, elinewidth=0.5)

    return xarr[0],ymedian, yminerr, ymaxerr
#enddef

if args.filesA:
    xv, ymedian, yminerr, ymaxerr = plot(args.filesA, 'bx' , "datA")
    np.savetxt("filesA_x_ymed_ymin_ymax.csv", (xv, ymedian,yminerr,ymaxerr), delimiter=',')

if args.filesB:
    xv, ymedian, yminerr, ymaxerr = plot(args.filesB, 'ro', "datB")
    np.savetxt("filesB_x_ymed_ymin_ymax.csv", (xv, ymedian,yminerr,ymaxerr), delimiter=',')

csfont = {'fontname':'DejaVu Sans'}
plt.legend(loc='upper right')
plt.ylabel('Variant Execution Time (milisec)', **csfont)
plt.xlabel('Search time (sec)',**csfont)
#plt.title('P80, Median, and P20 of the Best Variant Found')
if args.t:
    plt.title(args.t)

if args.o:
    print("Saving chart on {}.".format(args.o))
    plt.savefig("./"+args.o)
else:
    print("No output filename provided. Showing...")
    plt.show()
#endfi




