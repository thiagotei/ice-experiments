#!/usr/bin/env bash

. ../matmul/hierarchstudy/runparams.sh

comp="icc"
initpnt="2lvl"
nproc=1
#algos="rand anneal mixA mixB mixC mixD mixE"
algos="anneal mixA mixB mixC mixD mixE"
#algos="rand"
lowerPerc=20
upperPerc=80

for al in $algos; do
	export hypalg=$al
	if [[ "$hypalg" = *mix* ]]
	then
		export hypalg="mix"
	fi

	hypmixdsc=${mixdsc[${al}]}

	filesHyp=../matmul/hierarchstudy/hyperopt/output-py3.6.8-hyp0.2.2-V2perm-${comp}-w${initpnt}init-${hypalg}-${hypmixdsc}-${nproc}thread-
	filesOt=../matmul/hierarchstudy/opentuner/output-V2perm-${comp}-w${initpnt}init-opentuner-otprune-1thread-
	numFilHyp=`ls -l ${filesHyp}* | wc -l`
	numFilOt=`ls -l ${filesOt}* | wc -l`
	echo "$numFilHyp hyperopt files"
	echo "$numFilOt opentuner files"

	./runitall.sh \
		../matmul/hierarchstudy/hyperopt/output-py3.6.8-hyp0.2.2-V2perm-${comp}-w${initpnt}init-${hypalg}-${hypmixdsc}-${nproc}thread-  \
		../matmul/hierarchstudy/opentuner/output-V2perm-${comp}-w${initpnt}init-opentuner-otprune-1thread- \
		HypVsOptu_V2perm_${initpnt}init_${comp}_${hypalg}${hypmixdsc}_otprune_${lowerPerc}perc${upperPerc}_${numFilHyp}vs${numFilOt}runs ${hypalg}${hypmixdsc} otprune $lowerPerc $upperPerc

done 
