#!/usr/bin/env bash

interv=30
lowerPerc=20
upperPerc=80

tmppath=`mktemp -d`
#ofname="Default"
#echo "CSV file name output: " $1
#echo "Pattern 1: " $1 # do not use the *
#echo "Pattern 2: " $2 # do not use the *
#ofname=$1
echo "Using dir: " $tmppath >&2

for inppat in ${@}; do
    for f in ${inppat}* ; do
        echo $f >&2
        tmpfile=${tmppath}/`basename ${f}`.proc
        echo $tmpfile >&2
        ./searchres_new.sh $f > $tmpfile 

    done

    comp=`echo $inppat |awk -F- 'NF == 10{print $5} NF == 7{ print $3} NF == 8{print $3}'`
    leg="V2perm"
    initpnt=`echo $inppat | awk -F- 'NF == 10{print $6} NF == 7{ print $4} NF == 8{print $4}'`
    stool=`echo $inppat | awk -F- '/hyp/{print "hyperopt"} /opentuner/{print "opentuner"}'`
    stech=`echo $inppat | awk -F- '/hyp/ && NF == 10{print $7} /hyp/&&NF==7{print "tpe"}  /opentuner/{print "bandit"}'`


    fileA=${tmppath}/`basename ${inppat}`*.proc

    echo "Calling compsearches: $fileA " >&2
    echo "comp: $comp desc: $leg initpnt: $initpnt stool: $stool stech: $stech" >&2
    echo "#"$inppat

    #./compsearches.py --filesA $fileA --filesB $fileB --t $ofname --o $title".pdf" --limit 7200 --interval $interv --lowP $lowerPerc --upP $upperPerc
    ./compsearches_new.py --files $fileA --limit 7200 --interval $interv --lowP $lowerPerc --upP $upperPerc \
               --desc $leg --comp $comp --stool $stool --stech $stech --initpnt $initpnt

    echo "Done! compsearches: $fileA " >&2
done

#echo "Removing "${tmppath}
#rm -rf ${tmppath}

