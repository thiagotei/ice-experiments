#!/usr/bin/env bash

interv=30
lowerPerc=20
upperPerc=80

tmppath=`mktemp -d`
title="Default"
echo "Pattern 1: " $1 # do not use the *
echo "Pattern 2: " $2 # do not use the *
echo "Title: " $3
title=$3
patB=base
echo "Using dir: " $tmppath
for f in ${1}* ${2}*; do
	echo $f
	tmpfile=${tmppath}/`basename ${f}`.proc
	echo $tmpfile
	./searchres_new.sh $f > $tmpfile 

done

fileA=${tmppath}/`basename ${1}`*.proc
fileB=${tmppath}/`basename ${2}`*.proc

echo "Calling compsearches: $fileA $fileB $title"

./compsearches.py --filesA $fileA --filesB $fileB --t $title --o $title".pdf" --limit 7200 --interval $interv --lowP $lowerPerc --upP $upperPerc

echo "Done! compsearches: $fileA $fileB $title $title.pdf"

echo "Removing "${tmppath}
rm -rf ${tmppath}

