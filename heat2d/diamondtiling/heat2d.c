#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <pips_runtime.h>

#define T 1000L
#ifndef N
#define N 4000L 
#endif

typedef double bhtype;

double mysecond(void);

void heat2d(bhtype a[2][N+2][N+2])
{
    int i, j, t;

    #pragma @ICE loop=heat2d
    for (t = 0; t < T; t++) {
	    for (i = 1; i < N+1; i++) {
		    for (j = 1; j < N+1; j++) {
			    a[(t+1)%2][i][j] =   0.125 * (a[t%2][i+1][j] - 2.0 * a[t%2][i][j] + a[t%2][i-1][j])
				    + 0.125 * (a[t%2][i][j+1] - 2.0 * a[t%2][i][j] + a[t%2][i][j-1])
				    + a[t%2][i][j];
		    }
	    }
    }
}

int main()
{
  bhtype (*a)[2][N+2][N+2];
  a=malloc((2)*(N+2)*(N+2)*sizeof(bhtype));
  int i,j;

  for(i=1;i<N+1;i++)
    for(j=1;j<N+1;j++)
      (*a)[0][i][j]=((bhtype)j)/N;
  double  tStart = mysecond();
  heat2d(*a);
  double  lTime = mysecond() - tStart;

//  printf("*** Minimum global time  : %.3f (ms)\n",lTime);

  printf("Heat2D size = %ld steps = %ld | ", N, T);
  printf("Time        = %7.5lf ms | ", lTime * 1.0e3);
  double rate = (T-1) * (N) * ((N) / lTime);
  printf("Rate        = %f MStencils/s\n", rate * 1.0e-6);
 
  return 0;
}
