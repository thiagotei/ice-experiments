#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <pips_runtime.h>

#define T 1000L
#ifndef N
#define N 256L
#endif

typedef double bhtype;

double mysecond(void);

void heat3d(bhtype A[2][N+2][N+2][N+2])
{
    int i, j, t, k;

    #pragma @ICE loop=heat3d
    for (t = 0; t < T-1; t++) {
        for (i = 1; i < N+1; i++) {
            for (j = 1; j < N+1; j++) {
                for (k = 1; k < N+1; k++) {
                    A[(t+1)%2][i][j][k] =   0.125 * (A[t%2][i+1][j][k] - 2.0 * A[t%2][i][j][k] + A[t%2][i-1][j][k])
                                        + 0.125 * (A[t%2][i][j+1][k] - 2.0 * A[t%2][i][j][k] + A[t%2][i][j-1][k])
                                        + 0.125 * (A[t%2][i][j][k-1] - 2.0 * A[t%2][i][j][k] + A[t%2][i][j][k+1])
                                        + A[t%2][i][j][k];
                }
            }
        }

    }
}

int main()
{
  bhtype (*a)[2][N+2][N+2][N+2];
  a=malloc((2)*(N+2)*(N+2)*(N+2)*sizeof(bhtype));
  int i,j,k;

  for(i=1;i<N+1;i++)
    for(j=1;j<N+1;j++)
      for(k=1;k<N+1;k++)
        (*a)[0][i][j][k]=((bhtype)j)/N;
  double  tStart = mysecond();
  heat3d(*a);
  double  lTime = mysecond() - tStart;

//  printf("*** Minimum global time  : %.3f (ms)\n",lTime);

  printf("Heat3D size = %ld steps = %ld\n", N, T);
  printf("Time        = %.2e secs\n", lTime);
  double rate = (T-1) * (N) * (N) * ((N) / lTime);
  printf("Rate        = %.2e MStencils/s\n", rate * 1.0e-6);
 
  return 0;
}
