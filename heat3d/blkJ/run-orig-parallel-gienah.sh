#!/bin/bash

comp=icc
dim=256L
make CC=${comp} USERDEFS="mysecond.c -DN=${dim}" clean orig

for nproc in 1 10 20
do
	seq ${nproc} | parallel taskset -c '$((2*({%}-1)))' ./orig &> result-orig-${dim}-gienah-${comp}-parallel-${nproc}.log

done
