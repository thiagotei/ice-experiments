#!/bin/bash

nproc=1;
ntest=100;
dimZ=1600;
dimY=$dimZ;
dimX=$dimZ;


#for nproc in 20 40 80 1
for nproc in 80
do

tmpdimZ=$((${dimZ}/${nproc}))
echo "Running search ${proc}... "

sed -e "s/numproc=PROC/numproc=${nproc}/g" \
    -e "s/dimZ=DIMZ/dimZ=${tmpdimZ}/g" \
    -e "s/dimY=DIMY/dimY=${dimY}/g" \
    -e "s/dimX=DIMX/dimX=${dimX}/g" \
    heat3d-p9-remote.locus > heat3d-p9-remote.${nproc}.locus

heatfilename="heat3d.z.${tmpdimZ}.y.${dimY}.x.${dimX}.c"

sed -e "s/define NX 256/define NX ${dimX}/g" \
    -e "s/define NY 256/define NY ${dimY}/g" \
    -e "s/define NZ 256/define NZ ${tmpdimZ}/g" \
    heat3d.c > $heatfilename

ice-locus-opentuner.py -t heat3d-p9-remote.${nproc}.locus -f $heatfilename \
	-o suffix -u '.ice' --search \
	--ntests ${ntest} \
	--tfunc mytiming.py:getTimingMatMul &> heat3d-remote-p9-${dimZ}-ZXY-total-${tmpdimZ}-Z-tiled-${nproc}-proc-`date +%Y%m%d%H%M`.log
done
