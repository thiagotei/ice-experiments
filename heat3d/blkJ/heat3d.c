#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <pips_runtime.h>

#define T 1000L
#ifndef NX
#define NX 256L
#endif

#ifndef NY
#define NY 256L
#endif

#ifndef NZ
#define NZ 256L
#endif

typedef double bhtype;

double mysecond(void);

void heat3d(bhtype A[2][NZ+2][NY+2][NX+2])
{
    int i, j, t, k;

    #pragma @ICE loop=heat3d
    for (t = 0; t < T-1; t++) {
        for (i = 1; i < NZ+1; i++) {
            for (j = 1; j < NY+1; j++) {
                for (k = 1; k < NX+1; k++) {
                    A[(t+1)%2][i][j][k] =   0.125 * (A[t%2][i+1][j][k] - 2.0 * A[t%2][i][j][k] + A[t%2][i-1][j][k])
                                        + 0.125 * (A[t%2][i][j+1][k] - 2.0 * A[t%2][i][j][k] + A[t%2][i][j-1][k])
                                        + 0.125 * (A[t%2][i][j][k-1] - 2.0 * A[t%2][i][j][k] + A[t%2][i][j][k+1])
                                        + A[t%2][i][j][k];
                }
            }
        }

    }
}

int main()
{
  bhtype (*a)[2][NZ+2][NY+2][NX+2];
  a=malloc((2)*(NZ+2)*(NY+2)*(NX+2)*sizeof(bhtype));
  int i,j,k;

  for(i=1;i<NZ+1;i++)
    for(j=1;j<NY+1;j++)
      for(k=1;k<NX+1;k++)
        (*a)[0][i][j][k]=((bhtype)j)/NX;

  double  tStart = mysecond();
  heat3d(*a);
  double  lTime = mysecond() - tStart;

//  printf("*** Minimum global time  : %.3f (ms)\n",lTime);

  printf("Heat 3D steps = %ld Time =  %7.5lf secs Z= %ld Y= %ld X= %ld ", T, lTime, NZ, NY, NX);
  //printf("Time        = %7.5lf secs\n", lTime);
  double rate = (T-1) * (NZ) * (NY) * ((NX) / lTime);
  printf("Rate = %.2e MStencils/s\n", rate * 1.0e-6);
 
  return 0;
}
