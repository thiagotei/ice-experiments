#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <sys/time.h>
#include <pips_runtime.h>

#include <assert.h>

#define N 1000000
#define T 1000

//#pragma declarations
double a[N];
//#pragma enddeclarations

//#ifdef TIME
#define IF_TIME(foo) foo;
//aelse
//#define IF_TIME(foo)
//#endif

void init_array()
{
    int i;

    for (i=0; i<N; i++) {
            a[i] = i*i;
    }
}


void print_array()
{
    int i;

    for (i=0; i<N; i++) {
            fprintf(stderr, "%0.2lf ", a[i]);
            if (i%80 == 20) fprintf(stderr, "\n");
    }
    fprintf(stderr, "\n");
}

double rtclock()
{
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

#define __PLACE_TO_INSERT_FORWARD_DECLARATIONS

int main()
{
    int i, t;

    double t_start, t_end;

    init_array() ;

#ifdef PERFCTR
    PERF_INIT; 
#endif

    IF_TIME(t_start = rtclock());

//#pragma scop
#pragma @ICE loop=seidel1d
    for (t=0; t<=T-1; t++)  {
        for (i=1; i<=N-2; i++)  {
                a[i] = (a[i-1] + a[i] + a[i+1])/3.0;
        }
    }
//#pragma endscop

    IF_TIME(t_end = rtclock());
//    IF_TIME(fprintf(stdout, "%0.6lfs\n", t_end - t_start));
    IF_TIME(printf("Seidel1d size = %d steps = %d | Time = %7.5lf ms\n", N,T,(t_end - t_start)*1.0e3))

#ifdef PERFCTR
    PERF_EXIT; 
#endif

  if (fopen(".test", "r")) {
#ifdef MPI
    if (my_rank == 0) {
        print_array();
    }
#else
    print_array();
#endif
  }

    return 0;
}
