#!/usr/bin/env bash

        #awk '/Running/{printf("%s,%s,%s,%s",$3,$5,$6,$8)} 
         #/Matrixtransp=/{printf(",%s,%s",$2,$3); for(i=8;i <8+$6; ++i) {printf(",%lf",$i)}; printf("\n");}' | \
echo leg,comp,searchtool,run,matshapeM,matshapeN,time
for fileinp in "$@"; do
    echo "Processing $fileinp ..." >&2
    echo "#$fileinp"
    grep "Running\|Matrixtransp=" $fileinp | \
        awk '/Running/{lineA=$3","$5","$6","$8} 
         /Matrixtransp=/{lineB=$2","$3; for(i=8;i <8+$6; ++i) {printf("%s,%s,%lf\n",lineA,lineB,$i)};}' | \
        awk -F ',' '$1 == $5 && $2 == $6 {print "base-"$4","$3",None,1,"$1","$2","$7  ; next} {print "RECORD INCORRECT!"}'
done
