#!/usr/bin/env bash

#cc=clang-8 #icc #gcc #
#cxx=clang++-8 #icpc #g++ #
complst="clang++-8,clang-8 icpc,icc g++-6,gcc-6"
optflst="-O3 -O2 -O1 max"

for comps in $complst; do 
    for optf in $optflst; do
        for mval in `seq 512 512 8192`; do
            for nval in `seq 512 512 8192`; do
                cxx=${comps%,*}
                cc=${comps#*,}
                echo Running M= $mval N= $nval $cc $cxx $optf `date`
                xoptparam=""
                if [[ $optf != *max* ]]
                then
                    xoptparam="OPTFLAGS=${optf}"
                fi
                make CC=$cc CXX=$cxx clean $xoptparam USERDEFS="-DM=${mval} -DN=${nval}" orig
                ./orig
                #mv answer-mattransp-${nval}-${mval}.txt orig-answer-mattransp-${nval}-${mval}.txt 
                echo Done M= $mval N= $nval $cc $cxx $optf `date`
            done
        done
    done
done
