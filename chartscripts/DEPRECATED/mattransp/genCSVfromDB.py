#!/usr/bin/env python

import sys, argparse
import statistics as stat

try:
    import locus.tools.search.resultsdb.models as m
    from locus.tools.search.resultsdb.connect import connect
    from locus.tools.search.resultsdb.misc import hashlocusfile
except ImportError:
    raise("Load virtual env with ice-locus installed to use this script!!")
#

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-l', type=str, required=True, nargs='+',
        help="Locus programs related info from DB.")

args=parser.parse_args()

def dbnameparse(dbname):
    if '://' not in dbname:
        newdbname = 'sqlite:///' + dbname
    #
    return newdbname
###

def getExperimentResults(dbinp, locusfiles):
    dbname = dbnameparse(dbinp)
    debug = False
    engine, session = connect(dbname, debug)

    print(f"leg,comp,searchtool,run,matshapeM,matshapeN,time")
    for lf in locusfiles:
        tgthash,_ = hashlocusfile(lf)
        x = session.query(m.Search,m.LocusFile.locusfilename).join(m.LocusFile).filter(m.LocusFile.hash == tgthash)
        #print(f"{x}\n")

        for se, lfname in x:
            #envvar = {}
            comp = "N/A"
            sM = "N/A"
            sN = "N/A"
            for evalue in se.envvalues:
                #print(f"envvar: {evalue} {evalue.envvar}")
                if evalue.envvar.name == 'ICELOCUS_CC':
                    comp = evalue.value
                elif evalue.envvar.name == 'ICELOCUS_MATSHAPE_M':
                    sM = evalue.value
                elif  evalue.envvar.name == 'ICELOCUS_MATSHAPE_N':
                    sN = evalue.value
                #
            ##
            #print(f"Search: {se.id}")
            bestmetric = float('inf')
            bestvar = None
            for v in se.variants:
                emetrics = [e.metric for e in v.experiments]
                medmetric = stat.median(emetrics)
                if medmetric < bestmetric:
                    bestmetric = medmetric
                    bestvar = v
                #
            ##
            if bestvar:
                v = bestvar
                for e in v.experiments:
                    print(f"{lfname},{comp},{se.searchtool},{v.id},{sM},{sN},{e.metric}")
                ##
            #
        ##
    ###

    session.close()
###

if __name__ == '__main__':
    getExperimentResults(args.i, args.l)
#


