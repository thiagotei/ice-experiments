#!/usr/bin/env python

from os import path
import argparse, sys

#from chartscripts.common.misc import dbnameparse, getDBresults
#print(f"path: {path.dirname(path.abspath(__file__))}")

# Loading the common modules...
pathcommon=path.dirname(path.abspath(__file__))+'/..'
if pathcommon not in sys.path:
    sys.path.insert(0, pathcommon)

from common.misc import dbnameparse, getDBresults

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-l', type=str, required=True, nargs='+',
        help="Locus programs related info from DB.")

args=parser.parse_args()

def genCSVfromDB(dbinp, locusfiles):
    res_gen = getDBresults(dbinp, locusfiles, ['ICELOCUS_CC','ICELOCUS_MATSHAPE'])
    #metainfo = next(res_gen)
    #lfname = metainfo['lfname']
    #comp = metainfo['ICELOCUS_CC']
    #shape = metainfo['ICELOCUS_MATSHAPE'] 
    #setool = metainfo['searchtool']

    print(f"leg,comp,searchtool,run,matshapeM,matshapeN,matshapeK,time")
    # Variant and experiments loop
    for metainfo,var, exp in res_gen:
        #print(f"tmp: {type(tmp), {tmp}}")
        lfname = metainfo['lfname']
        comp = metainfo['ICELOCUS_CC']
        shape = metainfo['ICELOCUS_MATSHAPE'] 
        setool = metainfo['searchtool']

        print(f"{lfname},{comp},{setool},{var.id},{shape},{shape},{shape},{exp.metric}")
    #
###

if __name__ == '__main__':
    genCSVfromDB(args.i, args.l)
#



