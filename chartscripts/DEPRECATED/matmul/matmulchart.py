#!/usr/bin/env python

import numpy as np
import pandas as pd
import argparse
import math
import matplotlib.pyplot as plt
import itertools

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input csv file path.")
parser.add_argument('-o', type=str, required=True, help="Output pdf file name.")

args=parser.parse_args()

linestyles=["-","--","-.",":"]
markerstyles=[".","+","x",'s']

def calc_gemm_flops(x):
    return (x*x*(x+x))/1e9 # gigaflops
##

def plottime(dfinp) :
    df = dfinp[dfinp['matshapeM'] == dfinp['matshapeN']] 
    strgs = df.groupby('leg') #df['leg'].unique()
    linesty=itertools.cycle(linestyles)
    mksty  =itertools.cycle(markerstyles)

    for keyleg, stleg in strgs:
        print(stleg)
        ccs = stleg.groupby('comp')
        for keycc, stcc in ccs:
            x = stcc['matshapeM'].unique()
            #print("x",x)
            #y = st.groupby('len').median().time/1e3
            #y = st.groupby('len').min().time/1e3
            y = stcc.groupby(['matshapeM']).median().time*1.0e-3
            #ymin = st.groupby('len').min().time/1e3
            #ymax = st.groupby('len').max().time/1e3
            print(keyleg,keycc, y)
            label = keyleg+" "+keycc #st.leg

            plt.plot(x, y, label=label, marker=next(mksty), linestyle=next(linesty), fillstyle='none')
        ##
    ##

    ticks=df['matshapeM'].unique()

    plt.grid(b=True, which='both', linestyle='-', alpha=0.2)
    #leg=str(size)
    #plt.errorbar(x, y, yerr=[ymin, ymax], label=leg, capsize=2)
    #plt.title("Eigenproblem on Stellar machine")
    plt.xticks(ticks)
    plt.tick_params(axis='x', labelsize=8, labelrotation=40.0)
    plt.xlabel("N")
    plt.ylabel("Time (sec)")
    plt.legend(loc='upper left')
    figname=args.o
    plt.savefig(figname, bbox_inches='tight')
    print(f"Saved chart in {figname} .")
#enddef

def plot_gtransf_perCC(dfinp, xlabel="N"):
    # get only the squared ones
    df = dfinp[dfinp['matshapeM'] == dfinp['matshapeN']] 

    datacomp = df.groupby('comp') #data['comp'].unique()
    nrows = 1
    ncols = len(datacomp)
    sbidx = 0
    fig, axes = plt.subplots(nrows, ncols, sharey=True, figsize=(15,5))
    lgd = None

    for keycomp, grpcomp in datacomp:
        pltobj = axes[sbidx] if ncols > 1 else axes
        linesty = itertools.cycle(linestyles)
        mksty = itertools.cycle(markerstyles)
        pltobj.grid(b=True, which='both',linestyle='-', alpha=0.2)

        strgs = grpcomp.groupby('leg') #df['leg'].unique()
        for keyleg, grpleg in strgs:
            x = grpleg['matshapeM'].unique()
            xflop = calc_gemm_flops(x) #x*x/1e9 
            ymed = grpleg.groupby(['matshapeM']).time.median()
            #ymax = grpleg.groupby(['matshapeM']).max().time
            ymax = grpleg.groupby(['matshapeM']).time.quantile(.2)
            #ymin = grpleg.groupby(['matshapeM']).min().time
            ymin = grpleg.groupby(['matshapeM']).time.quantile(.8)
            #print(f"{keycomp} {keyleg} {ymed} {ymin} {ymax}"
            ymed_sec = ymed*1.0e-3
            ymax_sec = ymax*1.0e-3
            ymin_sec = ymin*1.0e-3
            y = xflop/ymed_sec
            yflopmax = (xflop/ymax_sec)-y
            yflopmin = y-(xflop/ymin_sec)
            print(f"{keycomp} {keyleg} {y} {yflopmin} {yflopmax}")
            label = keyleg

            #pltobj.plot(x, y, label=label, marker=next(mksty),
            pltobj.errorbar(x, y, yerr=[yflopmin, yflopmax], label=label, capsize=2, marker=next(mksty),
                    linestyle=next(linesty), fillstyle='none')
        ##
        if sbidx == 1 or ncols == 1:
            lgd = pltobj.legend(loc='upper center', ncol=3)
        ##
        if sbidx % nrows == (nrows - 1):
            pltobj.set_xlabel(xlabel)
        #
        if sbidx == 0:
            pltobj.set_ylabel("GFlops")
        #
        ticks=grpcomp['matshapeM'].unique()
        pltobj.tick_params(axis='x', labelsize=8) #, labelrotation=40.0)
        pltobj.set_xticks(ticks)
        pltobj.set_title(keycomp)
        sbidx += 1
    ##

    figname=args.o
    fig.tight_layout()
    plt.savefig(figname,
            bbox_extra_artists=(lgd,),
            bbox_inches='tight')
    print(f"Saved chart in {figname} .")
###


def autolabel(rects, pltobj, lim=None):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        #if lim is None or height > lim:
        vpos = height if lim is None or height < lim else 0.9*lim
        pltobj.text(rect.get_x() + rect.get_width()/2., vpos,
            #f'{int(height)}',
            f'{height:.2f}',
            ha='center', va='bottom')
        #
    ##
###

def plot_gtransf_perCC_bars(dfinp, xlabel="Compiler Flags"):
    # get only the squared ones
    df = dfinp[dfinp['matshapeM'] == dfinp['matshapeN']] 
    df = df[df['matshapeN'] == df['matshapeK']] 

    datacomp = df.groupby('comp') #data['comp'].unique()
    nrows = 1
    ncols = len(datacomp)
    sbidx = 0
    fig, axes = plt.subplots(nrows, ncols, sharey=True, figsize=(15,5))
    lgd = None
    yupperb = 0.5

    for keycomp, grpcomp in datacomp:
        pltobj = axes[sbidx] if ncols > 1 else axes
        linesty = itertools.cycle(linestyles)
        mksty = itertools.cycle(markerstyles)
        pltobj.grid(b=True, which='both',linestyle='-', alpha=0.2)

        label = grpcomp['matshapeM'].unique()
        shape= grpcomp['matshapeM'].unique()
        xflop = calc_gemm_flops(shape)
        y = grpcomp.groupby(['leg']).time.median()
        y_sec = y*1.0e-3
        y = xflop/y_sec
        x = y.index
        print(f"==******==>\nx: {x}  xflop: {xflop} ysec: {y_sec} y: {y}")
        rects = pltobj.bar(x, y, label=label)
        autolabel(rects, pltobj, yupperb)

        if sbidx == 1 or ncols == 1:
            lgd = pltobj.legend(loc='upper center', ncol=3)
        ##
        if sbidx % nrows == (nrows - 1):
            pltobj.set_xlabel(xlabel)
        #
        if sbidx == 0:
            pltobj.set_ylabel("GFlops")
        #
        ticks = y.index
        pltobj.tick_params(axis='x', labelsize=8, labelrotation=40.0)
        pltobj.set_xticks(ticks)
        pltobj.set_title(keycomp)
        pltobj.set_yscale('linear') #('log')
        pltobj.set_ylim([0.0, yupperb])
        sbidx += 1
    ##

    figname=args.o
    fig.tight_layout()
    plt.savefig(figname,
            bbox_extra_artists=(lgd,),
            bbox_inches='tight')
    print(f"Saved chart in {figname} .")
###


df = pd.read_csv(args.i, comment="#")
print(df)
print(f"Reading file {args.i} ...")
#print(f"Plot selected {args.p}!")

df.sort_values(['matshapeM','matshapeN','matshapeK'], ascending=True, inplace=True)

#plottime(df)
plot_gtransf_perCC(df)
#plot_gtransf_perCC_bars(df)


