#!/usr/bin/env python

import numpy as np
import pandas as pd
import argparse
import math
import matplotlib.pyplot as plt
import itertools

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input csv file path.")
parser.add_argument('-o', type=str, required=True, help="Output pdf file name.")
parser.add_argument('-p', type=int, required=False, default=1,
        help="Which chart to plot? 1: per_opt 2: wallclk "+
             "cacheoblvi 3 per_flops 4")

args=parser.parse_args()

linestyles=["-","--","-.",":"]
markerstyles=[".","+","x",'s']

def plottime(df) :
    strgs = df.groupby('leg') #df['leg'].unique()
    linesty=itertools.cycle(linestyles)
    mksty  =itertools.cycle(markerstyles)

    for key,st in strgs:
        #print(st)
        x = st['len'].unique()
        #print("x",x)
        #y = st.groupby('len').median().time/1e3
        y = st.groupby('len').min().time/1e3
        #ymin = st.groupby('len').min().time/1e3
        #ymax = st.groupby('len').max().time/1e3
        print(key, y)
        label = key #st.leg

        #plt.errorbar(x, y, yerr=[y-ymin, ymax-y], label=label, capsize=2, marker=next(mksty), linestyle=next(linesty))
        plt.plot(x, y, label=label, marker=next(mksty), linestyle=next(linesty), fillstyle='none')

        #x = df['len'].unique()
        #y = df.groupby('len').median().time #['time'] #[['time']]
        #ymin = df.groupby('len').min().time
        #ymax = df.groupby('len').max().time
        #print(f"{type(y)}\n{y}\n{x}")
    ##
    ticks=df['len'].unique()

    plt.grid(b=True, which='both', linestyle='-', alpha=0.2)
    #leg=str(size)
    #plt.errorbar(x, y, yerr=[ymin, ymax], label=leg, capsize=2)
    #plt.title("Eigenproblem on Stellar machine")
    plt.xticks(ticks)
    plt.tick_params(axis='x', labelsize=8, labelrotation=40.0)
    plt.xlabel("N")
    plt.ylabel("Time (sec)")
    plt.legend(loc='upper left')
    figname=args.o
    plt.savefig(figname, bbox_inches='tight')
    print(f"Saved chart in {figname} .")
##

df = pd.read_csv(args.i, comment="#")
print(df)
print(f"Reading file {args.i} ...")
print(f"Plot selected {args.p}!")

df.sort_values(['len'], ascending=True, inplace=True)

plottime(df)

