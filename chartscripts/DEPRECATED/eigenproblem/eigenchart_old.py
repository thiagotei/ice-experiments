#!/usr/bin/env python

import numpy as np
import pandas as pd
import argparse
import math
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input csv file path.")
parser.add_argument('-o', type=str, required=True, help="Output pdf file name.")
parser.add_argument('-p', type=int, required=False, default=1,
        help="Which chart to plot? 1: per_opt 2: wallclk "+
             "cacheoblvi 3 per_flops 4")

args=parser.parse_args()

df = pd.read_csv(args.i)
print(df)
print(f"Reading file {args.i} ...")
print(f"Plot selected {args.p}!")

df.sort_values(['len'], ascending=True, inplace=True)

#for size in df['len']:
#    y = df[(data.len == size)].median()
#    ymin = df[(data.len == size)].min()
#    ymax = df[(data.len == size)].max()

x = df['len'].unique()
y = df.groupby('len').median().time #['time'] #[['time']]
ymin = df.groupby('len').min().time
ymax = df.groupby('len').max().time
#print(f"{type(y)}\n{y}\n{x}")

#leg=str(size)
#plt.errorbar(x, y, yerr=[ymin, ymax], label=leg, capsize=2)
plt.title("Eigenproblem on Stellar machine")
plt.errorbar(x, y, yerr=[y-ymin, ymax-y], label="QR", capsize=2, marker='.')
plt.xlabel("N")
plt.ylabel("Time (miliseconds)")
plt.legend(loc='upper left')
figname=args.o
plt.savefig(figname)
print(f"Saved chart in {figname} .")

