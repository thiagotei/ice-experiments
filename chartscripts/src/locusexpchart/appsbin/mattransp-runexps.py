#!/usr/bin/env python
'''
@author: Thiago Teixeira
'''

import logging, argparse, os, datetime
from locusexpchart.common.runexps.models import (Problem, Strat, OpenTuner,
                        HyperOpt)
from locusexpchart.common.runexps.utils import setEnv, unsetEnv
from locusexpchart.mattransp.utils import genOrigMTranspArgc

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
console = logging.StreamHandler()
log.addHandler(console)



parser = argparse.ArgumentParser(description="Run matrix transpose set of"
        " ice-locus experiments.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--nruns", required=False, default=1, type=int,
        help="Number of runs for each configuration.")
parser.add_argument("--st", nargs='+', required=False, 
        default=('cobliv', 'vertvec', 'vertrec', 'horiz', 'fixed'),
        choices=('cobliv', 'vertvec', 'vertrec', 'horiz', 'fixed', 'bsearch'),
        help="Strategies to run")
parser.add_argument("--exp", required=False, default='reg',
        choices=('reg','argc'),
        help="Selects the experiment: regular (compile-time known shapes) or"
        " argc (run-time known shapes).")
parser.add_argument("--cc", nargs='+', required=False,
        default=('gcc','icc','clang'),
        choices=('gcc','icc','clang'),
        help="Compilers to use,")
parser.add_argument("--initv", required=False, default=1024, type=int,
        help="Init value of shape sequence.")
parser.add_argument("--endv", required=False, default=8192, type=int,
        help="End value of shape sequence.")
parser.add_argument("--stepv", required=False, default=1024, type=int,
        help="Step value of shape sequence.")
parser.add_argument("--nth", nargs='+', required=False, default=[0],
        help="Number of OMP threads")
parser.add_argument('--dryrun', default=False, action='store_true',
        help="Run script but do not invoke ice-locus execution.")

args = parser.parse_args()

def runall():
    locusdef = {'cobliv': 'mattransp-cacheobliv',
                'fixed': 'mattransp-cacheobliv-fixed', 
                'bsearch': 'mattransp-cacheobliv-fixed-basesearch', 
                'vertvec': 'mattransp-vec-sym', 
                'vertrec': 'mattransp-rec-sym', 
                'horiz': 'mattransp-rec'}
    compdef = {'gcc': ('g++-6', 'gcc-6'),
               'icc': ('icpc', 'icc'), 
               'clang': ('clang++-8', 'clang-8')}

    approx = [100]
    nruns = args.nruns
    #comps = [('clang++-8','clang-8'), ('icpc','icc'), ('g++-6','gcc-6')]
    shapes = [(x,x) for x in range(args.initv, args.endv+1, args.stepv)]
    #shapes = [(1024, 1024)]

    locusfilesuf = '-argc.locus' if args.exp == 'argc' else '.locus'

    transp = Problem('transp', 
                    ['ICELOCUS_MATSHAPE_M','ICELOCUS_MATSHAPE_N'],
                    'ICELOCUS_MTRANSP_STOP')

    if not args.dryrun:
        genOrigMTranspArgc(shapes)
    #

    srcsuf = 'argc' if args.exp == 'argc' else ''
    baseargs=['--tfunc','mytiming.py:getTiming', '-o','suffix','--search',
            '-f', 'mattransp'+srcsuf+'.cpp']

    #cacheobliv = Strat(prob=transp, stool=OpenTuner(), stopfunc=,
    cacheobliv = Strat(prob=transp, stool=OpenTuner(), stopfunc=None,
            args=baseargs+['--ntests','10','-t', locusdef['cobliv']+locusfilesuf],
            desc='cacheobliv')

    cacheobliv_fixed = Strat(prob=transp, stool=OpenTuner(), stopfunc=None,
            args=baseargs+['--ntests','10','-t', locusdef['fixed']+locusfilesuf],
            desc='cacheobliv_fixed')

    cacheobliv_bsearch = Strat(prob=transp, stool=OpenTuner(), stopfunc=None,
            args=baseargs+['--ntests','10','-t', locusdef['bsearch']+locusfilesuf],
            desc='cacheobliv_fixed_bsearch')

    rchoice_vert_vec = Strat(prob=transp, stool=OpenTuner(), stopfunc=None,
            args=baseargs+['--ntests','5','-t', locusdef['vertvec']+locusfilesuf],
            desc='rchoice-vert-vec')

    recargs = ['--timeout', '--exprec', '--approxexprec','100','--saveexprec']
    baseuplim = 10
    incuplim =  10
    upperlimvals = [x for x in range(baseuplim, len(shapes)*incuplim+baseuplim,incuplim)]
    basestopv = 15 
    incstopv = 5
    stopafvals = [x for x in range(basestopv, len(shapes)*incstopv+basestopv,incstopv)]
    reccmplxargs = {'--upper-limit': {l: r for l,r in zip(shapes,upperlimvals)} ,
                        '--stop-after': {l: r for l,r in zip(shapes, stopafvals)}}

    rchoice_vert_rec = Strat(prob=transp, stool=HyperOpt(), stopfunc=lambda x:x//8,
            args=baseargs + recargs + ['-t', locusdef['vertrec']+locusfilesuf],
            cmplxargs=reccmplxargs, desc='rchoice-vert-vec')

    rchoice_horiz = Strat(prob=transp, stool=HyperOpt(), stopfunc=lambda x: x//8,
            args=baseargs + recargs + ['-t', locusdef['horiz']+locusfilesuf],
            cmplxargs=reccmplxargs, desc='rchoice-horiz')

    stratdef = {'cobliv': cacheobliv, 'vertvec': rchoice_vert_vec, 
                'vertrec': rchoice_vert_rec, 'horiz': rchoice_horiz, 
                'fixed': cacheobliv_fixed, 'bsearch': cacheobliv_bsearch}

    #strategies += [cacheobliv, rchoice_vert_vec, rchoice_vert_rec, rchoice_horiz]
    strategies = [v for k,v in stratdef.items() if k in args.st]
    comps = [c for k,c in compdef.items() if k in args.cc]

    t1 = datetime.datetime.now()
    nthreads = args.nth
    infostr = (f"Exp: {args.exp} Runs: {args.nruns} Compilers: {comps} Threads:"
               f" {nthreads} Strategies selected: {', '.join([v.desc for v in strategies])}"
               f" {shapes}")
    log.info(f"{infostr} {t1}")

    # number of runs
    for nr in range(nruns):
        for nth in nthreads:
            if nth == 0:
                unsetEnv('OMP_NUM_THREADS')
            else:
                setEnv('OMP_NUM_THREADS', str(nth))
            #
            # strategies
            for strat in strategies:
                # comps
                for cxx, cc in comps:
                    setEnv('ICELOCUS_CXX', cxx)
                    setEnv('ICELOCUS_CC', cc)

                    # shapes 
                    for sh in shapes:
                        for shname, shval in zip(strat.prob.shapenames, sh):
                            setEnv(shname, str(shval))
                        ##
                        if strat.prob.stopname is not None and \
                                strat.stopfunc is not None:
                            setEnv(strat.prob.stopname, str(strat.stopfunc(sh[0])))
                        #
                        log.info(f"Searching:\n\trun= {nr}\n\tstrat= {strat}\n\tcomps= {cxx}"
                                 f"\n\tshapes= {sh}")
                        if not args.dryrun:
                            strat.exec(key=sh)
                    ##
                ##
            ##
        ##
    ##
    t2 = datetime.datetime.now()
    log.info(f"{infostr} {t2} lasted: {t2-t1}")
###

if __name__ == '__main__':
    runall()
#
