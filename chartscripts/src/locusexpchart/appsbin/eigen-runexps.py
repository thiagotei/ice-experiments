#!/usr/bin/env python
'''
@author: Thiago Teixeira
'''
import logging, argparse, os, tempfile, sys, locale
from locusexpchart.common.runexps.models import (Problem, Strat, OpenTuner,
                        HyperOpt, BaseCase)
from locusexpchart.common.runexps.utils import setEnv, unsetEnv, cmdexec
from locusexpchart.eigenproblem.utils import rescsvheader

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
console = logging.StreamHandler()
log.addHandler(console)

compdef = {'gcc': ('g++', 'gcc'),
           'icc': ('icpc', 'icc'), 
           'clang': ('clang++', 'clang')}
compkeys = tuple(k for k in compdef.keys())

parser = argparse.ArgumentParser(description="Run eigenproblem set of"
        " ice-locus experiments.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--cc", nargs='+', required=False,
        default='icc', #compkeys,
        choices=compkeys,
        help="Compilers to use.")
parser.add_argument("--st", nargs='+', required=False, 
        default=('cobliv', ),
        choices=('cobliv', 'coblivqrtd', 'coblivbisec', 
                'vertrec', 'horiz'),
        help="Strategies to run")
parser.add_argument('--runbase', required=False, default=False, 
                    action='store_true',
                    help='Only run the base cases, parse the results, and '
                         ' save in csv file (baseexps_*.csv) in current directory.')
parser.add_argument("--initv", required=False, default=256, type=int,
        help="Init value of shape sequence.")
parser.add_argument("--endv", required=False, default=2048, type=int,
        help="End value of shape sequence.")
parser.add_argument("--stepv", required=False, default=256, type=int,
        help="Step value of shape sequence.")
parser.add_argument('--nth', nargs='+', required=False, default=[1],
                    help='Number of threads.')
parser.add_argument('--dryrun', default=False, action='store_true',
        help="Run script but do not invoke ice-locus execution.")

args = parser.parse_args()

def parserBaseOutput(bufout, buferr):
    """ Receives buffers with stdout, stderr of the program executed as input.
    """
    try:
        # usually we ran this stuff where the function exists
        sys.path.append('.')
        import mytiming
    except ImportError:
        raise("Tried to load function that parsers "
              " the output that is provided to the search...")
    ##

    print(f"Output:\n{bufout}\n{buferr}")
    res = mytiming.getTiming(bufout, buferr)
    log.info(f"res: {res}")
    return res
###

def runall():

    nruns = 1
    minshape = args.initv #256
    maxshape = args.endv #2048
    stepshape = args.stepv #256
    shapes = [(x,x) for x in range(minshape, maxshape+1, stepshape)]
    #shapes = [(256,)]

    #nthreads = [x for x in range(0,18+1,2)]
    #nthreads = [0,3,6,9,12,15,18]
    #nthreads = [1,9,18]
    #nthreads = [0]
    nthreads = args.nth

    #comps = [('clang++-8','clang-8'), ('icpc','icc'), ('g++-6','gcc-6')]
    #comps = [('g++','gcc')]
    #comps = [('icpc','icc')]
    comps = [c for k,c in compdef.items() if k in args.cc]

    prob = Problem('eigen', 
                    ['ICELOCUS_MATSHAPE'], 'ICELOCUS_EIGEN_STOP')

    baseargs=['-o','suffix',
              '-f','eigen.cpp'] #, '--debug']

    if args.runbase:

        ############## Base strategies ##################

        cmdcc = ['make', 'clean','eigen.ice.exe']
        cmdrun = ['./eigen.ice.exe']
        base_qrtd = Strat(prob=prob, stool=BaseCase('eigen-base-qrtd.locus'), stopfunc=None,
                args=baseargs, desc="base_qrtd")

        base_bisec = Strat(prob=prob, stool=BaseCase('eigen-base-bisec.locus'), stopfunc=None,
                args=baseargs, desc="base_bisec")

        base_lapDC = Strat(prob=prob, stool=BaseCase('eigen-base-lapackDC.locus'), stopfunc=None,
                args=baseargs, desc="base_lapDC")

        strategies = [base_qrtd, base_bisec, base_lapDC]
        nth = 0
        varid = 1

        #temporary file with results
        tmpfd, tmppath = tempfile.mkstemp(dir='./', prefix='baseexps_', suffix='.csv')

        with open(tmpfd, 'w') as tmpf:
            log.info(f"Saving results in {tmppath}...")
            tmpf.write(','.join(rescsvheader)+'\n')
            for nth in nthreads:
                if nth == 0:
                    unsetEnv('OMP_NUM_THREADS')
                else:
                    setEnv('OMP_NUM_THREADS', str(nth))
                #

                for strat in strategies:
                    #gen code using locus
                    tmpf.write(f'#File: {strat.stool.locusfilename}\n')
                    if not args.dryrun:
                        strat.exec()
                    #

                    for cxx, cc in comps:
                        for sh in shapes:
                            #compile
                            setEnv('CXX', cxx)
                            setEnv('CC', cc)
                            #cmdexec(['CXX='+cxx,'CC='+cc]+cmdrun)
                            if not args.dryrun:
                                cmdexec(cmdcc)
                            #
                            #run
                            with tempfile.TemporaryFile() as fstdout, \
                                 tempfile.TemporaryFile() as fstderr:
                                if not args.dryrun:
                                    cmdexec(cmdrun+[str(sh[0]),'./inps/mat_'+str(sh[0])+'.inp'],
                                            fstdout, fstderr)
                                    # needs to get the default encoding as the tempfile saved using binary and
                                    # encoded.
                                    enc = locale.getpreferredencoding()
                                    log.info(f"Default encoding: {enc}")
                                    fstdout.seek(0)
                                    fstderr.seek(0)
                                    bufout = fstdout.read().decode(enc)
                                    buferr = fstderr.read().decode(enc)
                                    res = parserBaseOutput(bufout, buferr)
                                    if res is not None and 'exps' in res:
                                        for vtot, vcor in res['exps']:
                                            tmpf.write(f'{strat.desc},{cc},none,{nth},{varid},{sh[0]},{vtot},{vcor}\n')
                                        ##
                                    #
                                #
                            ##
                        ##
                    ##
                ##
            ##
            log.info(f"Saved results in {tmppath}!")
        ##
    else:

        ############## Search strategies ##################

        commonargs=baseargs+['--tfunc','mytiming.py:getTiming','--search']

        cobliv = Strat(prob=prob, stool=OpenTuner(), stopfunc=None,
                args=commonargs+['--ntests','20','-t','eigen-cacheobliv.locus'])

        cobliv_qrtd = Strat(prob=prob, stool=OpenTuner(), stopfunc=None,
                args=commonargs+['--ntests','10','-t','eigen-cacheobliv-qrtd.locus'])

        cobliv_bisec = Strat(prob=prob, stool=OpenTuner(), stopfunc=None,
                args=commonargs+['--ntests','10','-t','eigen-cacheobliv-bisec.locus'])

        recargs = ['--timeout', '--exprec', '--approxexprec','100','--saveexprec']

        baseuplim = 3
        incuplim =  3
        tmpval = baseuplim
        upperlimvals = [x for x in range(baseuplim, len(shapes)*incuplim+baseuplim,incuplim)]

        basestopv = 5
        incstopv = 3
        stopafvals = [x for x in range(basestopv, len(shapes)*incstopv+basestopv,incstopv)]

        reccmplxargs = {'--upper-limit': {l: r for l,r in zip(shapes,upperlimvals)} ,
                        '--stop-after': {l: r for l,r in zip(shapes, stopafvals)}}

        log.info(f"reccmplxargs: {reccmplxargs}")

        rchoice_vert_rec = Strat(prob=prob, stool=HyperOpt(), stopfunc=lambda x: x//16,
                args= commonargs + recargs + ['-t', 'eigen-vertrec.locus'],
                cmplxargs=reccmplxargs, desc='rchoice-vert-rec')

        rchoice_horiz = Strat(prob=prob, stool=HyperOpt(), stopfunc=lambda x: x//16,
                args= commonargs + recargs + ['-t', 'eigen-horiz.locus'],
                cmplxargs=reccmplxargs, desc='rchoice-horiz')

        stratdef = {'cobliv': cobliv, 'coblivqrtd': cobliv_qrtd, 
                'coblivbisec': cobliv_bisec, 'vertrec': rchoice_vert_rec,
                'horiz': rchoice_horiz}

        strategies = [v for k,v in stratdef.items() if k in args.st]
        #strategies += [rchoice_vert_rec, rchoice_horiz]

        # number of runs
        for nr in range(nruns):
            for nth in nthreads:
                if nth == 0:
                    unsetEnv('OMP_NUM_THREADS')
                else:
                    setEnv('OMP_NUM_THREADS', str(nth))
                #
                # strategies
                for strat in strategies:
                    # comps
                    for cxx, cc in comps:
                        setEnv('ICELOCUS_CXX', cxx)
                        setEnv('ICELOCUS_CC', cc)

                        # shapes 
                        for sh in shapes:
                            for shname, shval in zip(strat.prob.shapenames, sh):
                                setEnv(shname, str(shval))
                            ##
                            if strat.prob.stopname is not None and \
                                    strat.stopfunc is not None:
                                setEnv(strat.prob.stopname, str(strat.stopfunc(sh[0])))
                            #
                            log.info(f"Searching run= {nr} strat= {strat} comps= {cxx}"
                                     f" shapes= {sh} nth= {nth}")

                            if not args.dryrun:
                                strat.exec(key=sh)
                            #
                        ##
                    ##
                ##
            ##
        ##
    ## if args.runbase
###

if __name__ == '__main__':
    runall()
#
