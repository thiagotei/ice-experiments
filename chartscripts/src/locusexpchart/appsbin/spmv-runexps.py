#!/usr/bin/env python
'''
@author: Thiago Teixeira
'''

import logging, argparse, os, datetime
from locusexpchart.common.runexps.models import (Problem, Strat, OpenTuner,
                        HyperOpt)
from locusexpchart.common.runexps.utils import setEnv, unsetEnv
from locusexpchart.mattransp.utils import genOrigMTranspArgc

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
console = logging.StreamHandler()
log.addHandler(console)

parser = argparse.ArgumentParser(description="Run matrix transpose set of"
        " ice-locus experiments.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--st", nargs='+', required=False, 
        default=('evalfmt48',),
        choices=('evalfmt', 'evalfmt48', 'horiz', 'cobliv', 'h2by2', 'h3by3'),
        help="Strategies to run")
parser.add_argument("--cc", nargs='+', required=False,
        default=('icc',),
        choices=('gcc','icc','clang'),
        help="Compilers to use.")
parser.add_argument('--dryrun', default=False, action='store_true',
        help="Run script but do not invoke ice-locus execution.")
parser.add_argument("--deb", default=False, action='store_true',
        help="Add debug flag to the search.")
parser.add_argument('--inp', nargs='+', required=True,
        help="Input matrices in matlab format (.mat)")
parser.add_argument("--nth", nargs='+', required=False, default=[1],
        help="Number of OMP threads")

args = parser.parse_args()

def runall():
    locusdef = {'evalfmt': 'spmv-evalfmt.locus',
                'evalfmt48': 'spmv-evalfmt-48.locus',
                'cobliv' : 'spmv-cacheobliv.locus',
                'horiz' : 'spmv-horiz.locus',
                'h2by2' : 'spmv-2by2.locus',
                'h3by3' : 'spmv-3by3.locus'}

    compdef = {'gcc': ('g++-6', 'gcc-6'),
               'icc': ('icpc', 'icc'), 
               'clang': ('clang++-8', 'clang-8')}

    spmv = Problem('spmv',
                    ['ICELOCUS_SPMAT'],
                    'ICELOCUS_STOPBLKS')

    baseargs=['--tfunc','mytiming.py:getTiming', '-o','suffix','--search']

    if args.deb:
        baseargs.append('--debug')
    #

    evalfmt = Strat(prob=spmv, stool=OpenTuner(), stopfunc=None,
            args=['-f', 'spmv.cpp']+baseargs+['--ntests','15','-t', 
                locusdef['evalfmt'],'--stop-after','60'],
            desc='evalfmt')

    evalfmt48 = Strat(prob=spmv, stool=OpenTuner(), stopfunc=None,
            args=['-f', 'spmv48.cpp']+baseargs+['--ntests','15','-t', 
                locusdef['evalfmt48'],'--stop-after','60'],
            desc='evalfmt48')

    cobliv = Strat(prob=spmv, stool=OpenTuner(), stopfunc=None,
            args=['-f', 'spmvt3.cpp']+baseargs+['--ntests','30','-t', 
                locusdef['cobliv']],
            desc='cobliv')

    h2by2 = Strat(prob=spmv, stool=OpenTuner(), stopfunc=None,
            args=['-f', 'spmvt3.cpp']+baseargs+['--ntests','1500','-t', 
                locusdef['h2by2']],
            desc='h2by2')

    h3by3 = Strat(prob=spmv, stool=OpenTuner(), stopfunc=None,
            args=['-f', 'spmvt3.cpp']+baseargs+['--ntests','2000','-t', 
                locusdef['h3by3']],
            desc='h3by3')

    horiz = Strat(prob=spmv, stool=HyperOpt(), stopfunc=None,
            args=['-f', 'spmvt3.cpp']+baseargs+['--ntests', '200',
                '--timeout', '-t', locusdef['horiz'], '--equery',
                '--exprec', '--approxexprec', '100','--saveexprec'],
            desc='horiz')

    stratdef = {'evalfmt': evalfmt, 'evalfmt48': evalfmt48,
            'cobliv': cobliv, 'horiz': horiz, 'h2by2': h2by2,
            'h3by3': h3by3}

    strategies = [v for k,v in stratdef.items() if k in args.st]
    comps = [c for k,c in compdef.items() if k in args.cc]

    t1 = datetime.datetime.now()
    nthreads = args.nth
    infostr = (f"Compilers: {comps} Threads:"
               f" {nthreads} Strategies selected: {', '.join([v.desc for v in strategies])}"
               f" {args.inp}")
    log.info(f"{infostr} {t1}")
    for nth in nthreads:
        if nth == 0:
            unsetEnv('OMP_NUM_THREADS')
        else:
            setEnv('OMP_NUM_THREADS', str(nth))
        #

        for strat in strategies:
            # comps
            for cxx, cc in comps:
                setEnv('ICELOCUS_CXX', cxx)
                setEnv('ICELOCUS_CC', cc)
                for inp in args.inp:
                    setEnv('ICELOCUS_SPMAT', inp)
                    stopblks = 4
                    setEnv('ICELOCUS_STOPBLKS', str(stopblks))
                    log.info(f"Searching: nth= {nth} strat= {strat} comp= {cxx} inp= {inp} *stop= {stopblks}")
                    if not args.dryrun:
                        strat.exec()
                ##
            ##
        ##
    ##

    t2 = datetime.datetime.now()
    log.info(f"{infostr} {t2} lasted: {t2-t1}")
###

if __name__ == '__main__':
    runall()
#


