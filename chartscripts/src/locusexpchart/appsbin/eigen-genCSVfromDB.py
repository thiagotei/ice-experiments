#!/usr/bin/env python

from os import path
import argparse, sys, logging


from locusexpchart.common.charts.dbutils import (getDBBestVar, 
        getAllDBexpsOPT, printDBCfgs)
from locusexpchart.eigenproblem.utils import rescsvheader

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console = logging.StreamHandler()
log.addHandler(console)

parser = argparse.ArgumentParser(description="")
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-l', type=str, required=True, nargs='+',
        help="Locus programs related info from DB.")
parser.add_argument('--inccfg', default=False, action='store_true',
        help="Add cfg DB id to the 1st field.")

args=parser.parse_args()

def genCSVfromDB(dbinp, locusfiles):
    #res_gen = getDBBestVar(dbinp, locusfiles, 
    res_gen = getAllDBexpsOPT(dbinp, locusfiles,
            ['ICELOCUS_CC','ICELOCUS_MATSHAPE','OMP_NUM_THREADS'])

    print(",".join(rescsvheader))

    if args.inccfg:
        printDBCfgs(dbinp, locusfiles)
    #

    # Variant and experiments loop
    for metainfo, var, exp, expvals in res_gen:
        #print(f"tmp: {type(tmp), {tmp}}")
        lfname = metainfo['lfname']
        comp = metainfo['ICELOCUS_CC']
        shape = metainfo['ICELOCUS_MATSHAPE'] 
        nth = metainfo['OMP_NUM_THREADS']
        setool = metainfo['searchtool']

        if args.inccfg:
            lfname += "_"+str(var.configuration.id)
        #
        metric=expvals.metric
        desc=expvals.desc
        if metric != float('inf') and expvals.desc == 'Total':
            #print(f"{lfname},{comp},{setool},{nth},{var.id},{shape},{descdict['Total']},{descdict['Core']}")
            print(f"{lfname},{comp},{setool},{nth},{var.id},{shape},{metric}")
        elif metric == float('inf'):
            print(f"Found inconsistent expval! Likely ok to skip. varid:"
                    f" {var.id} expval: {exp.id} metric: {metric}", file=sys.stderr)
        #

        #descdict = {ev.desc: ev.metric for ev in expvals}
        ##print(f"exp: {exp.id} expvals: {descdict}")
        #print(f"{lfname},{comp},{setool},{nth},{var.id},{shape},{descdict['Total']},{descdict['Core']}")
    #
###

if __name__ == '__main__':
    genCSVfromDB(args.i, args.l)
#
