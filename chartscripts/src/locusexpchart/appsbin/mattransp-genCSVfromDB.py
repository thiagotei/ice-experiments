#!/usr/bin/env python

from os import path
import argparse, sys, logging

#print(f"Before loading dbutils from {__name__}")

from locusexpchart.common.charts.dbutils import (getDBBestVar, getAllDBexps,
        getAllDBexpsOPT, printDBCfgs)

#print(f"After loading dbutils from {__name__}")
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console = logging.StreamHandler()
log.addHandler(console)

parser = argparse.ArgumentParser(description="Get experimental data from db for mattransp"
        " benchmark accordingo the Locus files given.")
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-l', type=str, required=True, nargs='+',
        help="Locus programs related info from DB.")
parser.add_argument('--inccfg', default=False, action='store_true',
        help="Add cfg DB id to the 1st field.")

args=parser.parse_args()

def genCSVfromDB(dbinp, locusfiles):
    #res_gen = getDBBestVar(dbinp, locusfiles,
    #res_gen = getAllDBexps(dbinp, locusfiles,
    res_gen = getAllDBexpsOPT(dbinp, locusfiles,
            ['ICELOCUS_CC','ICELOCUS_MATSHAPE_M','ICELOCUS_MATSHAPE_N','OMP_NUM_THREADS'])

    print(f"leg,comp,searchtool,numthreads,run,matshapeM,matshapeN,time")

    printDBCfgs(dbinp, locusfiles)

    # Variant and experiments loop
    for metainfo, var, exp, expvals in res_gen:
        #print(f"tmp: {type(tmp), {tmp}}")
        lfname = metainfo['lfname']
        comp = metainfo['ICELOCUS_CC']
        shapeM = metainfo['ICELOCUS_MATSHAPE_M'] 
        shapeN = metainfo['ICELOCUS_MATSHAPE_N'] 
        nth =  metainfo.get('OMP_NUM_THREADS', '0')
        setool = metainfo['searchtool']
        #print(f"Cfg: {var.configuration.id} {var.configuration.data}")
        if args.inccfg:
            lfname += "_"+str(var.configuration.id)
        #
        #if var.configuration.data[310]  == 0:
        #    lfname += "_128"
        #else: 
        #    lfname += "_512"
        #
        metric=expvals.metric
        if metric != float('inf'):
            print(f"{lfname},{comp},{setool},{nth},{var.id},{shapeM},{shapeN},{metric}")
        #print(f"{[(ev.desc,ev.metric) for ev in expvals]}")
        #print(f"{lfname},{comp},{setool},{nth},{var.id},{shapeM},{shapeN}")
        #descdict = {ev.desc: ev.metric for ev in expvals}
        #if 'Total' in descdict:
        #    print(f"{lfname},{comp},{setool},{nth},{var.id},{shapeM},{shapeN},{descdict['Total']}")
        #    #print(f"{lfname},{comp},{setool},{nth},{var.id},{shapeM},{shapeN}")
        else:
            #print(f"Found inconsistent expval! {descdict} likely ok to skip. varid:"
            print(f"Found inconsistent expval! Likely ok to skip. varid:"
                    f" {var.id} expval: {exp.id} metric: {metric}", file=sys.stderr)
        #
    #
###

if __name__ == '__main__':
    genCSVfromDB(args.i, args.l)
#

