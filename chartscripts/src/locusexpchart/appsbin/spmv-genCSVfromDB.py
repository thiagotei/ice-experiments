#!/usr/bin/env python

from os import path
import argparse, sys, logging, yaml

#print(f"Before loading dbutils from {__name__}")

from locusexpchart.common.charts.dbutils import (getDBBestVar, 
    getAllDBexpsOPT, printDBCfgs)

#print(f"After loading dbutils from {__name__}")
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console = logging.StreamHandler()
log.addHandler(console)

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-l', type=str, required=True, nargs='+',
        help="Locus programs related info from DB.")
parser.add_argument('--inccfg', default=False, action='store_true',
        help="Add cfg DB id to the 1st field.")
parser.add_argument('--cfgdic', type=str, default=None,
        help="Yaml file with a label for each cfg.")
args=parser.parse_args()

def genCSVfromDB(dbinp, locusfiles):
    #res_gen = getDBBestVar(dbinp, locusfiles, 
    res_gen = getAllDBexpsOPT(dbinp, locusfiles, 
            ['ICELOCUS_CC','ICELOCUS_SPMAT','ICELOCUS_SPMV','OMP_NUM_THREADS'])

    print(f"leg,comp,searchtool,numthreads,run,mat,time")

    if args.inccfg:
        printDBCfgs(dbinp, locusfiles)
        if args.cfgdic is not None:
            with open(args.cfgdic)  as yf:
                cfgdic = yaml.full_load(yf)
                print("#cfginc: "+str(cfgdic))
            ##
        #
    #

    # Variant and experiments loop
    for metainfo, var, exp, expvals in res_gen:
        #print(f"tmp: {type(tmp), {tmp}}")
        lfname = metainfo['lfname']
        comp = metainfo['ICELOCUS_CC']
        fullshape = metainfo.get('ICELOCUS_SPMAT')
        if fullshape is None:
            fullshape = metainfo.get('ICELOCUS_SPMV') # old files had this
            if fullshape is None:
                raise ValueError('Error! Could not find ICELOCUS_SPMAT nor ICELOCUS_SPMV!')
            #
        #
        # Remove the path and the extension
        shape = path.splitext(path.basename(fullshape))[0]
        nth =  metainfo.get('OMP_NUM_THREADS', '0')
        setool = metainfo['searchtool']

        if args.inccfg:
            lfname += "_"+str(var.configuration.id)
            if args.cfgdic is not None:
                cfg = var.configuration.data
                intersec = {k:cfgdic[k][v] for k,v in cfg.items() }
                #print("intersec",intersec, cfgdic['fmt'].format(intersec) )
                print(f"#{lfname}")
                lfname = cfgdic['fmt'].format(intersec)
            #
        #

        #descdict = {ev.desc: ev.metric for ev in expvals}
        #print(f"{lfname},{comp},{setool},{nth},{var.id},{shape},{shape},{shape},{descdict['Total']}")

        metric=expvals.metric
        if metric != float('inf'):
            print(f"{lfname},{comp},{setool},{nth},{var.id},{shape},{metric}")
        else:
            print(f"Found inconsistent expval! Likely ok to skip. varid:"
                    f" {var.id} expval: {exp.id} metric: {metric}", file=sys.stderr)
        #

    #
###

if __name__ == '__main__':
    genCSVfromDB(args.i, args.l)
#



