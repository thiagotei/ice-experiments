'''
@author: Thiago Teixeira
'''

import os
from locusexpchart.common.runexps.utils import cmdexec


def genOrigAnswer(origfilename=None, dstfilename=None, 
                  bldcmd=None, runcmd=None):
    #origprefix = origprefix or 'answer-mattransp-'
    #dstprefix = dstprefix or "orig-answer-mattransp-"

    cmdexec(bldcmd)
    cmdexec(runcmd)

    os.rename(origfilename, dstfilename)
    ##
###

def genOrigMTranspArgc(shapes):
    if not os.path.isfile('.test'):
        open('.test','a').close()
    #
    for sh in shapes:
        suf="-".join([str(v) for v in sh])+".txt"
        bldcmd = ['make','ORIG=mattransp','CXX=g++','CC=gcc',
                'USERDEFS=-DM='+str(sh[0])+' -DN='+str(sh[1])+'','clean','orig']
        runcmd = ['./orig']+[str(v) for v in sh]
        origfilename = "answer-mattransp-"+suf
        dstfilename = "orig-answer-mattransp-"+suf
        if not os.path.isfile(dstfilename):
            genOrigAnswer(origfilename, dstfilename, bldcmd, runcmd)
        #
    ##
###

