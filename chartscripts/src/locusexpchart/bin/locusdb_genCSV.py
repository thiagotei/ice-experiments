#!/usr/bin/env python3

from os import path
import argparse, sys, logging, statistics as stat
import pandas as pd

#print(f"Before loading dbutils from {__name__}")

from locusexpchart.common.charts.dbutils import (getDBBestVar, 
    getAllDBexpsOPTAllvars, printDBCfgs, getLocusProgram, getAllDescMetrics)
from locus.frontend.optlang.locus.locusparser import LocusParser
import locus.tools.search.searchbase as searchbase
from locus.backend.genlocus import GenLocus
from locus.frontend.optlang.locus.optimizer import replEnvVars

#print(f"After loading dbutils from {__name__}")
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console = logging.StreamHandler()
log.addHandler(console)

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-l', type=str, required=True, nargs='+',
        help="Locus programs related info from DB.")
parser.add_argument('--inccfg', default=False, action='store_true',
        help="Add cfg DB id to the 1st field.")
parser.add_argument('--genLocus', default=False, action='store_true',
        help="Generate Locus program representing "
             "0th, 25th, 50th, 75th and 100th percentile for each shape.")
parser.add_argument('--allmetrics', default=False, action='store_true',
        help="Save all metrics in the output. Replace any use of --metrics.")
parser.add_argument('--metrics', type=str, default=("Time",), nargs='*',
                    help="Metrics name to be on the ouput. "
                         "Remember that experiments can have multiple measurements."
                         "First metric is the one used to select the percentile values for --genLocus.")

args=parser.parse_args()

def genCSVfromDB(dbinp, locusfiles):
    if args.inccfg and args.genLocus:
        raise RuntimeError("It is either --inccfg or --genLocus, not both!")
    #

    if args.inccfg or args.genLocus:
        printDBCfgs(dbinp, locusfiles)
    #

    # Store results for same shape
    shapeDict = {}

    # Store configuration data
    cfgData = {}

    headstr = None
    envvarsalias = {}
    envvarsorder = []
    envdict = {}

    if args.allmetrics:
        all_dm = getAllDescMetrics(dbinp, locusfiles)
        l1 = None
        seid1 = None
        lf1 = None
        # Compare if the metrics are the same, if not abort because the 
        # metrics will be in output header and the data will be inconsistent.
        for lf, se_id, uniq_dms in all_dm:
            # result is a list of tuples
            clist = [e[0] for e in uniq_dms]
            if l1 is None:
                l1 = clist
                seid1 = se_id
                lf1 = lf
            else:
                if l1 != clist:
                    raise RuntimeError(f"Different metrics for: {lf1} vs {lf} search: {seid1} vs {se_id}\n{l1}\n  !=\n{lf1}")
            #   #
        ##
        metricsout = l1
    else:
        metricsout = args.metrics
    #

    print("# Metrics on the output:", metricsout)

    res_gen = getAllDBexpsOPTAllvars(dbinp, locusfiles)

    # Variant and experiments loop
    for metainfo, var, exp, expvals in res_gen:
        #print(f"tmp: {type(tmp), {tmp}}")
        lfname = metainfo['lfname']
        setool = metainfo['searchtool']
        searchid = metainfo['searchid']
        hostname = metainfo['hostname']
        cfgid = var.configuration.id
        #print("Data:", var.configuration.data)

        envdict = metainfo['envdict']
        if headstr is None:
            #Only execute in the first one
            for vname in metainfo['envdict'].keys():
                envvarsorder.append(vname)
                #remove the prefix and put them lowercase
                envvarsalias[vname] = vname.replace('ICELOCUS_','').lower()
            #
            envvarsorder.sort()
            headstr = "leg,searchid,searchtool,hostname,"
            headstr += ",".join([envvarsalias[k] for k in envvarsorder ])
            headstr += ",cfgid,runid,"+','.join(metricsout)
            print(headstr)
            #print(f"leg,comp,searchtool,numthreads,matshapeM,matshapeN,matshapeK,cfgid,runid,time")
        else:
            tmp = []
            # just to check that the vars are the same.
            for vname in metainfo['envdict'].keys():
                #remove the prefix and put them lowercase
                tmp.append(vname)
            ##
            tmp.sort()
            if tmp != envvarsorder:
                raise RuntimeError("Error! New different set of envvars! Why is that?")
            #
        #

        if cfgid not in cfgData:
            cfgData[cfgid] = var.configuration.data
        else:
            if cfgData[cfgid] != var.configuration.data:
                raise RuntimeError(f"Cfdid {cfgid} presented inconsistent data "
                                   f"{cfgData[cfgid]} !!!=== var.configuration.data")
            #
        #

        #if args.inccfg:
        #    lfname += "_"+str(var.configuration.id)
        #

        #descdict = {ev.desc: ev.metric for ev in expvals}
        #print(f"{lfname},{comp},{setool},{nth},{var.id},{shape},{shape},{shape},{descdict['Total']}")
        
        # Remembe! there are more than one metric for each experiment
        metric= expvals.metric
        mdesc = expvals.desc
        #print(type(metric), expvals.desc)
        if metric != float('inf'):
            #print(f"metric: {metric}")
            varTuple = var.id
            #print(f"{lfname},{comp},{setool},{nth},{shapeM},{shapeN},{shapeK},{var.id},{metric}")
            shapeTuple = (lfname, searchid, setool, hostname) + tuple(envdict[k] for k in envvarsorder) #(lfname, comp, setool, nth, shapeM, shapeN, shapeK)
            #print(f"   ShapeTuple? {shapeTuple}")

            if shapeTuple not in shapeDict:
                # create a dict that saves the run with the fastest median metric
                shapeDict[shapeTuple] = {}
            ##
            if cfgid not in shapeDict[shapeTuple]:
                shapeDict[shapeTuple][cfgid] = {}
            ##
            if var.id not in shapeDict[shapeTuple][cfgid]:
                shapeDict[shapeTuple][cfgid][varTuple] = {}
            ##
            if mdesc not in shapeDict[shapeTuple][cfgid][varTuple]:
                shapeDict[shapeTuple][cfgid][varTuple][mdesc] = []
            ##
            shapeDict[shapeTuple][cfgid][varTuple][mdesc].append(metric)
        else:
            print(f"Found inconsistent expval! Likely ok to skip. varid:"
                    f" {var.id} expval: {exp.id} metric: {metric}", file=sys.stderr)
        #
    #

    shapeDictMedian = {}
    # Get the median values of the runs of each shape
    for k, v in shapeDict.items():
        newk = k
        # Each configuration
        for k_cfg, v_cfg in v.items():
            # if result per configuration, add id to the key
            if args.inccfg:
                #print("v->",v)
                cfgid = k_cfg
                newk = k + (cfgid,)
            #
            if newk not in shapeDictMedian:
                shapeDictMedian[newk] = []
            #
            # Each metric of the experiment
            for k_run, v_var in v_cfg.items():
                # Go through metrics
                v_metrics = []
                #for k_m, v_m in v_var.items():
                for k_m in metricsout:
                    #print("k_m:",k_m, v_var.keys(), k_m in v_var)
                    #if k_m in args.metrics:
                    if k_m in v_var:
                        # for each run and metric get the median
                        v_run = v_var[k_m]
                        #print("newk: ", newk, k_run, k_m, v_run )
                        med = stat.median(v_run)
                        #v_metrics.append(k_m)
                        v_metrics.append(med)
                    else:
                        raise RuntimeError(f"Metric {k_m} not found in {v_var.keys()}!")
                    #
                ##
                shapeDictMedian[newk].append((k_cfg, k_run)+tuple(v_metrics))
            ##
        ##
    ##

    import numpy as np
    # Get best for each shape
    for k,v in shapeDictMedian.items():
        #print("type(v): ",type(v),"k:",k,"v:",v)
        #cfgid, runid, metricdesc, min_, *otherms = min(v, key = lambda t: t[2])
        cfgid, runid, min_, *otherms = min(v, key = lambda t: t[2])
        #print("rest:",otherms)

        if args.inccfg:
            # In inccfg cfgid is already part of k
            print(f"{','.join(str(vn) for vn in k)},{runid},{min_},{','.join(str(s) for s in otherms)}")
        else:
            print(f"{','.join(str(vn) for vn in k)},{cfgid},{runid},{min_},{','.join(str(s) for s in otherms)}")

        # Print out the Locus program to generate the quantile variants.
        if args.genLocus:
            # f[2] is the time
            arr = np.asarray([f[3] for f in v])
            print("Total number of configurations:", len(arr))
            cfgtogen = []
            cfgalias = {}
            cfgtime = {}
            if len(arr) > 5:
                quantPerc = [0.0,0.25,0.5,0.75,1.0]
                # Get 5 quantiles
                quant = np.quantile(arr, quantPerc)
                # Get indices of the 5 quantiles
                idx = [(np.abs(arr - i)).argmin() for i in quant]
                cfgtogen = [v[i][0] for i in idx]
                # cfgid -> cfg time
                cfgtime = {v[i][0] : v[i][3] for i in idx}
                # cfd id -> string representing cfg
                cfgalias = {c : str(c)+"_"+str(p)+"Perc" for c, p in zip(cfgtogen,reversed(quantPerc))}
            else:
                # Just gen Locus programs of all, 5 or less, configurations.
                cfgtogen = [c[0] for c in v]
                # cfg id -> cfg time
                cfgtime = {c[0] : c[3] for c in v}
                # cfd id -> string representing cfg
                cfgalias = {c : str(c) for c in cfgtogen}
            #
            print("Selected configurations:", cfgtogen)
            lprog = getLocusProgram(dbinp, cfgtogen)
            # Iterate over locus program for each cfgid
            for c, l in lprog:
                #locusfilename = l.locusfilename
                locuscode = l.data
                # Put through Locus parser
                lp = LocusParser()
                locustree = lp.parse(locuscode, False)
                searchnodes = {}
                replEnvVars(locustree, envdict)
                fakeparser = argparse.ArgumentParser()
                fakeparser.add_argument('--debug', required=False, action='store_true',
                                        default=False)
                fakeargs = fakeparser.parse_args([])
                searchbase.applyDFAOpts(locustree, fakeargs)
                searchbase.buildSearchNodes(locustree.topblk, searchnodes)
                for name, ent in locustree.items():
                    searchbase.buildSearchNodes(ent, searchnodes)
                ##
                # Change it to the respective configuration
                print("Cfg:", c, cfgData[c], cfgtime[c])
                revertse = {}
                searchbase.convertDesiredResult(searchnodes, cfgData[c], revertse)
                # Print it out
                fname = '_'.join(str(n) for n in k)+"_cfg_"+str(cfgalias[c])+".locus" #+"_"+locusfilename
                GenLocus.searchmode(locustree, fname)
            ##
        ##
    ##
###

def main():
    try:
        genCSVfromDB(args.i, args.l)
    except Exception as e:
        print(f"Error: {e}", file=sys.stderr)
        sys.exit(1)

if __name__ == '__main__':
    main()
#

