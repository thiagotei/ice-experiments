#!/usr/bin/env python

from os import path
import argparse, sys, logging

#print(f"Before loading dbutils from {__name__}")

from locusexpchart.common.charts.dbutils import (getSearchTimeStats)


log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console = logging.StreamHandler()
log.addHandler(console)

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-a', default=False, action='store_true', help="Print all the records")

args=parser.parse_args()


def getAll(dbinp, printall):
    getSearchTimeStats(dbinp, printall)

def main():
    getAll(args.i, args.a)

if __name__ == '__main__':
    getAll(args.i, args.a)
#

