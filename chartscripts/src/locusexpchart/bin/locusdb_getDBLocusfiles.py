#!/usr/bin/env python

from os import path
import argparse, sys, logging

#print(f"Before loading dbutils from {__name__}")

from locusexpchart.common.charts.dbutils import getDBlocusfiles

parser = argparse.ArgumentParser(description="Get locus files data from db "
        " accordingo to the Locus files given.")
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-l', type=str, required=True, nargs='+',
        help="Locus programs related info from DB.")

args=parser.parse_args()

def main():
    getDBlocusfiles(args.i, args.l)

if __name__ == '__main__':
    getDBlocusfiles(args.i, args.l)
#
