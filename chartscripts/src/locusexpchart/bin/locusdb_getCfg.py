#!/usr/bin/env python

from os import path
import argparse, sys, logging

from locusexpchart.common.charts.dbutils import getCfgfromId

parser = argparse.ArgumentParser(description="Print configuration based on its id.")
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-c', type=int, required=True, help="configuration id number")

args=parser.parse_args()

def main():
    getCfgfromId(args.i, args.c)

if __name__ == '__main__':
    getCfgfromId(args.i, args.c)
#
