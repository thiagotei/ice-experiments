#!/usr/bin/env python

import sys, argparse, logging, datetime
import statistics as stat

try:
    import locus.tools.search.resultsdb.models as m
    from locus.tools.search.resultsdb.connect import connect
    from locus.tools.search.resultsdb.misc import hashlocusfile
except ImportError:
    raise("Load virtual env with ice-locus installed to use this script!!")
#

log = logging.getLogger(__name__)
#print(f"Get logger from {__name__}")

def dbnameparse(dbname):
    if '://' not in dbname:
        newdbname = 'sqlite:///' + dbname
    #
    return newdbname
###

def getAllDBexps(dbinp, locusfiles, envinfo=None):
    """ Returns all experiments from searches that matches the locusfiles.
    """
    dbname = dbnameparse(dbinp)
    debug = False
    engine, session = connect(dbname, debug)

    for lf in locusfiles:
        tgthash,_ = hashlocusfile(lf)
        #log.info(f"EITA File: {lf} hash: {tgthash}")
        print(f"#File: {lf} hash: {tgthash}")
        x = session.query(m.Search,m.LocusFile.locusfilename).join(m.LocusFile).filter(m.LocusFile.hash == tgthash)
        #print(f"{x}\n")

        for se, lfname in x:
            retinfo = {'lfname': lfname, 'searchtool': se.searchtool}
            if envinfo is not None:
                for evalue in se.envvalues:
                    #print(f"envvar: {evalue} {evalue.envvar}")
                    varname = evalue.envvar.name
                    if varname in envinfo and varname not in retinfo:
                            retinfo[varname] = evalue.value
                        #
                    #
                ##
            #
            for v in se.variants:
                #emetrics = [e.metric for e in v.experiments]
                for e in v.experiments:
                    yield retinfo, v, e, e.expvalues
            ##
        ##
    ##

    session.close()
###

def saveretinf(se, lfname, envinfo):
    retinfo = {'lfname': lfname, 'searchtool': se.searchtool}
    if envinfo is not None:
        for evalue in se.envvalues:
            #print(f"envvar: {evalue} {evalue.envvar}")
            varname = evalue.envvar.name
            if varname in envinfo and varname not in retinfo:
                    retinfo[varname] = evalue.value
                #
            #
        ##
    #
    return retinfo
###


def getAllDescMetrics(dbinp, locusfiles, envinfo=None, debugdb=False):
    """ Returns all the metrics from the searches that matches the locusfiles.
    """
    dbname = dbnameparse(dbinp)
    engine, session = connect(dbname, debugdb)

    for lf in locusfiles:
        tgthash,_ = hashlocusfile(lf)
        #log.info(f"EITA File: {lf} hash: {tgthash}")
        print(f"#File: {lf} hash: {tgthash}")
        x = session.query(m.Search,m.LocusFile.locusfilename).join(m.LocusFile).filter(m.LocusFile.hash == tgthash)
        #print(f"{x}\n")

        for se, lfname in x:
            retinfo = saveretinf(se, lfname, envinfo)

            unique_descs = session.query(m.ExpValue.desc
                    ).filter(m.Variant.searchid == se.id
                    ).filter(m.Variant.id == m.Experiment.variantid
                    ).filter(m.Experiment.id == m.ExpValue.expid
                    ).distinct().all()
            #print(f"{lf} search: {se.id} unique descs: {unique_descs}")
            yield lf, se.id, unique_descs
            #for desc in unique_descs:
            #    print(f"Unique {lf} desc: {desc}")
            ##
        ##
    ##
    session.close()
###


def getAllDBexpsOPT(dbinp, locusfiles, envinfo=None, debugdb=False):
    """ Returns all experiments from searches that matches the locusfiles.
        This is much faster but returns one expvalue per yield.
        CURRENTLY ONLY WORKS for one timer, i.e., one expvalue per
        experiment.!!!
    """
    dbname = dbnameparse(dbinp)
    engine, session = connect(dbname, debugdb)

    for lf in locusfiles:
        tgthash,_ = hashlocusfile(lf)
        #log.info(f"EITA File: {lf} hash: {tgthash}")
        print(f"#File: {lf} hash: {tgthash}")
        x = session.query(m.Search,m.LocusFile.locusfilename).join(m.LocusFile).filter(m.LocusFile.hash == tgthash)
        #print(f"{x}\n")

        for se, lfname in x:
            retinfo = saveretinf(se, lfname, envinfo)

            #res = session.query(m.Variant,m.Experiment,m.ExpValue).join(m.Experiment).join(m.ExpValue).filter(m.Variant.searchid == se.id)
            # This was way faster. Each acess to a obj field that represents
            # another table means an select to the query database. For a lot of
            # data it becomes very slow!
            res = session.query(m.Variant,m.Experiment,m.ExpValue
                    ).filter(m.Variant.searchid == se.id
                    ).filter(m.Variant.id == m.Experiment.variantid
                    ).filter(m.Experiment.id == m.ExpValue.expid)
            for v, e, evalu in res:
                #yield retinfo, v, e, e.expvalues
                #print(f"{v} | {e} | {evalu} | {evalu.metric}")
                yield retinfo, v, e, evalu
            ##
        ##
    ##
    session.close()
###

def saveenvvars(se, lfname):
    retinfo = {'lfname': lfname, 'searchtool': se.searchtool}
    envvars = {}
    for evalue in se.envvalues:
        varname = evalue.envvar.name
        #print(f"  Envvar name: {varname} value: {evalue.value} ")
        if varname not in envvars:
            envvars[varname] = evalue.value 
        else:
            raise RuntimeError(f"Error getting env vars! Multiple values for {varname}!")
        #
    #
    retinfo['envdict'] = envvars
    return retinfo
###

def getAllDBexpsOPTAllvars(dbinp, locusfiles, envinfo=None, debugdb=False):
    """ Returns all experiments from searches that matches the locusfiles.
        This is much faster but returns one expvalue per yield.
        (Thu Jan 16 11:34:01 AM PST 2025 I dont think that's true anymore...
         CURRENTLY ONLY WORKS for one timer, i.e., one expvalue per
        experiment.!!!)
    """
    dbname = dbnameparse(dbinp)
    engine, session = connect(dbname, debugdb)

    for lf in locusfiles:
        tgthash,_ = hashlocusfile(lf)
        #log.info(f"EITA File: {lf} hash: {tgthash}")
        print(f"#File: {lf} hash: {tgthash}")
        x = session.query(m.Search,m.LocusFile.locusfilename).join(m.LocusFile).filter(m.LocusFile.hash == tgthash)
        #print(f"{x}\n")

        for se, lfname in x:
            #print(f" Search {se.id}: ")
            retinfo = saveenvvars(se, lfname)
            retinfo['hostname'] = se.hostname
            retinfo['searchid'] = se.id

            #res = session.query(m.Variant,m.Experiment,m.ExpValue).join(m.Experiment).join(m.ExpValue).filter(m.Variant.searchid == se.id)
            # This was way faster. Each acess to a obj field that represents
            # another table means an select to the query database. For a lot of
            # data it becomes very slow!
            res = session.query(m.Variant,m.Experiment,m.ExpValue
                    ).filter(m.Variant.searchid == se.id
                    ).filter(m.Variant.id == m.Experiment.variantid
                    ).filter(m.Experiment.id == m.ExpValue.expid)
            for v, e, evalu in res:
                #yield retinfo, v, e, e.expvalues
                #print(f"   {v} | {e} | {evalu} | {evalu.metric}")
                yield retinfo, v, e, evalu
            ##
        ##
    ##
    session.close()
###
def printDBCfgs(dbinp, locusfiles, envinfo=None, debugdb=False):

    dbname = dbnameparse(dbinp)
    engine, session = connect(dbname, debugdb)

    for lf in locusfiles:
        tgthash,_ = hashlocusfile(lf)
        #log.info(f"EITA File: {lf} hash: {tgthash}")
        print(f"#File: {lf} hash: {tgthash}")
        x = session.query(m.Configuration,m.LocusFile.locusfilename).join(m.LocusFile).filter(m.LocusFile.hash == tgthash)
        #print(f"{x}\n")

        for cfg, lfname in x:
            #retinfo = saveretinf(se, lfname, envinfo)
            print(f"#Cfg: {cfg.id} {cfg.data}")
        ##
    ##
    session.close()
###

def getLocusProgram(dbinp, cfgid, debugdb=False):
    """Find the cfgid in the database and print the locus program associated with it."""
    dbname = dbnameparse(dbinp)
    engine, session = connect(dbname, debugdb)

    ret = []
    for c in cfgid:
        #print("Append Locus program used by cfgid ", c)
        x =  session.query(m.LocusFile).join(m.Configuration).filter(m.Configuration.id == c)
        for resq in x:
            #print(resq.id,")", resq.locusfilename, resq.data)
            # in this case I believe there is only one result possible.
            ret.append((c,resq))
            break # just to make sure returns only the 1st result of the query.
        ##
    ##
    return ret
###

def getDBBestVar(dbinp, locusfiles, envinfo=None):
    """ For each search for the locusfiles database return the median value of
        the fastest variant.
    """
    dbname = dbnameparse(dbinp)
    debug = False
    engine, session = connect(dbname, debug)

    for lf in locusfiles:
        tgthash,_ = hashlocusfile(lf)
        #log.info(f"EITA File: {lf} hash: {tgthash}")
        print(f"#File: {lf} hash: {tgthash}")
        x = session.query(m.Search,m.LocusFile.locusfilename).join(m.LocusFile).filter(m.LocusFile.hash == tgthash)
        #print(f"{x}\n")

        for se, lfname in x:
            #print(f"#Prof 1 {datetime.datetime.now()}")
            retinfo = {'lfname': lfname, 'searchtool': se.searchtool}
            if envinfo is not None:
                for evalue in se.envvalues:
                    #print(f"envvar: {evalue} {evalue.envvar}")
                    varname = evalue.envvar.name
                    if varname in envinfo and varname not in retinfo:
                            retinfo[varname] = evalue.value
                        #
                    #
                ##
            #

            print(f"#Prof 2 numvariants: {len(se.variants)} {datetime.datetime.now()}",  file=sys.stderr)
            #print(f"Search: {se.id}")
            bestmetric = float('inf')
            bestvar = None
            for v in se.variants:
                #emetrics = [e.metric for e in v.experiments]
                # Assuming the 1st value of expvalues as metric
                emetrics = [e.expvalues[0].metric for e in v.experiments]
                medmetric = stat.median(emetrics)
                if medmetric < bestmetric:
                    bestmetric = medmetric
                    bestvar = v
                #
            ##
            print(f"#Prof 3 {datetime.datetime.now()}",  file=sys.stderr)
            if bestvar:
                v = bestvar
                for e in v.experiments:
                    yield retinfo, v, e, e.expvalues
                    #print(f"{lfname},{comp},{se.searchtool},{v.id},{sM},{sN},{e.metric}")
                ##
            #
            print(f"#Prof 4 {datetime.datetime.now()}", file=sys.stderr)
        ##
    ###

    session.close()
###

def getDBlocusfiles(dbinp, locusfilenames):
    dbname = dbnameparse(dbinp)
    debug = False
    engine, session = connect(dbname, debug)

    for lf in locusfilenames or []:

        x = session.query(m.LocusFile).filter(m.LocusFile.locusfilename == lf)
        for lf in x:
            print(f"#File: {lf.locusfilename} hash: {lf.hash} program:\n{lf.data}")
        ##
    ##
    session.close()
###

def showDBinfo(dbinp):
    dbname = dbnameparse(dbinp)
    debug = False
    engine, session = connect(dbname, debug)

    x = session.query(m.Search,m.LocusFile).join(m.LocusFile)
    print(f"         id\thostname\tsearchtool\tsearchteach\tlocushash{' '*56}\tnumber of variants\tlocusfilename")
    for se, lf in x:
        print(f"#Search: {se.id}\t{se.hostname:8}\t{se.searchtool}\t{se.searchtech:11}\t{lf.hash:65}\t{len(se.variants):18}\t{lf.locusfilename}")
    ##

    session.close()
###

def getSearchTimeStats(dbinp, printall=False):
    """Return stats about the search time"""
    from datetime import datetime
    dbname = dbnameparse(dbinp)
    debug = False
    engine, session = connect(dbname, debug)

    durations = []
    #select id,strftime('%H:%M', CAST ((julianday(finished_at) - julianday(started_at)) AS REAL), '12:00') from searches;
    x = session.query(m.Search.id,m.Search.started_at, m.Search.finished_at)
    for _id,s,f in x:
        if s is not None and f is not None:
            diff = (f-s)
            durations.append(diff)
        else:
            diff = None
        #
        if printall:
            print(f"{_id}: {s} {f} {diff}")
        #
    #
    _min = min(durations)
    _max = max(durations)
    med = stat.median(durations)
    print(f"Min: {_min} Median: {med} Max: {_max}")

    session.close()
###

def getCfgfromId(dbinp, cfgid):
    dbname = dbnameparse(dbinp)
    debug = False
    engine, session = connect(dbname, debug)
    x = session.query(m.Configuration.data).filter(m.Configuration.id == cfgid)

    print(f"Cfgid: {cfgid}")
    for data in x:
        print(f"{data}")
    print("Done.")

    session.close()
##
