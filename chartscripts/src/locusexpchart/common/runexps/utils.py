'''
@author: Thiago Teixeira
'''

import os, glob, shutil, logging, subprocess, sys

log = logging.getLogger(__name__)

def cmdexec(cmd, bufoutP=None, buferrP=None):
    retcode = 0
    bufout = bufoutP or sys.stdout
    buferr = buferrP or sys.stderr
    try:
        #log.info(f'Running cmd {cmd} ...')
        print(f'Running cmd {cmd} ...')
        cplproc = subprocess.run(cmd, stdout=bufout, stderr=buferr, shell=False)
        log.info(f'Done cmd.')
        #if bufoutP is not None:
        #    bufoutP.seek(0)
        ##
        #if buferrP is not None:
        #    buferrP.seek(0)
        ##
        #print(f'Done cmd.\n{bufoutP.read().decode("UTF-8") if bufoutP is not None else None}')
    except FileNotFoundError as e:
        log.error(f"{e}")
        raise
    except Exception as e:
        raise
    else:
        retcode = cplproc.returncode
        #print(f'{cplproc.stdout}\nstderr:\n{cplproc.stderr}')
    #finally:
    return retcode
#

def setEnv(name, val):
    os.environ[name] = val
####

def unsetEnv(name):
    if name in os.environ:
        del os.environ[name]
    #
####
