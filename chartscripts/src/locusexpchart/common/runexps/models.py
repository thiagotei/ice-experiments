'''
@author: Thiago Teixeira
'''

import logging, os, glob, shutil

from locusexpchart.common.runexps.utils import cmdexec

log = logging.getLogger(__name__)

class SearchTool():
    def __init__(self, locusfilename=None):
        self.locusfilename = locusfilename
    ###
####

class OpenTuner(SearchTool):
    def __init__(self, locusfilename=None):
        #self.main = opentuner_main
        super().__init__(locusfilename)
    ###

    def __str__(self):
        return (f"Opentuner")
    ###

    #@staticmethod
    def exec(self, args=None):
        filelst = glob.glob("./opentuner.*")
        for f in filelst:
            if os.path.isfile(f):
                os.remove(f)
                log.info(f"Removed file {f}")
            elif os.path.isdir(f):
                shutil.rmtree(f, ignore_errors=True)
                log.info(f"Removed dir {f}")
            #
        ##
        #self.main(args)
        if self.locusfilename is not None:
            addloc = ['-t',self.locusfilename]
        else:
            addloc = []
        #
        nargs = ['ice-locus-opentuner.py'] + addloc + args
        cmdexec(nargs)
        #opentuner_main(args)
    ###
####

class HyperOpt(SearchTool):

    _mixdsc = {"rand": ("rand","None"),
               "tpe" : ("tpe","None"),
               "anneal": ("anneal","None"),
               "mixA": ("mix",".25:rand:.75:tpe"),
               "mixB": ("mix",".25:rand:.75:anneal"),
               "mixC": ("mix",".25:rand:.50:anneal:0.25:tpe"),
               "mixD": ("mix",".50:rand:.50:anneal"),
               "mixE": ("mix",".75:rand:.25:anneal")}

    def __init__(self, locusfilename=None):
        #self.main = hyperopt_main
        super().__init__(locusfilename)
    ###

    def __str__(self):
        return (f"Hyperopt")
    ###

    def exec(self, args=None, alg='mixC'):
        #self.main(args)
        #hyperopt_main(args)
        if self.locusfilename is not None:
            addloc = ['-t',self.locusfilename]
        else:
            addloc = []
        #
        nargs = ['ice-locus-hyperopt.py','--hypalg', self._mixdsc[alg][0],
                 '--hypmixdsc', self._mixdsc[alg][1]] + addloc + args
        cmdexec(nargs)
    ###
####

class BaseCase(SearchTool):
    """This for when there is no search"""
    def __init__(self, locusfilename=None):
        #list of commands to execute (gen code, compiler,and execute)
        #self.cmds = cmds or []
        super().__init__(locusfilename)
    ###

    def exec(self, args=None):
        if self.locusfilename is not None:
            addloc = ['-t',self.locusfilename]
        else:
            addloc = []
        #

        nargs = ['ice-locus.py'] + addloc + args
        #log.info(f"[BaseCase] {nargs}")
        #print(f"[BaseCase] {nargs}")
        cmdexec(nargs)
        ##
    ###
####

class Problem():
    def __init__(self, name, shapenames, stopname=None):
        self.name = name
        self.shapenames = shapenames
        self.stopname = stopname
    ###

    def __str__(self):
        return (f"Problem: {self.name}")
    ###
####

class Strat():
    def __init__(self, prob, stool, stopfunc, args, cmplxargs=None, desc="N/A"):
        self.prob = prob
        self.stool = stool
        self.stopfunc = stopfunc
        self.args = args
        self.cmplxargs = cmplxargs
        self.desc = desc
    ###

    def __str__(self):
        return (f"Strategy:\n\tprob= {self.prob}\n\tsearchtool= {self.stool}"
                f"\n\targs={self.args}")
               #f" cmplxargs= {self.cmplxargs}")
    ###

    def exec(self, xargs=None, key=None):
        xargs = xargs or []
        if key is not None and self.cmplxargs is not None:
            log.debug(f"key: {key} cmplargs: {self.cmplxargs}")
            for c in self.cmplxargs:
                log.debug(f"c: {c}")
                if key in self.cmplxargs[c]:
                    xargs += [str(c), str(self.cmplxargs[c][key])]
                #
            ##
        #
        log.debug(f"xargs: {xargs}")
        self.stool.exec(self.args+xargs)
    ###
####
