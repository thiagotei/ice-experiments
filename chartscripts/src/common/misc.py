#!/usr/bin/env python

import sys, argparse
import statistics as stat

try:
    import locus.tools.search.resultsdb.models as m
    from locus.tools.search.resultsdb.connect import connect
    from locus.tools.search.resultsdb.misc import hashlocusfile
except ImportError:
    raise("Load virtual env with ice-locus installed to use this script!!")
#

def dbnameparse(dbname):
    if '://' not in dbname:
        newdbname = 'sqlite:///' + dbname
    #
    return newdbname
###

def getDBresults(dbinp, locusfiles, envinfo=None):
    dbname = dbnameparse(dbinp)
    debug = False
    engine, session = connect(dbname, debug)

    for lf in locusfiles:
        tgthash,_ = hashlocusfile(lf)
        x = session.query(m.Search,m.LocusFile.locusfilename).join(m.LocusFile).filter(m.LocusFile.hash == tgthash)
        #print(f"{x}\n")

        for se, lfname in x:
            retinfo = {'lfname': lfname, 'searchtool': se.searchtool}
            if envinfo is not None:
                for evalue in se.envvalues:
                    #print(f"envvar: {evalue} {evalue.envvar}")
                    varname = evalue.envvar.name
                    if varname in envinfo and varname not in retinfo:
                            retinfo[varname] = evalue.value
                        #
                    #
                ##
            #

            #print(f"Search: {se.id}")
            bestmetric = float('inf')
            bestvar = None
            for v in se.variants:
                emetrics = [e.metric for e in v.experiments]
                medmetric = stat.median(emetrics)
                if medmetric < bestmetric:
                    bestmetric = medmetric
                    bestvar = v
                #
            ##
            if bestvar:
                v = bestvar
                for e in v.experiments:
                    yield retinfo,v,e
                    #print(f"{lfname},{comp},{se.searchtool},{v.id},{sM},{sN},{e.metric}")
                ##
            #
        ##
    ###

    session.close()
###


