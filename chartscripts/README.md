Locus/ICE Experiments and Chart Library
================


These are auxiliary library to run and generates charts automatically.

Installation
-------

1. Install the virtual environment tool: `pip install virtualenv`
2. Create a virtual environment: `virtualenv <path>/<to>/<venv> --python=python3`
3. Activate the virtual environment: `source <path/<to>>/<venv>/bin/activate`
4. Go to ice-locus-expchart path
5. Install: `pip install` (or for develop `pip install -e .`)
6. Check installation: `locusdb-searchtimes --help`


Methodology
-------

1. Run scripts, the data will be saved on the locus.db/<mach>.db
2. Get data from DB to csv (e.g., locusdb-genCSV -i <locus.db/machineA.db> --inccfg -l <opt.locus>).
3. Generate chart from csv.
