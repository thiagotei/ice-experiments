#!/usr/bin/env python

import sys, os, shutil

from pyps import *


def findLoopWithLabel(loops, label):
        for l1 in loops:
            if l1.label == label:
                return l1
            else:
                ret = findLoopWithLabel(l1.loops(), label)
                if ret:
                    return ret
        return None


# removing previous output
if os.path.isdir("jacobi2D.database"):
    shutil.rmtree("jacobi2D.database", True)
ws = workspace("jacobi2D.c", name="jacobi2D",deleteOnClose=True)

ws.props.ABORT_ON_USER_ERROR = True
ws.props.MEMORY_EFFECTS_ONLY = False
ws.activate("MUST_REGIONS")
ws.activate("RICE_REGIONS_DEPENDENCE_GRAPH")

fct = ws.fun.jacobi2D
fct.display ()

## TILING 

loop3 = findLoopWithLabel(fct.loops(),"loop0")
loop3.loop_tiling (LOOP_TILING_MATRIX = "1 0 0, 1 4 4 , 1 -4 4")
fct.display ()

fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()

## PARALLELIZATION 
fct.privatize_module()
fct.coarse_grain_parallelization()

fct.clean_labels()
fct.display()

# close the workspace
ws.close()

