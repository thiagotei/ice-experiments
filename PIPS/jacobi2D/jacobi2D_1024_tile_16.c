#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#ifndef N
#define N 1024
#endif

#define pips_min(a,b) ((a) < (b) ? (a) : (b))
#define pips_max(a,b) ((a) > (b) ? (a) : (b))

#define  EDIV(x,y) ((x<0)?((x-y+1)/y):((x)/y))

void jacobi2D(int n, float a[N][N][N], float c)
{
  int i, j, t;
   
 loop0:
  for(t = 1; t <N-1; t ++)
  #pragma omp parallel for private(j,i)
    for(j = 1; j <N-1; j ++) 
      for(i = 1; i <N-1; i ++) 
	a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
}

void jacobi2D_tile_32(int n, float a[1024][1024][1024], float c)
{
   int i, j, t;
   //PIPS generated variable
   int t_t, j_t, i_t;

   for(t_t = 0; t_t <= 1021; t_t += 1)
#pragma omp parallel for private(j_t,i_t,j,t,i)
     for(j_t = 0; j_t <= 31; j_t += 1)
	//#pragma omp parallel for private(j,t,i)
	for(i_t = 0; i_t <= 31; i_t += 1) {
            t = t_t+1;
            for(j = 32*j_t+1; j <= pips_min(1022, 32*j_t+32); j += 1)
               for(i = 32*i_t+1; i <= pips_min(1022, 32*i_t+32); i += 1)
                  a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
         }
}

void jacobi2D_tile_16(int n, float a[1024][1024][1024], float c)
{
   int i, j, t;
   //PIPS generated variable
   int t_t, j_t, i_t;

   for(t_t = 0; t_t <= 1021; t_t += 1)
#pragma omp parallel for private(j_t,i_t,j,t,i)
      for(j_t = 0; j_t <= 63; j_t += 1)
         for(i_t = 0; i_t <= 63; i_t += 1) {
            t = t_t+1;
            for(j = 16*j_t+1; j <= pips_min( 1022, 16*j_t+16); j += 1)
               for(i = 16*i_t+1; i <= pips_min( 1022, 16*i_t+16); i += 1)
                  a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
         }
}


void jacobi2D_tile3D_16(int n, float a[1024][1024][1024], float c)
{
   int i, j, t;
   //PIPS generated variable
   int t_t, j_t, i_t;

   for(t_t = 0; t_t <= 1021; t_t += 1)
#pragma omp parallel for private(j_t,i_t,j,t,i)
     for(j_t = -32; j_t <= 31; j_t += 1)
       //#pragma omp parallel for private(j,t,i)
       for(i_t = pips_max(pips_max( EDIV(-(16*j_t+t_t)-16,16), EDIV(16*j_t-t_t,16)), EDIV(-t_t,16)); i_t <= pips_min(pips_min(EDIV(16*j_t-t_t+1036,16), EDIV(-(16*j_t+t_t)+1021,16)), EDIV(-t_t+1021,16)); i_t += 1) {
	 t = t_t+1;
	 //#pragma omp parallel for private(i)
	 for(j = 1; j <= 1022; j += 1)
	   //#pragma omp parallel for 
	   for(i = pips_max(pips_max( 1, 32*i_t-j+2*t), j-32*j_t-31); i <= pips_min(pips_min(1022, 32*i_t-j+2*t+31), j-32*j_t); i += 1)
	     a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
         }
}

void jacobi2D_tile3D_32(int n, float a[1024][1024][1024], float c)
{
   int i, j, t;
   //PIPS generated variable
   int t_t, j_t, i_t;

   for(t_t = 0; t_t <= 1021; t_t += 1)
#pragma omp parallel for private(j_t,i_t,j,t,i)
      for(j_t = -16; j_t <= 15; j_t += 1)
	//#pragma omp parallel for private(j,t,i)
	for(i_t = pips_max(pips_max( EDIV(-(32*j_t+t_t)-32, 32), EDIV(32*j_t-t_t, 32)), EDIV(-t_t, 32)); i_t <= pips_min( pips_min( EDIV(32*j_t-t_t+1052, 32), EDIV(-(32*j_t+t_t)+1021, 32)), EDIV(-t_t+1021, 32)); i_t += 1) {
            t = t_t+1;
            for(j = 1; j <= 1022; j += 1)
	      for(i = pips_max(pips_max( 1, 64*i_t-j+2*t), j-64*j_t-63); i <= pips_min(pips_min( 1022, 64*i_t-j+2*t+63), j-64*j_t); i += 1)
                  a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
         }
}


void jacobi2D_3D_32_u2(int n, float a[1024][1024][1024], float c)
{
  //PIPS generated variable
  int t_t, j_t, i_t, j, i1, i2;

  for(t_t = 0; t_t <= 1021; t_t += 1)
#pragma omp parallel for private(j_t,i_t,j,i2,i1)
    for(j_t = -16; j_t <= 15; j_t += 1)
      for(i_t = pips_max(pips_max( EDIV(-(32*j_t+t_t)-32, 32), EDIV(32*j_t-t_t, 32)), EDIV(-t_t, 32)); i_t <= pips_min(pips_min( EDIV(32*j_t-t_t+1052, 32), EDIV(-(32*j_t+t_t)+1021, 32)), EDIV(-t_t+1021, 32)); i_t += 1)
	{
	  for(j = 0; j <= 1021; j += 2) {

	    for(i2 = pips_max(pips_max( 1, 64*i_t-j+2*t_t+1), j-64*j_t-62); i2 <= pips_min(pips_min( 1022, 64*i_t-j+2*t_t+64), j-64*j_t+1); i2 += 1) 
	      a[i2][j+1][t_t+1] = c*(a[i2-1][j+1][t_t]+a[i2+1][j+1][t_t]+a[i2][j][t_t]+a[i2][j+2][t_t]+a[i2][j+1][t_t]+a[i2][j+1][t_t]);
	    for(i1 = pips_max(pips_max( 1, 64*i_t-j+2*t_t), j-64*j_t-61); i1 <= pips_min(pips_min( 1022, 64*i_t-j+2*t_t+63), j-64*j_t+2); i1 += 1)
	      a[i1][j+2][t_t+1] = c*(a[i1-1][j+2][t_t]+a[i1+1][j+2][t_t]+a[i1][j+1][t_t]+a[i1][j+3][t_t]+a[i1][j+2][t_t]+a[i1][j+2][t_t]);
	  }
	}
}
	
void jacobi2D_tile2D_8(int n, float a[1024][1024][1024], float c)
{
   int i, j, t;
   //PIPS generated variable
   int t_t, j_t, i_t;

loop0:
   for(t_t = 0; t_t <= 1021; t_t += 1)
     //    #pragma omp parallel for private(i_t,j,t,i)
      for(j_t = -64; j_t <= 63; j_t += 1)
#pragma omp parallel for private(j,t,i)
	for(i_t = pips_max( j_t, -j_t-1); i_t <= pips_min( j_t+128, -j_t+127); i_t += 1) {
	   t = t_t+1;
	   //#pragma omp parallel for private(i)
               for(j = 1; j <= 1022; j += 1)
		 for(i = pips_max(pips_max( 1, 16*i_t-j+2), j-16*j_t-15); i <= pips_min(pips_min( 1022, 16*i_t-j+17), j-16*j_t); i += 1)
                     a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
	}
}

void jacobi2D_tile2D_16(int n, float a[1024][1024][1024], float c)
{
   int i, j, t;
   //PIPS generated variable
   int t_t, j_t, i_t;

   for(t_t = 0; t_t <= 1021; t_t += 1)
#pragma omp parallel for private(j_t,i_t,j,t,i)
      for(j_t = -32; j_t <= 31; j_t += 1)
	//#pragma omp parallel for private(j,t,i)
	for(i_t = pips_max(j_t, -j_t-1); i_t <= pips_min(j_t+64, -j_t+63); i_t += 1) {
            t = t_t+1;
	    //#pragma omp parallel for private(i)
            for(j = 1; j <= 1022; j += 1)
	      for(i = pips_max(pips_max( 1, 32*i_t-j+2), j-32*j_t-31); i <= pips_min(pips_min(1022, 32*i_t-j+33), j-32*j_t); i += 1)
                  a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
         }
}

void jacobi2D_tile2D_32(int n, float a[1024][1024][1024], float c)
{
   int i, j, t;
   //PIPS generated variable
   int t_t, j_t, i_t;

   for(t_t = 0; t_t <= 1021; t_t += 1)
#pragma omp parallel for private(j_t,i_t,j,t,i)
      for(j_t = -16; j_t <= 15; j_t += 1)
	//#pragma omp parallel for private(i_t,j,t,i)
         for(i_t = pips_max( j_t, -j_t-1); i_t <= pips_min(j_t+32, -j_t+31); i_t += 1) {
            t = t_t+1;
            for(j = 1; j <= 1022; j += 1)
	      for(i = pips_max(pips_max( 1, 64*i_t-j+2), j-64*j_t-63); i <= pips_min(pips_min( 1022, 64*i_t-j+65), j-64*j_t); i += 1)
                  a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
         }
}


int main()
{
  float (*a)[N][N][N],(*a1)[N][N][N];
  int i,j,k, n=N, c=.25;
  a=malloc((N)*(N)*(N)*sizeof(float));
  a1=malloc((N)*(N)*(N)*sizeof(float));
  struct timeval tvStart,tvEnd;
  double linStart = 0,linEnd = 0,lTime = 0;

  
#pragma omp parallel for private(k,j,i)
  for(k=0;k<N;k++)
      for(j=0;j<N;j++)
	for(i=0;i<N;i++)
	  (*a)[i][j][k]=1 ;

  printf("calcul de jacobi2D_tile_16 \n");
  gettimeofday (&tvStart,NULL);
  jacobi2D_tile_16(N,*a,c);
  gettimeofday (&tvEnd,NULL);

  linStart = ((double)tvStart.tv_sec * 1000 + (double)(tvStart.tv_usec/1000.0));
  linEnd = ((double)tvEnd.tv_sec * 1000 + (double)(tvEnd.tv_usec/1000.0));
  lTime = linEnd-linStart;
  printf("*** Minimum global time  : %.3f (ms)\n",lTime);
    
  return 0;
}
