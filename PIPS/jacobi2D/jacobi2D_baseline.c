#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#ifndef N
#define N 256
#endif

#define pips_min(a,b) ((a) < (b) ? (a) : (b))
#define pips_max(a,b) ((a) > (b) ? (a) : (b))

#define  EDIV(x,y) ((x<0)?((x-y+1)/y):((x)/y))

void Jacobi2D(int n, float a[N][N][N], float c)
{
  int i, j, t;
   
  for(t = 1; t <N-1; t ++)
    for(j = 1; j <N-1; j ++) 
      for(i = 1; i <N-1; i ++) 
	a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
} 



int main()
{
  float (*a)[N][N][N],(*a1)[N][N][N];//, a2[N][N][N], c=.25;
  a1=malloc((N)*(N)*(N)*sizeof(float));
  float  c=.25;
  int i,j,k, n=N;
  struct timeval tvStart,tvEnd;
  double linStart = 0,linEnd = 0,lTime = 0;

  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      for(k=0;k<N;k++)
	(*a1)[i][j][k]=1;
  gettimeofday (&tvStart,NULL);
  Jacobi2D(N,*a1,c);
  gettimeofday (&tvEnd,NULL);

  linStart = ((double)tvStart.tv_sec * 1000 + (double)(tvStart.tv_usec/1000.0));
  linEnd = ((double)tvEnd.tv_sec * 1000 + (double)(tvEnd.tv_usec/1000.0));
  lTime = linEnd-linStart;
  printf("*** Minimum global time  : %.3f (ms)\n",lTime);

  return 0;
}
