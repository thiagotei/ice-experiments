#include <stdio.h>
#ifndef N
#define N 25
#endif

#define pips_min(a,b) ((a) < (b) ? (a) : (b))
#define pips_max(a,b) ((a) > (b) ? (a) : (b))

#define  EDIV(x,y) ((x<0)?((x-y+1)/y):((x)/y))

void Jacobi2D(int n, float a[N][N][N], float c)
{
  int i, j, t;
   
 loop0:
  for(t = 1; t <N-1; t ++)
    for(j = 1; j <N-1; j ++) 
      for(i = 1; i <N-1; i ++) 
	a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
} 

void Jacobi2D_v1(int n, float a[25][25][25], float c)
{
  int i, j, t;
  //PIPS generated variable
  int t_t, j_t, i_t;

 loop0:
  for(t_t = 0; t_t <= 22; t_t += 1)
    for(j_t = -3; j_t <= 2; j_t += 1)
      for(i_t = pips_max(pips_max(EDIV((-4*j_t-t_t-4),4), EDIV((4*j_t-t_t),4)), EDIV(-t_t,4));
	  i_t <= pips_min( pips_min(EDIV((4*j_t-t_t+25),4), EDIV((-4*j_t-t_t+22),4)), EDIV((-t_t+22),4)); i_t += 1)

	for(t = t_t+1; t <= t_t+1; t += 1)
	  for(j = 1; j <= 23; j += 1)
	    for(i = pips_max(pips_max( 1, 8*i_t-j+2*t), j-8*j_t-7); i <= pips_min(pips_min( 23, 8*i_t-j+2*t+7), j-8*j_t); i += 1)
	      a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1
													       ]);
}


int main()
{
  float a[N][N][N],a1[N][N][N], a2[N][N][N], c=.25;
  int i,j,k, n=N;

  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      for(k=0;k<N;k++)
	a[i][j][k]=1,
	  a1[i][j][k]=1,
	  a2[i][j][k]=1 ;

  Jacobi2D(N,a,c);
  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      for(k=0;k<N;k++)
	printf("%f-",a[i][j][k]);
  printf("calcul de Jacobi2D_v1 \n");
  
  Jacobi2D_v1(N,a1,c);
  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++) {
      for (k = 1; k < N; k++) // printf("a[%d][%d]=%f",i,j,a[i][j]);
	if (a[i][j][k]==a1[i][j][k])
	  printf("%f--",a1[i][j][k]);
	else printf("error--");
      printf("\n");
    }
  

  return 0;
}
