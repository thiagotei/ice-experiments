#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#ifndef N
#define N 256
#endif

#define pips_min(a,b) ((a) < (b) ? (a) : (b))
#define pips_max(a,b) ((a) > (b) ? (a) : (b))

#define  EDIV(x,y) ((x<0)?((x-y+1)/y):((x)/y))

void Jacobi2D(int n, float a[N][N][N], float c)
{
  int i, j, t;
   
  for(t = 1; t <N-1; t ++)
    for(j = 1; j <N-1; j ++) 
      for(i = 1; i <N-1; i ++) 
	 a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
}



void Jacobi2D_omp(int n, float a[N][N][N], float c)
{
  int i, j, t;
  //PIPS generated variable
  int t_t, j_t, i_t;

  for(t_t = 0; t_t <= 253; t_t += 1)
    //#pragma omp parallel for private(i_t,j,t,i)
    for(j_t = -32; j_t <= 31; j_t += 1)
       #pragma omp parallel for private(j,t,i)
      for(i_t = pips_max(pips_max( EDIV(-(4*j_t+t_t)-4,4), EDIV(4*j_t-t_t,4)), EDIV(-t_t,4)); i_t <= pips_min(pips_min(EDIV(4*j_t-t_t+256,4), EDIV(-(4*j_t+t_t)+253,4)), EDIV(-t_t+253,4)); i_t += 1) {

	t = t_t+1;
	//	#pragma omp parallel for private(i)
	for(j = 1; j <= 254; j += 1)
	  //	  #pragma omp parallel for 
	  for(i = pips_max(pips_max(1, 8*i_t-j+2*t), j-8*j_t-7); i <= pips_min(pips_min(254, 8*i_t-j+2*t+7), j-8*j_t); i += 1)
	    a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
      }

}


int main()
{
  float (*a)[N][N][N],(*a1)[N][N][N];//, a2[N][N][N], c=.25;
  // a=malloc((N)*(N)*(N)*sizeof(float));
  a1=malloc((N)*(N)*(N)*sizeof(float));
  //a2=malloc((N+1)*(N+1)*(N+1)*sizeof(float));
  float  c=.25;
  int i,j,k, n=N;
  struct timeval tvStart,tvEnd;
  double linStart = 0,linEnd = 0,lTime = 0;

  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      for(k=0;k<N;k++)
	(*a1)[i][j][k]=1;
  gettimeofday (&tvStart,NULL);
  Jacobi2D_omp(N,*a1,c);
  gettimeofday (&tvEnd,NULL);

  linStart = ((double)tvStart.tv_sec * 1000 + (double)(tvStart.tv_usec/1000.0));
  linEnd = ((double)tvEnd.tv_sec * 1000 + (double)(tvEnd.tv_usec/1000.0));
  lTime = linEnd-linStart;
  printf("*** Minimum global time  : %.3f (ms)\n",lTime);
  return 0;
}
