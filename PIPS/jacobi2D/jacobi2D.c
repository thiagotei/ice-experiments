#include <stdio.h>
#ifndef N
#define N 25
#endif

void jacobi2D(int n, float a[N][N][N], float c)
{
  int i, j, t;
   
 loop0:
  for(t = 1; t <N-1; t ++)
    for(j = 1; j <N-1; j ++) 
      for(i = 1; i <N-1; i ++) 
	a[i][j][t] = c*(a[i-1][j][t-1]+a[i+1][j][t-1]+a[i][j-1][t-1]+a[i][j+1][t-1]+a[i][j][t-1]+a[i][j][t-1]);
} 

int main()
{
  float a[N][N][N], c=.25;
  int i,j,k, n=N;
  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      for(k=0;k<N;k++)
	a[i][j][k]=1;

  jacobi2D(N,a,c);

  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      for(k=0;k<N;k++)
	printf("%f-",a[i][j][k]);
  return 0;
}
