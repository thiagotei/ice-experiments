#!/usr/bin/env python

import sys, os, shutil

from pyps import *


def findLoopWithLabel(loops, label):
        for l1 in loops:
            if l1.label == label:
                return l1
            else:
                ret = findLoopWithLabel(l1.loops(), label)
                if ret:
                    return ret
        return None


# removing previous output
if os.path.isdir("matmul_tiling2.database"):
    shutil.rmtree("matmul_tiling2.database", True)
ws = workspace("matmul_tiling.c", name="matmul_unroll2",deleteOnClose=True)

ws.props.ABORT_ON_USER_ERROR = True
ws.props.MEMORY_EFFECTS_ONLY = False
ws.activate("MUST_REGIONS")
ws.activate("RICE_REGIONS_DEPENDENCE_GRAPH")

fct = ws.fun.Matrix_Mult
fct.display ()

## TILING 

fct.flag_loops()
fct.display ()
loop3 = findLoopWithLabel(fct.loops(),"loop0")
loop3.loop_tiling (LOOP_TILING_MATRIX = "2 0 0 , 0 4 0 , 0 0 1")
fct.display ()

#UNROLL INNER LOOPS

fct.flag_loops()
fct.display ()
loop3 = findLoopWithLabel(fct.loops(),"l99996")
loop3.unroll(UNROLL_RATE=4)
print "%%%% Unroll 4"
fct.display()

fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
print "%%%% Simplification"
fct.display()

print "%%%% Loop Fusion"
fct.loop_fusion()
fct.display()

## ACCUMULATION

fct.scalarization()
fct.split_initializations()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()

fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
print "%%%% Accumulation"
fct.display()

## UNROLL INNER LOOPS

fct.flag_loops()
fct.display ()
loop3 = findLoopWithLabel(fct.loops(),"l99995")
loop3.unroll(UNROLL_RATE=2)
print "%%%% Unrool Inner"
fct.display()

fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
print "%%%% Dead code"
fct.display()

fct.loop_fusion()
print "%%%% Loop fusion"
fct.display()

## SIMPLIFICATION

fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.display()

## PARALLELIZATION 
fct.privatize_module()
fct.coarse_grain_parallelization()
fct.display()

fct.clean_labels()
fct.display()

# close the workspace
ws.close()


# RECIPE PREAMBLE (Matrix_Mult)
# RECIPE RECTANGULAR TILING (3,2,4,1)
# RECIPE UNROLL_JAM("l99996",4)
# RECIPE_AGGREGATION (4)
# RECIPE UNROLL_JAM("l99995",2)
# RECIPE_SIMPLIFICATION++
# RECIPE CLEAN_LABELS
# RECIPE PARALLELIZATION
# RECIPE POSTAMBLE
