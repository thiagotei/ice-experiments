#include <stdio.h>
#ifndef N
#define N 100
#endif

void Matrix_Mult(float a[N][N], float b[N][N], float c[N][N])
{
  int i, j, k;
   
  for(i = 0; i <N; i ++)
    for(j = 0; j <N; j ++) 
      c[i][j] = 0;

 loop0:
  for(j = 0; j <N; j ++)
  loop1:
    for(k = 0; k <N; k ++)
    loop2:
      for(i = 0; i <N; i ++) {
 	c[i][j] = c[i][j]+a[i][k]*b[k][j];
      }
  
}
int main()
{
   float a[N][N], b[N][N], c[N][N];
    int i,j;
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            a[i][j]=b[i][j]=i;

    Matrix_Mult(a,b,c);

    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            printf("%f-",c[i][j]);
    return 0;
}
