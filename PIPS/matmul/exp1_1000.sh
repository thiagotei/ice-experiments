export OMP_DISPLAY_ENV=true
export OMP_PROC_BIND=close
export OMP_PLACES=cores
export OMP_NUM_THREADS=8
for i in 1 2 3 4 5;  do  ./matmul_1000_orig; done
export OMP_DISPLAY_ENV=false
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8_u4_i2; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16_u4_i2; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32_u4_i2; done
export OMP_DISPLAY_ENV=true
export OMP_NUM_THREADS=16
for i in 1 2 3 4 5;  do  ./matmul_1000_orig; done
export OMP_DISPLAY_ENV=false
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8_u4_i2; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16_u4_i2; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32_u4_i2; done
export OMP_DISPLAY_ENV=true
export OMP_NUM_THREADS=32
for i in 1 2 3 4 5;  do  ./matmul_1000_orig; done
export OMP_DISPLAY_ENV=false
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8_u4_i2; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16_u4_i2; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32_u4_i2; done
export OMP_DISPLAY_ENV=true
export OMP_NUM_THREADS=40
for i in 1 2 3 4 5;  do  ./matmul_1000_orig; done
export OMP_DISPLAY_ENV=false
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8_u4_i2; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16_u4_i2; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32_u4_i2; done
export OMP_DISPLAY_ENV=true
export OMP_NUM_THREADS=64
for i in 1 2 3 4 5;  do  ./matmul_1000_orig; done
export OMP_DISPLAY_ENV=false
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_8x8_u4_i2; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_16x16_u4_i2; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32_u4; done
for i in 1 2 3 4 5;  do  ./matmul_1000_tile_32x32_u4_i2; done
export OMP_DISPLAY_ENV=true
