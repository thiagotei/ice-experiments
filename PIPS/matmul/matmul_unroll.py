#!/usr/bin/env python

import sys, os, shutil

from pyps import *


def findLoopWithLabel(loops, label):
        for l1 in loops:
            if l1.label == label:
                return l1
            else:
                ret = findLoopWithLabel(l1.loops(), label)
                if ret:
                    return ret
        return None


# removing previous output
if os.path.isdir("matmul_unroll2.database"):
    shutil.rmtree("matmul_unroll2.database", True)
ws = workspace("matmul_unroll.c", name="matmul_unroll2",deleteOnClose=True)

ws.props.ABORT_ON_USER_ERROR = True
ws.props.MEMORY_EFFECTS_ONLY = False
ws.activate("MUST_REGIONS")
ws.activate("RICE_REGIONS_DEPENDENCE_GRAPH")

fct = ws.fun.Matrix_Mult
fct.display ()

## UNROLL - JAM

fct.flag_loops()
fct.display ()
loop3 = findLoopWithLabel(fct.loops(),"loop1")
loop3.unroll(UNROLL_RATE=4)
fct.display ()

fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.display()

fct.loop_fusion()
fct.display()

## ACCUMULATION
fct.scalarization()
fct.split_initializations()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()

fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.display()

## UNROLL - JAM

fct.flag_loops()
fct.display ()
loop3 = findLoopWithLabel(fct.loops(),"loop0")
loop3.unroll(UNROLL_RATE=2)
fct.display()

fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.display()

fct.loop_fusion()
fct.display()

## NESTED LOOPS ==> ADDITIONAL FUSION

fct.flag_loops()
fct.display ()
loop3 = findLoopWithLabel(fct.loops(),"l99991")
loop3.force_loop_fusion()
fct.display()

## SIMPLIFICATION

fct.partial_eval()
fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.display()

## PARALLELIZATION 
fct.privatize_module()
fct.coarse_grain_parallelization()
fct.display()

fct.clean_labels()
fct.display()

# close the workspace
ws.close()


# RECIPE PREAMBLE (Matrix_Mult)
# RECIPE UNROLL_JAM(loop1,4)
# RECIPE_AGGREGATION (4)
# RECIPE UNROLL_JAM(loop0,2)
# RECIPE FUSION("l99991")
# RECIPE_SIMPLIFICATION
# RECIPE PARALLELIZATION
# RECIPE CLEAN_LABELS
# RECIPE POSTAMBLE
