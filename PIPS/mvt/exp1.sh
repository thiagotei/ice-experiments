export OMP_DISPLAY_ENV=true
export OMP_PROC_BIND=spread
export OMP_PLACES=cores
export OMP_NUM_THREADS=8
for i in 1 2 3 4 5;  do  ./mvt_orig; done
for i in 1 2 3 4 5;  do  ./mvt_tile_8x8; done
for i in 1 2 3 4 5;  do  ./mvt_tile_8x8_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_16x16; done
for i in 1 2 3 4 5;  do  ./mvt_tile_16x16_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32_uh8; done
export OMP_NUM_THREADS=16
for i in 1 2 3 4 5;  do  ./mvt_orig; done
for i in 1 2 3 4 5;  do  ./mvt_tile_8x8; done
for i in 1 2 3 4 5;  do  ./mvt_tile_8x8_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_16x16; done
for i in 1 2 3 4 5;  do  ./mvt_tile_16x16_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32_uh8; done
export OMP_NUM_THREADS=32
for i in 1 2 3 4 5;  do  ./mvt_orig; done
for i in 1 2 3 4 5;  do  ./mvt_tile_8x8; done
for i in 1 2 3 4 5;  do  ./mvt_tile_8x8_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_16x16; done
for i in 1 2 3 4 5;  do  ./mvt_tile_16x16_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32_uh8; done
export OMP_NUM_THREADS=40
for i in 1 2 3 4 5;  do  ./mvt_orig; done
for i in 1 2 3 4 5;  do  ./mvt_tile_8x8; done
for i in 1 2 3 4 5;  do  ./mvt_tile_8x8_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_16x16; done
for i in 1 2 3 4 5;  do  ./mvt_tile_16x16_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32_uh8; done
export OMP_NUM_THREADS=64
for i in 1 2 3 4 5;  do  ./mvt_orig; done
for i in 1 2 3 4 5;  do  ./mvt_tile_8x8; done
for i in 1 2 3 4 5;  do  ./mvt_tile_8x8_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_16x16; done
for i in 1 2 3 4 5;  do  ./mvt_tile_16x16_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32_uh4; done
for i in 1 2 3 4 5;  do  ./mvt_tile_32x32_uh8; done

