# RECIPE PREAMBLE (FILE,MODULE)
ws = workspace(FILE.c , name=FILE,deleteOnClose=True)
ws.activate("MUST_REGIONS")
ws.activate("RICE_REGIONS_DEPENDENCE_GRAPH")
ws.props.MEMORY_EFFECTS_ONLY=False
ws.props.PRETTYPRINT_DIV_INTRINSICS=False
fct = ws.fun.MODULE

# RECIPE RECTANGULAR TILING (loop,xD, c1,..,cx)
v1=vect_xD(c1,..,cx)
mat = matvect(v1,IdMat(xD))
loop1.loop_tiling (LOOP_TILING_MATRIX = mat)

# RECIPE GENERAL TILING (loop,mat="2 0 0 , 0 4 0 , 0 0 1")
loop1.loop_tiling (LOOP_TILING_MATRIX = mat)

# RECIPE DIAMOND 2D TILING (loop,k)
loop1.loop_tiling (LOOP_TILING_MATRIX = "K K , K -K")

# RECIPE FLAGS
fct.flag_loops()

# RECIPE SELECT LOOP(xx)
loop1 = findLoopWithLabel(fct.loops(),"xx")

# RECIPE UNROLL(loop1,K)
loop1.unroll(UNROLL_RATE=K)
+ RECIPE_SIMPLIFICATION++

# RECIPE FUSION()
fct.loop_fusion()

# RECIPE FUSION(loop1)
  RECIPE SELECT LOOP(loop1)
  fct.loop_fusion()

# RECIPE UNROLL_JAM(loop1,K)
   RECIPE UNROLL(loop1,K)
  # RECIPE_SIMPLIFICATION++
   RECIPE FUSION
   
# RECIPE_AGGREGATION (N)
fct.scalarization()
fct.split_initializations()
while i <= N+1
  fct.forward_substitute()
  i+=1
  
+ RECIPE_SIMPLIFICATION

# RECIPE HORIZONTAL_UNROLL(loop1,K)
  RECIPE UNROLL_JAM(loop1,K)
  RECIPE_AGGREGATION (K)

## RECIPE_SIMPLIFICATION++
# with Constant propagation
# 
fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()

#  RECIPE_SIMPLIFICATION
# wihtout Constant propagation
# useful when you wnat to keep symbolic variables
fct.simplify_control()
fct.dead_code_elimination()


# RECIPE PARALLELIZATION 
fct.privatize_module()
fct.coarse_grain_parallelization()

# RECIPE PRINTCODE
fct.display()

# RECIPE CLEAN_LABELS
fct.clean_labels()

# RECIPE POSTAMBLE
ws.close()
