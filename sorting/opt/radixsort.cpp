#include  "radixsort.h"


// A utility function to get maximum value in arr[] 
int getMax(int arr[], int b, int e) 
{ 
    int mx = arr[b]; 
    for (int i = b+1; i <= e; i++) 
        if (arr[i] > mx) 
            mx = arr[i]; 
    return mx; 
} 
  
// A function to do counting sort of arr[] according to 
// the digit represented by exp. 
void countSort(int arr[], int b, int e, int exp) 
{ 
    int nelem = e - b + 1;
    //int output[nelem]; // output array 
    std::vector<int> output(nelem, 0); // output array 
    int i, count[10] = {0}; 
  
    // Store count of occurrences in count[] 
    for (i = b; i <= e; i++) 
        count[ (arr[i]/exp)%10 ]++; 
  
    // Change count[i] so that count[i] now contains actual 
    //  position of this digit in output[] 
    for (i = 1; i < 10; i++) 
        count[i] += count[i - 1]; 
  
    // Build the output array 
    //for (i = n - 1; i >= 0; i--) 
    for (i = e; i >= b; i--) 
    { 
        output[count[ (arr[i]/exp)%10 ] - 1] = arr[i]; 
        count[ (arr[i]/exp)%10 ]--; 
    } 
  
    // Copy the output array to arr[], so that arr[] now 
    // contains sorted numbers according to current digit 
    //for (i = 0; i < n; i++) 
    //for (i = 0; i < nelem; i++) 
    //    arr[i+b] = output[i]; 
    memcpy(&arr[b], &output[0], sizeof(int)*nelem);
} 
  
// The main function to that sorts arr[] of size n using  
// Radix Sort 
void radixsort(int b, int e, int arr[]) 
{ 
    // Find the maximum number to know number of digits 
    int m = getMax(arr, b, e); 
  
    // Do counting sort for every digit. Note that instead 
    // of passing digit number, exp is passed. exp is 10^i 
    // where i is current digit number 
    for (int exp = 1; m/exp > 0; exp *= 10) 
        countSort(arr, b, e, exp); 
} 
