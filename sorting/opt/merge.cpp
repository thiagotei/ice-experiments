#include "merge.h"

void merge(int begin, int end, int middle, int a[])
{
	int len = end-begin+1;
    	std::vector<int> b(len, 0);
	
	int i = begin, j = middle, k = 0;
	while(i < middle && j <= end)
	{
		if (a[i] < a[j]) 
		{
			b[k] = a[i];
			i++;
		} else 
		{
			b[k] = a[j];
			j++;
		}	
		k++;
	}
	
	while(i < middle)
	{
		b[k] = a[i];
		i++;
		k++;
	}
	while(j <= end)
	{
		b[k] = a[j];
		j++;
		k++;
	}

	memcpy(&a[begin], &b[0], sizeof(int)*len);
}
