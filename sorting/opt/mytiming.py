import re

def getTiming(std, error):
    r = re.search('.*Sorting.*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    result = 'N/A'
    if m and len(m) >= 5:
       result = float(m[4])

    return result


