//#include "../utils/utils.h"
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <limits>
#include "common.h" 
#include "utils.h"
#include "insertionsort.h"
#include "quicksort.h"
#include "selectionsort.h"
#include "radixsort.h"
#include "radixsort16.h"
#include "merge.h"

int main(int argc, char *argv[]) {
    // Matrix dimensions (global)
    int iter, niter = 5;
    long long int n, r;

    // Timing variables
    double time, timeEnd, meanTime = 0.0;
    double minTime = std::numeric_limits<double>::max();
    double maxTime = 0.0;

    if (argc != 3) {
    std::cout << "Missing n and range parameter!" << std::endl;
    return 1;
    }

    /* Get command line arguments */
    n = atoi(argv[1]);
    r = atoi(argv[2]);

    // Allocate matrices
    //inp = create(k, n);
    //
    std::vector<ATYPE> orig;
    std::vector<ATYPE> inp;
    //std::vector<int> out(n, 0);

    // Initialize matrices
    initGlobal<ATYPE>(orig, n, r, 10);

    //printMatrix(A, m, k);
    //printMatrix(B, k, n);

    // Matrix multiplication!
    //time = getTime();

    std::ostringstream os;
    os << "answer-sort-" << n << "-" << r << ".txt";
    std::filebuf fout;
    fout.open(os.str(), std::ios::out);
    std::ostream fos(&fout);

    //char bufname[32];
    //snprintf(bufname, BUFNAMESIZE, "answer-sort-%lld.txt", n);
    //FILE *fout = fopen(bufname, "w");
    //if(!fout){
    //    fprintf(stderr,"ERROR! Could not open %s to output results!\n", bufname);
    //    exit(1);
    //}

    if(fopen(".test", "r")) {
        printArray<ATYPE>(orig, fos);
    }

    for (iter = 0; iter < niter; iter++) {
        inp = orig;
        time = rtclock();

    #pragma @ICE block=fastsort
    std::sort(inp.begin(), inp.end());
    #pragma @ICE endblock

        timeEnd = rtclock();
        time = timeEnd - time;
        if (time < minTime) {
            minTime = time;
        }
        if (time > maxTime) {
            maxTime = time;
        }
        meanTime += time;

        if(fopen(".test", "r") && iter == 0) {
            printArray<ATYPE>(inp, fos);
        }
    }
    printf("Sorting\t%lld\t%lld\tmin: %7.5lf\tmean: %7.5lf\tmax: %7.5lf ms %d it\n", 
            n, r, minTime*1.0e3, meanTime*1.0e3/niter, maxTime*1.0e3, niter);

    //fclose(fout);
    fout.close();
    return 0;
}
    //insertionsort(0, n-1, &inp[0]);
    //quicksort(0, n-1, &inp[0]);
    //selectionsort(0, n-1, &inp[0]);

    //quicksort(0, n/4, &inp[0]);
    //quicksort(n/4, n/2, &inp[0]);
    //merge(0, (n/2)-1, n/4, &inp[0]);
    //selectionsort(n/2, n-1, &inp[0]);
    //merge(0, n-1, n/2, &inp[0]);

