#!/usr/bin/env bash

#matshape=2048
#matshape=256
export ICELOCUS_SORT_NELEM=2097152 #1048576
export ICELOCUS_SORT_RNG=${ICELOCUS_SORT_NELEM}
export ICELOCUS_SORT_STOP=131072

ntests=20000000000
#ntests=15
cfile=sort.cpp
#uplimit=1000000
uplimit=35
stopaf=300
#locusfile=sorting.locus
#locusfile=sorting-svar.locus
locusfile=sorting-2.locus

ice-locus-hyperopt.py \
    --stop-after ${stopaf} --timeout --upper-limit ${uplimit} \
    --exprec -t ${locusfile} -f ${cfile} \
    --tfunc mytiming.py:getTiming -o suffix -u '.ice' --search --ntests ${ntests} \
    --saveexprec
    #--convonly

