#include "insertionsort.h"

void insertionsort(int begin, int end, int * arr)
{
        int j, temp;
                
        for (int i = begin; i <= end; i++){
                j = i;
                
                while (j > begin && arr[j] < arr[j-1])
		{
                          temp = arr[j];
                          arr[j] = arr[j-1];
                          arr[j-1] = temp;
                          j--;
        	}
	}
}
