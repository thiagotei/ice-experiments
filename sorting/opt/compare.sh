#!/bin/bash

resfile=./thunder_exp/range_small_fixed/sorting_`date +%Y%m%d%H%M`.res

echo "comp sort n rng min mean max it" > $resfile

ORIGFILE=output_orig.txt

#for comp in g++ icpc; do
for comp in g++ ; do
	make clean
	#for i in `seq 10 25`; do
	#for i in `seq 22 25`; do
	for i in `seq 23 25`; do

		n=$((2**${i}))
		#rng=$((2**30))
		rng=$((2**2))
		echo $comp "n: " $n " orig " "range: " $rng
		make CXX=$comp sort.exe
		./sort.exe $n $rng 2> $ORIGFILE | awk -v comp=$comp 'END{print comp " stl " $2 " " $3 " " $5 " " $7 " " $9 " " $11}' >> $resfile 

		#for sort in qk ins sel rdx; do
		for sort in qk rdx rdx16; do
			BASEOUTFILE=output_orig_${sort}.txt
			make CXX=$comp sort.${sort}.exe
			./sort.${sort}.exe $n $rng 2> $BASEOUTFILE | awk -v comp=$comp -v sort=$sort  'END{print comp " " sort " " $2 " " $3 " " $5 " " $7 " " $9 " " $11}' >> $resfile
			diff -q $BASEOUTFILE $ORIGFILE || exit
		done
	done	
done 
