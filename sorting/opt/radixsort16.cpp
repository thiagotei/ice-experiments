#include  "radixsort16.h"


// A utility function to get maximum value in arr[] 
int getMax16(int arr[], int b, int e) 
{ 
    int mx = arr[b]; 
    for (int i = b+1; i <= e; i++) 
        if (arr[i] > mx) 
            mx = arr[i]; 
    return mx; 
} 
  
void showbits( unsigned int x )
{
    for (int i = (sizeof(int) * 8) - 1; i >= 0; i--)
    {
       putchar(x & (1u << i) ? '1' : '0');
    }
    printf("\n");
}

// A function to do counting sort of arr[] according to 
// the digit represented by exp. 
void countSort16(int arr[], int b, int e, int exp) 
{ 

    int nelem = e - b + 1;
    //int output[nelem]; // output array 
    std::vector<int> output(nelem, 0); // output array 
    int i, count[16] = {0}; 
  
    // Store count of occurrences in count[] 
    for (i = b; i <= e; i++) 
        count[ (arr[i] >> exp) & 0xF ]++; 
        //count[ (arr[i]/exp)%10 ]++; 
  
    // Change count[i] so that count[i] now contains actual 
    //  position of this digit in output[] 
    //printf("Cnt[%d] = %d\n", 0, count[0]);
    for (i = 1; i < 16; i++) {
        count[i] += count[i - 1]; 
	//printf("Cnt[%d] = %d\n", i, count[i]);
    }
  
    // Build the output array 
    //for (i = n - 1; i >= 0; i--) 
    for (i = e; i >= b; i--) 
    { 
	int pos = (arr[i] >> exp) & 0xF;
	//printf("%d pos: %d v: %d\n", exp ,pos, arr[i]);
	//showbits();
        output[count[ pos ] - 1] = arr[i]; 
        count[ pos ]--; 
    } 
  
    // Copy the output array to arr[], so that arr[] now 
    // contains sorted numbers according to current digit 
    //for (i = 0; i < n; i++) 
    //for (i = 0; i < nelem; i++) 
    //    arr[i+b] = output[i]; 
    memcpy(&arr[b], &output[0], sizeof(int)*nelem);
} 
  
// The main function to that sorts arr[] of size n using  
// Radix Sort 
void radixsort16(int b, int e, int arr[]) 
{ 
    // Find the maximum number to know number of digits 
    int m = getMax16(arr, b, e); 
    //printf("m %d size %ld\n",m, sizeof(m));
  
    // Do counting sort for every digit. Note that instead 
    // of passing digit number, exp is passed. exp is 10^i 
    // where i is current digit number 
    //for (int exp = 1; m/exp > 0; exp *= 10) 
    //for (int exp = 0; m >> exp > 0; exp += 4) {
	//printf("exp %d, %d\n",exp, m >> exp);
	//countSort16(arr, b, e, exp); 
    //}

    int exp = 0;
    for (int x = m ; x > 0; x = x >> 4) {
        //printf("x %d exp %d\n", x, exp);
	countSort16(arr, b, e, exp); 
	exp += 4;
    }
} 
