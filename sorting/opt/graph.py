#!/usr/bin/env python

import pandas as pd
import plotly.plotly as py
import plotly.graph_objs as go

datadir = '.'
fname = "sorting_thunder.res"
df = pd.read_csv(datadir+"/"+fname, delimiter="\s+")

x = df['n'].unique()

trace_qk_gcc = go.Scatter(
		x = x,
		#y = df.query('comp == g++ & sort == qk')['mean'],
		y = df[(df['comp'] == "g++") & (df["sort"] == "qk")]['mean'],
		mode = "lines+markers",
		name = "QK g++"
		)
data = [trace_qk_gcc]
#fig = go.Figure(data=data)
#py.plot(fig)
py.plot(data, filename="qk_gcc_scatter")


