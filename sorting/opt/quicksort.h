#ifndef __QUICKSORT_H__
#define __QUICKSORT_H__

#include <iostream>
#include <ostream>


void quicksort(int begin, int end, int *arr);
int pivot(int first, int last, int *a);
void swap(int& a, int& b);
//void swap(int a, int b, int *arr );
int partition(int a[], int l, int u);
int partitionNew(int a[], int l, int u);
int partitionPiv(int a[], int l, int u, int pivot);

#endif
