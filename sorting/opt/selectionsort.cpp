#include "selectionsort.h"


void swap(int *xp, int *yp)  
{  
    int temp = *xp;  
    *xp = *yp;  
    *yp = temp;  
}  
  
void selectionsort(int begin, int end, int arr[])  
{  
    int i, j, min_idx;  
  
    // One by one move boundary of unsorted subarray  
    for (i = begin; i < end; i++)  
    {  
        // Find the minimum element in unsorted array  
        min_idx = i;  
        for (j = i+1; j <= end; j++)  
        	if (arr[j] < arr[min_idx])  
            		min_idx = j;  
  
        // Swap the found minimum element with the first element  
        swap(&arr[min_idx], &arr[i]);  
    }  
}  
