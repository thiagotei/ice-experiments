//#include "../utils/utils.h"
#include "utils.h"

int main(int argc, char *argv[]) {
    // Matrix dimensions (global)
    int n, iter, niter = 1;

    // Timing variables
    double time, timeEnd;

    if (argc != 2) {
	std::cout << "Missing n parameter!" << std::endl;
	return 1;
    }

    /* Get command line arguments */
    n = atoi(argv[1]);

    // Allocate matrices
    //inp = create(k, n);
    std::vector<int> inp;

    // Initialize matrices
    initGlobal<int>(inp, n);
   
    //printMatrix(A, m, k);
    //printMatrix(B, k, n);

    // Matrix multiplication!
    //time = getTime();
    //printArray<int>(inp);
    time = rtclock();
    for (iter = 0; iter < niter; iter++) {
	std::sort(inp.begin(), inp.end());	
    }
    timeEnd = rtclock();
    time = timeEnd - time;
    //printArray<int>(inp);
    printf("Sorting\t%d\t%7.5lf ms\n", n, time*1.0e3/niter);
    return 0;
}
