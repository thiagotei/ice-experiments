#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdio.h>
#include <stdlib.h>
//#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <iostream>
#include <vector>

#define MIN(a,b) (((a) < (b)) ? (a) : (b))

/*
double getTime() {
    struct timespec tp;
    int err;
    err = clock_gettime(CLOCK_MONOTONIC, &tp);
    if(err) {
        fprintf(stderr, "Error reading time. Aborting...\n");
        exit(-1);
    }
    //return tp.tv_sec*1.0 + tp.tv_nsec*1.0e-9;
    return tp.tv_sec*1.0 + tp.tv_usec*1.0e-6;
}
*/


double rtclock()
{
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

template <class T>
void initGlobal(std::vector<T>& x,      // Array to be initialized
                long long int n,          // 
                long long int range,
                unsigned int seed=time(NULL)) {

    srand(seed);
    long long int i;
    for(i = 0; i < n; i++) {
    T v = (T) rand() % range;
    x.push_back(v);
    }
}

inline double*  create(int nr, int nc) {
    return (double *)malloc(sizeof(double)*nr*nc);
}

template <class T>
//void printArray(const std::vector<T>& x, FILE *buf=stderr) {
void printArray(const std::vector<T>& x, std::ostream &buf=std::cerr) {

    typename std::vector<T>::const_iterator it = x.begin();
    for (; it != x.end(); ++it) {
        //std::cerr << *it << " ";
        buf << *it << " ";
        //fprintf(buf, "%d ", *it);
    }
    buf << std::endl;
    //fprintf(buf, "\n");
}

#endif // __UTILS_H__
