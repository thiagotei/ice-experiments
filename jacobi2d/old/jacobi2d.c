#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <pips_runtime.h>

#define T 1000
#ifndef N
#define N 256
#endif

double mysecond(void);

void jacobi2d(float a[2][N][N])
{
  int i, j, t;

#pragma @ICE loop=jacobi2d
    for (t=0; t<T; t++) {
        for (i=2; i<N-1; i++) {
            for (j=2; j<N-1; j++) {
                a[(t+1)%2][i][j]= 0.2*(a[t%2][i][j]+a[t%2][i][j-1]+a[t%2][i][1+j]+a[t%2][1+i][j]+a[t%2][i-1][j]);
            }
        }
    }
}

int main()
{
  float (*a)[2][N][N];
  a=malloc((2)*(N)*(N)*sizeof(float));
  int i,j;

  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      (*a)[0][i][j]=j/N;
  double  tStart = mysecond();
  jacobi2d(*a);
  double  lTime = mysecond() - tStart;

//  printf("*** Minimum global time  : %.3f (ms)\n",lTime);

  printf("Jacobi2D size = %d steps = %d\n", N, T);
  printf("Time        = %.2e secs\n", lTime);
  double rate = 5.0 * (T-1) * (N-2) * ((N-2) / lTime);
  printf("Rate        = %.2e MFLOP/s\n", rate * 1.0e-6);
 
  return 0;
}
