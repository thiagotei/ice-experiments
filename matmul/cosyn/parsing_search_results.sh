#!/usr/bin/env bash

# Check if an argument is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <files to parse> -- example: $0 locus_search_*.txt"
    exit 1
fi

testround="three"

#if [ $# -eq 2 ]; then
output_dir=./ #results_lsearch_${testround}
#else
#  echo "Usage $1 <output dir>"
#fi

#if [ ! -d $output_dir ]; then
#  echo "$output_dir directory does not exist!"
#  exit 1
  #echo "$output_dir directory already exists! If you want to overwrite it, please remove it first. Be careful!"
  #exit 1
#else
  #mkdir $output_dir
#fi

#set -x

for f in "$@"; do
  # check if file exists
  if [ ! -f "$f" ]; then
    echo "Error!! $f is not a file!"
    continue
  else
    echo "Parsing $f ..."
  fi

  # get the run dir
  rundir=`grep -m 1 "Run dir" $f | cut -d" " -f11`
  if [ -z "$rundir" ]; then
    echo "Error!! Run dir not found in $f"
    rundir="N/A rundir"
  fi

  # get the best time
  besttime=`grep "Final Summary" $f | cut -d" " -f18`
  if [ -z "$besttime" ]; then
    echo "Error!! Best time not found in $f"
    continue
  fi
  #echo "Best time: " $besttime 

  # get compiler
  cc=`grep ICELOCUS_CC $f | cut -d" " -f11`
  if [ -z "$cc" ]; then
    echo "Error!! Compiler not found in $f"
    continue
  fi
  cc=${cc//\"/}
  # Assuming the compiler use omp for vectorization
  cc=${cc} #-omp

  # get configuration
  shapeM=`grep "(\"ICELOCUS_MATSHAPE_M\") by" $f | cut -d" " -f11`
  if [ -z "$shapeM" ]; then
    echo "Error!! Shape M not found in $f"
    continue
  fi
  shapeM=${shapeM//\"/}  # Remove quotes from the string
  
  shapeN=`grep "(\"ICELOCUS_MATSHAPE_N\") by" $f | cut -d" " -f11`
  if [ -z "$shapeN" ]; then
    echo "Error!! Shape N not found in $f"
    continue
  fi
  shapeN=${shapeN//\"/}  # Remove quotes from the string
  
  shapeK=`grep "(\"ICELOCUS_MATSHAPE_K\") by" $f | cut -d" " -f11`
  if [ -z "$shapeK" ]; then
    echo "Error!! Shape K not found in $f"
    continue
  fi
  shapeK=${shapeK//\"/}  # Remove quotes from the string

  numth=`grep "(\"ICELOCUS_OMP_NUMTH\") by" $f | cut -d" " -f11`
  if [ -z "$numth" ]; then
    echo "Error!! Num threads not found in $f"
    continue
  fi
  numth=${numth//\"/}  # Remove quotes from the string

  host=`grep "run: Host = " $f | cut -d" " -f10`
  if [ -z "$host" ]; then
    echo "Error!! Host not found in $f"
    continue
  fi
  host=${host//\"/}  # Remove quotes from the string

  # calculate the gflops
  #gflop=`echo "scale=8;($shapeM * $shapeN * $shapeK * 2) / (1000000000)" | bc -l`
  gflop=$(python3 -c "print($shapeM * $shapeN * $shapeK * 2 * 1.e-9)")
  #gflops=$(echo "scale=8;($gflop / ( $besttime ))" | bc -l)
  gflops=$(python3 -c "print($gflop / $besttime)")


  echo "Shape: $shapeM $shapeN $shapeK Numth = $numth GFLOP = $gflop fptime = $besttime GFLOPS = $gflops $f $rundir"
  conf="${shapeM}:${shapeN}:${shapeK}"

  #output_file=results_${testround}/time.out.${cc}.${ifw}:${ifh}:${numimgs}:${nIFM}:${nOFM}:${kw}:${kh}.lsearch_${testround}
  output_file=${output_dir}/time.out.${cc}.locus.${numth}th.${conf}.${testround}.${host}
  if [ -f $output_file ]; then
    #echo "File $output_file already exists! If you want to overwrite it, please remove it first. Be careful!"
    # Check the one with highest GFLOPS
    old_gflops=`grep -i "GFLOPS" ${output_file} | cut -d" " -f4`
    if (( $(echo "$gflops > $old_gflops" | bc -l) )); then
      echo "Overwriting $output_file ! Old GFLOPS = $old_gflops New GFLOPS = $gflops"
      rm $output_file
      #echo "Conf: $conf" > $output_file
      #echo "GFLOP = $gflop fptime = $besttime GFLOPS = $gflops" >> $output_file
    else
      echo "Not overwriting $output_file ! Old GFLOPS = $old_gflops New GFLOPS = $gflops"
    fi
    #exit 1
  fi

  # print both in results_${testround}/time.out.icx-omp.7:7:8:32:128:5:5:2:2:1.vs3_2
  echo "Conf: $conf" > $output_file
  echo "GFLOP = $gflop fptime = $besttime" >> $output_file
  echo "Iterations 1 GFlops $gflops" >> $output_file

done
