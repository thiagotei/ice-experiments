#!/usr/bin/env python3

import re
import statistics as stat

#Matrixsize= 16 16 256 | Iterations 5 Time(milisec) 0.08235 0.08056 0.08053 0.08041 0.08042
#Matrixsize= 2048 2048 2048 beta= 0.335009 alpha= -0.364523 | Iterations 5 Time(milisec) 1003.33858 987.56492 982.16702 981.46810 981.45794
#Matrixsize= 2048 2048 2048 | Iterations 5 Time(milisec) 1003.33858 987.56492 982.16702 981.46810 981.45794

buf="""Warmup iterations: 2 ...
Matrixsize= 11 11 704 | Iterations 30 Time(milisec) 0.01507000 0.01506700 0.01506100 0.01513000 0.01506000 0.01509500 0.01502700 0.01505400 0.01501400 0.01498500 0.01505400 0.01502000 0.01505400 0.01490600 0.01510000 0.01505200 0.01509200 0.01508600 0.01507400 0.01502500 0.01512600 0.01510400 0.01509000 0.01504900 0.01483900 0.01501800 0.01501700 0.01513600 0.01498100 0.01493700
Iterations 30 GFlops 8.84555785e+01 8.84379696e+01 8.84027517e+01 8.88077573e+01 8.83968820e+01 8.86023197e+01 8.82031837e+01 8.83616642e+01 8.81268783e+01 8.79566585e+01 8.83616642e+01 8.81620962e+01 8.83616642e+01 8.74929564e+01 8.86316679e+01 8.83499249e+01 8.85847107e+01 8.85494929e+01 8.84790571e+01 8.81914444e+01 8.87842787e+01 8.86551465e+01 8.85729715e+01 8.83323159e+01 8.70996901e+01 8.81503569e+01 8.81444872e+01 8.88429752e+01 8.79331799e+01 8.76749155e+01 
"""

bufnew="""Results will be written to ./outputs/output_matmul_3_3_3000.txt .
Warmup iterations: 10 .
Matrixsize= 3 3 3000 Iterations 30000
AvgTime(milisec) 0.0000023468 GFLOPS 2.30102888e+01"""

bufPMU="""Warmup iterations: 2 .
Initializing PMU analyzer... 
Matrixsize= 256 256 256 Iterations 5
AvgTime(seconds) 0.0135487224 GFLOPS 2.47657536e+00
PMU Analyzer Results: 
PMU Events Names L1-dcache-load-misses l2_rqsts.all_demand_miss LLC-load-misses dTLB-load-misses fp_arith_inst_retired.vector fp_arith_inst_retired.scalar_double
PMU Events Avg 16965723 1098 279 16 14680064 4194306
PMU Events Min 16965502 213 1 0 14680064 4194304
PMU Events Max 16966378 5856 1787 113 14680065 4194307
"""

def getTimingMatMul(std, error):
  r = re.search('.*AvgTime.*',std)
  m = []
  if r:
    m = re.split('\s+', r.group())
  if m and len(m) >= 4:
    vals = []
    unit = m[0]
    unit = unit[unit.rfind('(')+1:unit.rfind(')')]
    metric = float(m[1])
    vals.append(float(m[1]))
    gvals = []
    gvals.append(float(m[3]))
    # Get Gflops
    if len(gvals) == len(vals):
      exps = list(zip(vals, gvals))
    else:
      raise RuntimeError('Mismatch in number of iterations for timing and Gflops')
    #

    # Get PMU Events
    events_data = {}
    keys = ['Names', 'Avg', 'Min', 'Max']
    keys_metric = [k for k in keys if k != 'Names']
    for k in keys:
      r = re.search(f'.*PMU Events {k}.*', std)
      m = []
      if r:
        m = re.split('\s+',r.group())
        events_data[k] = m[3:]
      
    #print(f"PMU: {events_data}")
    
    unitpmu = []
    descpmu = []
    expspmu = []
    if events_data:
      for i,n in enumerate(events_data['Names']):
        for k in keys_metric:
          l = n+'_'+k
          expspmu.append(int(events_data[k][i]))  # Append to tuple
          unitpmu.append('hwcounter')  # Append to tuple
          descpmu.append(l)
        ##
      ##

    final_exps = exps[0] + tuple(expspmu)

    result = {'metric': metric,
            # it must be tuple of tuples despite being a single timer
            #'exps': tuple([(v,) for v in vals]),
            'exps': (final_exps,),
            'unit': (unit, 'total') + tuple(unitpmu),
            'desc': ('Time','GFlops') + tuple(descpmu)}
  else:
    result = {'metric': 'N/A'}

  return result

def getTimingMatMulOld(std, error):
  r = re.search('.*Time.*',std)
  m = []
  if r:
    m = re.split('\s+', r.group())
  if m and len(m) >= 7:
    vals = []
    unit = m[7]
    unit = unit[unit.rfind('(')+1:unit.rfind(')')]
    numit = int(m[6])
    for it in range(8,8+numit):
      vals.append(float(m[it]))
    ##
    metric = stat.median(vals)

    # Get Gflops
    rg = re.search('.*GFlops.*',std)
    mg = []
    if r:
      mg = re.split('\s+', rg.group())
    if mg and len(mg) >= 4:
      gvals = []
      numit = int(mg[1])
      for it in range(3,3+numit):
        gvals.append(float(mg[it]))
      ##
    #
    if len(gvals) == len(vals):
      exps = zip(vals, gvals)
    else:
      raise RuntimeError('Mismatch in number of iterations for timing and Gflops')
    #
    
    result = {'metric': metric,
            # it must be tuple of tuples despite being a single timer
            #'exps': tuple([(v,) for v in vals]),
            'exps': tuple(exps),
            'unit': (unit, 'GFlops'),
            'desc': ('Total','Total')}
  else:
    result = {'metric': 'N/A'}

  return result
#

if __name__ == '__main__':
  print(getTimingMatMul(bufPMU, ""))
