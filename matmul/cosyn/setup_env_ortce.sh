#!/usr/bin/env bash

echo "Setting environment for ORTCE..."

#ml load intel/oneapi/2024.2 gnu/10.5.0 boost/1.80.0 intel-comp-rt/agama-ci-devel/821.36
conda activate conda-env 
# ml load intel/oneapi/2023.2.1
source /opt/intel/oneapi/compiler/2025.0/env/vars.sh
# required by onednn conv 
#source /opt/intel/oneapi/dnnl/2024.2/env/vars.sh

# activate locus python environment

myhomedir=/nfs/site/home/tsantosf
#source ${myhomedir}/locus/pyenv2/bin/activate
export ICE_CMPOPTS_PATH=${myhomedir}/locus/uiuc-compiler-opts-fullblk/install-gcc10.5

export OMP_PLACES="threads"
export OMP_PROC_BIND="close"
export OMP_NUM_THREADS=1

echo "Done setting environment for ORTCE..."
