#!/usr/bin/env bash

M=8
N=8
K=1024
export ICELOCUS_MATSHAPE_M=$M
export ICELOCUS_MATSHAPE_N=$N
export ICELOCUS_MATSHAPE_K=$K
./sedchange.sh $M $N $K matmul.base.c matmul.c;

locus -t matmul-v5.locus -f matmul.c  -o suffix -u ".opt${M}x${N}x${K}" --preproc /nfs/site/home/tsantosf/libxsmm/include --debug 
