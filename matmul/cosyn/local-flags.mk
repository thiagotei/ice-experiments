
CC ?= icc


#ifeq ($(CC), icc)
#OPTFLAGS := -O3 -xHost -ansi-alias #-ipo #-fp-model precise
#OMPFLAGS := -qopenmp
#else
#  ifeq ($(CC), xlc)
#    OPTFLAGS := -O3 -qhot
#    OMPFLAGS := #-qsmp
#  else
#    #@echo 'Blah'#'$(CC)'
#    ifeq ($(findstring clang, $(CC)), clang)
#      OPTFLAGS := -O3 -fvectorize
#      OMPFLAGS := -fopenmp
#    else
#      #gcc
#      OPTFLAGS := -O3 -mtune=native -ftree-vectorize # -march=native
#      OMPFLAGS := -fopenmp
#    endif
#endif
#endif

ifeq ($(CC),icx) 
  ifeq ($(USE_OPENMP),true)
    OMPFLAGS := -qopenmp
    ifeq ($(USE_OFFLOAD),true)
      GPUFLAGS = -fiopenmp -fopenmp-targets=spir64
    endif
  endif
  #OPTFLAGS= $(GPUFLAGS) $(OMPFLAGS) -g -qopt-report=3 -fpic -diag-disable 1879,3415,10006,10010,10411,13003 -O3 -fno-alias -ansi-alias -qoverride-limits -fp-model=fast -DNDEBUG -D_REENTRANT #-xSSE4.2 #-qnextgen-diag
  OPTFLAGS += $(GPUFLAGS) $(OMPFLAGS) -fpic -diag-disable 1879,3415,10006,10010,10411,13003 -D_REENTRANT -O3 -ansi-alias -fp-model=fast -xCore-AVX512 -mprefer-vector-width=512 #-DNDEBUG -xSSE4.2 #-qnextgen-diag
else ifeq ($(CC),icc)
  ifeq ($(USE_OPENMP),true)
    OMPFLAGS := -qopenmp
    ifeq ($(USE_OFFLOAD),true)
      GPUFLAGS = -fiopenmp -fopenmp-targets=spir64
    endif
  endif
  #OPTFLAGS=-g -qno-offload -fpic -wd266 -diag-disable 1879,3415,10006,10010,10411,13003 -O3 -fno-alias -ansi-alias -qoverride_limits -fp-model fast=2 -DNDEBUG -D_REENTRANT -xCore-AVX512 -qopt-zmm-usage=high
  #OPTFLAGS=-g -qopt-report=5 -qno-offload -fpic -wd266 -diag-disable 1879,3415,10006,10010,10411,13003 -O3 -fno-alias -ansi-alias -qoverride_limits -fp-model fast=2 -DNDEBUG -D_REENTRANT -xSSE4.2
  OPTFLAGS += -fpic -diag-disable 1879,3415,10006,10010,10411,13003,10441 -D_REENTRANT -O3 -fno-alias -ansi-alias -qoverride-limits -fp-model=fast  -xCore-AVX512 -qopt-zmm-usage=high #-DNDEBUG
  #OPTFLAGS=-O3 -ansi-alias -fp-model fast=2 -xCore-AVX512  -qopt-zmm-usage=high 
  #OPTFLAGS=-g -O1
else ifeq ($(CC),gcc)
  OPTFLAGS +=-Wall -O3 
endif

INCS += -I.
CFLAGS += -Wall
LDFLAGS += -lm

