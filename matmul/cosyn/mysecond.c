#include <mysecond.h>

/* The gettimeofday function may have low resolution, so do not use this
   for high-resolution (microsecond) timing.

   Note that gettimeofday is not guaranteed to be monotone increasing.
   However,for short timing tests, it is likely to be to be accurate.
   This function is used because it is the most portable timer without
   using either OpenMP or MPI.
*/
double mysecondOld(void)
{
    struct timeval tVal;
    gettimeofday(&tVal,NULL);
    return (double)tVal.tv_sec + 1.0e-6 * (double)tVal.tv_usec;
}

double mysecond(void)
{

    struct timespec tVal;
    clock_gettime(CLOCK_MONOTONIC, &tVal);
    return (double)tVal.tv_sec + 1.0E-9 * (double)tVal.tv_nsec;
}

void mygettime(struct timespec *tVal)
{
    clock_gettime(CLOCK_MONOTONIC, tVal);
}

// Returns difference in seconds
double mydifftime(struct timespec *beg, struct timespec *end)
{
    double accum = (double) (end->tv_sec - beg->tv_sec) + ((double)(end->tv_nsec - beg->tv_nsec) * 1.0E-9);
    return accum;
}

// Returns in miliseconds 
double mydifftimems(struct timespec *beg, struct timespec *end) 
{
    //long int tmp1 = (end->tv_nsec - beg->tv_nsec);
    //long int tmp2 = tmp1 * 1.0E-6;
    //double tmp2 = tmp1 * 1.0E-6;
    //long int tmp3 = (end->tv_sec - beg->tv_sec);
    //long int tmp4 = tmp3 * 1E3;
    //double tmp4 = tmp3 * 1.0E3;
    //printf("[mysecond/mydifftimems] tmp1: %ld  tmp2: %lf tmp3: %ld tmp4: %lf\n",tmp1, tmp2, tmp3, tmp4);
    double accum =  ((double)(end->tv_sec - beg->tv_sec) * 1.0E3) + ((double)(end->tv_nsec - beg->tv_nsec) * 1.0E-6);
    return accum;
}

// Returns in microseconds 
double mydifftimeus(struct timespec *beg, struct timespec *end) 
{
    double accum = ((double)(end->tv_sec - beg->tv_sec) * 1.0E6) + ((double)(end->tv_nsec - beg->tv_nsec) * 1.0E-3);
    return accum;
}

// Returns in nanoseconds
double mydifftimens(struct timespec *beg, struct timespec *end) 
{
    double accum = ((end->tv_sec - beg->tv_sec) * 1E9) + (end->tv_nsec - beg->tv_nsec);
    return accum;
}

/* By adding this name, most Fortran program will be able to use
   mysecond() as a double precision function */
double mysecond_(void)
{
    return mysecond();
}
