#!/usr/bin/env python3

import toml, argparse

# receive file path from command line using argparse 
args = argparse.ArgumentParser()
args.add_argument("file", help="path to toml file")
arg = args.parse_args()

def parsefile():

  with open(arg.file, "r") as f:
    data = toml.load(f)
    print(data)
    shapes=data['run']['input']
    output = []
    for s in shapes:
      # replace space by :
      n = s.replace(" ", ":")
      output.append(n)
  #
  print("\n".join(output))
#

if __name__ == "__main__":
  parsefile()