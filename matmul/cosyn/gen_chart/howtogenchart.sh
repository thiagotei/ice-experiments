#!/usr/bin/env bash

declare -A mydict
mydict["oneprime"]="1'"
mydict["two"]="2"
mydict["three"]="3"
mydict["three_locusv4"]="3"

#groups=(oneprime two three three_locusv4)
groups=(three)

for group in ${groups[@]}; do
    echo "Group: $group ..."
    if [ ! -f agg_results_${group}.csv ]; then
        echo "agg_results_${group}.csv not found! Generating csv..."
        if [ ! -d "../results_QZ1J-ICX-PVC_${group}" ]; then
            echo "Error!! ../results_QZ1J-ICX-PVC_${group} not found!"
            continue
        fi
        ./group_data.sh ../results_QZ1J-ICX-PVC_${group} > /dev/null
        if [ $? -ne 0 ]; then
            echo "Error!!"
            exit 1
        fi
        if [ ! -f "agg_results.csv" ]; then
            echo "Error!! agg_results.csv not found!"
            exit 1
        fi
        line_count=$(wc -l < "agg_results.csv")
        if [ "$line_count" -lt 2 ]; then
            echo "Error!! agg_results.csv is empty!"
            exit 1
        fi
        mv -v agg_results.csv agg_results_${group}.csv
        echo "Done generating csv!!"
    else
        echo "agg_results_${group}.csv found!"
    fi

    #sufcharname="mklVSicx_${group}.png"
    sufcharname="mklVSicxVSlocus_${group}.pdf"
    chartname="gemm_cc_speedup_${sufcharname}"
    if [ ! -f "$chartname" ]; then
        echo "${chartname} not found! Generating chart..."
        python3 gen_plot.py -i agg_results_${group}.csv -o $sufcharname --chart compspeedup --title "DGEMM Single-core (${mydict[$group]})" --basecode "Mkl-seq" --laprec 2 #--sortx gflops #--ytitle "Speedup over Baseline"
        if [ $? -ne 0 ]; then
            echo "Error!!"
            exit 1
        fi
    else
        echo "${chartname} found! Want to redo it? rm -v ${chartname}"
        continue
    fi
done