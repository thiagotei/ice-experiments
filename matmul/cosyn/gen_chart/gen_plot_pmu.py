#!/usr/bin/env python3
import pandas as pd
import argparse, math
import matplotlib.pyplot as plt
import numpy as np
from natsort import natsorted, natsort_keygen, index_natsorted

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input csv file path.")
parser.add_argument('-o', type=str, required=True, help="Output pdf file name.")
parser.add_argument("--chart", nargs='+', required=False,
        default=('corr'),
        choices=('corr','relcnts','metric'),
        help="Data analytics and chart options.")
parser.add_argument("--metric", type=str, required=False, default='GFlops',
        help="Metric used as reference to plot.")
parser.add_argument("--descending", action='store_false', required=False,
        help="Sort the metric in descending order.")
args=parser.parse_args()

def autolabel(rects, pltobj, scale=None, prec=2, lim=None):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        #if lim is None or height > lim:
        vpos = height if lim is None or height < lim else lim
        height = height if scale is None else height * scale
        pltobj.text(rect.get_x() + rect.get_width()/2., vpos,
            #f'{int(height)}',
            #f'{round(height,2):.2f}',
            f'{round(height,prec)}' if prec > 0 else f'{int(round(height))}',
            #ha='center', va='bottom', fontsize='5')
            ha='center', va='bottom', fontsize='x-small')
        #
    ##
###

def plot_counters_per_config(df):
  # Get the unique shapes
  shapes_gb= df.groupby(['matshape_m','matshape_n','matshape_k'])
  shapes = shapes_gb.groups.keys()
  #print(shapes)

  numcols = 1
  numrows = math.ceil(len(shapes))
  grid = (numrows, numcols)
  figsize = (numcols * 20, numrows * 8)
  plt.figure(figsize=figsize)

  for i, shape in enumerate(shapes):
    sbrow = i // numcols
    sbcol = i % numcols
    ax_i = plt.subplot2grid(grid, (sbrow,sbcol))
    #print(shape)
    
    # Get the group
    shape_group = shapes_gb.get_group(shape)
    metric = 'GFlops'
    cnts = ['GFlops',
            'cpu-cycles_Avg',
            'L1-dcache-load-misses_Avg',
            'l2_rqsts.all_demand_miss_Avg',
            'LLC-load-misses_Avg',
            'fp_arith_inst_retired.512b_packed_double_Avg',
            'fp_arith_inst_retired.vector_Avg']

    # get all cnts for the shape as value from 1 to 100
    for cnt in cnts:
      maxv = shape_group[cnt].max()
      minv = shape_group[cnt].min()
      shape_group[cnt+'rel'] = (shape_group[cnt] - minv) / (maxv - minv + 1e-6) * 100
      #shape_group[cnt+'rel'] = shape_group[cnt].rank(pct=True) * 100
      print(shape_group[cnt+'rel'])
    #
    sorted_sg = shape_group.sort_values(by=metric, ascending=True)

    cfgs = sorted_sg['cfgid'].unique()
    bar_width = 1 / (len(cnts) + 1)
    bar_positions = np.arange(len(cfgs))

    for j, cnt in enumerate(cnts):
      yval = sorted_sg[cnt+'rel']
      bars = ax_i.bar(bar_positions + j * bar_width, yval, bar_width, label=cnt)
    ##

    ax_i.set_xticks(bar_positions + bar_width * (len(cnts) - 1) / 2)
    ax_i.set_xticklabels(cfgs, rotation=0, fontsize=8, ha='center')
    ax_i.set_title(f"{shape[0]}x{shape[1]}x{shape[2]}")
    ax_i.set_xlabel('Configuration ID')
    ax_i.set_ylabel('Rank (%)')
    ax_i.legend(loc='upper left')
    plt.savefig(args.o)
    print(f"Saved chart in {args.o} .")
##

def plot_correlation_per_shape(df):
  # Get the unique shapes
  shapes_gb= df.groupby(['matshape_m','matshape_n','matshape_k'])
  shapes = shapes_gb.groups.keys()
  #print(shapes)

  numcols = 1
  numrows = math.ceil(len(shapes))
  grid = (numrows, numcols)
  figsize = (numcols * 20, numrows * 8)
  plt.figure(figsize=figsize)

  for i, shape in enumerate(shapes):
    sbrow = i // numcols
    sbcol = i % numcols
    ax_i = plt.subplot2grid(grid, (sbrow,sbcol))
    #print(shape)
    
    # Get the group
    shape_group = shapes_gb.get_group(shape)
    #print(shape_group[['cfgid','GFLops']])
    metric = 'GFlops'
    #metric_median = shape_group.groupby(['matshape_m','matshape_n','matshape_k','cfgid'])[metric].median().reset_index()
    #metric_median = shape_group.groupby(['cfgid'], as_index=False)[metric].median()
    #metric_median_sorted = metric_median.sort_values(by=metric, ascending=True)
    #print("Index after mean:", metric_median.index.values)
    #print(shape_group.iloc[metric_median.index])

    #cfgs = metric_median['cfgid'].unique()
    #bar_positions = np.arange(len(cfgs))
    #print("GFlops median:")
    #print(metric_median)


    #metric_median = shape_group.groupby(['cfgid'], as_index=False)[metric].median()
    #print(list(metric_median.index))
    #print(metric_median)
    #metric_median_t = shape_group.groupby(['cfgid'])[metric].transform('median')
    #print(metric_median_t)
    #print("df",df[metric])
    #print("median",metric_median_t.idx())
    #print(df[metric_median_t == df[metric]])

    columns_to_correlate=['GFlops',
                  'cpu-cycles_Avg',
                  'L1-dcache-load-misses_Avg', 
                  #'L1-dcache-load-misses_Min', 
                  #'L1-dcache-load-misses_Max', 
                  'l2_rqsts.all_demand_miss_Avg', 
                  #'l2_rqsts.all_demand_miss_Min', 
                  #'l2_rqsts.all_demand_miss_Max', 
                  #'LLC-load-misses_Avg', 
                  #'LLC-load-misses_Min', 
                  #'LLC-load-misses_Max', 
                  #'dTLB-load-misses_Avg', 
                  #'dTLB-load-misses_Min', 
                  #'dTLB-load-misses_Max', 
                  'fp_arith_inst_retired.512b_packed_double_Avg',
                  'fp_arith_inst_retired.vector_Avg', 
                  #'fp_arith_inst_retired.vector_Min', 
                  #'fp_arith_inst_retired.vector_Max', 
                  #'fp_arith_inst_retired.scalar_double_Avg', 
                  #'fp_arith_inst_retired.scalar_double_Min', 
                  #'fp_arith_inst_retired.scalar_double_Max'
                  ]
    # Get the correlation matrix
    corr = shape_group[columns_to_correlate].corr().round(2)
    mask = np.triu(np.ones_like(corr, dtype=bool))
    #corr = shape_group[columns_to_correlate].corrwith(shape_group[metric])
    #print(type(corr),corr)
    # Plot the correlation matrix
    import seaborn as sns
    sns.heatmap(corr, annot=True, mask=mask, vmax=1, vmin=-1, ax=ax_i)
    #im = ax_i.imshow(corr.reshape(1,-1), cmap='viridis', aspect='auto')
    #ax_i.colorbar(im)

    ax_i.set_title(f"{shape[0]}x{shape[1]}x{shape[2]}")   

    if False:
      ax_i.bar(bar_positions, metric_median['GFLops'], label='GFlops median')
      ax_i.set_xticks(bar_positions)
      ax_i.set_xticklabels(cfgs, rotation=45, fontsize=8, ha='center')
      ax_i.set_title(f"{shape[0]}x{shape[1]}x{shape[2]}")
      ax_i.set_xlabel('Configuration ID')
      ax_i.set_ylabel('GFlops')
  #

  plt.savefig(args.o)
#

def plot_metric_per_shape(df, metric="GFlops", ascending=True):
  # Get the unique shapes
  shapes_gb= df.groupby(['matshape_m','matshape_n','matshape_k'])
  shapes = shapes_gb.groups.keys()
  #print(shapes)

  numcols = 1
  numrows = math.ceil(len(shapes))
  grid = (numrows, numcols)
  figsize = (numcols * 20, numrows * 8)
  plt.figure(figsize=figsize)

  for i, shape in enumerate(shapes):
    sbrow = i // numcols
    sbcol = i % numcols
    ax_i = plt.subplot2grid(grid, (sbrow,sbcol))
    #print(shape)
    
    # Get the group
    shape_group = shapes_gb.get_group(shape)
    #gb_shape_cfg = shape_group.groupby(['matshape_m','matshape_n','matshape_k','cfgid'])[metric].median()
    #print("gb_shape_cfg: ", gb_shape_cfg)
    metric_median = shape_group.groupby(['matshape_m','matshape_n','matshape_k','cfgid'])[metric].median().reset_index()
    metric_median = metric_median.sort_values(by=metric, ascending=ascending)
    cfgs = metric_median['cfgid'].unique()
    print("after sorting... cfgs: ", cfgs) 
    bar_positions = np.arange(len(cfgs))
    #print("metric median:")
    #print(metric_median)

    # Get the group
    shape_group = shapes_gb.get_group(shape)
    #print(shape_group[['cfgid','GFLops']])
    bars = ax_i.bar(bar_positions, metric_median[metric], label=f'{metric} median')
    autolabel(bars, ax_i, 1, 1, lim=None)

    if metric != 'GFlops':
      metric_2 = 'GFlops'
      shape_grp2 = shapes_gb.get_group(shape)
      # Sort acording to the cfgs found in the metric, so metric_2 is sorted according to metric.
      shape_grp2['cfgid'] = pd.Categorical(shape_grp2['cfgid'], categories=cfgs)
      shape_grp2 = shape_grp2.sort_values(by='cfgid')
      #print("shape_grp2:")
      #print(shape_grp2[['cfgid',metric, metric_2]])
      #metric2_median = metric2_median.sort_values(by=metric, ascending=ascending)
      # Create a secondary y-axis
      ax2 = ax_i.twinx()
      ax2.set_ylabel(metric_2)
      ax2.plot(bar_positions, shape_grp2[metric_2], 'r--', label=f'{metric_2} median')
      # Combine legends from both axes
      lines, labels = ax_i.get_legend_handles_labels()
      lines2, labels2 = ax2.get_legend_handles_labels()
      ax2.legend(lines + lines2, labels + labels2, loc='upper center')
    #
    ax_i.set_xticks(bar_positions)
    ax_i.set_xticklabels(cfgs, rotation=0, fontsize=8, ha='center')
    ax_i.set_title(f"{shape[0]}x{shape[1]}x{shape[2]}")
    ax_i.set_xlabel('Configuration ID')
    ax_i.set_ylabel(metric)
  #

  plt.savefig(args.o)
#

if __name__ == '__main__':
  # Read the CSV file
  print(f"Reading data from {args.i}...")
  df = pd.read_csv(args.i, comment='#')
  print(f"Data read successfully!")

  if 'corr' in args.chart:
    plot_correlation_per_shape(df)

  if 'relcnts' in args.chart:
    plot_counters_per_config(df)
  
  if 'metric' in args.chart:
    plot_metric_per_shape(df, metric=args.metric, ascending=args.descending)