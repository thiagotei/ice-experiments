#!/usr/bin/env bash
#set -x

codes=(
mkl-seq
naive
locus
)

source ../shapes.sh

compilers=(
icx
#icc
#icc-omp
#icx-omp
)

# Check if an argument is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <root path>"
    exit 1
fi

# Get the first argument
rootpath=$1

if [ ! -d "$rootpath" ]; then
    echo "Error!! $rootpath is not directory!"
    exit 1
fi

output=agg_results.csv

# Columns
echo "compiler,code,conf,numth,vsrun,gflops" > ${output}
testround="2"


one_prime=(${shapesMNK_bM_prime[@]})
two=(${shapesMNK_bN[@]})
three=(${shapesMNK_bK[@]})

configstorun=(${shapesMNK_bM[@]} ${one_prime[@]} ${two[@]} ${three[@]})
totalconfigs=${#configstorun[@]}

for codel in ${codes[@]}
do
    for cc in ${compilers[@]}
    do
	    for config in ${configstorun[@]}
	    do 
            echo $codel $cc $config
            #continue
            M=`echo $config | cut -d':' -f1`
            N=`echo $config | cut -d':' -f2`
            K=`echo $config | cut -d':' -f3`

            pat=".*time\.out\.${cc}\.${codel}\.[0-9]+th\.${config}\..*"
            res_find=`find ${rootpath} -maxdepth 1 \( -type f -o -type l \) -regex "${pat}"`
            if [ -z "${res_find}" ]; then
                echo "No files found pattern:" ${pat}
            fi
            for res in ${res_find}
            do
                echo "File:" ${res}
                IFS='.' read -ra columns <<< "`basename ${res}`"
                line_preamble="${cc},${codel},${columns[5]},${columns[4]},${columns[6]}"
                #while IFS= read -r line; do
                #    echo "${line_preamble},${line}" >> ${output}
                #done < "${res}"
                #numiter=`grep -i "GFLOPs" ${res} | awk '{print $2}'`
                #gflops=`grep -i "GFLOPS" ${res} | awk '{print $9}'`
                gflops=`grep -i "GFLOPS" ${res} | cut -d' ' -f4-`

                # Check if gflops is empty
                if [ -z "$gflops" ]; then
                    echo "WARNING!!! No GFLOPS found in ${res}! You likely need run the experiment $codel $cc $config again!"
                    continue
                fi

                for g in ${gflops}; do
                    echo "${line_preamble},${g}" >> ${output}
                done
            done
        done
    done
done
