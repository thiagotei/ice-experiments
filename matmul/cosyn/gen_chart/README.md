
Generate Charts
=====

This has been tested on ORTCE:

1. Collect all the data in results directory (e.g., "../results/time.out.icc.*"): `./group_data.sh ../`
2. Create a virtual environment: `python3 -m venv ../pyenv`
3. Activate virtual environment: `source ../pyenv/bin/activate`
4. Install deps: `pip install pandas matplotlib`

Example
===

1. Generate chart with all configurations on x-axis:
  * `python3 gen_plot.py -i agg_results.csv -o all.png --chart compspeedup --title "Convolution Single-core" --ytitle "Speedup over Baseline"`
  * `python3 gen_plot.py -i agg_results.csv -o mklvsicx.png --chart comp --title "DGEMM Single-core"`

2. Generate histogram over basecode
  * `./gen_plot.py -i agg_results_oneprime.csv --chart comphistogram -o histogram_oneprime.pdf --basecode "Mkl-seq" --title "Histogram of Speedup for Big M (1') - Single-core"`
  * `./gen_plot.py -i agg_results_two.csv --chart comphistogram -o histogram_two.pdf --basecode "Mkl-seq" --title "Histogram of Speedup for Big N (2) - Single-core"`
  * `./gen_plot.py -i agg_results_three.csv --chart comphistogram -o histogram_three.pdf --basecode "Mkl-seq" --title "Histogram of Speedup for Big K (3) - Single-core"`

3. Generate chart usinng PMU counters
  1. Generate data from database
    * `locusdb-genCSV -i ../locus.db/QZ1J-ICX-PVC.db  -l ../matmul-v5.locus --inccfg --allmetrics  > ./pmu_results_v2_all.csv`
  2. Generate chart of metric for each configuration evaluated during the search:
    * `./gen_plot_pmu.py -i pmu_results_v3_all.csv -o gflops.pdf --chart metric`
    * `./gen_plot_pmu.py -i pmu_results_v4_all.csv -o cpu_cycles_v4.pdf --chart metric --metric cpu-cycles_Avg --descending`
  3. Generate chart comparing multiple counters relatively
    * `./gen_plot_pmu.py -i pmu_results_v3_all.csv -o counters.pdf --chart relcnts`
  4. Generate heatmap of the correlation coefficient between counters
    * `./gen_plot_pmu.py -i pmu_results_v3_all.csv -o corr.pdf --chart corr`
