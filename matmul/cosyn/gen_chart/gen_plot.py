#!/usr/bin/env python3
import pandas as pd
import argparse, math
import matplotlib.pyplot as plt
import numpy as np
from natsort import natsorted, natsort_keygen, index_natsorted

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input csv file path.")
parser.add_argument('-o', type=str, required=True, help="Output pdf file name.")
#parser.add_argument('--nlcol', type=int, required=False, help="number of columns"
#        "in the legend.", default=1)
parser.add_argument("--chart", nargs='+', required=False,
        default=('conf'),
        choices=('comp','optv','conf','confspeedup','compspeedup','comphistogram'),
        help="Chart options: time and speedup")
parser.add_argument("--basecode", type=str, required=False, default='Naive', help="Baseline code for speedup charts.")
parser.add_argument("--basecc", type=str, required=False, default='icx', help="Baseline compiler for speedup charts.")
parser.add_argument("--title", type=str, required=False, default=None, help="Title of the chart.")
parser.add_argument("--ytitle", type=str, required=False, default=None, help="Y-axis title.")
parser.add_argument("--laprec", type=int, required=False, default=1, help="Bars label number precision (number of decimal digits).")

args=parser.parse_args()

def autolabel(rects, pltobj, scale=None, prec=2, lim=None):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        #if lim is None or height > lim:
        vpos = height if lim is None or height < lim else lim
        height = height if scale is None else height * scale
        pltobj.text(rect.get_x() + rect.get_width()/2., vpos,
            #f'{int(height)}',
            #f'{round(height,2):.2f}',
            f'{round(height,prec)}' if prec > 0 else f'{int(round(height))}',
            #ha='center', va='bottom', fontsize='5')
            ha='center', va='bottom', fontsize='x-small')
        #
    ##
###

def parse_conf(conf):
  split = conf.split(':')
  M = split[0]
  N = split[1]
  K = split[2]

  #return f"M {M} N {N} K {K}"
  return f"{M} x {N} x {K}"
###

def plot_cycles_per_configuration(df, speedup=False, gflops=True, limy=False):
  
  if gflops:
    metric = 'gflops'
  else:
    metric = 'time'
  #

  # Group by 'compiler', 'conf', and 'code', and calculate the median 'time' for each group
  median_times = df.groupby(['compiler', 'code', 'conf'])[metric].median().reset_index()
  #print("WARNING: Only icc compiler is considered!")
  #median_times = median_times[median_times['compiler'] == 'icc']

  # Display the first few rows of the median times dataframe
  print(median_times.head())

  confs = natsorted(median_times['conf'].unique())
  numconfs = len(confs)

  numcols = 6
  numrows = math.ceil(numconfs / numcols)
  grid = (numrows, numcols)
  figsize=(4 * numcols, 4 * numrows)
  #fig, axs = plt.subplots(numrows, numcols, figsize=figsize)
  plt.figure(figsize=figsize)

  for i, conf in enumerate(confs):
    sbrow = i // numcols
    sbcol = i % numcols
    subset = median_times[median_times['conf'] == conf]
    #ax_i = axs[sbrow][sbcol]
    ax_i = plt.subplot2grid(grid, (sbrow,sbcol))

    # Get unique codes
    comps = subset['compiler'].unique()
    codes = subset['code'].unique()
    if speedup:
      # remove code 1.0 from the list
      codes = [opt for opt in codes if opt != '1.0']
    #

    # Set the width of each bar
    bar_width = 1 / (len(codes) + 1)
    bar_positions = np.arange(len(comps))

    if speedup:
      baseline_cc = 'icx'
      baseline = subset[(subset['code'] == '1.0') & (subset['compiler'] == baseline_cc)][metric]
      if baseline.empty:
        raise ValueError(f"No baseline found for configuration {conf}, compiler {baseline_cc} and code 1.0.")

    if limy:
      locustime = subset[(subset['code'] == 'locus')][metric]
      if locustime.empty:
        print("!!!! No locustime so no lim on y !!!")
      else:
        limy_time = max(locustime.values)
      #
    else:
      limy_time = None
    #

    #for cc, comp in enumerate(comps):
      #print(f"Compiler loop: {cc} {comp}")
      #subset_cc = subset[subset['compiler'] == comp]

    for j, opt in enumerate(codes):
      opt_subset = subset[subset['code'] == opt]
      if speedup:
        opt_time = baseline.values / opt_subset[metric].values
      else:
        opt_time = opt_subset[metric]

      bars = ax_i.bar(bar_positions + j * bar_width, opt_time, bar_width, label=opt)
      if speedup or gflops:
        autolabel(bars, ax_i, 1, 1)
      else:
        autolabel(bars, ax_i, 1e-7, 1, lim=limy_time)
    ##

    conf_parsed = parse_conf(conf)

    # Set plot title and labels
    ax_i.set_title(f'{conf_parsed}')

    if sbrow == numrows-1:
      ax_i.set_xlabel('Compiler')

    if sbcol == 0:
      if speedup:
        ax_i.set_ylabel(f'Speedup over version 1.0 {baseline_cc}')
      elif gflops:
        ax_i.set_ylabel('GFLOPs')
      else:
        ax_i.set_ylabel('Median CPU cycles')
          
    # Set x-ticks and labels
    ax_i.set_xticks(bar_positions + bar_width * (len(codes) - 1) / 2)
    ax_i.set_xticklabels(comps, rotation=60, fontsize=8, ha='right')
    if speedup or gflops:
      ax_i.ticklabel_format(axis='y', style='plain', scilimits=(0,0), useMathText=True)
    else:
      ax_i.ticklabel_format(axis='y', style='sci', scilimits=(7,7), useMathText=True)
    
    # Set y-axis limits
    if limy and not locustime.empty:
      ax_i.set_ylim([0, limy_time*1.10])  # Replace y_min and y_max with your desired limits

    # Add legend
    if sbrow == 0 and sbcol == 0:
      ax_i.legend()
  ##

  # Adjust layout to prevent overlap
  plt.tight_layout()

  if speedup:
    figname="conf_speedup_"+args.o
  elif gflops:
    figname="conf_gflops_"+args.o
  else:
    figname="conf_"+args.o

  # Save the plot
  plt.savefig(figname)
  print(f"Saved chart in {figname} .")
##

def plot_histogram_per_compiler(df):
  metric = 'gflops'

  df['parsed_conf'] = df['conf'].apply(lambda x: parse_conf(x))

  # Group by 'compiler', 'conf', and 'code', and calculate the median 'time' for each group
  median_times = df.groupby(['compiler', 'code', 'parsed_conf'])[metric].median().reset_index()

  confs = median_times['parsed_conf'].unique()
  numconfs = len(confs)
  plotwidth = 6 * math.ceil(numconfs/36)

  comps = natsorted(median_times['compiler'].unique())
  numcomps = len(comps)

  codes = natsorted(median_times['code'].unique())
  numcodes = len(codes)

  numcols = 1
  numrows = math.ceil(numcomps / numcols)
  grid = (numrows, numcols)
  #figsize=(16 * numcols, 6 * numrows)
  #figsize=(plotwidth * numcols, 8 * numrows)
  figsize=(10, 10)
  #fig, axs = plt.subplots(numcomps, 1, figsize=(45, 8 * numcomps))
 # plt.figure(figsize=figsize)

  baseline_cc = args.basecc #'icx'
  baseline_code = args.basecode #'1.0'
  baseline = median_times[(median_times['code'] == baseline_code) & (median_times['compiler'] == baseline_cc)]
  if baseline.empty:
    raise ValueError(f"No baseline found for compiler {baseline_cc} and code {baseline_code}.")
  print("baseline:", baseline)
  #

  # WHich confs code was faster
  codes_performance ={}
  # Speedup over baseline for each code
  codes_speedup = {}
  for conf in confs:
    #print(conf)
    #print(median_times[(median_times['parsed_conf'] == conf)])
    max_metric_shape = median_times.loc[median_times[(median_times['parsed_conf'] == conf)][metric].idxmax()]
    fastestcode = max_metric_shape['code']
    maxval = max_metric_shape[metric]
    print("shape:",conf,"fastestcode:", fastestcode,"maxval:", maxval)
    if fastestcode not in codes_performance:
      codes_performance[fastestcode] = 1
    else:
      codes_performance[fastestcode] += 1
    #


    baseline_val = baseline[baseline['parsed_conf'] == conf][metric].values[0]
    print("baseline_val:", baseline_val)
    for code in codes:
      if code == baseline_code:
        continue
      #
      if code not in codes_speedup:
        codes_speedup[code] = []
      #
      if baseline_val == 0:
        print(f"WARNING: baseline value is 0 for code {code} and conf {conf}. Moving to next...")
        continue
      #
      codespeed = median_times[(median_times['code'] == code) & (median_times['parsed_conf'] == conf)][metric].values[0]
      speedup =  codespeed / baseline_val
      print("shape:",conf, "code:", code, "codespeed:", codespeed, "speedup:", speedup)
      codes_speedup[code].append(speedup)
  #
  print("codes_performance:", codes_performance)
  print("codes_speedup:", codes_speedup)

  for code, speedups in codes_speedup.items():
      plt.hist(speedups, bins=10, alpha=0.5, label=f'{code}')
  
  plt.grid(axis='y', color='green', alpha=0.3, linestyle=':', linewidth=0.5)
  ax = plt.gca()
  ax.spines['top'].set_visible(False)
  ax.spines['left'].set_visible(False)
  ax.spines['right'].set_visible(False)
  plt.xlabel(f'Speedup over {baseline_code} ')
  plt.ylabel(f'Frequency (total of {numconfs} shapes)')
  if args.title is None:
    title = 'Histogram of Speedup for Different Codes'
  else:
    title = args.title
  #
  plt.title(title)
  plt.legend()
  plt.savefig(args.o)
#  for i, comp in enumerate(comps):
#    sbrow = i // numcols
#    sbcol = i % numcols
#    ax_i = plt.subplot2grid(grid, (sbrow,sbcol))
#    subset = median_times[median_times['compiler'] == comp]


  ##
##

def plot_metric_per_compiler(df, gflops=True, speedup=False, title=None, ytitle=None):

  if gflops:
    metric = 'gflops'
  else:
    metric = 'time'
  #
  df['parsed_conf'] = df['conf'].apply(lambda x: parse_conf(x))

  # Group by 'compiler', 'conf', and 'code', and calculate the median 'time' for each group
  median_times = df.groupby(['compiler', 'code', 'parsed_conf'])[metric].median().reset_index()

  # Sort the grouped DataFrame by the parsed_conf column
  median_times = median_times.sort_values(by=['compiler','code','parsed_conf'], key=natsort_keygen())

  # Display the first few rows of the median times dataframe
  print(median_times.head())

  numconfs = len(median_times['parsed_conf'].unique())
  plotwidth = 6 * math.ceil(numconfs/36)

  #confsperplot = 4
  #numplots= math.ceil(numconfs / confsperplot)

  comps = natsorted(median_times['compiler'].unique())
  numcomps = len(comps)

  numcols = 1
  numrows = math.ceil(numcomps / numcols)
  grid = (numrows, numcols)
  #figsize=(16 * numcols, 6 * numrows)
  figsize=(plotwidth * numcols, 8 * numrows)
  #fig, axs = plt.subplots(numcomps, 1, figsize=(45, 8 * numcomps))
  plt.figure(figsize=figsize)

  if speedup:
    baseline_cc = args.basecc #'icx'
    baseline_code = args.basecode #'1.0'
    baseline = median_times[(median_times['code'] == baseline_code) & (median_times['compiler'] == baseline_cc)][metric]
    if baseline.empty:
      raise ValueError(f"No baseline found for compiler {baseline_cc} and code {baseline_code}.")
    #
  #

  for i, comp in enumerate(comps):
    sbrow = i // numcols
    sbcol = i % numcols
    ax_i = plt.subplot2grid(grid, (sbrow,sbcol))
    subset = median_times[median_times['compiler'] == comp]

    # Get unique codes

    confs = subset['parsed_conf'].unique()
    codes = natsorted(subset['code'].unique())
    # Define the custom order for the 'code' column. Locus in the end!
    if 'Locus' in codes:
      codes.remove('Locus')
      codes.append('Locus')
      print("Codes new order:", codes)
    #

    if speedup:
      # remove baseline_code from the list
      codes = [opt for opt in codes if opt != baseline_code]
    #

    # Set the width of each bar
    numcodes = len(codes)
    bar_width = 1.0 / (len(codes) + 1)
    bar_positions = np.arange(len(confs))
    barp = np.linspace(-((numcodes-1)*0.5), (numcodes-1)*0.5, numcodes)
    print("code positions:", barp)

    #print("shape positions:", bar_positions)
    # Centralize the bars around the pivot position given by bar_positions
    codebars = [[p + k*bar_width for p in bar_positions] for k in barp]
    #print("codebars: ", len(codebars), codebars)

    for j, opt in enumerate(codes):
      opt_subset = subset[subset['code'] == opt]
      if speedup and gflops:
        if len(baseline.values) != len(opt_subset[metric].values):
          print(f"ERROR! {opt} has only {len(opt_subset[metric].values)} values, while baseline has {len(baseline.values)} values. Moving to next...")
          continue
        #
        opt_time = opt_subset[metric].values / baseline.values
      elif speedup:
        opt_time = baseline.values / opt_subset[metric].values
      else:
        opt_time = opt_subset[metric]
      #

      bars = ax_i.bar(codebars[j], opt_time, bar_width, label=opt)
      if speedup or gflops:
        autolabel(bars, ax_i, 1, args.laprec)
      else:
        autolabel(bars, ax_i, 1e-7, args.laprec)
      #
    ##

    # Set plot title and labels
    if title is None:
      ax_i.set_title(f'{comp}')
    else:
      ax_i.set_title(f'{title}')
    #

    if sbrow == numrows-1:
      ax_i.set_xlabel('Shapes (M x N x K)')
    #

    # Set plot title and labels
    if sbcol == 0:
      if args.ytitle is None:
        if speedup:
          ax_i.set_ylabel(f'Speedup over version {baseline_code} {baseline_cc}')
        elif gflops:
          ax_i.set_ylabel('GFLOPs')
        else:
          ax_i.set_ylabel('Median CPU cycles')
      else:
        ax_i.set_ylabel(f'{ytitle}')
      #
    #

    # Add a horizontal line at y=1 for speedup plots
    if speedup:
      ax_i.axhline(y=1, color='r', linestyle='--', linewidth=1)
    #

    # Set x-ticks and labels
    ax_i.set_xticks(bar_positions)
    parsed_confs = confs #[parse_conf(conf) for conf in confs]
    ax_i.set_xticklabels(parsed_confs, rotation=60, fontsize=8, ha='right', rotation_mode='anchor')
    if speedup or gflops:
      ax_i.ticklabel_format(axis='y', style='plain', scilimits=(0,0), useMathText=True)
    else:
      ax_i.ticklabel_format(axis='y', style='sci', scilimits=(7,7), useMathText=True)
    #

    ax_i.grid(axis='y', color='green', alpha=0.3, linestyle=':', linewidth=0.5)
    ax_i.spines['top'].set_visible(False)
    ax_i.spines['left'].set_visible(False)
    ax_i.spines['right'].set_visible(False)

    # Add legend
    if sbrow == 0 and sbcol == 0:
      ax_i.legend()
    #
  ##

  # Adjust layout to prevent overlap
  plt.tight_layout()

  # Show the plot
  figname="gemm_cc_"+args.o

  if speedup:
    figname="gemm_cc_speedup_"+args.o
  elif gflops:
    figname="gemm_cc_gflops_"+args.o
  else:
    figname="gemm_cc_"+args.o
  plt.savefig(figname)
  print(f"Saved chart in {figname} .")
##

def plot_cycles_per_code(df):

  # Group by 'compiler', 'conf', and 'code', and calculate the median 'time' for each group
  median_times = df.groupby(['compiler', 'conf', 'code'])['time'].median().reset_index()

  # Display the first few rows of the median times dataframe
  print(median_times.head())

  # Get unique code values
  codes = natsorted(median_times['code'].unique())
  numopts = len(codes)

  fig, axs = plt.subplots(numopts, 1, figsize=(12, 8 * numopts))

  # Loop through each code and create a plot
  for i, code in enumerate(codes):
      # Filter the data for the current code
      subset = median_times[median_times['code'] == code]
      
      # Get unique configurations and compilers
      confs = subset['conf'].unique()
      compilers = subset['compiler'].unique()
      
      # Set the width of each bar
      bar_width = 0.35
      bar_positions = np.arange(len(confs))
      
      # Plot bars for each compiler
      for j, compiler in enumerate(compilers):
          compiler_subset = subset[subset['compiler'] == compiler]
          bars = axs[i].bar(bar_positions + j * bar_width, compiler_subset['time'], bar_width, label=compiler)
          autolabel(bars, axs[i], 1e-7, 0)
      
      # Set plot title and labels
      axs[i].set_title(f'Median Time by Compiler and Configuration for code {code}')
      axs[i].set_xlabel('Configuration')
      axs[i].set_ylabel('Median CPU cycles')
          
      # Set x-ticks and labels
      axs[i].set_xticks(bar_positions + bar_width * (len(compilers) - 1) / 2)
      axs[i].set_xticklabels(confs, rotation=60, fontsize=8, ha='right')
      axs[i].ticklabel_format(axis='y', style='sci', scilimits=(7,7), useMathText=True)
      
      # Add legend
      axs[i].legend()

  # Adjust layout to prevent overlap
  plt.tight_layout()

  # Show the plot
  figname="opt_"+args.o
  plt.savefig(figname)
  print(f"Saved chart in {figname} .")
##

if __name__ == '__main__':

  print(f"Reading data from {args.i}...")

  # Read the CSV file
  df = pd.read_csv(args.i)

  print(f"code values before replacement:")
  print(df['code'].unique())

  # Replace the specific code value
  #df['code'] = df['code'].replace('1.0_back_prop_se', '1.0')
  #df['code'] = df['code'].replace('1.1_back_prop_sx5', '1.1')
  #df['code'] = df['code'].replace('1.15_back_prop_sx5', '1.15')
  #df['code'] = df['code'].replace('1.2_back_prop_sx5', '1.2')
  #df['code'] = df['code'].replace('2.0_back_prop_sx5', '2.0')
  #df['code'] = df['code'].replace('2.1_back_prop_sx5', '2.1')
  #df['code'] = df['code'].replace('2.2_back_prop_sx5', '2.2')
  df['code'] = df['code'].str.capitalize()

  print(f"code values after replacement:")
  print(df['code'].unique())


  print(f"Data read successfully!")

  if 'comp' in args.chart:
    plot_metric_per_compiler(df, speedup=False, title=args.title, ytitle=args.ytitle)

  if 'compspeedup' in args.chart:
    plot_metric_per_compiler(df, speedup=True, title=args.title, ytitle=args.ytitle)

  if 'comphistogram' in args.chart:
    plot_histogram_per_compiler(df)

  if 'conf' in args.chart:
    plot_cycles_per_configuration(df)

  if 'confspeedup' in args.chart:
    plot_cycles_per_configuration(df, speedup=True)

  if 'optv' in args.chart:
    plot_cycles_per_code(df)


