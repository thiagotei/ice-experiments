## Evalutation of GEMM patterns ##


This code is based on the plutobench directory.

Three sets of shapes evaluated:

One)
```
N>>M, M=K
```

Two)
```
M>>N, N=K
```

Three)
```
K>>M, M=N
```

Get a node and compile the baseline (PMU_ANALYZER is optional):
    * `srun --nodes=1  --partition=QZ1J-ICX-PVC --time 02:00:00 --pty /bin/bash`
    * `source /opt/intel/oneapi/compiler/2025.0/env/vars.sh`
    * `CC=icx CXX=icpx make USE_PMU_ANALYZER=true USERDEFS="-DMATDIM_M=256 -DMATDIM_N=256 -DMATDIM_K=256" matmul.base.exe`
    * `PMU_ANALYZER_CONFIG_FILE=./pmu_config.yaml ./matmul.base.exe`

### Environment

Setting up the environment:

1. Create a conda environment
  * `conda create -n conda-env python=3.10`
2. Activate the conda env created
  * `conda activate conda-env`
3. Install Locus on that conda environment
  * `pushd <locus dir>`
  * `pip install . (pip install -e . for developement)`

### Search

To run the search on SLURM systems:
```
./locus_search_multiple.sh
```

### Results
To get results from the Locus database:
```
#Activate the python environment that contains the ice-locus-expchart installed 
source ~/pyvirtenvs/envpy3.6-ice/bin/activate
./matmul-genCSVfromDB.py -i locus.db/sadr.db -l matmul.locus > locus-sadr-res.csv
``` 

### Locus DB useful scripts

They belong to `ice-locus-expchart`:

1. `expchart-showDBInfo.py -i locus.db/sadr.db`
2. `expchart-getDBLocusfiles.py -i locus.db/sadr.db -l matmul.locus`

### Debug

To generate compiler reports for specific shape:
```
make DEBUG=true USERDEFS="-DMATDIM_M=4 -DMATDIM_N=4 -DMATDIM_K=4000" CC=icx clean matmul.base.exe
```
