#!/usr/bin/env bash

codes=(
mkl-seq
naive
)

source shapes.sh

one_prime=(${shapesMNK_bM_prime[@]})
two=(${shapesMNK_bN[@]})
three=(${shapesMNK_bK[@]})

#configstorun=(${shapesMNK_bM[@]:0:3})
#configstorun=($shapesMNK_bM_prime[@]) #(${shapesMNK_bM[@]})
#configstorun=(${one_prime[@]})
#configstorun=(${two[@]})

#configstorun=(${three[@]})
#default_testround=three

#configstorun=(${two[@]})
#default_testround=two

configstorun=(${one_prime[@]})
default_testround=oneprime

totalconfigs=${#configstorun[@]}

partition=${SLURM_JOB_PARTITION:-"p"}
host=$(hostname)
export USE_OPENMP=${USE_OPENMP:-true}
export CC=${CC:-icx}
export CXX=${CXX:-icpx}

expname=${CC}

if [ "$USE_OPENMP" = true ]; then
  echo "Using OpenMP"
  export OMP_PLACES=cores
  export OMP_PROC_BIND=close # spread
  export OMP_NUM_THREADS=${OMP_NUM_THREADS:-1} # use just one socket
  export numth=${OMP_NUM_THREADS}
#  expname=${expname}-omp
fi

if [ "$CC" = icc ]; then
  ml load intel/oneapi/2023.2.1
elif [ "$CC" = icx ]; then
  if ! command -v $CC >/dev/null 2>&1; then
    source /opt/intel/oneapi/compiler/2025.0/env/vars.sh
    echo "Using Intel oneAPI compiler 2025.0"
  fi
fi

clet_index=1

for c in ${codes[@]};
do
  echo "+++++++++++++++++++++"
  echo "Compiling $c ${clet_index}/${#codes[@]}"
  echo "+++++++++++++++++++++"
  echo "                     "

  if [ "${c:0:3}" = "mkl" ]; then
    source /opt/intel/oneapi/mkl/2025.0/env/vars.sh 
    p="../mkl"
    if [ "${c}" = "mkl-seq" ]; then
      cmd="make -C $p clean mmm_mkl.seq.exe"
      bin="mmm_mkl.seq.exe"

    elif [ ${c} = "mkl-par" ]; then
      cmd="make -C $p clean mmm_mkl.par.exe"
      bin="mmm_mkl.par.exe"

    fi
    testround=${default_testround}
  elif [ "${c}" = "naive" ]; then
    p="."
    cmd="" #"make clean -C $p"
    #bin="mmm_baseline.exe"
    testround=${default_testround}
  else
    echo "undefined codelet ${c} ! Next"
    continue
  fi
  export WARMUP=10

  echo $cmd
  echo "  "
  eval $cmd
  if [ $? -ne 0 ]; then
    echo "Error compiling $c -> $cmd"
    exit 1
  fi

  echo $p/$bin

  config_index=1

  for config in ${configstorun[@]};
  do
    outfilepath=results_${partition}_${testround}
    # check if dir results_${testround} exists
    if [ ! -d $outfilepath ]; then
      mkdir -v $outfilepath
    fi

    outfile="${outfilepath}/time.out.${expname}.${c}.${numth}th.${config}.${testround}.${host}"

    numit=3000
    outfile_nohost=${outfile%.*}

    if ls "${outfile_nohost}"* 1> /dev/null 2>&1; then
      echo "Output file ${outfile_nohost}.* exists!"
      echo "+++++++++++++++++++++"	
      echo "Skipping configuration $c ${config_index}/${totalconfigs} $config. Already ran $numtests tests!"
      echo "+++++++++++++++++++++"
      config_index=$((config_index+1)) 
      continue
    fi

    echo "+++++++++++++++++++++"
    echo "Running configuration $c $config ${config_index}/${totalconfigs}"
    echo "+++++++++++++++++++++"
    echo "                     "

    echo "___________________"
    echo "Running test ${run} ${numit} ($c $config ${config_index}/${totalconfigs}) ..."
    echo "                   "

    M=`echo ${config} | cut -d':' -f1`
    N=`echo ${config} | cut -d':' -f2`
    K=`echo ${config} | cut -d':' -f3`

    if [ "${c:0:3}" = "mkl" ]; then
      ${p}/${bin} $M $N $K $numit 2>&1 > ${outfile}

    elif [ "${c}" = "naive" ]; then
      echo "Late compiling matmul naive $M $N $K ..."
      #basetgt=matmul.${M}_${N}_${K}
      basetgt=matmul.base
      tgt=${basetgt}.exe
      rm -v $tgt # compile every time 
      if [ ! -f $tgt ]; then
        #./sedchange.sh $M $N $K matmul.base.c ${basetgt}.c
        cmd="make USERDEFS=\"-DMATDIM_M=${M} -DMATDIM_N=${N} -DMATDIM_K=${K}\" -C $p $tgt"
        echo $cmd
        echo "  "
        eval $cmd
        if [ $? -ne 0 ]; then
          echo "Error compiling $c $tgt -> $cmd"
          exit 1
        fi
      fi
      
      rm .test # no printing of the output
      echo "Running $tgt $M $N $K $numit ..."
      ${p}/${tgt} $numit 2>&1 > ${outfile}
    else
      echo "undefined codelet ${c} ${shape} ! Next"
      continue
    fi
    if [ $? -ne 0 ]; then
      echo "Error running ./$bin $c $config"
      exit 1
    fi
    config_index=$((config_index+1))
  done
  clet_index=$((clet_index+1))
done

echo "Done running all configurations! $(date +'%Y-%m-%d %H:%M:%S')"