#ifndef __MYSECOND_H__
#define __MYSECOND_H__

#include <bits/types/struct_timespec.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif
double mysecondOld(void);
double mysecond(void);
double mydifftime(struct timespec *beg, struct timespec *end);
double mydifftimems(struct timespec *beg, struct timespec *end);
double mydifftimeus(struct timespec *beg, struct timespec *end);
double mydifftimens(struct timespec *beg, struct timespec *end);
void mygettime(struct timespec *tVal);
double mysecond_(void);
#ifdef __cplusplus
}
#endif

#endif
