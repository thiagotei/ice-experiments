#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
//#include <assert.h>
//#include <unistd.h>
#include <float.h>
//#ifdef USE_ORIGINAL_TIMER
//#include <mysecond.h>
//#else
#include <utils/libxsmm_timer.h>
//#endif
//#include <pips_runtime.h>
//#ifdef USE_PMU_ANALYZER
#include <pmu_analyzer.h>
//#endif

#ifndef MATDIM_M
#define MATDIM_M 2048
#endif

#ifndef MATDIM_N
#define MATDIM_N 2048
#endif

#ifndef MATDIM_K
#define MATDIM_K 2048
#endif

//#define alpha 1
//#define beta 1

//#pragma declarations
double A[MATDIM_M][MATDIM_K];
double B[MATDIM_K][MATDIM_N];
double C[MATDIM_M][MATDIM_N];
//#pragma enddeclarations
//#ifdef TIME
#define IF_TIME(foo) foo;
//#else
//#define IF_TIME(foo)
//#endif

void init_array()
{
    int i, j;

    for (i=0; i<MATDIM_M; i++) {
        for (j=0; j<MATDIM_K; j++) {
            A[i][j] = (i + j);
        }
    }
    for (i=0; i<MATDIM_K; i++) {
        for (j=0; j<MATDIM_N; j++) {
            B[i][j] = (double)(i*j);
        }
    }

    for (i=0; i<MATDIM_M; i++) {
        for (j=0; j<MATDIM_N; j++) {
            C[i][j] = 0.0;
        }
    }

}

void print_array(FILE * out)
{
    int i, j;

    for (i=0; i<MATDIM_M; i++) {
        for (j=0; j<MATDIM_N; j++) {
            //if (i == MATDIM_M-1 && j == MATDIM_N-1) {
            //  fprintf(out, "%lf ", C[i][j]+1.0);
            //} else {
              fprintf(out, "%lf ", C[i][j]);
            //}
            if (j%80 == 79) fprintf(out, "\n");
        }
        fprintf(out, "\n");
    }
    fclose(out);
}


int main(int argc, char *argv[])
{
    int i, j, k, iterations = 5, warmup = 2;
    FILE * outF = stderr;

    if (argc < 1) {
        fprintf(stderr, "Usage: %s [<iterations> [<output result filename>]]\n", argv[0]);
        exit(1);
    } else if (argc >= 2) {
        iterations = atoi(argv[1]);
        if (argc >= 3) {
            if ( ! fopen(".test", "r") || (outF = fopen(argv[2], "w")) == NULL) {
                fprintf(stderr, "ERROR! Could not open file: %s or create .test file in current dir and try again!\n", argv[2]);
                exit(1);
            } else {
                printf("Results will be written to %s .\n", argv[2]);
            }
        }
    }

#ifdef USE_ORIGINAL_TIMER
    double *t_it = (double *) malloc(iterations * sizeof(double));
    if (!t_it){printf("Could not allocate t_it.\n"); exit(1);}
#else
    double l_total = 0.0;
#endif

    init_array();

    const char * const env_warmup = getenv("WARMUP");
    warmup = (NULL == env_warmup ? warmup : atoi(env_warmup));
    printf("Warmup iterations: %d .\n", warmup);

#ifdef USE_PMU_ANALYZER
    printf("Initializing PMU analyzer... \n");
    PMU_INIT();
    int trace_id = 0;
#endif

#ifdef USE_CACHE_FLUSH
    // Global variables.
    printf("Cache will be flushed between iterations... \n");
    const size_t bigger_than_cachesize = 100 * 1024 * 1024;
    long *p = (long *) malloc(sizeof(long) * bigger_than_cachesize);
#endif

    for (int it = 0; it < (iterations + warmup); it++) {
       {
#ifdef USE_PMU_ANALYZER
        PMU_TRACE_START(trace_id);
#endif
#ifdef USE_ORIGINAL_TIMER
       struct timespec t_start, t_end;
       IF_TIME(mygettime(&t_start))
#else
       libxsmm_timer_tickint start, end;
       start = libxsmm_timer_tick();
#endif

#pragma @ICE loop=matmul
       for(i=0; i<MATDIM_M; i++) {
          for(j=0; j<MATDIM_N; j++) {
             for(k=0; k<MATDIM_K; k++) {
                C[i][j] += A[i][k] * B[k][j];
             }
          }
       }

#ifdef USE_ORIGINAL_TIMER
       IF_TIME(mygettime(&t_end))
       IF_TIME(if (warmup <= it) t_it[it - warmup]  = mydifftimems(&t_start, &t_end))
#else
       end = libxsmm_timer_tick();
       if (warmup <= it) {
          libxsmm_timer_tickint comptime = libxsmm_timer_ncycles(start, end);
          l_total += libxsmm_timer_duration(0, comptime);
       }
#endif
#ifdef USE_PMU_ANALYZER
       PMU_TRACE_END(trace_id);
       trace_id++;
#endif
       }

#ifdef USE_CACHE_FLUSH
       {
         printf("Let's flush cache!\n");
         // When you want to "flush" cache. 
         for(int i = 0; i < bigger_than_cachesize; i++)
         {
           p[i] = rand();
         }
       }
#endif
       if (it== 0 && fopen(".test", "r")) {
#ifdef MPI
         if (my_rank == 0) {
          print_array(outF);
         }
#else
         print_array(outF);
#endif
       }
    }
    double gflopA = (2.0*MATDIM_M*MATDIM_N*MATDIM_K)*1E-9;

#ifdef USE_PMU_ANALYZER
    const char ** events_name = NULL;
    int events_count = 0;
    int total_traces = 0;
    uint64_t ** events_values = NULL;
    PMU_CLOSE_GET_EVENTS(&events_name, &events_count, &events_values, &total_traces);
#endif

#ifdef USE_ORIGINAL_TIMER
    IF_TIME(printf("Matrixsize= %d %d %d | Iterations %d Time(milisec) ", MATDIM_M, MATDIM_N, MATDIM_K, iterations))
    IF_TIME(for(int it = 0; it < iterations; it++) 
              printf("%7.10lf ", t_it[it]);
            printf("\n"))
    IF_TIME(printf("Iterations %d GFlops ", iterations))
    IF_TIME(for(int it = 0; it < iterations; it++) 
              printf("%.8e " , gflopA / (t_it[it] * 1E-3));
            printf("\n"))
    free(t_it);
#else
    printf("Matrixsize= %d %d %d Iterations %d\nAvgTime(seconds) %7.10lf GFLOPS %.8e\n", MATDIM_M, MATDIM_N, MATDIM_K, iterations, l_total/iterations, (gflopA* (double)iterations)/l_total);
#endif

#ifdef USE_PMU_ANALYZER
    uint64_t * events_avg = (uint64_t *) malloc(events_count * sizeof(uint64_t));
    uint64_t * events_min = (uint64_t *) malloc(events_count * sizeof(uint64_t));
    uint64_t * events_max = (uint64_t *) malloc(events_count * sizeof(uint64_t));
    printf("PMU Analyzer Results: \n");
    printf("PMU Events Names");
    for (int i = 0; i < events_count; i++) {
      printf(" %s", events_name[i]);
      events_avg[i] = 0;
      events_min[i] = UINT64_MAX;
      events_max[i] = 0;
    }
    printf("\n");
    for (int i = 0; i < total_traces; i++) {
#ifdef USE_PMU_ANALYZER_VERBOSE
      printf("PMU Trace %d ", i);
#endif
      for (int j = 0; j < events_count; j++) {
#ifdef USE_PMU_ANALYZER_VERBOSE
        printf("%lu ", events_values[i][j]);
#endif
        events_avg[j] += events_values[i][j];
        if (events_values[i][j] < events_min[j]) {
          events_min[j] = events_values[i][j];
        }
        if (events_values[i][j] > events_max[j]) {
          events_max[j] = events_values[i][j];
        }
      }
#ifdef USE_PMU_ANALYZER_VERBOSE
      printf("\n");
#endif
    }
    for (int i = 0; i < events_count; i++) {
      free(events_values[i]);
    }
    //print avg min max of events
    printf("PMU Events Avg");
    for (int i = 0; i < events_count; i++) {
      printf(" %lu", events_avg[i] / total_traces);
    }
    printf("\nPMU Events Min");
    for (int i = 0; i < events_count; i++) {
      printf(" %lu", events_min[i]);
    }
    printf("\nPMU Events Max");
    for (int i = 0; i < events_count; i++) {
      printf(" %lu", events_max[i]);
    }
    printf("\n");
    free(events_values);
    free(events_name);
    free(events_avg);
    free(events_min);
    free(events_max);
#endif

  return 0;
}
