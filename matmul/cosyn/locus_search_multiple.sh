#!/usr/bin/env bash

source shapes.sh

one_prime=(${shapesMNK_bM_prime[@]})
two=(${shapesMNK_bN[@]})
three=(${shapesMNK_bK[@]})

#configstorun=(${three[@]:1})
#configstorun=(${three[@]})

#configstorun=(${two[@]})

configstorun=(${one_prime[@]:0:3})

#USE_DEP=1

depjobids="${DEPJOBIDS:-}"
for config in ${configstorun[@]}
do
    echo "Sbatching config: $config"
#    configfile="codelet_${config}.data"
#    if [ ! -s $configfile ]; then
#        echo "File $configfile not found! Creating one..."
#        echo "100 $config" > $configfile
#    fi

    if [ -z $depjobids ]; then
        depjobs=""
    else
        depjobs="--dependency=afterok:$depjobids"
    fi
    export ICELOCUS_MATSHAPE_M=`echo $config | cut -d':' -f1`
    export ICELOCUS_MATSHAPE_N=`echo $config | cut -d':' -f2`
    export ICELOCUS_MATSHAPE_K=`echo $config | cut -d':' -f3`
    #if [ -z $USE_DEP ]; then
    #    depjobs=""
    #fi
    cmd="sbatch ${depjobs} ./locus_search.slurm"
    res=$(eval $cmd)
    #res="Submitted batch job 322352"
    echo $cmd
    echo $res
    if [ -z $depjobids ]; then
        depjobids=$(echo $res | awk '{print $4}')
    else
        depjobids="$depjobids:$(echo $res | awk '{print $4}')"
    fi
done