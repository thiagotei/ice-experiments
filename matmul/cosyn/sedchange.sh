#!/usr/bin/env bash

if [ "$#" -ne 5 ]; then
    echo "ERROR! Usage: $0 <valM> <valN> <valK> <inputfile> <outputfile>"
    exit 1
fi
valM=$1
valN=$2
valK=$3
inpf=$4
outf=$5
sed -e "s/define MATDIM_M 2048/define MATDIM_M ${valM}/g" -e "s/define MATDIM_N 2048/define MATDIM_N ${valN}/g" -e "s/define MATDIM_K 2048/define MATDIM_K ${valK}/g" $inpf > $outf
