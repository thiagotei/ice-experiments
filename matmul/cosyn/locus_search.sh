#!/usr/bin/env bash

handle_slurm_signal() {
    date +"%Y-%m-%d %H:%M:%S"
	local proc=$1
    echo "Caught SLURM signal for. Saving partial results?"
    # Perform any cleanup tasks here
    pkill -SIGINT $proc
    date +"%Y-%m-%d %H:%M:%S"
    exit 0         
} 

source ~/.bashrc
source ./setup_env_ortce.sh

export ICELOCUS_CC=${CC:-"icx"} #'gcc-7'
export ICELOCUS_CXX=${CXX:-"icpx"}
export ICELOCUS_OMP_NUMTH=${OMP_NUM_THREADS:-"1"}
export ICELOCUS_OMP_PLACES=$OMP_PLACES
export ICELOCUS_OMP_PROCBIND=$OMP_PROC_BIND
# Save at the locus.db some info about the input
#export ICELOCUS_CONF=`cat codelet.data | head -n 1 | awk '{print $2}'`
if [ -z "$ICELOCUS_MATSHAPE_M" ] || [ -z "$ICELOCUS_MATSHAPE_N" ] || [ -z "$ICELOCUS_MATSHAPE_K" ]; then
    echo "ERROR! ICELOCUS_MATSHAPE_M: $ICELOCUS_MATSHAPE_M ICELOCUS_MATSHAPE_N: $ICELOCUS_MATSHAPE_N ICELOCUS_MATSHAPE_K: $ICELOCUS_MATSHAPE_K not properly set!"
    exit 1
else
    echo "ICELOCUS_MATSHAPE_M: $ICELOCUS_MATSHAPE_M ICELOCUS_MATSHAPE_N: $ICELOCUS_MATSHAPE_N ICELOCUS_MATSHAPE_K: $ICELOCUS_MATSHAPE_K set"
fi

export WARMUP=10

# .test file is required to actually generate the results file.
touch .test

stopaf=60 #116 #5000
ntests=1000 #300 #200 #1000
uplimit=30 # in seconds
#cfile=codelet_vs2.1_5x5_7ifm.c

export PMU_ANALYZER_CONFIG_FILE=pmu_config.txt
conf_hash=$(date | sha256sum | awk '{print $1}' | cut -c 1-10)
export ICELOCUS_RUNDIRPATH=./.locus/run-PMU-${ICELOCUS_MATSHAPE_M}:${ICELOCUS_MATSHAPE_N}:${ICELOCUS_MATSHAPE_K}-${conf_hash}
echo "Saving results at $ICELOCUS_RUNDIRPATH !"

cfile=matmul.c
cmd="./sedchange.sh $ICELOCUS_MATSHAPE_M $ICELOCUS_MATSHAPE_N $ICELOCUS_MATSHAPE_K matmul.base.c $cfile"
eval $cmd
if [ $? -ne 0 ]; then
    echo "ERROR! Failed $cmd"
    exit 1
fi

# check if cfile exists 
if [ ! -f $cfile ]; then
    echo "ERROR! File $cfile not found!"
    exit 1
fi

rm -vf "./outputs/output_matmul_${ICELOCUS_MATSHAPE_M}_${ICELOCUS_MATSHAPE_N}_${ICELOCUS_MATSHAPE_K}.txt"
origoutf="./outputs/output_orig_matmul_${ICELOCUS_MATSHAPE_M}_${ICELOCUS_MATSHAPE_N}_${ICELOCUS_MATSHAPE_K}.txt"
if [ ! -f $origoutf ]; then
    make CC=$ICELOCUS_CC CXX=$ICELOCUS_CXX clean orig
    cmd="./orig 1 $origoutf"
    echo $cmd
    eval $cmd
    if [ $? -ne 0 ]; then
        echo "ERROR! Failed to run ./orig 1 $origoutf"
        exit 1
    else
        echo "Good! Orig output saved at $origoutf !"
    fi
else
    echo "Good! Orig output exists at $origoutf !"
fi

suffix='.opt'
locusfile=matmul-v5.locus

hyptool="locus-hyperopt"
hypargs="--hypalg ${hypalg} --hypmixdsc ${hypmixdsc}"

otutool="locus-opentuner"
otuargs="--otprune"

exhtool='locus-exhaustive'
exhargs='--debug' #'--convonly'

setool="$otutool"
#setool="$exhtool"

if [ $setool = "$hyptool" ]; then
    seargs=$hypargs
elif [ $setool = "$otutool" ]; then
    seargs=$otuargs
    rm -rvf opentuner.db opentuner.log
elif [ $setool = "$exhtool" ]; then
    seargs=$exhargs
else
    echo "ERROR! Tool $setoll not recognized!"
    exit
fi

#seargs+=" --debug" # --convonly"
seargs+=" --initseed"
#seargs+="--upper-limit ${uplimit}"
seargs+=" --stop-after $stopaf --timeout "

if [ -n "$SLURM_JOB_PARTITION" ]; then
    dbname="locus.db/${SLURM_JOB_PARTITION}.db"
    echo "Creating one database ( $dbname ) for all the searches in the same partition!!"
    seargs+=" --database $dbname"
fi

trap 'handle_slurm_signal "${setool}"' SIGUSR1 SIGTERM
set -x
$setool $seargs \
    -t ${locusfile} -f $cfile \
    --tfunc mytiming.py:getTimingMatMul -o suffix \
     --preproc 'I/nfs/site/home/tsantosf/libxsmm/include' 'I/nfs/site/home/tsantosf/counters-test/pmu_analyzer/pmu_analyzer_git/include' 'DUSE_PMU_ANALYZER' \
    -u ${suffix} --search --ntests ${ntests} --applyDFAOpt
set +x
