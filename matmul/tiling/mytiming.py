import re

def getTimingMatMul(std, error):
    r = re.search('.*Time.*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    result = 'N/A'
    if m and len(m) >= 3:
       unit_t = m[8]
       time = float(m[7])
       unit_f = m[13]
       flops = float(m[12])
       result = {'metric': time,
                 'exps': ((time,flops),),
                 'unit': (unit_t, unit_f),
                 'desc': ('Total time', 'flops')}
    else:
       result = {'metric': 'N/A'}

    return result


