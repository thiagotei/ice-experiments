#!/bin/bash

die () {
	echo >&2 "$@"
	exit 1
}
ntests=3
if [ "$#" -eq 1 ];then ntests=${1}; fi #|| die "1 filename required -- input to be encrypted and the output will be input + .enc , $# provided

locusf=mmc.locus
srcfile=mmc.c

export ICELOCUS_CC=icc
export OMP_PLACES=cores
export OMP_PROC_BIND=close
export OMP_NUM_THREADS=10

#../../common/bosshowtorun.sh  -s -n ${ntests} -t ${locusf} -f ${srcfile}
ice-locus-exhaustive.py --search -n ${ntests} -t ${locusf} -f ${srcfile} --tfunc mytiming.py:getTimingMatMul -o suffix -u .opt
