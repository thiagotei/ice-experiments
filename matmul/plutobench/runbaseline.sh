#!/usr/bin/env bash

comp=icc

#for s in `seq 2750 250 5000`;
#for s in `seq 250 250 500`;
for s in 2048;
do
	echo Size $s
	make CC=${comp} USERDEFS="-DK=${s} -DM=${s} -DN=${s}" clean orig
	./orig
done
