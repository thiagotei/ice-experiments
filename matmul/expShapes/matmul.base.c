#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
//#include <assert.h>
//#include <unistd.h>
#include <float.h>
#include <mysecond.h>
#include <pips_runtime.h>

#ifndef M
#define M 2048
#endif

#ifndef N
#define N 2048
#endif

#ifndef K
#define K 2048
#endif

//#define alpha 1
//#define beta 1

//#pragma declarations
double A[M][K];
double B[K][N];
double C[M][N];
//#pragma enddeclarations
//#ifdef TIME
#define IF_TIME(foo) foo;
//#else
//#define IF_TIME(foo)
//#endif

void init_array()
{
    int i, j;

    for (i=0; i<M; i++) {
        for (j=0; j<K; j++) {
            A[i][j] = (i + j);
        }
    }
    for (i=0; i<K; i++) {
        for (j=0; j<N; j++) {
            B[i][j] = (double)(i*j);
        }
    }

    for (i=0; i<M; i++) {
        for (j=0; j<N; j++) {
            C[i][j] = 0.0;
        }
    }

}

void print_array(FILE * out)
{
    int i, j;

    for (i=0; i<M; i++) {
        for (j=0; j<N; j++) {
            //if (i == M-1 && j == N-1) {
            //  fprintf(out, "%lf ", C[i][j]+1.0);
            //} else {
              fprintf(out, "%lf ", C[i][j]);
            //}
            if (j%80 == 79) fprintf(out, "\n");
        }
        fprintf(out, "\n");
    }
    fclose(out);
}


int main(int argc, char *argv[])
{
    int i, j, k, iterations = 5;
    struct timespec t_start, t_end;
    FILE * outF = stderr;

    if (argc < 1) {
        fprintf(stderr, "Usage: %s [<output result filename>]\n", argv[0]);
        exit(1);
    } else if (argc >= 1) {
        if (argc == 2) {
            if ( ! fopen(".test", "r") || (outF = fopen(argv[1], "w")) == NULL) {
                fprintf(stderr, "ERROR! Could not open file: %s or create .test file in current dir and try again!\n", argv[1]);
                exit(1);
            }
        }
    }

    double *t_it = (double *) malloc(iterations * sizeof(double));
    if (!t_it){printf("Could not allocate t_it.\n"); exit(1);}

    init_array();

#ifdef PERFCTR
    PERF_INIT; 
#endif
    for (int it = 0; it < iterations; it++) {
       IF_TIME(mygettime(&t_start))

#pragma @ICE loop=matmul
       for(i=0; i<M; i++) {
          for(j=0; j<N; j++) {
             for(k=0; k<K; k++) {
                C[i][j] += A[i][k] * B[k][j];
             }
          }
       }


       IF_TIME(mygettime(&t_end))
       IF_TIME(t_it[it] = mydifftimems(&t_start, &t_end))

       if (it== 0 && fopen(".test", "r")) {
#ifdef MPI
         if (my_rank == 0) {
          print_array(outF);
         }
#else
         print_array(outF);
#endif
       }
    }

    IF_TIME(printf("Matrixsize= %d %d %d | Iterations %d Time(milisec) ", M, N, K, iterations))
    IF_TIME(for(int it = 0; it < iterations; it++) 
              printf("%7.5lf " ,t_it[it]);
            printf("\n"))


#ifdef PERFCTR
    PERF_EXIT; 
#endif
    free(t_it);

  return 0;
}
