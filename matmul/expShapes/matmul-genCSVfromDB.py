#!/usr/bin/env python

from os import path
import argparse, sys, logging, statistics as stat
import pandas as pd

#print(f"Before loading dbutils from {__name__}")

from locusexpchart.common.charts.dbutils import (getDBBestVar, 
    getAllDBexpsOPT, printDBCfgs, getLocusProgram)
from locus.frontend.optlang.locus.locusparser import LocusParser
import locus.tools.search.searchbase as searchbase
from locus.backend.genlocus import GenLocus
from locus.frontend.optlang.locus.optimizer import replEnvVars

#print(f"After loading dbutils from {__name__}")
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console = logging.StreamHandler()
log.addHandler(console)

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-l', type=str, required=True, nargs='+',
        help="Locus programs related info from DB.")
parser.add_argument('--inccfg', default=False, action='store_true',
        help="Add cfg DB id to the 1st field.")
#parser.add_argument('--genLocus', default=False, action='store_true',
#        help="Generate Locus program representing "
#             "0th, 25th, 50th, 75th and 100th percentile for each shape.")

args=parser.parse_args()

def genCSVfromDB(dbinp, locusfiles):
#    if args.inccfg and args.genLocus:
#        raise RuntimeError("It is either --inccfg or --genLocus, not both!")
#    #

    #res_gen = getDBBestVar(dbinp, locusfiles, 
    res_gen = getAllDBexpsOPT(dbinp, locusfiles, 
            ['ICELOCUS_CC','ICELOCUS_MATSHAPE_M', 
                'ICELOCUS_MATSHAPE_N',
                'ICELOCUS_MATSHAPE_K',
                'ICELOCUS_OMP_NUMTH'])

    if args.inccfg: # or args.genLocus:
        printDBCfgs(dbinp, locusfiles)
    #

    # Store results for same shape
    shapeDict = {}

    # Store configuration data
    cfgData = {}

    # Variant and experiments loop
    for metainfo, var, exp, expvals in res_gen:
        #print(f"tmp: {type(tmp), {tmp}}")
        lfname = metainfo['lfname']
        comp = metainfo['ICELOCUS_CC']
        shapeM = metainfo['ICELOCUS_MATSHAPE_M'] 
        shapeN = metainfo['ICELOCUS_MATSHAPE_N'] 
        shapeK = metainfo['ICELOCUS_MATSHAPE_K'] 
        nth =  metainfo.get('ICELOCUS_OMP_NUMTH', '1')
        setool = metainfo['searchtool']
        cfgid = var.configuration.id
        #print("Data:", var.configuration.data)

        if cfgid not in cfgData:
            cfgData[cfgid] = var.configuration.data
        else:
            if cfgData[cfgid] != var.configuration.data:
                raise RuntimeError(f"Cfdid {cfgid} presented inconsistent data "
                                   f"{cfgData[cfgid]} !!!=== var.configuration.data")
            #
        #

        #if args.inccfg:
        #    lfname += "_"+str(var.configuration.id)
        #

        #descdict = {ev.desc: ev.metric for ev in expvals}
        #print(f"{lfname},{comp},{setool},{nth},{var.id},{shape},{shape},{shape},{descdict['Total']}")

        metric=expvals.metric
        if metric != float('inf'):
            varTuple = var.id
            #print(f"{lfname},{comp},{setool},{nth},{shapeM},{shapeN},{shapeK},{var.id},{metric}")
            shapeTuple = (lfname, comp, setool, nth, shapeM, shapeN, shapeK)
            if shapeTuple not in shapeDict:
                # create a dict that saves the run with the fastest median metric
                shapeDict[shapeTuple] = {}
            ##
            if cfgid not in shapeDict[shapeTuple]:
                shapeDict[shapeTuple][cfgid] = {}
            ##
            if var.id not in shapeDict[shapeTuple][cfgid]:
                shapeDict[shapeTuple][cfgid][varTuple] = []
            ##
            shapeDict[shapeTuple][cfgid][varTuple].append(metric)
        else:
            print(f"Found inconsistent expval! Likely ok to skip. varid:"
                    f" {var.id} expval: {exp.id} metric: {metric}", file=sys.stderr)
        #
    #

    print(f"leg,comp,searchtool,numthreads,matshapeM,matshapeN,matshapeK,cfgid,runid,time")

    shapeDictMedian = {}
    # Get the median values of the runs of each shape
    for k, v in shapeDict.items():
        newk = k
        # Each configuration
        for k_cfg, v_cfg in v.items():
            # if result per configuration, add id to the key
            if args.inccfg:
                #print("v->",v)
                cfgid = k_cfg
                newk = k + (cfgid,)
            #
            if newk not in shapeDictMedian:
                shapeDictMedian[newk] = []
            #

            # for each run get the median
            for k_run, v_run in v_cfg.items():
                #
                #print("newk: ", newk, k_run, v_run )
                med = stat.median(v_run)
                shapeDictMedian[newk].append((k_cfg, k_run, med))
            ##
        ##
    ##
    import numpy as np
    # Get best for each shape
    for k,v in shapeDictMedian.items():
        cfgid, runid, min_ = min(v, key = lambda t: t[2])

        if args.inccfg:
            # In inccfg cfgid is already part of k
            print(f"{','.join(str(v) for v in k)},{runid},{min_}")
        else:
            print(f"{','.join(str(v) for v in k)},{cfgid},{runid},{min_}")

        # Print out the Locus program to generate the quantile variants.
#        if args.genLocus:
#            envdict = {'ICELOCUS_CC' : k[1],
#               'ICELOCUS_MATSHAPE_M' : k[4],
#               'ICELOCUS_MATSHAPE_N' : k[5],
#               'ICELOCUS_MATSHAPE_K' : k[6],
#               'ICELOCUS_OMP_NUMTH'  : k[3]}
#            #print(envdict)
#
#            arr = np.asarray([f[2] for f in v])
#            print("len", len(arr))
#            if len(arr) > 5:
#                # Get 6 quantiles
#                quant = np.quantile(arr, [0.0,0.25,0.5,0.75,1.0])
#                # Get indices of the 6 quantiles
#                idx = [(np.abs(arr - i)).argmin() for i in quant]
#                print(idx)
#                for i in idx:
#                    print(f"v[{i}] = ",v[i])
#                    cfgid = v[i][0]
#                    lprog = getLocusProgram(dbinp, (cfgid,))
#                    locusfilename = lprog[0].locusfilename
#                    locuscode = lprog[0].data
#                    # Put through Locus parser
#                    lp = LocusParser()
#                    locustree = lp.parse(locuscode, False)
#                    searchnodes = {}
#                    replEnvVars(locustree, envdict)
#                    searchbase.buildSearchNodes(locustree.topblk, searchnodes)
#                    for name, ent in locustree.items():
#                        searchbase.buildSearchNodes(ent, searchnodes)
#                    ##
#                    # Change it to the respective configuration
#                    print(cfgData[cfgid])
#                    revertse = {}
#                    searchbase.convertDesiredResult(searchnodes, cfgData[cfgid], revertse)
#                    # Print it out
#                    fname = "cfg_"+str(cfgid)+"_"+locusfilename
#                    GenLocus.searchmode(locustree, fname)
#                ##
#            else:
#                # Just gen Locus programs of all, 5 or less, configurations.
#                print("blah")
#            #
        ##
    ##
###

if __name__ == '__main__':
    genCSVfromDB(args.i, args.l)
#



