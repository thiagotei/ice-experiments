#!/usr/bin/env bash


valM=$1
valN=$2
valK=$3
inpf=$4
outf=$5
sed -e "s/define M 2048/define M ${valM}/g" -e "s/define N 2048/define N ${valN}/g" -e "s/define K 2048/define K ${valK}/g" $inpf > $outf
