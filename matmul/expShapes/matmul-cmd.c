#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
//#include <assert.h>
//#include <unistd.h>
#include <float.h>
#include <mysecond.h>
#include <pips_runtime.h>

//#pragma declarations
//double A[M][K];
//double B[K][N];
//double C[M][N];
//#pragma enddeclarations
//#ifdef TIME
#define IF_TIME(foo) foo;
#define EXPR(i,j,lda) ((i)*(lda))+(j)
//#else
//#define IF_TIME(foo)
//#endif

void init_array(int M, int N, int K, double *A, double *B, double *C)
{
    int i, j;

    for (i=0; i<M; i++) {
        for (j=0; j<K; j++) {
            A[EXPR(i,j,M)] = (i + j);
        }
    }
    for (i=0; i<K; i++) {
        for (j=0; j<N; j++) {
            B[EXPR(i,j,K)] = (double)(i*j);
        }
    }

    for (i=0; i<M; i++) {
        for (j=0; j<N; j++) {
            C[EXPR(i,j,M)] = 0.0;
        }
    }

}

void print_array(int M, int N, double *C)
{
    int i, j;

    for (i=0; i<M; i++) {
        for (j=0; j<N; j++) {
            fprintf(stderr, "%lf ", C[EXPR(i,j,M)]);
            if (j%80 == 79) fprintf(stderr, "\n");
        }
        fprintf(stderr, "\n");
    }
}

void allocateMat(double ** A, int rows, int cols) {
    *A = (double *) malloc(rows * cols * sizeof(double));
    if(!(*A)) fprintf(stderr, "Could not allocate matrix!");
}

int main(int argc, char *argv[])
{
    int i, j, k;
    double *A, *B, *C;
    int M, N, K, iterations = 5;
    const double alpha = 1.0, beta = 0.0;
    struct timespec t_start, t_end;

    if (argc < 4) {
        fprintf(stderr, "Usage: %s <m> <n> <k> [<num iter>]\n", argv[0]);
        exit(1);
    } else {
        M = atoi(argv[1]);
        N = atoi(argv[2]);
        K = atoi(argv[3]);
        if (argc == 5) iterations = atoi(argv[4]);
    }

    double *t_it = (double *) malloc(iterations * sizeof(double));
    if (!t_it){printf("Could not allocate t_it.\n"); exit(1);}

    allocateMat(&A, M, K);
    allocateMat(&B, K, N);
    allocateMat(&C, M, N);

    init_array(M, N, K, A, B, C);

#ifdef PERFCTR
    PERF_INIT; 
#endif
    for (int it = 0; it < iterations; it++) {
       IF_TIME(mygettime(&t_start))

#pragma @ICE loop=matmul
       for(i=0; i<M; i++)
          for(j=0; j<N; j++)  
             for(k=0; k<K; k++)
                C[EXPR(i,j,M)] = beta*C[EXPR(i,j,M)] + alpha*A[EXPR(i,k,M)] * B[EXPR(k,j,K)];

       IF_TIME(mygettime(&t_end))
       IF_TIME(t_it[it] = mydifftimems(&t_start, &t_end))

       if (it== 0 && fopen(".test", "r")) {
#ifdef MPI
         if (my_rank == 0) {
          print_array(M, N, C);
         }
#else
         print_array(M, N, C);
#endif
       }
    }

    IF_TIME(printf("Matrixsize= %d %d %d | Iterations %d Time(milisec) ", M, N, K, iterations))
    IF_TIME(for(int it = 0; it < iterations; it++) 
              printf("%7.5lf " ,t_it[it]);
            printf("\n"))


#ifdef PERFCTR
    PERF_EXIT; 
#endif
    free(t_it);
    free(A);
    free(B);
    free(C);

  return 0;
}
