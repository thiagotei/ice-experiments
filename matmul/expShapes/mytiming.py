import re
import statistics as stat

#Matrixsize= 16 16 256 | Iterations 5 Time(milisec) 0.08235 0.08056 0.08053 0.08041 0.08042
#Matrixsize= 2048 2048 2048 beta= 0.335009 alpha= -0.364523 | Iterations 5 Time(milisec) 1003.33858 987.56492 982.16702 981.46810 981.45794
#Matrixsize= 2048 2048 2048 | Iterations 5 Time(milisec) 1003.33858 987.56492 982.16702 981.46810 981.45794

def getTimingMatMul(std, error):
    r = re.search('.*Time.*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    if m and len(m) >= 7:
       vals = []
       unit = m[7]
       unit = unit[unit.rfind('(')+1:unit.rfind(')')]
       numit = int(m[6])
       for it in range(8,8+numit):
          vals.append(float(m[it]))
       ##
       metric = stat.median(vals)
       result = {'metric': metric,
                 # it must be tuple of tuples despite being a single timer
                 'exps': tuple([(v,) for v in vals]),
                 'unit': (unit,),
                 'desc': ('Total',)}
    else:
       result = {'metric': 'N/A'}

    return result


