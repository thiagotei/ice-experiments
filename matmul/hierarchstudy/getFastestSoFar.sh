#!/bin/bash

# arguments: <output file name> <logs from ice>

args=("$@")
resfilename=${args[0]} #hyperopt-V3-sadr-minsofar-12-exper.txt
if [ -f $resfilename ]; 
then
	echo "File ${resfilename} exists. To avoid problems make sure this file is correct and remove it. BE CAREFUL!"
	exit
fi

tmpfile=$(mktemp)

for i in ${args[@]:1}; do
	echo "Processing" $i
	#grep "Run cfg_id" $i  | awk 'NR==1{min=$17} $17!~/inf/{$17<min?min=$17:min; print $7,$17}' #| wc -l
	#grep "Run cfg_id" $i  | awk 'NR==1{min=$17} $17!~/inf/{$17<min?min=$17:min; print $7,min}' >> $tmpfile  #| wc -l
	grep "Variant result" $i  | awk 'NR==1{min=$21} $21!~/inf/{$21<min?min=$21:min; print $6,min}' >> $tmpfile  #| wc -l
done

sort -nk 1 $tmpfile > $resfilename
rm $tmpfile
