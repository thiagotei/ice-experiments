#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser()
parser.add_argument('--t', type=str, required=True, help="Title for the chart")
parser.add_argument('--files',  nargs='+', required=True, help="receive a list of files.")
args = parser.parse_args()

def plot(inpfile, leg):
    data = []
    with open(inpfile, 'r') as fi:
        for line in fi:
            v = line.split()
            data.append((float(v[0]), float(v[1])))
    #end with

    x = [v[0] for v in data]
    y = [v[1] for v in data]
    plt.plot(x, y, label=leg)
#enddef

for i, f in enumerate(args.files):
    print "Plotting ", i,": ",f
    plot(f, str(i))

plt.legend(loc='upper right')
plt.ylabel('Variant Execution Time')
plt.xlabel('Autotuning time (sec)')
#plt.title('P80, Median, and P20 of the Best Variant Found')
plt.title('Best Variant Found - '+args.t)
plt.show()



