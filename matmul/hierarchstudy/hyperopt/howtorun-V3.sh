#!/bin/bash

matshape=2048
#matshape=256
ntests=1000
stopafter=60
ice-locus-hyperopt.py --stop-after $stopafter --timeout -t matmul-V3.locus -f matmul.${matshape}.c --tfunc mytiming.py:getTimingMatMul -o suffix -u '.ice' --search --ntests ${ntests}

