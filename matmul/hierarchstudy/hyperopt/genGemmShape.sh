#!/bin/bash


die () {
	echo >&2 "$@"
	exit 1
}

if [ "$#" -eq 1 ]; then 
	shape=${1}; 
else
	die "1 param required -- matrix shape, $# provided"
fi
#shape=4096; 
sed  -e "s/define M 2048/define M ${shape}/g" -e "s/define N 2048/define N ${shape}/g" -e "s/define K 2048/define K ${shape}/g"  matmul.c > matmul.${shape}.c
