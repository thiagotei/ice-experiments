#!/usr/bin/env python
#import subprocess32 as subprocess
import os, sys, time, shlex

if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
    print("ouch using subprocess32")
else:
    import subprocess


#cmd = ['ping','www.google.com']
#cmd = ['ls','-la']
#cmd = './orig 2> blah.txt'
#cmd = './matmul.2048.ice 2> ./answer-matmul-2048.txt'
cmd = 'echo a && echo b'
#cmd = './orig 2> /dev/null'
#cmd = './origSuperShort 2> blah.txt'

STDOUT_FILE_NAME = './.tmp_ice_comp_stdout.txt'
STDERR_FILE_NAME = './.tmp_ice_comp_stderr.txt'

output = None
error_out = None
try:
    #obj = subprocess.run(cmd,  stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=5)
    #obj = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=5)
    #obj = subprocess.run(cmd, shell=True, timeout=5)
    #obj = subprocess.run(cmd, shell=True, stderr=open('comp_stderr.txt', 'w'),stdout=open('comp_stdout.txt', 'w'), timeout=5)

    #a = obj.stdout
    #b = obj.stderr
    #c= obj.returncode

    with open(STDOUT_FILE_NAME, 'w') as fstdout, \
            open(STDERR_FILE_NAME, 'w') as fstderr:
        t0 = time.time()
        #cplproc = subprocess.run(cmd, shell=True, stdout=fstdout, stderr=fstderr, timeout=10)
        cmdslist = ['echo', 'a','echo', 'b' ]#shlex.split(cmd)
        print("cmd ", cmd, "cmdlist vs", cmdslist)
        cplproc = subprocess.run(cmdslist, shell=False, stdout=fstdout, stderr=fstderr, timeout=50)
        t1 = time.time()
    #endwith

    #print('what','stdout:',a,'stderr:',b,'returncode:',c)
    print('what')
except subprocess.TimeoutExpired as e:
    #print "proc id ", cplproc.id
    print('process ran too long', 'stdout:',e.stdout, 'stderr:', e.stderr,'retcode:', e.timeout)
else:
    with open(STDOUT_FILE_NAME, 'r') as f:
        output = f.read()
    with open(STDERR_FILE_NAME, 'r') as f:
        error_out = f.read()
    #error_out = cplproc.stderr
    retcode = cplproc.returncode
    elapsed = t1 - t0
finally:
    os.remove(STDOUT_FILE_NAME)
    os.remove(STDERR_FILE_NAME)
#endtry

print("output", output, "error_out", error_out)
