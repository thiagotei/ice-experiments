#!/usr/bin/env bash

. ../runparams.sh

ice-locus-hyperopt.py  --stop-after $stopaf --timeout --upper-limit ${uplimit} \
	-t ${locusfile} -f $cfile \
	--tfunc mytiming.py:getTimingMatMul -o suffix \
	-u '.ice' --search --ntests ${ntests} --initseed --hypalg ${hypalg} --hypmixdsc ${hypmixdsc}  #--debug #--convonly

