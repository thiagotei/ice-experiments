import re

def getTimingMatMul(std, error):
    r = re.search('.*Time.*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    result = 'N/A'
    if m and len(m) >= 7:
       result = float(m[7])

    return result


