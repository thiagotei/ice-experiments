#!/usr/bin/env bash

#. ../runparams.sh
. ./runparams.sh


n=${ICELOCUS_GEMM_SHAPE}
if [ ! -f ./orig-answer-matmul-${n}.txt ]; then
	echo "Generating orig answer..."
	make CXX=$ICELOCUS_CXX CC=$ICELOCUS_CC USERDEFS="-DM=${n} -DN=${n} -DK=${n}" clean orig
	./orig
	mv answer-matmul-${n}.txt orig-answer-matmul-${n}.txt
fi 

if [ ! -f matmul.${n}.c ]; then
	sed -e "s/define M 256/define M ${n}/g" \
        -e "s/define N 256/define N ${n}/g" \
        -e "s/define K 256/define K ${n}/g" matmul.c > matmul.${n}.h;
fi

ice-locus-choco.py  --stop-after $stopaf --timeout --upper-limit ${uplimit} \
	-t ${locusfile} -f $cfile \
	--tfunc mytiming.py:getTimingMatMul -o suffix \
	-u '.ice' --search --ntests ${ntests} $xparams #--initseed \
    #--hypalg ${hypalg} --hypmixdsc ${hypmixdsc}  #--debug #--convonly

