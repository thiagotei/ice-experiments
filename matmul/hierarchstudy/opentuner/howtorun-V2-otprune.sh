#!/usr/bin/env bash

. ../runparams.sh

rm -rvf opentuner.db opentuner.log

ice-locus-opentuner.py --stop-after $stopaf --timeout --upper-limit ${uplimit} \
	-t ${locusfile} -f $cfile \
	 --tfunc mytiming.py:getTimingMatMul -o suffix \
	 -u '.ice' --search --ntests ${ntests} --initseed --otprune

