#!/bin/bash

#matshape=256
matshape=2048
ntests=1000
stopafter=60
rm -rvf opentuner.db opentuner.log
ice-locus-opentuner.py --stop-after $stopafter --timeout -t matmul-V3.locus -f matmul.${matshape}.c --tfunc mytiming.py:getTimingMatMul -o suffix -u '.ice' --search --ntests ${ntests} --otprune  

