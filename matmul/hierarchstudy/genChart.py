#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
#matplotlib.use('TkAgg')

parser = argparse.ArgumentParser()
parser.add_argument('--hyfi', type=argparse.FileType('r'), help="series of data A")
parser.add_argument('--otfi', type=argparse.FileType('r'), help="series of data B")
parser.add_argument('--bins', type=int, required=False, default=10, help="number of bins used to separate the data.")
args = parser.parse_args()

# read the files and expect 2 columns, one with time stamp relative to 
# the beginning of the search and a second with the variant's execution time

def binization(fd, fmt, label):
    raw_hy = []
    with fd as f:
        for line in f:
            v = line.split()
            raw_hy.append((float(v[0]), float(v[1])))
    #endif

    #sort based on time stamp
    raw_hy.sort(key=lambda tup: tup[0])

    hy_X = [v[0] for v in raw_hy]
    hy_Y = [v[1] for v in raw_hy]
    raw_hy_latest=hy_X[-1]
    #print args.bins, raw_hy_latest
    print (args.bins, raw_hy_latest)
    hybins = np.linspace(0.0, raw_hy_latest, args.bins)
    digitized = np.digitize(hy_X, hybins)-1
    bin_centers = hybins + (hybins[1]-hybins[0])/2

    Y = np.asarray(hy_Y, dtype='float')
    bin_medians = [np.median(Y[digitized == j]) for j in range(len(hybins))]
    #bin_p20 = [np.percentile(Y[digitized == j], 20) for j in range(len(hybins))]
    bin_p20 = []
    #bin_p80 = [np.percentile(Y[digitized == j], 80) for j in range(len(hybins))]
    bin_p80 = []
    #bin_min = [np.min(Y[digitized == j]) for j in range(len(hybins))]
    #bin_max = [np.max(Y[digitized == j]) for j in range(len(hybins))]

    #print bin_p80
    #print bin_medians
    #print bin_p20
    #print hybins
    #p20 = [m - p for m, p in zip(bin_medians, bin_p20)] 
    #p80 = [p - m for p, m in zip(bin_p80, bin_medians)]
    #plt.scatter(hy_c, hy_med)
    #plt.plot(hy_c, hy_med, 'o', label="Hyperopt")
    #plt.errorbar(bin_centers, bin_medians, yerr=[p20, p80], fmt=fmt, label=label)
    plt.errorbar(bin_centers, bin_medians,  fmt=fmt, label=label)

    return bin_centers, bin_medians, bin_p20, bin_p80
#enddef

if args.hyfi:
    hy_c, hy_med, bin_p20, bin_p80 = binization(args.hyfi, 'bo', 'Hyperopt')
    #p20 = [m - p for m, p in zip(hy_med, bin_p20)] 
    #p80 = [p - m for p, m in zip(bin_p80, hy_med)]
    #plt.scatter(hy_c, hy_med)
    #plt.plot(hy_c, hy_med, 'o', label="Hyperopt")
    #plt.errorbar(hy_c, hy_med, yerr=[p20, p80], fmt='bo', label="Hyperopt")
else:
    print ("No ot file!")
#endif
if args.otfi:
    ot_c, ot_med, bin_p20, bin_p80 = binization(args.otfi, 'ro', 'OpenTuner')
    #plt.scatter(ot_c, ot_med)
    #plt.plot(ot_c, ot_med, 'x', label="OpenTuner")
    #p20 = [m - p for m, p in zip(ot_med, bin_p20)] 
    #p80 = [p - m for p, m in zip(bin_p80, ot_med)]
    #plt.errorbar(ot_c, ot_med, yerr=[p20, p80], fmt='rx', label="OpenTuner")
else:
    print ("No ot file!")
#endif

plt.legend(loc='upper right')
plt.ylabel('Variant Execution Time')
plt.xlabel('Autotuning time (sec)')
plt.title('P80, Median, and P20 of the Best Variant Found')
#plt.show()
plt.savefig('chart.pdf')
 

