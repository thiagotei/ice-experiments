#!/usr/bin/env bash

. ./runparams.sh

BASEDIR=/home/tteixei2/ice-experiments/matmul/hierarchstudy
#BASEDIR=/home/thiago/Documents/ice-experiments/matmul/hierarchstudy
#version="V2" #"V3" # 
nproc=1;

#[ -f $BASEDIR ] && echo "$BASEDIR does not exist!";  exit
curhypexpdir=${BASEDIR}/hyperopt
#[ ! -d $curhypexpdir ] && mkdir -v $curhypexpdir
echo "Vai comecar" $nruns


cd $BASEDIR
#for r in 1 2 3 4 5 6 7 8; do 
for r in `seq $nruns`; do 
	for al in $algos; do
		export hypalg=$al
		if [[ "$hypalg" = *mix* ]]
		then
			export hypalg="mix"
		fi
		#if [ "$hypalg" == "mixB" ]
		#then
		#	export hypalg="mix"
		#fi

		export hypmixdsc=${mixdsc[${al}]}
		echo $r $al $hypmixdsc $ICELOCUS_COMP `date` ; 
		#cd ${BASEDIR}/hyperopt/${EXPDIR};
		cd $curhypexpdir;
		echo `pwd` `date`;
		./howtorun-V2.sh &> output-${version}-${hypalg}-${hypmixdsc}-${nproc}thread-`date +%Y%m%d%H%M`.txt; 
	#	cd ${BASEDIR}/opentuner; 
	##	echo `pwd` `date`;
	##	./howtorun-V2.sh &> output-${version}-opentuner-${nproc}thread-`date +%Y%m%d%H%M`.txt;
	#	echo `pwd` otprune `date`;
	#	./howtorun-V2-otprune.sh &> output-${version}-opentuner-otprune-${nproc}thread-`date +%Y%m%d%H%M`.txt;
		cd $BASEDIR
	done
done
