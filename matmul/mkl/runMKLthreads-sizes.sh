#!/usr/bin/env bash

export LD_LIBRARY_PATH=/opt/intel/oneapi/mkl/latest/lib/intel64/:$LD_LIBRARY_PATH

beta="0.335009"
alpha="-0.364523"

for t in 1; 
do
	#for s in `seq 250 250 5000`;
	#for exp in `seq 7 12`;
    for s in `seq 400 400 4000`;
    do
        for n in 2 5 10;
        do
            k=$n
            #s=$((2**$exp))
            echo Threads $t Size $s $n $k $beta $alpha;
            OMP_PLACES=cores OMP_PROC_BIND=close
            #export KMP_AFFINITY=compact #,verbose
            export MKL_NUM_THREADS=${t}
            ./mmm_mkl.seq.exe $s $n $k $beta $alpha
        done
	done
done
