#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdio.h>
#include <stdlib.h>
//#include <time.h>
#include <sys/time.h>
#include <math.h>

#define MIN(a,b) (((a) < (b)) ? (a) : (b))

/*
double getTime() {
    struct timespec tp;
    int err;
    err = clock_gettime(CLOCK_MONOTONIC, &tp);
    if(err) {
        fprintf(stderr, "Error reading time. Aborting...\n");
        exit(-1);
    }
    //return tp.tv_sec*1.0 + tp.tv_nsec*1.0e-9;
    return tp.tv_sec*1.0 + tp.tv_usec*1.0e-6;
}
*/


double rtclock()
{
	struct timeval Tp;
	int stat;
	stat = gettimeofday (&Tp, NULL);
	if (stat != 0) printf("Error return from gettimeofday: %d",stat);
	return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}


void initGlobal(double *x,      // Array to be initialized
                int nr,         // Number of rows
                int nc          // Number of columns
                ) {
    int i, j;
    for(j = 0; j < nc; j++) {
        for(i = 0; i < nr; i++) {
            x[i + nr*j] = i * nc + j;
        }
    }
}

void initZero(double *x,int nr, int nc) {
    int i;
    for (i = 0; i < nr*nc; i++) {
            x[i] = 0;
    }
}

inline double*  create(int nr, int nc) {
    return (double *)malloc(sizeof(double)*nr*nc);
}

void printMatrix(double *x, int nr, int nc) {
    int i, j;
    for (i = 0;i < nr; i++) {
        for (j = 0;j < nc; j++) {
            printf("%lf ",x[i + nr*j]) ;
        }
        printf("\n");
    }
}

void swapMatrix(double **A, double **B) {
    double *temp;
    temp = *A;
    *A = *B;
    *B = temp;
}

#endif // __UTILS_H__
