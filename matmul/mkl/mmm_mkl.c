//#include "../utils/utils.h"
#include <mkl.h>
#include "utils.h"
#ifdef USE_ORIGINAL_TIMER
#include <mysecond.h>
#else
#include <utils/libxsmm_timer.h>
#endif

int main(int argc, char *argv[]) {
    // Matrix dimensions (global)
    int m, n, k, iter, iterations = 5, warmup = 2;

    // Matrices
    double *A, *B, *C;
    double beta = 1.0, alpha = 1.0;

    // Timing variables
    struct timespec t_start, t_end;

    /* Get command line arguments */
    if (argc < 4) {
        fprintf(stderr, "Usage: %s <m> <n> <k> [<iterations> [<beta> [<alpha>]]]\n", argv[0]);
        exit(1);
    } else {
        m = atoi(argv[1]);
        n = atoi(argv[2]);
        k = atoi(argv[3]);
        if (argc >= 5) {
            iterations = atoi(argv[4]);
            if (argc >= 6) {
                beta = atof(argv[5]);
                if (argc >= 7) {
                    alpha = atof(argv[6]);
                }
            }
        }
    }

    if (m <= 0 || n <= 0 || k <= 0) {
        fprintf(stderr, "Invalid matrix dimensions M: %d N: %d K: %d\n", m, n, k);
        exit(1);
    }

#ifdef USE_ORIGINAL_TIMER
    double *t_it = (double *) malloc(iterations * sizeof(double));
    if (!t_it){printf("Could not allocate t_it.\n"); exit(1);}
#else
    double l_total = 0.0;
#endif

    // Allocate matrices
    A = create(m, k);
    B = create(k, n);
    C = create(m, n);

    // Initialize matrices
    initGlobal(A, m, k);
    initGlobal(B, k, n);
    initZero(C, m, n);

    //printMatrix(A, m, k);
    //printMatrix(B, k, n);

    const char * const env_warmup = getenv("WARMUP");
    warmup = (NULL == env_warmup ? warmup : atoi(env_warmup));
    printf("Warmup iterations: %d ...\n", warmup);
    // Matrix multiplication!
    for (iter = 0; iter < (iterations + warmup); iter++) {
      {
#ifdef USE_ORIGINAL_TIMER
       struct timespec t_start, t_end;
       mygettime(&t_start);
#else
       libxsmm_timer_tickint start, end;
       start = libxsmm_timer_tick();
#endif
       cblas_dgemm(CblasColMajor,
                CblasNoTrans, CblasNoTrans,
                m, n, k,
                alpha,
                A, m,
                B, k,
                beta,
                C, m);
#ifdef USE_ORIGINAL_TIMER
       mygettime(&t_end);
       if (warmup <= iter) t_it[iter - warmup] = mydifftimems(&t_start, &t_end);
#else
       end = libxsmm_timer_tick();
       if (warmup <= iter) {
          libxsmm_timer_tickint comptime = libxsmm_timer_ncycles(start, end);
          l_total += libxsmm_timer_duration(0, comptime);
       }
#endif
      }
    }
    //printMatrix(C, m, n);
    double gflopA = (2.0*m*n*k)*1E-9;
#ifdef USE_ORIGINAL_TIMER
    //double gflopB = 2.0*((m*n*k)+(m*n))*1E-9;
    printf("MKL numthreads= %d shape= %d %d %d beta= %lf alpha= %lf\n", mkl_get_max_threads(), m, n, k, beta, alpha);
    printf("Iterations %d Time(milisec) ", iterations);
    for(iter = 0; iter < iterations; iter++) {
        printf("%7.8lf ",t_it[iter]);
    }
    printf("\n");
    printf("Iterations %d GFlops ", iterations);
    for(iter = 0; iter < iterations; iter++) {
        printf("%.8e ", gflopA / (t_it[iter] * 1E-3));
    }
    printf("\n");
    free(t_it);
#else
    printf("MKL numthreads= %d shape= %d %d %d beta= %lf alpha= %lf Iterations %d\n", mkl_get_max_threads(), m, n, k, beta, alpha, iterations);
    printf("AvgTime(seconds) %7.10lf GFLOPS %.8e\n", l_total/iterations, (gflopA * (double)iterations)/l_total);
#endif
    return 0;
}
