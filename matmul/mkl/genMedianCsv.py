#!/usr/bin/env python3

import re, argparse
import statistics as stat

#  0           1 2      3    4  5  6     7        8      9      10  11       12  13            14      15      16      17          18          19      20          21          22      23          24          25      26
#MKL numthreads= 1 shape= 4000 10 10 beta= 0.335009 alpha= -0.364523 | Iterations 5 Time(milisec) GFlopsA GFlopsB 2.42530 3.03163e+03 2.75602e+03 0.11564 1.44549e+02 1.31408e+02 0.11251 1.40637e+02 1.27852e+02 0.11195 1.39942e+02 1.27220e+02 0.11123 1.39042e+02 1.26402e+02


parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input file")
args=parser.parse_args()

def getMedian(std):
    r = re.search('.*Time.*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    result = ""
    if m and len(m) >= 7:
       result = ",".join([m[0],m[2],m[4],m[5],m[6],m[8],m[10]])
       vals = []
       numit = int(m[13])
       for it in range(17,17+(numit*3),3):
          vals.append(float(m[it]))
       ##
       metric = stat.median(vals)
       result += ","+str(metric)
#    else:
#       result = 'N/A ->'+std

    return result
##

if __name__ == '__main__':
    with open(args.i, 'r') as f:
        print("leg,numthreads,shapeM,shapeN,shapeK,beta,alpha,time")
        for l in f.readlines():
            print(getMedian(l))
        ##
    ##
#

