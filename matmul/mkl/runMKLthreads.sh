#!/bin/bash


#for t in 1 2 4 6 8 10; 
for t in 1 2 3;
do 
for v in 1 2 3 4 5;do 
	echo Threads $t; 
	OMP_PLACES=cores OMP_PROC_BIND=close
	#export KMP_AFFINITY=compact #,verbose
	export MKL_NUM_THREADS=${t}
	#./mmm_mkl.exe 2048 2048 2048
	./mmm_mkl.par.exe 4096 4096 4096
done
done
