#!/bin/bash

die () {
    echo >&2 "$@"
    exit 1
}
ntests=3
if [ "$#" -eq 1 ];then ntests=${1}; fi #|| die "1 filename required -- input to be encrypted and the output will be input + .enc , $# provided

#ice-locus-opentuner.py -t matmul.locus -f mmc.c --tfunc mytiming.py:getTimingMatMul --debug -o inplace --search --ntests ${ntests}
#ice-locus-opentuner.py -t matmul.locus -f mmc.c --tfunc mytiming.py:getTimingMatMul -o inplace --search --ntests ${ntests}
#ice-locus-opentuner.py -t matmul.locus -f mmc_parsed.c --tfunc mytiming.py:getTimingMatMul -o inplace --ntests ${ntests}
ice-locus-opentuner.py -t matmul.locus -f matmul_1000.c --tfunc mytiming.py:getTimingMatMul -o inplace --ntests ${ntests}
