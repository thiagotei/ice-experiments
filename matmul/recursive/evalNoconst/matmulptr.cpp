#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
//#include <assert.h>
#include <unistd.h>
#include <sys/time.h>
#include <float.h>
//#include <pips_runtime.h>
//#include "globals.h"
//extern "C" {
#include <mysecond.h>
#include <string>
#include "variantsptr.h"
//}
//double A[M][K];
//double B[K][N];
//double C[M][N];

//#ifdef TIME
#define IF_TIME(foo) foo;
//#else
//#define IF_TIME(foo)
//#endif

void init_array(MATTYPE A, MATTYPE B, MATTYPE C,
                const int M, const int N, const int K)
{
    int i, j;

    for (i=0; i<M; i++) {
        for (j=0; j<K; j++) {
            //A[i][j] = (i + j);
            A[ind(i,j,K)] = (i + j);
        }
    }
    for (i=0; i<K; i++) {
        for (j=0; j<N; j++) {
            B[ind(i,j,N)] = (double)(i*j);
        }
    }

    for (i=0; i<M; i++) {
        for (j=0; j<N; j++) {
            C[ind(i,j,N)] = 0.0;
        }
    }

}

void print_array(MATTYPE C, const int M, const int N)
{
    int i, j;
    char bufname[32];
    snprintf(bufname, BUFNAMESIZE, "answer-matmul-%d-%d.txt",M, N);
    FILE *fout = fopen(bufname,"w");
    if(!fout){
          fprintf(stderr,"ERROR! Could not open %s to output results!\n", bufname);
          exit(1);
    }

    for (i=0; i<M; i++) {
        for (j=0; j<N; j++) {
            fprintf(fout, "%lf ", C[ind(i,j,N)]);
            if (j%80 == 79) fprintf(fout, "\n");
        }
        fprintf(fout, "\n");
    }

    fclose(fout);
}

/*double rtclock()
{
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}*/


int main(int argc, char** argv)
{
    int iterations = 5, M=128, N=128, K=128;
    double * t_it = (double *) malloc(iterations * sizeof(double));
    if (!t_it){ printf("Could not allocate t_it.\n"); exit(1);}

    if(argc >= 2) {
        M = N = K = std::stoi(argv[1]);
    } else {
        fprintf(stderr, "Usage: ./<bin> <dim>\n");
        return 1;
    }

    struct timespec t_start, t_end;
#ifdef ALIGN_MALLOC
    double *A = (double *) aligned_alloc(ALIGNFACTOR, M * K * sizeof(double));
    double *B = (double *) aligned_alloc(ALIGNFACTOR, K * N * sizeof(double));
    double *C = (double *) aligned_alloc(ALIGNFACTOR, M * N * sizeof(double));
    printf("Using aligned_alloc!\n");
#else
    double *A = (double *) malloc(M * K * sizeof(double));
    double *B = (double *) malloc(K * N * sizeof(double));
    double *C = (double *) malloc(M * N * sizeof(double));
    printf("Using regular malloc!\n");
#endif
    if (!A){ printf("Could not allocate A.\n"); exit(1);}
    if (!B){ printf("Could not allocate B.\n"); exit(1);}
    if (!C){ printf("Could not allocate C.\n"); exit(1);}

    init_array(A, B, C, M, N, K);
    printf("M=%d, N=%d, K=%d\n", M, N, K);

#ifdef PERFCTR
    PERF_INIT; 
#endif
    for (int it = 0; it < iterations; it++) {
        //IF_TIME(t_start = rtclock());
        IF_TIME(mygettime(&t_start));

#pragma @ICE block=gemm
        naiveIJK(0, M, 0, N, 0, K, N, K, A, B, C);
#pragma @ICE endblock

        IF_TIME(mygettime(&t_end));
        IF_TIME(t_it[it] = mydifftimems(&t_start, &t_end));

        //IF_TIME(if (t_it > 0 && t_it < min) min = t_it);
        //IF_TIME(if (t_it > 0 && t_it > max) max = t_it);
        //IF_TIME(t_all += t_it);

        if (it== 0 && fopen(".test", "r")) {
#ifdef MPI
            if (my_rank == 0) {
                print_array(C, M, N);
            }
#else
            print_array(C, M, N);
#endif
       }
    }
    //IF_TIME(avg = t_all/iterations);

    IF_TIME(printf("Matrixsize= %d %d %d | Iterations= %d | Time(milisec) " , M, N, K, iterations));
    for(int it = 0; it < iterations; it++) {
        IF_TIME(printf("%7.5lf ", t_it[it]));
    }
    IF_TIME(printf("\n"));

#ifdef PERFCTR
    PERF_EXIT; 
#endif

  free(t_it);
  free(A);
  free(B);
  free(C);
  return 0;
}
