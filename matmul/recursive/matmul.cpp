#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
//#include <assert.h>
#include <unistd.h>
#include <sys/time.h>
#include <float.h>
//#include <pips_runtime.h>
//#include "globals.h"
#include "variants.h"

double A[M][K];
double B[K][N];
double C[M][N];

//#ifdef TIME
#define IF_TIME(foo) foo;
//#else
//#define IF_TIME(foo)
//#endif

void init_array()
{
    int i, j;

    for (i=0; i<M; i++) {
        for (j=0; j<K; j++) {
            A[i][j] = (i + j);
        }
    }
    for (i=0; i<K; i++) {
        for (j=0; j<N; j++) {
            B[i][j] = (double)(i*j);
        }
    }

    for (i=0; i<M; i++) {
        for (j=0; j<N; j++) {
            C[i][j] = 0.0;
        }
    }

}

void print_array()
{
    int i, j;

    for (i=0; i<M; i++) {
        for (j=0; j<N; j++) {
            fprintf(stderr, "%lf ", C[i][j]);
            if (j%80 == 79) fprintf(stderr, "\n");
        }
        fprintf(stderr, "\n");
    }
}


double rtclock()
{
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

double t_start, t_end;

int main()
{
    int iterations = 5;

    double min=DBL_MAX, max=DBL_MIN, avg, t_all = 0.0, t_it;

    init_array();
    printf("M=%d, N=%d, K=%d\n", M, N, K);

#ifdef PERFCTR
    PERF_INIT; 
#endif
    for (int it = 0; it < iterations; it++) {
       IF_TIME(t_start = rtclock());

#pragma @ICE block=gemm
	naiveIJK(0, M, 0, N, 0, K, A, B, C);
#pragma @ICE endblock

       IF_TIME(t_end = rtclock());
       IF_TIME(t_it = t_end - t_start);

       IF_TIME(if (t_it > 0 && t_it < min) min = t_it);
       IF_TIME(if (t_it > 0 && t_it > max) max = t_it);
       IF_TIME(t_all += t_it);

       if (it== 0 && fopen(".test", "r")) {
#ifdef MPI
         if (my_rank == 0) {
          print_array();
         }
#else
         print_array();
#endif
       }
    }
    IF_TIME(avg = t_all/iterations);

    IF_TIME(printf("Matrixsize= %d %d %d | Time(ms) min= %7.5lf max= %7.5lf avg= %7.5lf | Iterations %d\n", M, N, K, min*1.0e3, max*1.0e3, avg*1.0e3, iterations));

#ifdef PERFCTR
    PERF_EXIT; 
#endif



  return 0;
}
