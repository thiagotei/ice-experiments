#!/usr/bin/env bash

shapevs="32 64 128 256 512 1024 2048"
#shapevs="128 256"
stopvs="32 64 128 256 512 1024 2048"

for shape in $shapevs; 
do 
    export ICELOCUS_MATSHAPE=${shape} #512 #1024 #
    export ICELOCUS_CXX=icpc
    export ICELOCUS_CC=icc

    sed -e "s/define M 256/define M ${ICELOCUS_MATSHAPE}/g" \
        -e "s/define N 256/define N ${ICELOCUS_MATSHAPE}/g" \
        -e "s/define K 256/define K ${ICELOCUS_MATSHAPE}/g" globals.h > globals.tmp.h;

    n=${ICELOCUS_MATSHAPE}
    echo "Generating orig answer..."
    make CC=icc CXX=icpc USERDEFS="-DM=${n} -DK=${n} -DN=${n}" clean orig
    ./orig
    mv answer-matmul-${n}.txt orig-answer-matmul-${n}.txt

    for stop in $stopvs; 
    do
        [ $shape -lt $stop  ] && continue

        echo Running shape= $shape stop= $stop `date`


        #n=${ICELOCUS_MATSHAPE}
        #if [ ! -f ./orig-answer-matmul-${n}.txt ]; then
        #    echo "Generating orig answer..."
        #    make CC=icc CXX=icpc USERDEFS="-DM=${n} -DK=${n} -DN=${n}" clean orig
        #    ./orig
        #    mv answer-matmul-${n}.txt orig-answer-matmul-${n}.txt
        #fi

        rm -v matmulptr.rec.exe
        make CC=icc CXX=icpc USERDEFS="-DM=${n} -DK=${n} -DN=${n} -DSTOPK=${stop} -DSTOPM=${stop} -DSTOPN=${stop}" clean matmulptr.rec.exe
        ./matmulptr.rec.exe
        diff -q answer-matmul-${n}.txt orig-answer-matmul-${n}.txt
        [ $? -eq 0 ] && echo "Output correct!!! $shape stop= $stop"

        echo "#################"

    done
done 
