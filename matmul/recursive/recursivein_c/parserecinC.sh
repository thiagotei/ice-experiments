#!/usr/bin/env bash

infile=$1
outfile=$2
grep Matrixsize $1 | awk '/Stop/{print $4"-"$8"," $14}' > $outfile
grep Matrixsize $1 | awk '!/Stop/{print $4"-"0"," $10}' >> $outfile
