#ifndef __GLOBALS_H__
#define __GLOBALS_H__


#ifndef M
//#define M 2048
#define M 256
#endif

#ifndef N
//#define N 2048
#define N 256
#endif

#ifndef K
//#define K 2048
#define K 256
#endif

#ifndef STOPM
#define STOPM 256
#endif

#ifndef STOPN
#define STOPN 256
#endif

#ifndef STOPK
#define STOPK 256
#endif

#define alpha 1
#define beta 1

typedef const int ITERC;
typedef int ITER;
typedef double * __restrict__ const MATTYPE;

#define ALIGNFACTOR 64

/*double A[M][K];
double B[K][N];
double C[M][N];
*/

#endif

