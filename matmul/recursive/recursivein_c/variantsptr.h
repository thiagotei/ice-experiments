#ifndef __VARIANTS_H__
#define __VARIANTS_H__

#include <stdlib.h>
#include <stdio.h>
#include "globals.tmp.h"
#define ind(i,j, ncol) (i)*(ncol)+(j)
#define BUFNAMESIZE 32

void naiveIJK(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIJKRegTileJ4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIJKRegTileJ8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJ(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileJ4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileK4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileK8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileK4J4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileK8J4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileI4K4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileI8K8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void recursiveGEMM(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C,
       ITERC stopm, ITERC stopn, ITERC stopp);

MATTYPE allocaTmp(ITERC mS, ITERC mE, ITERC nS, ITERC nE);

void MatrixAdd(ITERC mS, ITERC mE, ITERC nS, ITERC nE, ITERC lenN, MATTYPE C, MATTYPE tmp);
#endif

