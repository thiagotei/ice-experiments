#include "variantsptr.h"

/////////////////////
//
// I-J-K
//
/////////////////////

void naiveIJK(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIJK
for(i = mS; i < mE; i++) {
  for(j = nS; j < nE; j++) {
     for(k = pS; k < pE; k++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
    }
  }
}
#pragma @ICE endloop

//C[i][j] = beta*C[i][j] + alpha*A[i][k] * B[k][j];
}

void naiveIJKRegTileJ4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIJK
for(i = mS; i < mE; i++) {
  for(j = nS; j < nE; j+=4) {
     for(k = pS; k < pE; k++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+3,lenN)];
    }
  }
}
#pragma @ICE endloop

//C[i][j] = beta*C[i][j] + alpha*A[i][k] * B[k][j];
}

void naiveIJKRegTileJ8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIJK
for(i = mS; i < mE; i++) {
  for(j = nS; j < nE; j+=8) {
     for(k = pS; k < pE; k++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+3,lenN)];
	    C[ind(i,j+4,lenN)] = beta*C[ind(i,j+4,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+4,lenN)];
	    C[ind(i,j+5,lenN)] = beta*C[ind(i,j+5,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+5,lenN)];
	    C[ind(i,j+6,lenN)] = beta*C[ind(i,j+6,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+6,lenN)];
	    C[ind(i,j+7,lenN)] = beta*C[ind(i,j+7,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+7,lenN)];
    }
  }
}
#pragma @ICE endloop

//C[i][j] = beta*C[i][j] + alpha*A[i][k] * B[k][j];
}

/////////////////////
//
// I-K-J
//
/////////////////////

void naiveIKJ(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k++) {
    for(j = nS; j < nE; j++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileJ4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k++) {
    for(j = nS; j < nE; j+=4) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+3,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k+=4) {
    for(j = nS; j < nE; j++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k+=8) {
    for(j = nS; j < nE; j++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK4J4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k+=4) {
    for(j = nS; j < nE; j+=4) {
	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k  ,lenK)] * B[ind(k,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k  ,lenK)] * B[ind(k,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k  ,lenK)] * B[ind(k,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k  ,lenK)] * B[ind(k,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+3,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK8J4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k+=8) {
    for(j = nS; j < nE; j+=4) {
	    C[ind(i,j,lenN)]   = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j+3,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileI4K4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i+=4) {
  for(k = pS; k < pE; k+=4) {
    for(j = nS; j < nE; j++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j,lenN)];

	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+3,lenK)] * B[ind(k+3,j,lenN)];

	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+3,lenK)] * B[ind(k+3,j,lenN)];

	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+3,lenK)] * B[ind(k+3,j,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileI8K8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i+=8) {
  for(k = pS; k < pE; k+=8) {
    for(j = nS; j < nE; j++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+7,lenK)] * B[ind(k+7,j,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void recursiveGEMM(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C, 
       ITERC stopm, ITERC stopn, ITERC stopp)
{
    ITERC mlen = mE-mS;
    ITERC nlen = nE-nS;
    ITERC klen = pE-pS;
    //fprintf(stdout, "Opa rec started! %d %d %d stop: %d %d %d\n", mlen, nlen, klen, stopm, stopn, stopp );

    ITER maxnk = nlen;
    if (maxnk < klen) {maxnk = klen;}

    ITER maxmk = mlen;
    if (maxmk < klen) {maxmk = klen;}

    ITER maxmn = mlen;
    if (maxmn < nlen) {maxmn = nlen;}

    if ((mlen >= maxnk) && (mlen > stopm)) {
        //fprintf(stdout, "Breaking M\n");
        ITERC pivh = mS+(mlen/2);
        recursiveGEMM(mS, pivh, nS, nE, pS, pE, lenN, lenK, A, B, C, stopm, stopn, stopp);
        recursiveGEMM(pivh, mE, nS, nE, pS, pE, lenN, lenK, A, B, C, stopm, stopn, stopp);
    } else if ((nlen >= maxmk) && (nlen > stopn)) {
        //fprintf(stdout, "Breaking N\n");
        ITERC pivc = nS+(nlen/2);
        recursiveGEMM(mS, mE, nS, pivc, pS, pE, lenN, lenK, A, B, C, stopm, stopn, stopp);
        recursiveGEMM(mS, mE, pivc, nE, pS, pE, lenN, lenK, A, B, C, stopm, stopn, stopp);
    } else if ((klen >= maxmn ) && (klen > stopp)) {
        //fprintf(stdout, "Breaking K\n");
        ITERC pivw = pS+(klen/2);
        recursiveGEMM(mS, mE, nS, nE, pS, pivw, lenN, lenK, A, B, C, stopm, stopn, stopp);
        recursiveGEMM(mS, mE, nS, nE, pivw, pE, lenN, lenK, A, B, C, stopm, stopn, stopp);
    } else {
        //fprintf(stdout, "Running the leaf\n");
        naiveIKJRegTileI4K4(mS, mE, nS, nE, pS, pE, lenN, lenK, A, B, C);
    }
}

MATTYPE allocaTmp(ITERC mS, ITERC mE, ITERC nS, ITERC nE) {
	MATTYPE tmp = (double *) calloc((mE-mS)*(nE-nS),sizeof(double));
	return tmp;
}

void MatrixAdd(ITERC mS, ITERC mE, ITERC nS, ITERC nE, ITERC lenN, MATTYPE C, MATTYPE tmp)
{
ITER i, j;
for(i = mS; i < mE; i++)
    for(j = nS; j < nE; j++)
	    C[ind(i,j,lenN)] += tmp[ind(i,j,lenN)];
}
