#!/usr/bin/env bash

nruns=1
ntests=200000000
#ntests=3
cfile=matmulptr.cpp
inituplimit=5
initstopaf=5
incstopaf=5
locusfile=matmul-rec-sym.locus #matmul-rec.locus  #
approxvalues="100" #"65 85" # 50 75 100
searchtools="ice-locus-hyperopt.py" #ice-locus-opentuner.py"
#stoplist=256 #512 #"256" #"128 64"
expandedfile="" #"./savedExpandedTrees/aft_expandedrec_75p_2048_128_38be70d40a.locus"
xdebug="" #"--debug"
xparam=""


declare -A mixdsc
mixdsc=(["rand"]="None" ["tpe"]="None" ["anneal"]="None" 
        ["mixA"]=".25:rand:.75:tpe" ["mixB"]=".25:rand:.75:anneal" 
        ["mixC"]=".25:rand:.50:anneal:0.25:tpe" ["mixD"]=".50:rand:.50:anneal" 
        ["mixE"]=".75:rand:.25:anneal")


for approx in $approxvalues; do

    for search in $searchtools; do

        uplimit=$inituplimit
        stopaf=$initstopaf

        for matshape in `seq 512 512 4096`; do

            stopv=$((${matshape} / 8))

            #for stopv in $stoplist; do

            for r in `seq $nruns`; do

                export ICELOCUS_MATSHAPE=$matshape #1024 #512 #
                #export ICELOCUS_CXX=g++ #clang++-9 #icpc #
                #export ICELOCUS_CC=gcc #clang-9 #icc #
                export ICELOCUS_GEMM_STOP=$stopv #32

                echo Searching approx= $approx matshape= $ICELOCUS_MATSHAPE stop= $ICELOCUS_GEMM_STOP $search r= $r $locusfile `date`


                sed -e "s/define M 256/define M ${ICELOCUS_MATSHAPE}/g" \
                    -e "s/define N 256/define N ${ICELOCUS_MATSHAPE}/g" \
                    -e "s/define K 256/define K ${ICELOCUS_MATSHAPE}/g" globals.h > globals.tmp.h;

                n=${ICELOCUS_MATSHAPE}
                if [ ! -f ./orig-answer-matmul-${n}.txt ]; then
                    echo "Generating orig answer..."
                    make clean orig
                    ./orig
                    mv answer-matmul-${n}.txt orig-answer-matmul-${n}.txt
                fi


                if [[ $search == *hyperopt* ]]
                then
                    al="mixC"
                    xparam="--hypalg mix --hypmixdsc ${mixdsc[${al}]}"
                fi

                rm -rvf opentuner.*

                if [[ -z "$expandedfile" ]]; then
                    # if expandedfile var is empty run this
                    echo "Expading file from ${locusfile}"
                    $search --stop-after $stopaf --timeout --upper-limit ${uplimit} \
                        --exprec -t ${locusfile} -f ${cfile} \
                        --tfunc mytiming.py:getTiming \
                        -o suffix -u '.ice' --search \
                        --ntests ${ntests} --approxexprec $approx \
                        --saveexprec $xparam $xdebug
                else
                    echo "Using expanded file ${expandedfile}"
                    $search --stop-after $stopaf --timeout --upper-limit ${uplimit} \
                        -t ${expandedfile} -f ${cfile} \
                        --tfunc mytiming.py:getTiming \
                        -o suffix -u '.ice' --search \
                        --ntests ${ntests}  \
                        $xparam $xdebug
                fi
            done
            uplimit=$((uplimit + uplimit))
            stopaf=$((stopaf + incstopaf))
        done
	done
done
