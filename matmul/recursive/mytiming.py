import re
import statistics as stat

def getTiming(std, error):
    r = re.search('.*Time.*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    if m and len(m) >= 7:
        vals = []
        unit = m[8]
        unit = unit[unit.rfind('(')+1:unit.rfind(')')]
        numit = int(m[6])
        for it in range(9,9+numit):
            vals.append(float(m[it]))
        ##
        metric = stat.median(vals)
        result = {'metric': metric, 
                  # it must be tuple of tuples despite being a single timer
                  'exps': tuple([(v,) for v in vals]), 
                  'unit': (unit,),
                  'desc': ('Total',)}
        #rddesult = float(m[6])
    else:
        result = {'metric': 'N/A'} #'N/A'
    #

    return result
###

def getTimingBkp1(std, error):
    r = re.search('.*Time.*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    if m and len(m) >= 7:
        vals = []
        unit = m[8]
        unit = unit[unit.rfind('(')+1:unit.rfind(')')]
        numit = int(m[6])
        for it in range(9,9+numit):
            vals.append(float(m[it]))
        ##
        metric = stat.median(vals)
        result = {'metric': metric, 'evals': vals, 'unit': unit}
        #rddesult = float(m[6])
    else:
        result = {'metric': 'N/A'} #'N/A'
    #

    return result

