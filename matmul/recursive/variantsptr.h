#ifndef __VARIANTS_H__
#define __VARIANTS_H__

#include <stdlib.h>
#include "globals.tmp.h"
#define ind(i,j, ncol) (i)*(ncol)+(j)
#define BUFNAMESIZE 32

void naiveIJK(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIJK(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc);

void naiveIJKRegTileJ4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIJKRegTileJ8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJ(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJ(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc);

void naiveIKJRegTileJ4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileJ4(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc);

void naiveIKJRegTileK4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileK4(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc);

void naiveIKJRegTileK8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileK8(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc);

void naiveIKJRegTileK4J4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileK4J4(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc);

void naiveIKJRegTileK8J4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileK8J4(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc);

void naiveIKJRegTileI4K4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileI4K4(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc);

void naiveIKJRegTileI8K8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C);

void naiveIKJRegTileI8K8(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc);

MATTYPE allocaTmp(ITERC mS, ITERC mE, ITERC nS, ITERC nE);

void MatrixAddOld(ITERC mS, ITERC mE, ITERC nS, ITERC nE, ITERC lenN, MATTYPE C, MATTYPE tmp);

void MatrixAdd(ITERC mlen, ITERC nlen, MATTYPE C, ITERC LDC, MATTYPE T, ITERC LDT);
#endif

