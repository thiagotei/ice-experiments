matshapestr = getenv("ICELOCUS_MATSHAPE", "1024");
matshape = int(matshapestr);
melem = matshape;
nelem = matshape;
kelem = matshape;

cxx = getenv("ICELOCUS_CXX", "g++"); #"gcc";#"xlc";
cc = getenv("ICELOCUS_CC", "gcc"); #"gcc";#"xlc";
stop = 32; #int(getenv("ICELOCUS_GEMM_STOP", "32"));
mstop = stop;
nstop = stop;
kstop = stop;

buffername="gemmreplacement.txt";
sedchange = "val="+matshapestr+"; sed -e \"s/define M 256/define M ${val}/g\" -e \"s/define N 256/define N ${val}/g\" -e \"s/define K 256/define K ${val}/g\" globals.h > globals.tmp.h;";
tmpcname="tmpC";

inlineV=False;

useomp = getenv("OMP_NUM_THREADS",False);

Search {
    prebuildcmd = sedchange;
    measureorig = False;
    buildcmd = "make CXX="+cxx+" CC="+cc+" USERDEFS='-DM="+matshapestr+" -DN="+matshapestr+" -DK="+matshapestr+"' clean matmulptr.ice.exe";
    runcmd = "./matmulptr.ice.exe";
    buildorig = "make CXX="+cxx+" CC="+cc+" USERDEFS='-DM="+matshapestr+" -DN="+matshapestr+" -DK="+matshapestr+"' clean orig";
    runorig = "./orig";
    checkcmd = "diff -q ./answer-matmul-"+matshapestr+".txt ./orig-answer-matmul-"+matshapestr+".txt";
}

OptSeq recMatmul(mS, mE, nS, nE, kS, kE, outMat, nsuf, ldc, stpnt) {
#print "Mstop="+str(mstop);
  newsuf = nsuf + 1;
  mlen = mE-mS;
  nlen = nE-nS;
  klen = kE-kS;

  maxnk = nlen;
  if(maxnk < klen) {
    maxnk = klen;
  }

  maxmk = mlen;
  if(maxmk < klen) {
    maxmk = klen;
  }

  maxmn = mlen;
  if(maxmn < nlen) {
    maxmn = nlen;
  }

  if (mlen >= maxnk) && (mlen > mstop) {
#print "Split in M";
    pivh = mS+((mE-mS) // 2);
    if ((pivh > mS) && (pivh < mE)) {
      com = "//Split M";
      CallMethod.DCProlog(buffer=buffername, omp=False, comment=com);
      recMatmul(mS,   pivh, nS, nE, kS, kE, outMat, newsuf, ldc, stpnt);
      recMatmul(pivh, mE,   nS, nE, kS, kE, outMat, newsuf, ldc, (pivh-mS)*ldc+stpnt);
      CallMethod.DCEpilog(buffer=buffername, omp=False, comment=com);
    } else {
      RecursionStopped();
    }
  } elif (nlen >= maxmk) && (nlen > nstop) {
#print "Split in N";
    pivc = nS+((nE-nS) // 2);
    if ((pivc > nS) && (pivc < nE)) {
      com = "//Split N";
      CallMethod.DCProlog(buffer=buffername, omp=False, comment=com);
      recMatmul(mS, mE, nS,   pivc, kS, kE, outMat, newsuf, ldc, stpnt);
      recMatmul(mS, mE, pivc, nE,   kS, kE, outMat, newsuf, ldc, (pivc-nS)+stpnt);
      CallMethod.DCEpilog(buffer=buffername, omp=False, comment=com);
    } else {
      RecursionStopped();
    }
  } elif (klen >= maxmn) && (klen > kstop) {
#print "Split in K";
    pivw = kS+((kE-kS) // 2);
    if (pivw > kS) && (pivw < kE) {
      com = "//Split K";
      CallMethod.DCProlog(buffer=buffername, omp=useomp, comment=com);
      name = outMat;
      tmpstpnt = stpnt;
      tmpldc = ldc;
      if useomp != False {
        name = "CTMP"+str(nsuf);#tmpcname+asuf+str(nsuf);
        CallMethod.CreateTmpArray(mS=0, mE=mlen, nS=0, nE=nlen, tmpname=name, buffer=buffername);
        tmpstpnt = 0;
        tmpldc = nlen;
      }
#recMatmul(mS, mE, nS, nE, kS,   pivw, name, newasuf3l, newsuf);
      recMatmul(mS, mE, nS, nE, kS,   pivw, outMat, newsuf, ldc, stpnt);
      #recMatmul(mS, mE, nS, nE, pivw, kE, name, newsuf, nlen, 0);
      recMatmul(mS, mE, nS, nE, pivw, kE, name, newsuf, tmpldc, tmpstpnt);
      #CallMethod.MatrixAddandFree(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, 
      #                          outMat=outMat, omp=useomp, tmpname=name, buffer=buffername);
      #CallMethod.DestroyTmpArray(tmpname=name, buffer=buffername);
      #stpnt = str(mS*ldc+nS);
      if useomp != False {
        CallMethod.MatrixAddandFree(M=mlen, N=nlen, 
                                  outMat="&"+outMat+"["+str(stpnt)+"]", ldc=ldc, 
                                  tmpname=name, ldt=nlen,
                                  buffer=buffername, omp=useomp);
      }
      CallMethod.DCEpilog(buffer=buffername, omp=useomp, comment=com);
    } else {
      RecursionStopped();
    }
  } else {
#    if ((mE-mS == 32) && ((nE-nS == 32) && (kE-kS == 32))) ||
#       (((mE-mS == 64) && ((nE-nS == 64) && (kE-kS == 64))) ||
#       ((mE-mS == 512) && ((nE-nS == 512) && (kE-kS == 512)))) {
#        CallMethod.GemmNaiveIKJRegTile8(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername);
#    } else {
#        CallMethod.GemmNaiveIKJRegTile8(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername);
#    }
#        {
#            CallMethod.GemmNaiveIKJ(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername, inline=inlineV);
#        } OR {
#            CallMethod.GemmNaiveIKJRegTileJ4(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername, inline=inlineV);
#        } OR {
#            CallMethod.GemmNaiveIKJRegTileK4(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername, inline=inlineV);
#        } OR {
#            CallMethod.GemmNaiveIKJRegTileK8(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername, inline=inlineV);
#        } OR {
#            CallMethod.GemmNaiveIKJRegTileK4J4(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername, inline=inlineV);
#        } OR {
#            CallMethod.GemmNaiveIKJRegTileK8J4(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername, inline=inlineV);
#        } OR {
#            CallMethod.GemmNaiveIJK(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername, inline=inlineV);
#        } OR {
#            CallMethod.GemmNaiveIJKRegTileJ4(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername, inline=inlineV);
#        } OR {
#  CallMethod.GemmNaiveIKJRegTileI4K4(mS=mS, mE=mE, nS=nS, nE=nE, 
#      kS=kS, kE=kE, outMat=outMat,
#      buffer=buffername, 
#      inline=inlineV, omp=useomp);
      stpntA = str(mS*kelem+kS);
      stpntB = str(kS*nelem+nS);
  CallMethod.GemmNaiveIKJRegTileI4K4(M=mlen, N=nlen, K=klen,
      matA="&A["+stpntA+"]", lda=kelem, matB="&B["+stpntB+"]", ldb=nelem, 
      ldc=ldc, outMat="&"+outMat+"["+str(stpnt)+"]",
      buffer=buffername, 
      inline=inlineV, omp=useomp);

#        } OR {
#            CallMethod.GemmNaiveIKJRegTileI8K8(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername, inline=inlineV);
#        }
  }
}
CodeReg gemm {
  CallMethod.CreateFile(buffer=buffername);

#maxlvls = log(melem/stop);
# All must have the same stop value.
# this is because of the conditions (e.g., klen >= maxmn).
# it stops the recursion despite the values of the recursion are much lower.
# check paper Cache-Oblivious Algorithms. MATTEO FRIGO, CHARLES E. LEISERSON, et al.
# https://dl.acm.org/doi/pdf/10.1145/2071379.2071383 
#mstop = poweroftwo((melem // 8) .. melem); #enum(2048, 1024, 512, 256, 128, 64);
#mstop = enum((melem // 8), (melem // 4), (melem // 2), melem); #enum(2048, 1024, 512, 256, 128, 64);
  #mstop = enum((melem // 32), (melem // 16), (melem // 8), (melem // 4), (melem // 2)); #enum(2048, 1024, 512, 256, 128, 64);
  mstop = enum((melem // 16), (melem // 8), (melem // 4), (melem // 2)); #enum(2048, 1024, 512, 256, 128, 64);
  nstop = mstop; #poweroftwo(128 .. 2048); #enum(2048, 1024, 512, 256, 128, 64);
  kstop = mstop; #poweroftwo(128 .. 2048); #enum(2048, 1024, 512, 256, 128, 64);

#recMatmulBug(0, melem, 0, nelem, 0, kelem, "C", tmpcnameSuf);

  if useomp != False {
    CallMethod.OMPprolog(buffer=buffername);
  }
  recMatmul(0, melem, 0, nelem, 0, kelem, "C", 1, nelem, 0);
  if useomp != False {
    CallMethod.OMPepilog(buffer=buffername);
  }
  Builtins.Altdesc(source=buffername);
}
