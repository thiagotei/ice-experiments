matshapestr = getenv("ICELOCUS_MATSHAPE", "1024");
matshape = int(matshapestr);
melem = matshape;
nelem = matshape;
kelem = matshape;

cxx = getenv("ICELOCUS_CXX", "g++"); #"gcc";#"xlc";
cc = getenv("ICELOCUS_CC", "gcc"); #"gcc";#"xlc";
#stop = int(getenv("ICELOCUS_GEMM_STOP", "32"));
#mstop = int(stop);
#nstop = stop;
#kstop = stop;

buffername="gemmreplacement.txt";
inlineV=False;

useomp = getenv("OMP_NUM_THREADS",False);

Search {
    prebuildcmd = "";
    measureorig = False;
    buildcmd = "make CXX="+cxx+" CC="+cc+" USERDEFS='-DM="+matshapestr+" -DN="+matshapestr+" -DK="+matshapestr+"' clean matmulptr.ice.exe";
    runcmd = "./matmulptr.ice.exe";
    buildorig = "make CXX="+cxx+" CC="+cc+" USERDEFS='-DM="+matshapestr+" -DN="+matshapestr+" -DK="+matshapestr+"' clean orig";
    runorig = "./orig";
    checkcmd = "diff -q ./answer-matmul-"+matshapestr+".txt ./orig-answer-matmul-"+matshapestr+".txt";
}

OptSeq recMatmul(mS, mE, nS, nE, kS, kE, outMat, order, lvl, numlvls, ldc, stpnt) {
    #print "Mstop="+str(mstop);
    #print "Level= "+str(lvl);
    oldlvl = lvl;
    lvl = lvl + 1;
    mlen = mE-mS;
    nlen = nE-nS;
    klen = kE-kS;

    if oldlvl < numlvls {
        if order[oldlvl] == 0 {
            #print "Split in M";
            pivh = mS+(mlen // 2);
            if ((pivh > mS) && (pivh < mE)) {
                com = "//Split M";
                CallMethod.DCProlog(buffer=buffername, omp=False, comment=com);
                recMatmul(mS,   pivh, nS, nE, kS, kE, outMat, order, lvl, numlvls, ldc, stpnt);
                recMatmul(pivh, mE,   nS, nE, kS, kE, outMat, order, lvl, numlvls, ldc, stpnt);
                CallMethod.DCEpilog(buffer=buffername, omp=False, comment=com);
            } else {
                RecursionStopped();
            }
        } elif order[oldlvl] == 1 {
            #print "Split in N";
            pivc = nS+(nlen // 2);
            if ((pivc > nS) && (pivc < nE)) {
                com = "//Split N";
                CallMethod.DCProlog(buffer=buffername, omp=False, comment=com);
                recMatmul(mS, mE, nS,   pivc, kS, kE, outMat, order, lvl, numlvls, ldc, stpnt);
                recMatmul(mS, mE, pivc, nE,   kS, kE, outMat, order, lvl, numlvls, ldc, stpnt);
                CallMethod.DCEpilog(buffer=buffername, omp=False, comment=com);
            } else {
                RecursionStopped();
            }
        } elif order[oldlvl] == 2 {
            #print "Split in K";
            pivw = kS+(klen // 2);
            if (pivw > kS) && (pivw < kE) {
                com = "//Split K";
                CallMethod.DCProlog(buffer=buffername, omp=useomp, comment=com);
                name = outMat;
                tmpstpnt = stpnt;
                tmpldc = ldc;
                if useomp != False {
                  name = "CTMP"+str(oldlvl);#tmpcname+asuf+str(nsuf);
                  CallMethod.CreateTmpArray(mS=0, mE=mlen, nS=0, nE=nlen, tmpname=name, buffer=buffername);
                  tmpstpnt = 0;
                  tmpldc = nlen;
                }
                recMatmul(mS, mE, nS, nE, kS,   pivw, outMat, order, lvl, numlvls, ldc, stpnt);
                recMatmul(mS, mE, nS, nE, pivw, kE,   outMat, order, lvl, numlvls, ldc, stpnt);
                if useomp != False {
                  CallMethod.MatrixAddandFree(M=mlen, N=nlen, 
                                              outMat="&"+outMat+"["+str(stpnt)+"]", ldc=ldc, 
                                              tmpname=name, ldt=nlen,
                                              buffer=buffername, omp=useomp);
                }
                CallMethod.DCEpilog(buffer=buffername, omp=useomp, comment=com);
            } else {
                RecursionStopped();
            }
        } else {
            RaiseProblem();
        }
    } else {
      #CallMethod.GemmNaiveIKJRegTileI4K4(mS=mS, mE=mE, nS=nS, nE=nE, kS=kS, kE=kE, outMat=outMat, buffer=buffername, inline=inlineV);
      stpntA = str(mS*kelem+kS);
      stpntB = str(kS*nelem+nS);
      CallMethod.GemmNaiveIKJRegTileI4K4(M=mlen, N=nlen, K=klen,
          matA="&A["+stpntA+"]", lda=kelem, matB="&B["+stpntB+"]", ldb=nelem, 
          ldc=ldc, outMat="&"+outMat+"["+str(stpnt)+"]",
          buffer=buffername, 
          inline=inlineV, omp=useomp);
    }
    #print " Out lvl "+str(oldlvl);
    lvl = oldlvl;
}

CodeReg gemm {
    CallMethod.CreateFile(buffer=buffername);
    tltlst = [0];
    order = tltlst;
    #mlst = [0]; #[0,0,0,0];
    #nlst = [1];#[1,1,1,1];
    #klst = [2];#[2,2,2,2];
    #mlen = integer(0 .. 4);
    #klen = integer(0 .. 4);
    #nlen = integer(0 .. 4);
    {
        tltlst = [2,2,2]; #klst+mlst+nlst;
        order = tltlst;
    } OR { 
        tltlst = [0,0,0]; #klst+mlst+nlst;
        order = tltlst;
    } OR {
        tltlst = [1,1,1]; #klst+mlst+nlst;
        order = tltlst;
    } OR {
        tltlst = [0,0,0,1,1,1]; #klst+mlst+nlst;
        order = permutation(tltlst);
    } OR {
        tltlst = [0,0,0,2,2,2]; #klst+mlst+nlst;
        order = permutation(tltlst);
    } OR {
        tltlst = [1,1,1,2,2,2]; #klst+mlst+nlst;
        order = permutation(tltlst);
    } OR {
        tltlst = [0,0,0,1,1,1,2,2,2]; #klst+mlst+nlst;
        order = permutation(tltlst);
    }
    #tltlst = [0,0,1,1,2,2]; #klst+mlst+nlst;
    #print "list: "+str(tltlst);
    numlvls = len(tltlst);
    #print "order: "+str(order);

    if useomp != False {
      CallMethod.OMPprolog(buffer=buffername);
    }
    recMatmul(0, melem, 0, nelem, 0, kelem, "C", order, 0, numlvls, nelem, 0);
    if useomp != False {
      CallMethod.OMPepilog(buffer=buffername);
    }
    Builtins.Altdesc(source=buffername);
}
