#!/usr/bin/env bash

#ntests=20000000000
export ICELOCUS_CXX=icpc #128 #512 #
export ICELOCUS_CC=icc #128 #512 #

ntests=910
cfile=variantsptr.c ##matmulptr.cpp
uplimit=15
stopaf=50
locusfile=matmul-unroll.locus
lowerszp2=5
upperszp2=11
rangepw2=`seq $lowerszp2 $upperszp2`

