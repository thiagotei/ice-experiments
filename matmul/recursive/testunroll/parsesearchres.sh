#!/usr/bin/env bash

#csplit eval-kernelw-202003210030.txt  /Searching/ {*} -f searching -b "_%02d.log" -z

if [ $# -eq 0 ]; then
	echo "Required to pass 1 file. ./$0 <log file from execution>"
	exit 1
fi

greppedfile=`mktemp`
echo "Grepping into file $greppedfile"
grep -e "Searching" -e "Variant" $1 > $greppedfile

rm -v searching*.log

echo "Spliting file $greppedfile"
csplit $greppedfile /Searching/ {*} -f searching -b "_%02d.log" -z


#for f in "$@"
for f in searching*.log
do
echo Proccesing $f...
awk 'BEGIN{
        max=0;
        maxline=0;
        minline=0;
        min=100000000
    }
    NR==1 {print $0}
    NF>15 {
        if(max < $21){ max = $21; maxline=$0};
        if(min > $21){ min = $21; minline=$0}
    }
    END{
        print "min=" min " " minline  "\nmax=" max " " maxline
        print "speedup=" max/min
    }' $f
echo "Done"
echo ""
done
