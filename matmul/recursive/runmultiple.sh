#!/usr/bin/env bash


nruns=`seq 3`
mach="sadr"
prob="matmul"
xinfo="none"
complst="clang++-9,clang-9  icpc,icc g++-7,gcc-7"
#complst="clang++-9,clang-9  icpc,icc "
strat="rchoice-rec-sym" #"rchoice-rec" #"rchoice-vert-rec" #"cacheobliv" #"rchoice-vert-vec" #
runscript="./howtorun-recursion.sh" #"./howtorun-symmetrical.sh" #
for n in $nruns; do
for comps in $complst; do 
    echo $comps
    cxxcomp=${comps%,*}
    cccomp=${comps#*,}
    export ICELOCUS_CXX=${cxxcomp} #g++ #clang++-8  #icpc #
    export ICELOCUS_CC=${cccomp}  # gcc #clang-8 #icc 
    datev=`date +%Y%m%d%H%M%S`
    echo Running $n $ICELOCUS_CXX $ICELOCUS_CC $datev
    ##./runsearch-symmetrical.sh &> thunder_icc18_trcheobliv_regtile4_`date +%Y%m%d%H%M`.txt
    $runscript &> ${mach}_${cccomp}_${prob}_${strat}_${xinfo}_${datev}.txt
done
done
