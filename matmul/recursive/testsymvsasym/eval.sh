#!/usr/bin/env bash

sufs="sym.stop512 sym.stop256 asym.vsA asym.vsB"
shape="2048"

touch .test

for i in $sufs; do
    bin=matmulptr.${i}
    rm -rf $bin.{exe,o}
    make CC=icc CXX=icpc clean ${bin}.exe
    [ $? -ne 0 ] && echo "Compiler error " && continue
    ./${bin}.exe
    diff answer-matmul-${shape}.txt ../testdir/orig-answer-matmul-${shape}.txt 
    [ $? -ne 0 ] && echo "Problem in checking resu.ts" && continue
done
