
CC ?= icc


ifeq ($(CC), icc)
OPTFLAGS := -O3 -xHost -ansi-alias #-ipo #-fp-model precise
OMPFLAGS := -qopenmp
else
  ifeq ($(CC), xlc)
    OPTFLAGS := -O3 -qhot
    OMPFLAGS := #-qsmp
  else
    #@echo 'Blah'#'$(CC)'
    ifeq ($(findstring clang, $(CC)), clang)
      OPTFLAGS := -O3 -fvectorize
      OMPFLAGS := -fopenmp
    else
      #gcc
      OPTFLAGS := -O3 -mtune=native -ftree-vectorize # -march=native
      OMPFLAGS := -fopenmp
    endif
endif
endif

INC += -I.
CFLAGS += -Wall
LDFLAGS += -lm

