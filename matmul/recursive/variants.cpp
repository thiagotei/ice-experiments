#include "variants.h"

void naiveIJK(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   double A[M][K], double B[K][N], double C[M][N]){

ITER i, j, k;

for(i = mS; i < mE; i++)
  for(j = nS; j < nE; j++)
     for(k = pS; k < pE; k++)
	    C[i][j] = beta*C[i][j] + alpha*A[i][k] * B[k][j];

}

void naiveIKJ(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   double A[M][K], double B[K][N], double C[M][N]){

ITER i, j, k;

for(i = mS; i < mE; i++)
  for(k = pS; k < pE; k++)
    for(j = nS; j < nE; j++)
	    C[i][j] = beta*C[i][j] + alpha*A[i][k] * B[k][j];

}
