#!/usr/bin/env bash

#grep "Matrixsize"  $1| sort -k2,2n  -k8,8n | uniq -w 20

# loop over input files

echo leg,comp,searchtool,run,matshape,min,max,avg
for fileinp in "$@"; do
    if [ ! -f $fileinp ]; then
        echo "$fileinp does not exisit!" >&2
        continue
    fi
    echo "Processing $fileinp ..." >&2
    comp=`awk -F_ '{print $2}' <<< $fileinp`
    label=`awk -F_ '{print $4}' <<< $fileinp`

    # split the file by searching
    csplit $fileinp /Searching/ {*} -f $fileinp -b "_%02d.log" -z -s

    echo "#"$fileinp
    for inp  in ${fileinp}_*.log; do
        #grep "Searching " $inp | awk -v leg=$label '{print leg}' | tr '\n' ',' #| awk -v leg=$label '{print leg","$4","$6}' | tr '\n' ','
        grep "Searching " $inp | awk -v leg=$label -v cc=$comp '{printf("%s,%s,",leg,cc)} NF<=16{print $6","$8} NF>=17{print $8","$10}' | tr '\n' ','
        grep "Matrixsize" $inp | sort -k2,2n -k8,8n | uniq -w 20 | awk '{print $2","$8","$10","$12}'
        rm -v $inp >&2
    done
done
