#!/usr/bin/env bash

locusfile=matmul-rec.locus
cfile=matmulptr.cpp
nruns="30"
approxvalues="60 75 80 90" #"60 80 100"
stopvalues="64"
shapevalues="2048"
search="ice-locus-hyperopt.py"
xparam=""

for approx in $approxvalues; do
    for stopv in $stopvalues; do
        for shape in $shapevalues; do
            for r in `seq $nruns`; do

                export ICELOCUS_MATSHAPE=$shape
                export ICELOCUS_GEMM_STOP=$stopv
                export ICELOCUS_CXX=icpc
                export ICELOCUS_CC=icc

                echo Generating expanded tress to see distribution approx= $approx matshape= $shape r= $r `date`

                $search --convonly --search --exprec --approxexprec $approx \
                    -t ${locusfile} -f ${cfile} --saveexprec \
                    $xparam

            done
        done
    done
done
