#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>

extern double mysecond();

#define BUFNAMESIZE 32
#define TSCALE (1.0e3)
#define TLEG "ms"
#define REAL 0
#define IMAG 1


void print_array(int n, fftw_complex *out)
{
    char bufname[32];
    snprintf(bufname, BUFNAMESIZE, "answer-fftw-%d.txt", n);
    FILE *fout = fopen(bufname,"w");
    if(!fout){
        fprintf(stderr,"ERROR! Could not open %s to output results!\n", bufname);
        exit(1);
    }
    for(int i=0; i<n; i++) {
        fprintf(fout, "%lf %lf ", out[i][REAL], out[i][IMAG]);
    }
    fprintf(fout, "\n");
    fclose(fout);
}

int main(int argc, char **argv) {

    double min=DBL_MAX, max=DBL_MIN, avg=0.0, t_all = 0.0, t_it;
    double t_start, t_end;
    int n = 32, flagidx = 0, iterations = 5;
    if(argc > 1) {
        n = atoi(argv[1]);
    }
    if(argc > 2) {
        flagidx = atoi(argv[2]);
    }

    unsigned int flagarr[] = {FFTW_ESTIMATE, FFTW_MEASURE, FFTW_PATIENT, FFTW_EXHAUSTIVE};
    char *pldesc[] = {"ESTIMATE", "MEASURE", "PATIENCE", "EXHAUST"};
    printf("fftw n %d plan flag %s\n", n, pldesc[flagidx]);

    fftw_complex *in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);
    fftw_complex *out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);

    printf("FFTW_%s...\n", pldesc[flagidx]);

    t_start = mysecond();
    fftw_plan plan = fftw_plan_dft_1d(n, in, out, FFTW_FORWARD, flagarr[flagidx]);
    t_end = mysecond();

    if(plan) fftw_print_plan(plan);

    double t_plan = (t_end-t_start)*TSCALE;
    printf("\nFFTW_%s plan %7.5lf (%s)\n", pldesc[flagidx], t_plan, TLEG);

    for(int i = 0; i < n; ++i) {
        in[i][REAL] = 1.0;
        in[i][IMAG] = 0.0;
    }

    if(plan) {
        for (int it = 0; it < iterations; it++) {
            t_start = mysecond();
            fftw_execute(plan); /* repeat as needed */
            t_end = mysecond();
            //double t_exec = (t_end-t_start)*TSCALE;
            //printf("FFTW_%s exec %7.5lf (%s)\n", plandesc, t_exec, TLEG);

            t_it = t_end - t_start;
            if (t_it > 0 && t_it < min) min = t_it;
            if (t_it > 0 && t_it > max) max = t_it;
            t_all += t_it;

            //printf("Res: p %f\n",out[0][REAL]);
            if (it==0 && fopen(".test", "r")) {
                print_array(n, out);
            }
        }
        avg = t_all/iterations;
    }

    printf("FFTW n: %d FFTW_%s plan: %7.5lf "
           "exec: min= %7.5lf max= %7.5lf avg= %7.5lf (%s) | iteration %d\n",
      n, pldesc[flagidx], t_plan, min*TSCALE, max*TSCALE, avg*TSCALE, TLEG, iterations);

    fftw_destroy_plan(plan);
    fftw_free(in); fftw_free(out);
}
