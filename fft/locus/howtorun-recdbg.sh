#!/usr/bin/env bash

#matshape=2048
#matshape=256
#ntests=20000000000
ntests=5
cfile=fftwbase.cpp
uplimit=75
stopaf=480
locusfile=optspace0-dbg.locus #fftw-vs1.locus #fft-test.locus #
#locusfile=optspace-comb-8.locus
#--convonly --debug
#ice-locus-hyperopt.py --convonly --stop-after ${stopaf} --timeout --upper-limit ${uplimit} --exprec -t ${locusfile} -f ${cfile} --tfunc mytiming.py:getTiming -o suffix -u '.ice' --search --ntests ${ntests}
ice-locus-hyperopt.py   --timeout --upper-limit ${uplimit} --exprec --debug -t ${locusfile} -f ${cfile} --tfunc mytiming.py:getTiming -o suffix -u '.ice' #--search --ntests ${ntests}
#ice-locus-hyperopt.py   --timeout --upper-limit ${uplimit} --exprec --debug -t ${locusfile} -f ${cfile} --tfunc mytiming.py:getTiming -o suffix -u '.ice' #--search --ntests ${ntests}

#ice-locus-hyperopt.py -t fft-test.locus -f fftwbase.cpp -o suffix -u ".ice" --convonly --search --exprec --debug
