#!/usr/bin/env bash

min=21 #6
max=23 #20

fileres=baseline_fftw_thunder_2.log
rm -f $fileres
for i in `seq $min $max`;
do
    p=`echo "2^$i" | bc`
    for j in `seq 0 3`;
    do
        echo "["`date`"] Running power:" $i "size:" $p  "method:" $j;
        ./fftwbase.exe $p $j 2> /dev/null 1>> $fileres
    done
done
