#!/usr/bin/env bash

#export ICELOCUS_FFT_LEN=65536 #1048576 #524288  #1024  #262144 #131072 #32768 #256 #
#ntests=20000000000
ntests=500
cfile=fftwbase.cpp
#         
uplimit=([0]=1 [1]=1 [2]=1 [3]=1 [4]=1 [5]=1 [6]=1 [7]=1 [8]=1 [9]=2 [10]=3 [11]=4 [12]=4 [13]=5 [14]=5 )
#stopaf=([0]=1 [1]=1 [2]=1 [3]=1 [4]=1 [5]=1 [6]=1 [7]=1 [8]=1 [9]=2 [10]=3 [11]=4 [12]=8 [13]=15 [14]=35 )
stopaf=([0]=5 [1]=5 [2]=5 [3]=5 [4]=5 [5]=5 [6]=5 [7]=5 [8]=5 [9]=10 [10]=10 [11]=15 [12]=25 [13]=35 [14]=60 )
#stopaf=180
locusfile=fftw-vs6.locus #test2.locus #fftw-vs1.locus #fft-test.locus #
lenExpon=(`seq 6 22`)
#lenExpon=([0]=6)
search=ice-locus-hyperopt.py
run=1

