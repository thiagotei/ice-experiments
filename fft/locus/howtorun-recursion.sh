#!/usr/bin/env bash

. ./runparams.sh

#--convonly --debug
#ice-locus-hyperopt.py --convonly --stop-after ${stopaf} --timeout --upper-limit ${uplimit} --exprec -t ${locusfile} -f ${cfile} --tfunc mytiming.py:getTiming -o suffix -u '.ice' --search --ntests ${ntests}
ice-locus-hyperopt.py   --timeout --upper-limit ${uplimit} --exprec -t ${locusfile} -f ${cfile} --tfunc mytiming.py:getTiming -o suffix -u '.ice' --search --ntests ${ntests} --stop-after ${stopaf} --saveexprec #--debug
#ice-locus-hyperopt.py   --timeout --upper-limit ${uplimit} --exprec --debug -t ${locusfile} -f ${cfile} --tfunc mytiming.py:getTiming -o suffix -u '.ice' #--search --ntests ${ntests}

#ice-locus-hyperopt.py -t fft-test.locus -f fftwbase.cpp -o suffix -u ".ice" --convonly --search --exprec --debug
