#!/usr/bin/env bash


. ./runparams.sh

#locusfile=test2.locus  #fftw-vs4.locus
ice-locus-hyperopt.py --exprec \
	-t $locusfile -f ${cfile} --tfunc mytiming.py:getTiming \
	-o suffix -u '.ice' --search \
    --ntests ${ntests} --stop-after ${stopaf} \
	--saveexprec --convonly --approxexprec 100 \
    
#--convonly --exprec \
