#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <kernel/ifftw.h>
#include <api/api.h>
#include <dft/ct.h>

extern double mysecond();

#define TSCALE (1.0e3)
#define TLEG "ms"
#define REAL 0
#define IMAG 1
#define MAXNAM 64
#define min(X, Y) (((X) < (Y)) ? (X) : (Y))
#define N0(nembed)((nembed) ? (nembed) : &n)

typedef fftw_plan (*fnpln)(int, fftw_complex*, fftw_complex*, int, unsigned);

int main(int argc, char **argv) {

    int n = 32;
    int maxpat = 100000;
    if(argc > 1) {
        n = atoi(argv[1]);
    }
    if (argc > 2) {
        maxpat = atoi(argv[2]);// max patience, the number of plans to execute
    }
    printf("fftw n %d maxpat %d\n", n, maxpat);

    double t_start, t_end;
    fftw_complex *in, *out;
    fftw_plan theplan;
    //unsigned int flags[] = {FFTW_ESTIMATE, FFTW_ESTIMATE, FFTW_MEASURE, FFTW_PATIENT, FFTW_EXHAUSTIVE};

    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);

    int sign = FFTW_FORWARD;
    unsigned flags = FFTW_ESTIMATE;
    int howmany = 1, rank = 1, istride = 1, idist = 1, ostride = 1, odist = 1;
    const int *inembed = 0;
    const int *onembed = 0;

     R *ri, *ii, *ro, *io;
     if (!fftw_many_kosherp(rank, &n, howmany)){printf("Eita something weird!\n"); return 0;}

     EXTRACT_REIM(FFTW_FORWARD, in, &ri, &ii);
     EXTRACT_REIM(FFTW_FORWARD, out, &ro, &io);

     tensor *sz = fftw_mktensor_rowmajor(rank, &n, N0(inembed), N0(onembed), 2 * istride, 2 * ostride);
     tensor *vecsz = fftw_mktensor_1d(howmany, 2 * idist, 2 * odist);
     problem * pbl = fftw_mkproblem_dft_d(sz, vecsz,
                                        TAINT_UNALIGNED(ri, flags),
                                        TAINT_UNALIGNED(ii, flags),
                                        TAINT_UNALIGNED(ro, flags),
                                        TAINT_UNALIGNED(io, flags));

     //plan = fftw_mkapiplan_nosearch_generic(sign, flags, pbl);

     planner *ego = fftw_mkapiplan_locus_prol(flags, pbl);
     plan *pln1 = 0;

     solver *sW1 = fftw_findSolver(ego, "fftw_codelet_t3fv_32_avx", 0);
     //solver *sW1 = fftw_findSolver(ego, "fftw_codelet_t1fv_8_avx", 0);
     if(!sW1) {printf("Solver sW1 not found!\n");exit(1);}
     const ct_solver *sWct1 = (const ct_solver *) sW1;

     ctditinfo *ctinf1 = fftw_mkplan_ctdit_prol(sW1, pbl, ego);

     if(ctinf1) {
         plan *cldw = sWct1->mkcldw(sWct1,
                 ctinf1->r, ctinf1->m * ctinf1->d[0].os, ctinf1->m * ctinf1->d[0].os,
                 ctinf1->m, ctinf1->d[0].os,
                 ctinf1->v, ctinf1->ovs, ctinf1->ovs,
                 0, ctinf1->m,
                 ctinf1->p->ro, ctinf1->p->io, ego);
         if(!cldw) {printf("No cldw plan!\n");}

         //// VTrank
         plan *planv1 = 0;
         solver * sv1 = fftw_findSolver(ego, "fftw_dft_vrank_geq1_register", 0);
         if(!sv1) {printf("Solver sv1 not found!\n");exit(1);}

         vrankinfo *vrinf1 = fftw_mkplan_vrankgeq1_prol(sv1, ctinf1->cld_prb, ego);
         if(vrinf1) {

             //// CT
             //solver *sW1_1 = fftw_findSolver(ego, "fftw_codelet_t3fv_16_avx", 0);
             //solver *sW1_1 = fftw_findSolver(ego, "fftw_codelet_t1fv_16_avx", 0);
             solver *sW1_1 = fftw_findSolver(ego, "fftw_codelet_t3fv_32_avx", 0);
             if(!sW1_1) {printf("Solver sW1_1 not found!\n");exit(1);}
             //const ct_solver *sWct1_1 = (const ct_solver *) sW1_1;

             ctditinfo *ctinf1_1 = fftw_mkplan_ctdit_prol(sW1_1, vrinf1->cld_prb, ego);
             plan *pln1_1 = 0;

             if(ctinf1_1) {
                 //plan *cldw1_1 = sWct1_1->mkcldw(sWct1_1,
                 plan *cldw1_1 = ((const ct_solver *)sW1_1)->mkcldw((const ct_solver*)sW1_1,
                         ctinf1_1->r, ctinf1_1->m * ctinf1_1->d[0].os, ctinf1_1->m * ctinf1_1->d[0].os,
                         ctinf1_1->m, ctinf1_1->d[0].os,
                         ctinf1_1->v, ctinf1_1->ovs, ctinf1_1->ovs,
                         0, ctinf1_1->m,
                         ctinf1_1->p->ro, ctinf1_1->p->io, ego);
                 if(!cldw1_1) {printf("No cldw1_1 plan!\n");}

                 solver * s1 = fftw_findSolver(ego, "fftw_codelet_n1fv_64_avx", 0);
                 if(!s1) {printf("Solver s1 not found!\n");exit(1);}

                 plan *cld1_1 = s1->adt->mkplan(s1, ctinf1_1->cld_prb, ego);
                 if(!cld1_1) {printf("No cld1_1 plan!\n");}

                 pln1_1 = fftw_mkplan_ctdit_epil(sW1_1, cldw1_1, cld1_1, ctinf1_1);
                 if(!pln1_1) {
                     fprintf(stdout, "[test7] CT Plan 1_1 is null!!\n");
                 } else {
                     fprintf(stdout, "[test7] CT Plan 1_1 worked!!\n");
                 }

                 fftw_destroy_ctdit_info(ctinf1_1);

             } else {
                 fprintf(stdout, "Ctinf1_1 not applicable.\n");
             }
             //////////

             planv1 = fftw_mkplan_vrankgeq1_epil(sv1, pln1_1, vrinf1);
             fftw_destroy_vrank_info(vrinf1);
         } else {
             fprintf(stdout, "Vting not applicable.\n");
         }
         //////////

         pln1 = fftw_mkplan_ctdit_epil(sW1, cldw, planv1, ctinf1);
         if(!pln1) {
              fprintf(stdout, "[test7] CT Plan is null!!\n");
         } else {
              fprintf(stdout, "[test7] CT Plan worked!!\n");
         }

         fftw_destroy_ctdit_info(ctinf1);
     } else {
        fprintf(stdout, "Ctinf not applicable.\n");
     }

     theplan = fftw_mkapiplan_locus_epil(sign, ego, pln1, pbl);

     if(theplan){fftw_print_plan(theplan);printf("\n");}

     for(int i = 0; i < n; ++i) {
        in[i][REAL] = 1.0;
        in[i][IMAG] = 0.0;
     }

     if(theplan) {
        t_start = mysecond();
        fftw_execute(theplan);
        t_end = mysecond();
        double t_exec = (t_end-t_start)*TSCALE;

        printf("Res: p %f %s\n",out[0][REAL], out[0][REAL] == n ? "CORRECT" : "WRONG");
        printf("FFTW_%s exec %7.5lf (%s)\n", "GENERIC", t_exec, TLEG);
     }

     //fftw_cleanup();
     fftw_destroy_plan(theplan);
     fftw_free(in); fftw_free(out);
     return 0;
}
