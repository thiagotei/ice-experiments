#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>

extern double mysecond();

#define TSCALE (1.0e3)
#define TLEG "ms"
#define REAL 0
#define IMAG 1
#define min(X, Y) (((X) < (Y)) ? (X) : (Y))

typedef fftw_plan (*fnpln)(int, fftw_complex*, fftw_complex*, int, unsigned);

int main(int argc, char **argv) {

    int n = 32;
    int maxpat = 100000;
    if(argc > 1) {
        n = atoi(argv[1]);
    }
    if (argc > 2) {
        maxpat = atoi(argv[2]);// max patience, the number of plans to execute
    }
    printf("fftw n %d\n", n);

    double t_start, t_end;
    fftw_complex *in, *out;
    fftw_plan plan;
    unsigned int flags[] = {FFTW_ESTIMATE, FFTW_ESTIMATE, FFTW_MEASURE, FFTW_PATIENT, FFTW_EXHAUSTIVE};
    char *pldesc[] = {"GENERIC", "ESTIMATE", "MEASURE", "PATIENCE", "EXHAUST"};
    fnpln funcs[] = {fftw_plan_dft_1d_nosearch_generic, fftw_plan_dft_1d, fftw_plan_dft_1d, fftw_plan_dft_1d, fftw_plan_dft_1d};

    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);

    size_t numplans = min(sizeof(flags)/sizeof(flags[0]), maxpat);
    for(size_t i = 0; i < numplans; ++i) {
        printf("FFTW_%s: fl %d\n",pldesc[i], flags[i]);
        t_start = mysecond();
        //plan = fftw_plan_dft_1d(n, in, out, FFTW_FORWARD, flags[i]);
        plan = funcs[i](n, in, out, FFTW_FORWARD, flags[i]);
        t_end = mysecond();

        double t_plan = (t_end-t_start)*TSCALE;
        if(plan) fftw_print_plan(plan);
        printf("\nFFTW_%s plan %7.5lf (%s)\n", pldesc[i], t_plan, TLEG);

        for(int i = 0; i < n; ++i) {
            in[i][REAL] = 1.0;
            in[i][IMAG] = 0.0;
        }

        if(plan) {
            t_start = mysecond();
            fftw_execute(plan);
            t_end = mysecond();
            double t_exec = (t_end-t_start)*TSCALE;

            printf("Res: p %f\n",out[0][REAL]);
            printf("FFTW_%s exec %7.5lf (%s)\n", pldesc[i], t_exec, TLEG);
        } else {
            printf("FFTW_%s exec None\n", pldesc[i]);
            //printf("p_generic plan is null\n");
        }

        fftw_cleanup();
        fftw_destroy_plan(plan);
    }

    fftw_free(in); fftw_free(out);
    return 0;

/*    printf("FFTW aggregated n: %d plan: FFTW_ESTIMATE %7.5lf "
                                 "FFTW_MEASURE %7.5lf "
                                 "FFTW_PATIENT %7.5lf "
                                 "FFTW_EXHAUSTIVE %7.5lf | "
                           "exec: FFTW_ESTIMATE %7.5lf "
                                 "FFTW_MEASURE %7.5lf "
                                 "FFTW_PATIENT %7.5lf "
                                 "FFTW_EXHAUSTIVE %7.5lf | (%s)\n",
      n, t_plan_estimate, t_plan_measure, t_plan_patient, t_plan_exhaust,
         t_exec_estimate, t_exec_measure, t_exec_patient, t_exec_exhaust, TLEG);
*/
}
