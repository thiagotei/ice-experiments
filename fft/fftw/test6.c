#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <kernel/ifftw.h>
#include <api/api.h>
#include <dft/ct.h>

extern double mysecond();

#define TSCALE (1.0e3)
#define TLEG "ms"
#define REAL 0
#define IMAG 1
#define MAXNAM 64
#define min(X, Y) (((X) < (Y)) ? (X) : (Y))
#define N0(nembed)((nembed) ? (nembed) : &n)

typedef fftw_plan (*fnpln)(int, fftw_complex*, fftw_complex*, int, unsigned);

int main(int argc, char **argv) {

    int n = 32;
    int maxpat = 100000;
    if(argc > 1) {
        n = atoi(argv[1]);
    }
    if (argc > 2) {
        maxpat = atoi(argv[2]);// max patience, the number of plans to execute
    }
    printf("fftw n %d maxpat %d\n", n, maxpat);

    double t_start, t_end;
    fftw_complex *in, *out;
    fftw_plan theplan;
    //unsigned int flags[] = {FFTW_ESTIMATE, FFTW_ESTIMATE, FFTW_MEASURE, FFTW_PATIENT, FFTW_EXHAUSTIVE};

    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);

    int sign = FFTW_FORWARD;
    unsigned flags = FFTW_ESTIMATE;
    int howmany = 1, rank = 1, istride = 1, idist = 1, ostride = 1, odist = 1;
    const int *inembed = 0;
    const int *onembed = 0;
    ctditinfo * ctinf =  0;

     R *ri, *ii, *ro, *io;
     if (!fftw_many_kosherp(rank, &n, howmany)){printf("Eita something weird!\n"); return 0;}

     EXTRACT_REIM(FFTW_FORWARD, in, &ri, &ii);
     EXTRACT_REIM(FFTW_FORWARD, out, &ro, &io);

     tensor *sz = fftw_mktensor_rowmajor(rank, &n, N0(inembed), N0(onembed), 2 * istride, 2 * ostride);
     tensor *vecsz = fftw_mktensor_1d(howmany, 2 * idist, 2 * odist);
     problem * pbl = fftw_mkproblem_dft_d(sz, vecsz,
                                        TAINT_UNALIGNED(ri, flags),
                                        TAINT_UNALIGNED(ii, flags),
                                        TAINT_UNALIGNED(ro, flags),
                                        TAINT_UNALIGNED(io, flags));

     //plan = fftw_mkapiplan_nosearch_generic(sign, flags, pbl);

     planner *ego = fftw_mkapiplan_locus_prol(flags, pbl);
     plan *pln1 = 0;

     char bufW[MAXNAM + 1] = "fftw_codelet_t3fv_16_avx";
     solver *sW = fftw_findSolver(ego, bufW, 0);
     const ct_solver *sWct = (const ct_solver *) sW;

     ctinf = fftw_mkplan_ctdit_prol(sW, pbl, ego);

     if(ctinf) {
         //plan *cldw = 0;
         plan *cldw = sWct->mkcldw(sWct,
                 ctinf->r, ctinf->m * ctinf->d[0].os, ctinf->m * ctinf->d[0].os,
                 ctinf->m, ctinf->d[0].os,
                 ctinf->v, ctinf->ovs, ctinf->ovs,
                 0, ctinf->m,
                 ctinf->p->ro, ctinf->p->io, ego);
         if(!cldw) {printf("No cldw plan!\n");}

         /*problem * prb2 = fftw_mkproblem_dft_d(
                                fftw_mktensor_1d(ctinf->m, ctinf->r * ctinf->d[0].is, ctinf->d[0].os),
                                fftw_mktensor_2d(ctinf->r, ctinf->d[0].is, ctinf->m * ctinf->d[0].os,
                                                 ctinf->v, ctinf->ivs, ctinf->ovs),
                                ctinf->p->ri, ctinf->p->ii, ctinf->p->ro, ctinf->p->io);
         */
         char buf[MAXNAM + 1] = "fftw_codelet_n2fv_16_avx";
         solver * s = fftw_findSolver(ego, buf, 0);

         //plan *cld = 0; 
         //plan *cld = s->adt->mkplan(s, prb2, ego);
         plan *cld = s->adt->mkplan(s, ctinf->cld_prb, ego);
         if(!cld) {printf("No cld plan!\n");}

         pln1 = fftw_mkplan_ctdit_epil(sW, cldw, cld, ctinf);
         if(!pln1) {
              fprintf(stdout, "[test6] Plan is null!!\n");
         } else {
              fprintf(stdout, "[test6] Plan worked!!\n");
         }

         fftw_destroy_ctdit_info(ctinf);
     } else {
        fprintf(stdout, "Ctinf not applicable.\n");
     }

     theplan = fftw_mkapiplan_locus_epil(sign, ego, pln1, pbl);

     if(theplan){fftw_print_plan(theplan);printf("\n");}

     for(int i = 0; i < n; ++i) {
        in[i][REAL] = 1.0;
        in[i][IMAG] = 0.0;
     }

     if(theplan) {
        t_start = mysecond();
        fftw_execute(theplan);
        t_end = mysecond();
        double t_exec = (t_end-t_start)*TSCALE;

        printf("Res: p %f\n",out[0][REAL]);
        printf("FFTW_%s exec %7.5lf (%s)\n", "GENERIC", t_exec, TLEG);
     }

     //fftw_cleanup();
     fftw_destroy_plan(theplan);
     fftw_free(in); fftw_free(out);
     return 0;
}
