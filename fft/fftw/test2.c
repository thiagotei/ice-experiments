#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>

extern double mysecond();

#define TSCALE (1.0e3)
#define TLEG "ms"
#define REAL 0
#define IMAG 1

int main(int argc, char **argv) {

    int n = 32;
    if(argc > 1) {
        n = atoi(argv[1]);
    }
    printf("fftw n %d\n", n);

    double t_start, t_end;
    fftw_complex *in, *out;
    fftw_plan p_generic=NULL, p_estimate=NULL, p_patience=NULL, p_measure=NULL, p_exhaust=NULL;

    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);

    printf("FFTW_GENERICPLAN:\n");
    t_start = mysecond();
    p_generic = fftw_plan_dft_1d_nosearch_generic(n, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    t_end = mysecond();
    if(p_generic) fftw_print_plan(p_generic);
    double t_plan_generic = (t_end-t_start)*TSCALE;
    printf("\nFFTW_GENERICPLAN plan %7.5lf (%s)\n",t_plan_generic, TLEG);

    for(int i = 0; i < n; ++i) {
        in[i][REAL] = 1.0;
        in[i][IMAG] = 0.0;
    }

    t_start = mysecond();
    if(p_generic) {printf("plan is NOT null\n");fftw_execute(p_generic);} /* repeat as needed */
    else printf("p_generic plan is null\n");
    t_end = mysecond();
    double t_exec_generic = (t_end-t_start)*TSCALE;
    printf("FFTW_GENERICPLAN exec %7.5lf (%s)\n", t_exec_generic, TLEG);

    printf("Res: p %f\n",out[0][REAL]);

    fftw_cleanup();

    printf("FFTW_ESTIMATE:\n");
    t_start = mysecond();
    p_estimate = fftw_plan_dft_1d(n, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    t_end = mysecond();
    if(p_estimate) fftw_print_plan(p_estimate);
    double t_plan_estimate = (t_end-t_start)*TSCALE;
    printf("\nFFTW_ESTIMATE plan %7.5lf (%s)\n",t_plan_estimate, TLEG);

    printf("FFTW_MEASURE:\n");
    t_start = mysecond();
    //p_measure = fftw_plan_dft_1d(n, in, out, FFTW_FORWARD, FFTW_MEASURE);
    t_end = mysecond();
    if(p_measure) fftw_print_plan(p_measure);
    double t_plan_measure = (t_end-t_start)*TSCALE;
    printf("\nFFTW_MEASURE plan %7.5lf (%s)\n",t_plan_measure, TLEG);

    printf("FFTW_PATIENT:\n");
    t_start = mysecond();
    //p_patience = fftw_plan_dft_1d(n, in, out, FFTW_FORWARD, FFTW_PATIENT);
    t_end = mysecond();
    if(p_patience) fftw_print_plan(p_patience);
    double t_plan_patient = (t_end-t_start)*TSCALE;
    printf("\nFFTW_PATIENT plan %7.5lf (%s)\n",t_plan_patient, TLEG);

    printf("FFTW_EXHAUSTIVE:\n");
    t_start = mysecond();
    //p_exhaust = fftw_plan_dft_1d(n, in, out, FFTW_FORWARD, FFTW_EXHAUSTIVE);
    t_end = mysecond();
    if(p_exhaust) fftw_print_plan(p_exhaust);
    double t_plan_exhaust = (t_end-t_start)*TSCALE;
    printf("\nFFTW_EXHAUSTIVE plan %7.5lf (%s)\n", t_plan_exhaust, TLEG);

    for(int i = 0; i < n; ++i) {
        in[i][REAL] = 1.0;
        in[i][IMAG] = 0.0;
    }

    t_start = mysecond();
    if(p_estimate) fftw_execute(p_estimate); /* repeat as needed */
    t_end = mysecond();
    double t_exec_estimate = (t_end-t_start)*TSCALE;
    printf("FFTW_ESTIMATE exec %7.5lf (%s)\n", t_exec_estimate, TLEG);

    printf("Res: p %f\n",out[0][REAL]);

    t_start = mysecond();
    if(p_measure) fftw_execute(p_measure); /* repeat as needed */
    t_end = mysecond();
    double t_exec_measure = (t_end-t_start)*TSCALE;
    printf("FFTW_MEASURE exec %7.5lf (%s)\n", t_exec_measure, TLEG);

    t_start = mysecond();
    if(p_patience) fftw_execute(p_patience); /* repeat as needed */
    t_end = mysecond();
    double t_exec_patient = (t_end-t_start)*TSCALE;
    printf("FFTW_PATIENT exec %7.5lf (%s)\n", t_exec_patient, TLEG);

    t_start = mysecond();
    if(p_exhaust) fftw_execute(p_exhaust); /* repeat as needed */
    t_end = mysecond();
    double t_exec_exhaust = (t_end-t_start)*TSCALE;
    printf("FFTW_EXHAUSTIVE exec %7.5lf (%s)\n", t_exec_exhaust, TLEG);

    printf("FFTW aggregated n: %d plan: FFTW_ESTIMATE %7.5lf "
                                 "FFTW_MEASURE %7.5lf "
                                 "FFTW_PATIENT %7.5lf "
                                 "FFTW_EXHAUSTIVE %7.5lf | "
                           "exec: FFTW_ESTIMATE %7.5lf "
                                 "FFTW_MEASURE %7.5lf "
                                 "FFTW_PATIENT %7.5lf "
                                 "FFTW_EXHAUSTIVE %7.5lf | (%s)\n",
      n, t_plan_estimate, t_plan_measure, t_plan_patient, t_plan_exhaust,
         t_exec_estimate, t_exec_measure, t_exec_patient, t_exec_exhaust, TLEG);

    printf("Res: p %f\n",out[0][REAL]);

    fftw_destroy_plan(p_generic);
    //fftw_destroy_plan(p_estimate);
    //ftw_destroy_plan(p_patience);
    //fftw_destroy_plan(p_measure);
    //fftw_destroy_plan(p_exhaust);
    fftw_free(in); fftw_free(out);
}
