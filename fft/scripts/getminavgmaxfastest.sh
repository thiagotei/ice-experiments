#!/usr/bin/env bash

#grep "Matrixsize"  $1| sort -k2,2n  -k8,8n | uniq -w 20

# loop over input files

func() {
    echo leg,comp,searchtool,run,matshape,min,max,avg
    for fileinp in "$@"; do
        if [ ! -f $fileinp ]; then
            echo "$fileinp does not exisit!" >&2
            continue
        fi
        echo "Processing $fileinp ..." >&2
        comp=`awk -F_ '{print $2}' <<< $fileinp`
        label=`awk -F_ '{print $4}' <<< $fileinp`

        # split the file by searching
        csplit $fileinp /Searching/ {*} -f $fileinp -b "_%02d.log" -z -s

        echo "#"$fileinp
        for inp  in ${fileinp}_*.log; do
            #grep "Searching " $inp | awk -v leg=$label '{print leg}' | tr '\n' ',' #| awk -v leg=$label '{print leg","$4","$6}' | tr '\n' ','
            echo $inp >&2
            grep "Searching " $inp | awk -v leg=$label -v cc=$comp '{printf("%s,%s,",leg,cc)} NF==20{print $10","$12}' | tr '\n' ','
            grep "FFTW_LOCUS" $inp | sort -k2,2n -k7,7n | uniq -w 20 | awk '{print $2","$7","$9","$11}'
            rm -v $inp >&2
        done
    done
}

echo "N    N  len plstrg       N     pltime  N     N    etmin   N    etmax   N    etavg   N    N N         niter N stopaf #stopaf in milisec"
for fileinp in "$@"; do
    grep "FFTW n:\|Namespace" $@ | \
        awk  'BEGIN{stopafVal="NA";}
              /Namespace/ {for(i=1; i<= NF; ++i) if($i~/stop_after/) {split($i,lst,"="); split(lst[2], res,","); stopafVal=res[1]; break;}}
              /FFTW n:/ {print $0,"stopaf=",stopafVal*1000}
              #END{print "Eita",stopafVal}
             '
done
